@extends('layouts.app-main')

@section('content')

    <div class="cover-page cover-page-in @auth cover-page-1 @endauth s-project-page">
        <div class="cover-page-content">
            <!--<div class="cover-accordion-container">-->
            <div class="">
                <!-- slide left start -->
            @auth
                @include('profile.partials.slideleft')
            @endauth
            <!-- slide left end -->

                <div class="page-content-wrapper">

                    <section class="project-uno">
                        <section class="had-container row project-idpages" id="{{$project->id}}" style="margin-top: 4rem;">
                            <img src="{{asset('images/bg.png')}}" data-src="{{$project->poster}}" alt="banner"
                                 class="j-image j-filter lazy-image"/>
                            <div class="shadow-hero"></div>
                            <article class="container hero-0">
                                <div class="col s12 m12 l12 xl12 left-align mar-project4">
                                    <span class="font-banner">{{$project->project_name}}</span>
                                </div>
                                <div class="col s12 m12 l8 xl8 mar-project3">
                                    <div class="info info-project">
                                        <div class="center-align">
                                            <div class="info-poster call_modal2 u-cursor">
                                                <img src="{{asset('images/bg.png')}}" data-src="{{$project->poster}}"
                                                     alt="{{$project->project_name}}"
                                                     class="responsive-img u-cursor lazy-image">
                                            </div>

                                        </div>
                                        <div class="content-info padd-pjec">
                                            <p class="font-pre1">{{$project->tagline}}</p>
                                            <div class="padd-pjec2">
                                                @foreach($project->genres as $genre)
                                                    <button class="boton-gener txt-uppers"
                                                            onclick="location.href='https://www.storyrocket.com/searchs/genre?q=search&amp;faith&amp;espv=1';">
                                                        {{$genre->genre}}
                                                    </button>
                                                @endforeach
                                            </div>
                                            <div class="mar-heart2 cnt-icon list-favorite">

                                                @if($project->pdf_view)
                                                    <a href="{{$project->pdf_view}}"
                                                       class="icon-g  bottom-primary bottom-reque btn-view-pdf-sample not-top"
                                                       target="_blank">
                                                        <i class="material-icons dp48">insert_drive_file</i>
                                                        <span class="font-pdf">VIEW PDF SAMPLE</span>
                                                    </a>
                                                @else
                                                    <span class="p_pdf-none-ex">
                                                        <span class="icon3-pdf move-i"></span>
                                                        <span class="font-pdf-ex">NO PDF AVAILABLE</span>
                                                    </span>
                                                @endif

                                                <span class="tooltip mar-heart like-projct">
										            <i class="icon3-heart2 font-18"></i>
                                                    <span class="tooltiptext">Like Project</span>
                                                </span>
                                                <span class="add-collections tooltip cll-top project-readlist">
	            								    <i class="icon-icono-project4 font-18 fonts-readlist"></i><span
                                                            class="tooltiptext">Add to List</span>
                                                </span>
                                            </div>
                                            <div class="view-rating">
                                                RATING <span class="font-banner2">{{$project->rating_term}}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col s12 m12 l4 xl4 mar-projec5">
									<div class="card-video-p vidio-projects  ">
                                        <a href="!#135538096" data-type="vimeo" class="btn-v-p link-vidio-drop ">
                                            <div class="video-img-p">
                                                <img src="http://i.vimeocdn.com/video/529521489_640.jpg" alt="" class="responsive-img">
                                                <div class="overlay-video-p ic-play-p">
                                                    <i class="material-icons">play_arrow</i>
                                                    <div class="cont-ic-new">
                                                        <p class="font-new-v truncate">Quoting Matilda by Susan Savion Author Interview</p>

                                                    </div>
                                                </div>
                                            </div>
			                            </a>
                                    </div>
		                        </div>
                                <button id="show-enterprise"
                                        class="col s12 m12 l12 xl12  center-align border-espan SeeMore8">
                                    <i class="icon4-chevron-up"></i>
                                    <span>CLOSE</span>
                                </button>
                                <button id="hide-enterprise"
                                        class="col s12 m12 l12 xl12  center-align border-espan SeeMore8"
                                        style="display: none">
                                    <i class="icon2-chevron-down"></i>
                                    <span>EXPAND TO SEE MORE</span>
                                </button>
                            </article>
                            <article class="container hero-1 enterprise" style="display: block;" id="enterprise-info">
                                <div class="col s12 m12 l4 xl4 mar-ocu1">
                                    <i class="icon3-huella"></i>
                                    <div class="bloque-project">
                                        <span class="font-ocu1">Storyrocket Number</span>
                                        <p class="font-ocu2">{{$project->log_line_number}}</p>
                                    </div>
                                </div>
                                <div class="col s12 m12 l4 xl4 padd-ocu1 mar-ocu1">
			  <span>
				 <svg width="32px" height="32px" viewBox="0 0 32 32" version="1.1" xmlns="http://www.w3.org/2000/svg"
                      xmlns:xlink="http://www.w3.org/1999/xlink">
					<g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
						<g id="reell" fill="#F76859">
							<path d="M16,31.3 C7.6,31.3 0.8,24.4 0.8,16 C0.8,7.6 7.6,0.8 16,0.8 C24.4,0.8 31.3,7.6 31.3,16.1 C31.3,24.4 24.4,31.3 16,31.3 Z M16,2.3 C8.4,2.3 2.3,8.4 2.3,16 C2.3,23.6 8.5,29.8 16.1,29.8 C23.7,29.8 29.9,23.6 29.9,16 C29.8,8.4 23.6,2.3 16,2.3 Z"
                                  id="Shape"></path>
							<path d="M16,12.6 C13.7,12.6 11.9,10.8 11.9,8.5 C11.9,6.2 13.7,4.4 16,4.4 C18.3,4.4 20.1,6.2 20.1,8.5 C20.1,10.7 18.3,12.6 16,12.6 Z M16,5.8 C14.6,5.8 13.4,7 13.4,8.4 C13.4,9.8 14.6,11 16,11 C17.4,11 18.6,9.8 18.6,8.4 C18.6,7 17.4,5.8 16,5.8 Z"
                                  id="Shape"></path>
							<path d="M16,27.7 C13.7,27.7 11.9,25.9 11.9,23.6 C11.9,21.3 13.7,19.5 16,19.5 C18.3,19.5 20.1,21.3 20.1,23.6 C20.1,25.8 18.3,27.7 16,27.7 Z M16,20.9 C14.6,20.9 13.4,22.1 13.4,23.5 C13.4,24.9 14.6,26.1 16,26.1 C17.4,26.1 18.6,24.9 18.6,23.5 C18.6,22.1 17.4,20.9 16,20.9 Z"
                                  id="Shape"></path>
							<path d="M8.5,20.1 C6.2,20.1 4.4,18.3 4.4,16 C4.4,13.7 6.2,11.9 8.5,11.9 C10.8,11.9 12.6,13.7 12.6,16 C12.6,18.3 10.7,20.1 8.5,20.1 Z M8.5,13.4 C7.1,13.4 5.9,14.6 5.9,16 C5.9,17.4 7.1,18.6 8.5,18.6 C9.9,18.6 11.1,17.4 11.1,16 C11.1,14.6 9.9,13.4 8.5,13.4 Z"
                                  id="Shape"></path>
							<path d="M23.5,20.1 C21.2,20.1 19.4,18.3 19.4,16 C19.4,13.7 21.2,11.9 23.5,11.9 C25.8,11.9 27.6,13.7 27.6,16 C27.7,18.3 25.8,20.1 23.5,20.1 Z M23.5,13.4 C22.1,13.4 20.9,14.6 20.9,16 C20.9,17.4 22.1,18.6 23.5,18.6 C24.9,18.6 26.1,17.4 26.1,16 C26.2,14.6 25,13.4 23.5,13.4 Z"
                                  id="Shape"></path>
							<path d="M16,18.6 C14.6,18.6 13.4,17.5 13.4,16 C13.4,14.6 14.5,13.4 16,13.4 C17.4,13.4 18.6,14.5 18.6,16 C18.6,17.4 17.4,18.6 16,18.6 Z M16,14.9 C15.4,14.9 14.9,15.4 14.9,16 C14.9,16.6 15.4,17.1 16,17.1 C16.6,17.1 17.1,16.6 17.1,16 C17.1,15.4 16.6,14.9 16,14.9 Z"
                                  id="Shape"></path>
						</g>
					</g>
				</svg>
			  </span>
                                    <div class="bloque-project">
                                        <span class="font-ocu1">Intended Medium</span>
                                        <p class="font-ocu2">
                                            @foreach($project->mediums as $key => $medium)
                                                {{$medium->medium}}
                                                @if(($key + 1) != count($project->mediums))
                                                    ,
                                                @endif
                                            @endforeach
                                        </p>
                                    </div>
                                </div>
                                <div class="col s12 m12 l4 xl4 padd-ocu2 mar-ocu1">
		  <span>
			<svg width="31px" height="31px" viewBox="0 0 92 92" version="1.1" xmlns="http://www.w3.org/2000/svg"
                 xmlns:xlink="http://www.w3.org/1999/xlink">
				<g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
					<g id="wolrd" fill="#F76859">
						<path d="M88.56006,28.56946 C81.4887504,11.2892906 64.6710384,-2.57921509e-06 46,-2.57921509e-06 C27.3289616,-2.57921509e-06 10.5112496,11.2892906 3.43994,28.56946 C3.36915675,28.7020462 3.31376375,28.8422901 3.27484,28.98746 C-1.0916149,39.9089156 -1.0916149,52.0911544 3.27484,63.01261 C3.31376375,63.1577799 3.36915675,63.2980238 3.43994,63.43061 C10.5112496,80.7107794 27.3289616,92.0000726 46,92.0000726 C64.6710384,92.0000726 81.4887504,80.7107794 88.56006,63.43061 C88.6308432,63.2980238 88.6862363,63.1577799 88.72516,63.01261 C93.0916149,52.0911544 93.0916149,39.9089156 88.72516,28.98746 C88.6862363,28.8422901 88.6308432,28.7020462 88.56006,28.56946 L88.56006,28.56946 Z M46,88 C38.29749,88 31.44824,78.28668 28.12561,64.5 L63.87439,64.5 C60.55176,78.28668 53.70251,88 46,88 Z M27.28082,60.5 C25.5730563,50.9087589 25.5730563,41.0912411 27.28082,31.5 L64.71918,31.5 C65.577641,36.2854708 66.0062893,41.1381437 66,46 C66.0062893,50.8618563 65.577641,55.7145292 64.71918,60.5 L27.28082,60.5 Z M4,46 C3.99562646,41.0519513 4.8698097,36.1424264 6.58185,31.5 L23.17871,31.5 C21.6070955,41.1027881 21.6070955,50.8972119 23.17871,60.5 L6.58185,60.5 C4.8698097,55.8575736 3.99562646,50.9480487 4,46 L4,46 Z M46,4 C53.70251,4 60.55176,13.71332 63.87439,27.5 L28.12561,27.5 C31.44824,13.71332 38.29749,4 46,4 Z M68.82129,31.5 L85.41815,31.5 C88.8606143,40.8601867 88.8606143,51.1398133 85.41815,60.5 L68.82129,60.5 C69.6146007,55.7075917 70.0088568,50.8576168 70,46 C70.0088568,41.1423832 69.6146007,36.2924083 68.82129,31.5 L68.82129,31.5 Z M83.6944,27.5 L68.04248,27.5 C65.95575,18.16144 62.319,10.51331 57.71442,5.67474 C69.047311,8.99316669 78.4706272,16.9095056 83.6944,27.5 Z M34.28558,5.67474 C29.681,10.51331 26.04425,18.16144 23.95752,27.5 L8.3056,27.5 C13.5293728,16.9095056 22.952689,8.99316669 34.28558,5.67474 L34.28558,5.67474 Z M8.3056,64.5 L23.95752,64.5 C26.04425,73.83856 29.68103,81.48669 34.28558,86.32526 C22.952689,83.0068333 13.5293728,75.0904944 8.3056,64.5 L8.3056,64.5 Z M57.71442,86.32526 C62.319,81.48669 65.95575,73.83856 68.04248,64.5 L83.6944,64.5 C78.4706272,75.0904944 69.047311,83.0068333 57.71442,86.32526 L57.71442,86.32526 Z"
                              id="Shape"></path>
					</g>
				</g>
			</svg>
		  </span>
                                    <div class="bloque-project">
                                        <span class="font-ocu1">Language</span>
                                        <p class="font-ocu2">{{$project->language->language}}</p>
                                    </div>
                                </div>
                                <div class="col s12 m12 l4 xl4 mar-ocu2">
                                    <i class="icon3-id"></i>
                                    <div class="bloque-project">
                                        <span class="font-ocu1">	Registering Entity			  	</span>
                                        <p class="font-ocu2"> None </p>
                                    </div>
                                </div>
                                <div class="col s12 m12 l4 xl4 padd-ocu1 mar-ocu2">
		  <span>
			<svg width="32px" height="32px" viewBox="0 0 83 72" version="1.1" xmlns="http://www.w3.org/2000/svg"
                 xmlns:xlink="http://www.w3.org/1999/xlink">
				<g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
					<g id="noun_1051475_cc" fill="#F76859">
						<path d="M82.2,65.8 L82.2,6.2 C82.2,2.9 79.5,0.2 76.2,0.2 L6.8,0.2 C3.5,0.2 0.8,2.9 0.8,6.2 L0.8,65.9 C0.8,69.2 3.5,71.9 6.8,71.9 L76.2,71.9 C79.5,71.8 82.2,69.1 82.2,65.8 Z M76.2,67.8 L62.1,67.8 L62.1,55.4 L78.2,55.4 L78.2,65.8 C78.2,66.9 77.3,67.8 76.2,67.8 Z M4.8,38 L20.9,38 L20.9,51.4 L4.8,51.4 L4.8,38 Z M62.1,38 L78.2,38 L78.2,51.4 L62.1,51.4 L62.1,38 Z M62.1,34 L62.1,20.6 L78.2,20.6 L78.2,34 L62.1,34 Z M58.1,34 L24.8,34 L24.8,4.2 L58.1,4.2 L58.1,34 Z M20.8,34 L4.8,34 L4.8,20.6 L20.9,20.6 L20.9,34 L20.8,34 Z M24.8,38 L58.1,38 L58.1,67.8 L24.8,67.8 L24.8,38 Z M78.2,6.2 L78.2,16.6 L62.1,16.6 L62.1,4.2 L76.2,4.2 C77.3,4.2 78.2,5.1 78.2,6.2 Z M6.8,4.2 L20.9,4.2 L20.9,16.6 L4.8,16.6 L4.8,6.2 C4.8,5.1 5.7,4.2 6.8,4.2 Z M4.8,65.8 L4.8,55.4 L20.9,55.4 L20.9,67.8 L6.8,67.8 C5.7,67.8 4.8,66.9 4.8,65.8 Z"
                              id="Shape"></path>
					</g>
				</g>
			</svg>
		  </span>
                                    <div class="bloque-project">
                                        <span class="font-ocu1">Material Type</span>
                                        <p class="font-ocu2">{{$project->material}}</p>
                                    </div>
                                </div>
                                <div class="col s12 m12 l4 xl4 padd-ocu2 mar-ocu2">
		  <span>
			 <svg width="32px" height="32px" viewBox="0 0 54 54" version="1.1" xmlns="http://www.w3.org/2000/svg"
                  xmlns:xlink="http://www.w3.org/1999/xlink">
					<g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
						<g id="dollar" fill="#F76859">
							<path d="M27,-1.99999999e-05 C12.1121,-1.99999999e-05 0,12.11202 0,27.00002 C0,41.88792 12.1121,54.00002 27,54.00002 C41.8879,54.00002 54,41.88792 54,27.00002 C54,12.11202 41.8879,-1.99999999e-05 27,-1.99999999e-05 Z M27,3.99998 C39.7261,3.99998 50,14.27382 50,27.00002 C50,39.72612 39.7261,50.00002 27,50.00002 C14.2739,50.00002 4,39.72612 4,27.00002 C4,14.27382 14.2739,3.99998 27,3.99998 Z M27,12.00002 C25.8955,12.00002 25,12.89542 25,14.00002 L25,15.18752 C23.6815,15.44412 22.4745,15.96792 21.4688,16.78122 C19.9723,17.99162 19,19.90912 19,22.00002 C19,24.27682 20.3484,26.09032 21.8438,27.00002 C22.9074,27.64712 23.97,28.04352 25,28.40622 L25,34.56252 C23.9359,34.09192 23.1357,33.31982 22.7188,32.62502 C22.1491982,31.6758849 20.9180553,31.368152 19.9688597,31.9376528 C19.019664,32.5071536 18.7118001,33.7382638 19.2812,34.68752 C20.368,36.49872 22.3793,38.15872 25,38.75002 L25,40.00002 C25,41.10462 25.8955,42.00002 27,42.00002 C28.1046,42.00002 29,41.10462 29,40.00002 L29,38.81252 C30.3191,38.54672 31.5252,38.00122 32.5312,37.18752 C34.0277,35.97712 35,34.09092 35,32.00002 C35,29.72322 33.6517,27.87832 32.1562,26.96872 C31.0926,26.32172 30.0299,25.92522 29,25.56252 L29,19.43752 C30.064,19.89892 30.8645,20.64892 31.2812,21.34372 C31.649591,21.9578708 32.3176625,22.3288147 33.0337273,22.3168035 C33.7497922,22.3047922 34.4050469,21.9116508 34.7526329,21.2854901 C35.1002188,20.6593295 35.0873216,19.8952924 34.7188,19.28122 C33.632,17.47022 31.6206,15.83342 29,15.25002 L29,14.00002 C29,12.89542 28.1046,12.00002 27,12.00002 Z M25,19.34372 L25,24.09372 C24.582,23.92682 24.2096,23.75922 23.9375,23.59372 C23.1679,23.12552 23,23.05062 23,22.00002 C23,21.06172 23.3417,20.43862 24,19.90622 C24.27,19.68792 24.6098,19.49392 25,19.34372 Z M29,29.87502 C29.4179,30.04192 29.7904,30.20952 30.0625,30.37502 C30.8321,30.84322 31,30.94942 31,32.00002 C31,32.93832 30.6582,33.53012 30,34.06252 C29.73,34.28092 29.3901,34.46862 29,34.62502 L29,29.87502 Z"
                                  id="Shape"></path>
						</g>
					</g>
				</svg>
		  </span>
                                    <div class="bloque-project">
                                        <span class="font-ocu1">Funding Platform</span>
                                        <p class="font-ocu2">None</p>
                                    </div>
                                </div>
                                <div class="col s12 m12 l12 xl12 padd-last">
                                    <div class="col s12 m12 l4 xl4  mar-ocu2">
		  <span>
			 <svg width="32px" height="32px" viewBox="0 0 48 72" version="1.1" xmlns="http://www.w3.org/2000/svg"
                  xmlns:xlink="http://www.w3.org/1999/xlink">
				<g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
					<g id="noun_939254_cc" fill="#F76859">
						<path d="M24,0 C10.768311,0 0,10.7776 0,24.03125 C0,31.6501 4.077182,38.2511 8.78125,44.5 C11.842536,48.566557 15.195068,52.523541 18.09375,56.5 C15.601108,56.89757 13.383669,57.527279 11.5625,58.40625 C10.3326,58.99995 9.26664,59.69035 8.4375,60.59375 C7.60836,61.49715 7,62.6842 7,64 C7,65.504 7.789179,66.81355 8.8125,67.78125 C9.835821,68.74905 11.136166,69.48575 12.65625,70.09375 C15.696418,71.30985 19.641689,72 24,72 C28.358313,72 32.303581,71.30985 35.34375,70.09375 C36.863835,69.48575 38.164179,68.74905 39.1875,67.78125 C40.210821,66.81355 41,65.504 41,64 C41,62.6842 40.391642,61.49725 39.5625,60.59375 C38.733358,59.69035 37.667402,58.99995 36.4375,58.40625 C34.616327,57.527279 32.398891,56.89757 29.90625,56.5 C32.804932,52.523541 36.157464,48.566557 39.21875,44.5 C43.922818,38.2511 48,31.6501 48,24.03125 C48,10.7776 37.231689,0 24,0 Z M24,4 C35.068451,4 44,12.93502 44,24.03125 C44,30.19947 40.577185,36.05505 36.03125,42.09375 C32.132521,47.27285 27.560781,52.53965 24,58.21875 C20.439219,52.53965 15.867479,47.27285 11.96875,42.09375 C7.422815,36.05505 4,30.19947 4,24.03125 C4,12.93502 12.931549,4 24,4 Z M24,11 C16.843988,11 11,16.84399 11,24 C11,31.15602 16.843988,37 24,37 C31.156012,37 37,31.15602 37,24 C37,16.84399 31.156012,11 24,11 Z M24,15 C28.994253,15 33,19.00575 33,24 C33,28.99426 28.994253,33 24,33 C19.005748,33 15,28.99426 15,24 C15,19.00575 19.005748,15 24,15 Z M20.625,60.1875 C21.203477,61.099948 21.755096,62.018391 22.25,62.9375 C22.6023251,63.5740186 23.2724775,63.9690415 24,63.9690415 C24.7275225,63.9690415 25.3976749,63.5740186 25.75,62.9375 C26.244904,62.018391 26.796523,61.099948 27.375,60.1875 C30.383083,60.484036 32.997497,61.169233 34.71875,62 C35.627893,62.4388 36.276821,62.90195 36.625,63.28125 C36.973179,63.66065 37,63.8666 37,64 C37,64.1529 36.93226,64.4071 36.4375,64.875 C35.942738,65.3429 35.069382,65.8972 33.875,66.375 C31.486237,67.3305 27.925958,68 24,68 C20.07404,68 16.513764,67.3305 14.125,66.375 C12.930618,65.8972 12.057262,65.3429 11.5625,64.875 C11.067738,64.4071 11,64.1529 11,64 C11,63.8666 11.02682,63.66065 11.375,63.28125 C11.723181,62.90195 12.372105,62.4388 13.28125,62 C15.002508,61.169233 17.616917,60.484036 20.625,60.1875 Z"
                              id="Shape"></path>
					</g>
				</g>
			</svg>
		  </span>
                                        <div class="bloque-project4 tooltip5 ">
                                            <span class="font-ocu1">Location</span>
                                            <p class="font-ocu2-1 ">{{$project->local_name_line}}</p>
                                            <span class="tooltiptext5">The fictional small town of West Springs, Oklahoma is like any other small town. It has the small town pizzeria, blue collar workers, and a high school with a cast of characters that will make you laugh and cry as you grow with them.</span>
                                        </div>
                                    </div>
                                    <div class="col s12 m12 l4 xl4 padd-ocu1 mar-ocu2">
		  <span>
			  <svg width="30px" height="30px" viewBox="0 0 37 46" version="1.1" xmlns="http://www.w3.org/2000/svg"
                   xmlns:xlink="http://www.w3.org/1999/xlink">
					<g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
						<g id="clock" transform="translate(2.000000, 2.000000)" stroke="#F76859">
							<g id="Group">
								<g transform="translate(6.000000, 1.000000)" id="Shape" stroke-width="3">
									<polyline points="0.4 0.1 0.4 11.8 20.4 27.3 20.4 40.4"></polyline>
									<polyline points="20.4 0.1 20.4 11.8 0.4 27.3 0.4 40.4"></polyline>
								</g>
								<rect id="Rectangle-path" stroke-width="2.4648" x="0.1" y="40.3" width="33.2"
                                      height="2.2"></rect>
								<rect id="Rectangle-path" stroke-width="2.4648" x="0.1" y="0" width="33.2"
                                      height="2.2"></rect>
							</g>
						</g>
					</g>
				</svg>
		  </span>
                                        <div class="bloque-project4 tooltip5">
                                            <span class="font-ocu1">Era</span>
                                            <p class="font-ocu2-1 ">{{$project->era}}</p>
                                            <span class="tooltiptext5">Present day in the small fictional town of West Springs, Oklahoma.</span>
                                        </div>
                                    </div>
                                </div>
                            </article>
                        </section>
                    </section>
                    
                    <section class="j-section" id="project-dos">
                        <!--  barra gris-->
                        <section class="row hero-2 proj-1">
                            <article class="container">
                                <div class="col s12 m12 l12 xl12">
                                    <div class="col s12 m12 l12 xl12">
                                        <a href="#open-request"  class="icon-g  new-btn-request-project bottom-primary bottom-reque call_modal3 btn-rqust modal-trigger">
                                            <span class="icon3-rocket"></span>
                                            <span class="font-req">REQUEST FULL PROJECT</span>
                                        </a>
                                        <button id="login" data-friends="{{$project->user_id}}"
                                                class=" btn-rqust icon-g  btn-send-message-project bottom-primary bottom-reque vert-botn msj-users ">
                                            <span class="icon4-mail send-icoo"></span>
                                            <span class="font-req">SEND MESSAGE</span>
                                        </button>
                                        <div class="bloque-pro p-share">
                                            <div class="fb-share-button wb_facebook-1 tooltip4 fb_iframe_widget"
                                                 data-href="https://www.storyrocket.com/seasons-of-change-book-1-grace-restored-series"
                                                 style="float:left;margin-right: .3rem;" data-layout="button"
                                                 data-size="small" data-mobile-iframe="true" fb-xfbml-state="rendered"
                                                 fb-iframe-plugin-query="app_id=903523583103228&amp;container_width=57&amp;href=https%3A%2F%2Fwww.storyrocket.com%2Fseasons-of-change-book-1-grace-restored-series&amp;layout=button&amp;locale=en_US&amp;mobile_iframe=true&amp;sdk=joey&amp;size=small">
                                                <span style="vertical-align: bottom; width: 59px; height: 20px;"><iframe
                                                            name="f15cacb34b4f904" width="1000px" height="1000px"
                                                            frameborder="0" allowtransparency="true"
                                                            allowfullscreen="true" scrolling="no"
                                                            allow="encrypted-media"
                                                            title="fb:share_button Facebook Social Plugin"
                                                            src="https://www.facebook.com/v2.12/plugins/share_button.php?app_id=903523583103228&amp;channel=https%3A%2F%2Fstaticxx.facebook.com%2Fconnect%2Fxd_arbiter%2Fr%2FafATJJjxKE6.js%3Fversion%3D43%23cb%3Df179d02fba523e4%26domain%3Dwww.storyrocket.com%26origin%3Dhttps%253A%252F%252Fwww.storyrocket.com%252Ff2ab346aebb81%26relation%3Dparent.parent&amp;container_width=57&amp;href=https%3A%2F%2Fwww.storyrocket.com%2Fseasons-of-change-book-1-grace-restored-series&amp;layout=button&amp;locale=en_US&amp;mobile_iframe=true&amp;sdk=joey&amp;size=small"
                                                            style="border: none; visibility: visible; width: 59px; height: 20px;"
                                                            class=""></iframe></span></div>
                                            <div class="w-widget w-widget-twitter wb_twitter tooltip4 top-t"
                                                 style="float:left;margin-right: .3rem;">
                                                <iframe src="//platform.twitter.com/widgets/tweet_button.html#url=https://www.storyrocket.com/seasons-of-change-book-1-grace-restored-series&amp;counturl=https://www.storyrocket.com/seasons-of-change-book-1-grace-restored-series&amp;text=Check out this Storyrocket Project: “SEASONS OF CHANGE (Book 1, Grace Restored Series)” by C.J. Peterson&amp;count=horizontal&amp;size=m&amp;dnt=true"
                                                        scrolling="no" frameborder="0" allowtransparency="true"
                                                        style="border: none; overflow: hidden; width: 62px; height: 21px;"></iframe>

                                            </div>
                                            <div class="w-widget w-widget-gplus wb_google tooltip4 top-g"
                                                 style="float:left;margin-right: .3rem;">
                                                <div id="___plusone_0"
                                                     style="text-indent: 0px; margin: 0px; padding: 0px; background: transparent; border-style: none; float: none; line-height: normal; font-size: 1px; vertical-align: baseline; display: inline-block; width: 18px; height: 20px;">
                                                    <!-- Inserta esta etiqueta donde quieras que aparezca Botón Compartir. -->
                                                    <a href="https://plus.google.com/share?url=https://www.storyrocket.com/seasons-of-change-book-1-grace-restored-series"
                                                       onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;">
                                                        <img src="https://www.gstatic.com/images/icons/gplus-32.png"
                                                             style="width: 100%" alt="Share on Google+">
                                                    </a>
                                                    <!-- Inserta esta etiqueta después de la última etiqueta de compartir. -->
                                                    <script type="text/javascript">
                                                        window.___gcfg = {lang: 'es'};

                                                        (function () {
                                                            var po = document.createElement('script');
                                                            po.type = 'text/javascript';
                                                            po.async = true;
                                                            po.src = 'https://apis.google.com/js/platform.js';
                                                            var s = document.getElementsByTagName('script')[0];
                                                            s.parentNode.insertBefore(po, s);
                                                        })();
                                                    </script>
                                                </div>
                                            </div>
                                            <div class="w-widget w-widget-tmblr wb_tumblr tooltip4 top-t"
                                                 style="float:left;margin-right: .3rem;">
                                                <iframe frameborder="0" height="20px" scrolling="no" seamless="seamless"
                                                        src="https://embed.tumblr.com/widgets/share/button?color=blue&amp;notes=none&amp;canonicalUrl=https%3A%2F%2Fwww.storyrocket.com%2Fseasons-of-change-book-1-grace-restored-series&amp;"
                                                        style="visibility: visible;" width="55px"></iframe>
                                                <script>!function (d, s, id) {
                                                        var js, ajs = d.getElementsByTagName(s)[0];
                                                        if (!d.getElementById(id)) {
                                                            js = d.createElement(s);
                                                            js.id = id;
                                                            js.src = "https://assets.tumblr.com/share-button.js";
                                                            ajs.parentNode.insertBefore(js, ajs);
                                                        }
                                                    }(document, "script", "tumblr-js");</script>
                                            </div>
                                            <div class="w-widget w-widget-reddit wb_reddit tooltip4 top-r"
                                                 style="float:left;margin-right: .3rem;">
                                                <a href="//www.reddit.com/submit?url=https://www.storyrocket.com/seasons-of-change-book-1-grace-restored-series&amp;title=SEASONS OF CHANGE (Book 1, Grace Restored Series)&amp;text=Check out this Storyrocket Project: “SEASONS OF CHANGE (Book 1, Grace Restored Series)” by C.J. Peterson "
                                                   onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;">
                                                    <img src="//www.redditstatic.com/spreddit6.gif"
                                                         alt="submit to reddit" border="0">
                                                </a>
                                            </div>
                                            <a class="tooltip4 emai-t"
                                               href="mailto:?subject=Check out this Storyrocket Project: “SEASONS OF CHANGE (Book 1, Grace Restored Series)” by C.J. Peterson &amp;body=“SEASONS OF CHANGE (Book 1, Grace Restored Series)” by C.J. Peterson https://www.storyrocket.com/seasons-of-change-book-1-grace-restored-series"
                                               style="float:left;">
                                                <span class="icon4-mail"></span>
                                            </a>
                                        </div>

                                    </div>
                                    <!--<div class="col s12 m4 l4 xl6 right-align">
                                    <div class="star-pos">
                                        <span class="icon3-star"></span>
                                        <span class="font_star"><strong>856</strong> People have viewed this project</span>
                                    </div>
                                    </div>-->
                                </div>
                            </article>
                        </section>
                        <!--finn barra gris-->
                    </section>
                    <section id="project-tres">
                        <section class="row hero-3">
                            <section class="container">
                                <article class="col s12 m12 l8 xl8 colun-1">
                                    <div class="col s12 m7 l7 xl7 marg-autor">
                                        <span class="font-descrip1">Submited by:</span>
                                        <span class="font-descrip2  couponcode tooltip-load " data-type="users" data-key="1210">
                                            <a href="{{$project->url_profile}}" class="color-a">
                                                {{$project->full_name}} {{$project->last_name}} ({{$project->occupation}})
                                            </a>
				                            <div class="tol-dinac">
    		                                    <div class="coupontooltip">
			<div class="card-tool">
			<div class="arrow-tol"></div>
				<div class="tol-horizontal">
					<div class="tol-images">
						<figure class="m-none">
							<a href="https://www.storyrocket.com/C.J..1aaa86s6"><img
                                        src="https://storyrocket-aws3.s3.amazonaws.com/3df100d4.jpg" alt="tool"></a>
						</figure>
					</div>
					<div class="tol-content">
						<a href="https://www.storyrocket.com/C.J..1aaa86s6" class="font-tol">C.J. Peterson</a>
						<p class="font-tol2 truncate">
							<span class="font-tol2">Writer</span>
						</p><p>
						</p><p class="font-tol3 truncate">United States</p>
					</div>
				</div>
				<div class="tol-content2">
					<div class="truncate">
						<span class="font-tol4">
							I was raised in a Christian military home, but it was far from tranquil. Like many young people, I left home as soon as possible,                                                                                                                                                                                                                                                                                                                                                                                                                  ...
							<a href="https://www.storyrocket.com/C.J..1aaa86s6" class="font-tol7">Read More</a>
						</span>
					</div>
				</div>
				<div class="col s4 tol-margin">
					<span class="tol-float">
						<i class="icon-icono-profile37"></i>
					 </span>
					 <span class="font-tol3"><strong class="color-tip">8</strong> Projects</span>
					</div>
					 <div class="col s4 tol-margin">
					 <span class="tol-float">
						<i class="icon-icono-profile35"></i>
					 </span>
					 <span class="font-tol3"><strong class="color-tip">3854</strong> View</span>
					</div>
					 <div class="col s4 tol-margin">
					 <span class="tol-float">
						<i class="icon-icono-profile36"></i>
					 </span>
					 <span class="font-tol3"><strong class="color-tip">8</strong> Followers</span>
					</div>

			</div>
		</div>
	                                        </div>
			                            </span>
                                        @if(0)
                                            <div class="mar-sepa">
                                                <span class="font-descrip1">Writer(s):</span>
                                                <span class="font-descrip2 mar-descrip2 ">
                                                    <div class="couponcode tooltip-load pro_bloque elemt-write" data-type="users" data-key="1210">
                                                        <a href="{{$project->profile_writer_url}}" class="color-a space-cm">
                                                            {{$project->writer_name}}
                                                        </a>
                                                        <div class="tol-dinac"></div>
                                                    </div>
                                                                                                                                                                                                                                                    </span>
                                            </div>
                                        @endif
                                        @if($project->writer_name)
                                            <div class="mar-sepa">
                                                <span class="font-descrip1">Associated to:  </span>
                                                <span class="font-descrip2 mar-descrip2 ">
                                                    <div class="couponcode tooltip-load pro_bloque elemt-write" data-type="users" data-key="1210">
                                                        <a href="{{$project->profile_writer_url}}" class="color-a " target="_blank">
                                                            {{$project->writer_name}}
                                                        </a>
                                                        <div class="tol-dinac"></div>
                                                    </div>
                                                </span>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="col s12 m5 l5 xl5 mard-view border-descrip">

                                        <div class="mar-view2">
                                            <span class="icon3-eyes2"></span>
                                            <span class="font-view">{{$project->views}} views</span>
                                        </div>


                                        <div class="heart-1 mar-view2">
                                            <span class="icon3-heartp"></span>
                                            <span class="font-view">{{$project->votes}} Appreciations</span>
                                        </div>


                                        <div class="mar-view2">
                                            <span class="icon3-coments"></span>
                                            <span class="font-view">{{$project->comments}}  Comments</span>
                                        </div>

                                    </div>
                                    <article class="col s12 m12 l12 xl12 mar-descrip5 padd-article">
                                        <div class="border-pro3 pro_marg1 u-flex1 u-flexTop u-justifyContentSpaceBetween">
                                            <span class="pro-font9 pro-color1 u-boder">Logline</span>
                                        </div>
                                        <div>
                                            <p class="font-parra">Every life has Seasons of Change. Will those seasons
                                                open Katie to new opportunities or will they forever isolate her in
                                                survival mode?</p>
                                            <!--<a href="" class="font-parra2">Read more</a>-->
                                        </div>
                                    </article>

                                    <article class="col s12 m12 l12 xl12 mar-descrip5 padd-article">
                                        <div class="border-pro3 pro_marg1 u-flex1 u-flexTop u-justifyContentSpaceBetween">
                                            <span class="pro-font9 pro-color1 u-boder">Synopsis</span>
                                        </div>
                                        <div class="_list-info" data-read="1">
                                            <p class="font-parra">Katie MacKenna experienced one storm after another in
                                                her life. When Leukemia stole her mother from her and her father, Katie
                                                was only seven-years-old, and her father didn’t know how to cope after
                                                such a catastrophic loss. His response was to shut down and become
                                                abusive. The overwhelming devastation which surrounded Katie throughout
                                                her journey in life forced her to shut down just to survive as well.<br>
                                                <br>
                                                Trust is a difficult thing for many people, but for Katie it’s virtually
                                                impossible. Every life has Seasons of Change. Will those seasons open
                                                Katie to new opportunities or will they for...</p>
                                            <span class="pro-font6 read-mores more tos-more" id="0">Read more</span>
                                        </div>
                                    </article>
                                    <article class="col s12 m12 l12 xl12 mar-descrip5 padd-article">
                                        <div class="border-pro3 pro_marg1 u-flex1 u-flexTop u-justifyContentSpaceBetween">
                                            <span class="pro-font9 pro-color1 u-boder">Tags</span>
                                        </div>
                                        <div class="marg-tags">
                                            <button href="" class="boton-tags ">Christian</button>
                                            <button href="" class="boton-tags ">faith</button>
                                            <button href="" class="boton-tags ">inspiration</button>
                                            <button href="" class="boton-tags ">hope</button>
                                            <button href="" class="boton-tags ">action</button>
                                            <button href="" class="boton-tags ">YA</button>
                                            <button href="" class="boton-tags ">adventure</button>
                                            <button href="" class="boton-tags ">adult</button>
                                        </div>
                                    </article>
                                    <article class="col s12 m12 l12 xl12 mar-descrip5 padd-article coment-up">
                                        <div class="border-pro3 pro_marg1 u-flex1 u-flexTop u-justifyContentSpaceBetween">
                                            <span class="pro-font9 pro-color1 u-boder">Comments</span>
                                        </div>
                                        <div class="mod-coment-n">
                                            <ul id="list-coments">

                                                @if(!empty($comments))
                                                    @foreach ($comments as $commentsValue)

                                                        <li id="12-19-29">
                                                            <div class="mod-img-coment">
                                                                <a href="javascript:void(0);" class="u-cursor-def"><img src="https://storyrocket-aws3.s3.amazonaws.com/full5bdabde08e4741541062112sr_jpg" alt="Jennie Komp" class="responsive-img "></a>
                                                            </div>
                                                            <div class="mod-comet-1">
                                                                <p class="font-comet-1"><a class="u-cursor-def " href="javascript:void(0);">Jennie Komp</a> <span>This entire 5 book series (2 books of which have been published) was developed with marketing and merchandising in mind. Follow us at @myth_machine to see more

    Industry pros are calling it - Twilight meets Game of Thrones</span></p>
                                                                <p class="font-comet-2"><span class="font-comet-2">A month ago</span> - <span class="font-comet-2">
                                                                        </span></p></div>
                                                        </li>

                                                    @endforeach
                                                @endif

                                            </ul>
                                        </div>
                                        <div>
                                            <form id="add-coment" class="coment-group">
                                                <textarea name="textComent" id="comment_text" disable="false"
                                                          required="" placeholder="Write your opinion about the project"
                                                          class="text-coment"></textarea>
                                                <input type="submit" id="post-comment-btn" class="btn-coment "
                                                       value="POST COMMENT">
                                            </form>
                                        </div>
                                    </article>
                                </article>

                                <article class="col s12 m12 l4 xl4">
                                    <div class="col s12 m12 l12 xl12 colun-2"
                                         style="padding-left: 0px !important; padding-right: 0px !important;">
                                        <div class="border-pro3 pro_marg1 u-flex1 u-flexTop u-justifyContentSpaceBetween">
                                            <span class="pro-font9 pro-color1 u-boder">Cast Wish-List</span>
                                        </div>
                                        @if(!empty($projectCharacter))
                                            <div id="view-add-wishlist">
                                                <ul class="collapsible card-cast-p x_seall" data-collapsible="accordion"
                                                    id="listall-whist">
                                                    @foreach($projectCharacter as $projectCharacterVal)
                                                        <li>
                                                        <div class="collapsible-header cast-padding car-cast">
                                                            @if(file_exists($projectCharacterVal->image))
                                                            <div class="cast-img ">
                                                                <img src="{{$projectCharacterVal->image}}"
                                                                     class="cast" alt="Rachel Bilson">
                                                            </div>
                                                            @endif
                                                            <div class="cast-content ">
                                                                <div class="line-cast">
                                                                    <span class="font-cast1 truncate">
                                                                        @if($projectCharacterVal->real_actor)
                                                                            <strong>{{$projectCharacterVal->real_actor}}</strong> as
                                                                        @endif
                                                                        <strong>{{$projectCharacterVal->character_name}}</strong>
                                                                    </span>
                                                                </div>
                                                                <div class="line-cast">
                                                                    <span class="icon17-gender-female"></span>
                                                                    <span class="font-cast2">{{$projectCharacterVal->gender}}</span>
                                                                    <span class="icon3-years border-cast"></span>
                                                                    <span class="font-cast2">{{$projectCharacterVal->age}} Years old</span>
                                                                </div>
                                                                @if($projectCharacterVal->getty_images_id)
                                                                    <div class="line-cast">
                                                                        <span class="font-cast3">© Getty Images</span>
                                                                    </div>
                                                                @endif
                                                            </div>
                                                            <div class="bicon-cast ">
                                                                <span class="icon2-chevron-down dwn"></span>
                                                            </div>
                                                        </div>
                                                        <div class="collapsible-body body-cast">
                                                            <span class="font-body">It starts with a young seven-year-old, and watch as she grows to a beautiful eighteen-year-old. At eighteen, she stands to five-eight, with a slender frame. She has hazel eyes that have seen too much. She is an amazing artist, and often keeps to herself due to her home life. She is trying to keep it a secret for as long as possible.</span>
                                                        </div>
                                                    </li>
                                                    @endforeach

                                                </ul>
                                            </div>
                                        @endif
                                    </div>


                                    <!--- aparece cuando en responsive seccion comentns-->
                                    <article class="col s12 m12 l12 xl12 mar-descrip5 padd-article coment-res">
                                        <div class="border-pro3 pro_marg1 u-flex1 u-flexTop u-justifyContentSpaceBetween">
                                            <span class="pro-font9 pro-color1 u-boder">Comments</span>
                                        </div>
                                        <div>
                                            <ul>
                                                <li class="card-coment">
                                                    <div class="card-coment-img">
                                                        <img src="images/project/cast1.jpg" alt="coment"
                                                             class="responsive-img">
                                                    </div>
                                                    <div class="card-content-com">
						  <span class="font-coment couponcode"><a href=""><strong>Amanda Lum</strong></a>
						  </span><br>
                                                        <span class="font-coment2">Wow, really nicely done!!</span>
                                                    </div>
                                                </li>
                                                <li class="card-coment">
                                                    <div class="card-coment-img">
                                                        <img src="images/project/user1.jpg" alt="coment"
                                                             class="responsive-img">
                                                    </div>
                                                    <div class="card-content-com">
						  <span class="font-coment couponcode"><a href=""><strong>Toi Powell</strong></a>
						  </span><br>
                                                        <span class="font-coment2">Thank you very much <a href="">@Amanda Lum</a></span>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                        <div>
                                            <form id="add-coment" class="coment-group">
                                                <textarea name="" id="comment_text" required=""
                                                          placeholder="Write your opinion about the project"
                                                          class="text-coment"></textarea>
                                                <input type="submit" id="post-comment-btn" class="btn-coment"
                                                       value="Post comment">
                                            </form>
                                        </div>
                                    </article>
                                    <!---->
                                    <div class="col s12 m12 l12 xl12 hero-more">
                                        @if(!empty($projectAboutThisMember))
                                            <div class="border-pro3 pro_marg1 u-flex1 u-flexTop u-justifyContentSpaceBetween">
                                                <span class="pro-font9 pro-color1 u-boder">More about this member</span>
                                            </div>
                                            @foreach($projectAboutThisMember as $projectAboutThisMemberVal)

                                            <div class="col s6 m6 l6 xl6">
                                                <a href="{{$projectAboutThisMemberVal->urlproj}}" target="_blank">
                                                    <div class="more-img  couponcode tooltip-load " data-type="projects"
                                                         data-key="1260">
                                                        <img src="{{$projectAboutThisMemberVal->poster}}"
                                                             alt="{{$projectAboutThisMemberVal->project_name}}">
                                                        <div class="coupontooltip ">
                                                            <div class="tol-dinac"></div>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                            @endforeach

                                            <a target="_blank" class="btn-more" style="cursor: pointer" href="#">
                                                <button class="btn-more">
                                                        SEE MORE
                                                </button>
                                            </a>
                                        @endif
                                    </div>
                                </article>
                            </section>
                        </section>
                    </section>

                </div>
            </div>
        </div>
    </div>



<div id="open-request" class="modal modal-message-project">
    <div id="popup-fg">
        <div class="conteten-ms">
            <span class="font-pop">Request Full Project</span>
        </div>
        <form class="row padding-pop " id="frm-mjs">
            <div class="col s12 m2 l2 pop-padding">
                <div class="send-pop">
                    <img 
                        src="" 
                        data-src="{{empty($profile->photo) ? (empty($profile->facebook_photo) ? asset('images/avatar_default_user.png')  : $profile->facebook_photo ) : $profile->photo}}" class="responsive-img lazy-image">
                </div>
            </div>
            <div class="col s10 m10 l10 pop-padding2 modal-title-project">
                <span class="font-resq">{{$project->project_name}}</span>
                by {{$project->full_name}} {{$project->last_name}}
            </div>
            <div class="col s12 m12 l12 pop-padding3">
                <textarea class="text-area-pop" placeholder="Enter your message" id="modal-message-project"></textarea>
            </div>   
        </form>
        <div class="actions act-2">
        <button id="close-modal-userMessage" class="btn-cancel">Cancel</button>
        <button id="btn-composer-porject" class="btn-send">Send</button>
        </div>
    </div>
</div>
@endsection
