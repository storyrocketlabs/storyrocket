<div id="modalCreateProjectAssistent" class="modal wapper-new-one one">

    <div class="modal-create-project-assistent-flow">
        <div class="modal-create-project-assistent-flow-content">
            @include('projects.modals.create-assistant-one')
            @include('projects.modals.create-assistant-flow')
        </div>
    </div>

</div>
