<div class="assistent-flow-left assistent-flow-left-tags">
    <div class="assistent-flow-left-content">
        <form class="form-projct row" id="modalCreateProjectAssistentFormTags" name="modalCreateProjectAssistentFormTags" method="post">
            <div class="assistent-entity-tags">
                <div class="assistent-tags-content">
                    <div class="area-title-m-cp">
                        <h4 class="title-m-cp">Include Tags!</h4>
                        <p class="font-m-cp">This will generate more views for your project.</p>
                    </div>
                    <form id="frm-tag">
                        <div class="col s12 m12 l12 xl12 steps-form mar-cap ">
                            <div class="a-lbl">
                                <label class="lbl-m-cp">Tags</label>
                                <span class="icon-wap-i tooltip-wap">
                                    <i class="material-icons">info</i>
                                    <span class="tooltiptext-wap">Add the title to your project.  i.e. “Wedding Crashers,”  “Lethal Weapon 2,” “Borat: Cultural Learnings Of America For Make Benefit Glorious Nation Of Kazakhstan”</span>
                                </span>
                                <div class="select-m-cp-n">
                                    <div class="_tag-div">
                                        <input class="input-pas add-element tag-in hide required" data-action="add_projects" value="horror" data-type="ajax_insrt" type="name" name="" id="Tag" data-id="Tag" required="">
                                    </div>
                                </div>
                                <div class="mar-cap2 wap-border-b">
                                    <div class="chip">Horror<i class="close material-icons">close</i></div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <a  href="#" goto="poster" currentpage="tags"  class="btn-next-step-left btn-nex-m-cp next-1 links next-views" currentvalidate="true" >Continue</a>
                        <a  href="#" class="btn-next-step-left btn-nex-m-cp return-back bk"  goto="pdf" currentpage="tags">Back</a>
                    </form>
                </div>
            </div>
        </form>
    </div>
</div>