<div class="assistent-only-title assistent-flow-one ">
    <div class="assistent-flow-one-content">
        <form class="form-project row" id="modalCreateProjectAssistentFormName" name="modalCreateProjectAssistentFormName" action="" method="post">
            <div class="wapper-cont-new" id="wap">
                <div class="wap-icon-c">
                    <div class="modal-close wap-icon clear-popup">
                        <i class="material-icons">clear</i>
                    </div>
                </div>
                <div class="cont-new-p">
                    <div class="cont-new-p-2">
                        <div class="padding-warp1">
                            <figure>
                                <img src="{{asset('images/ilustracion.svg')}}" alt="icon" class="responsive-img">
                            </figure>
                        </div>
                        <div class="margin-wap1">
                            <p class="font-wap1">Hello {{$profile->full_name}}, I'm the Storyrocket Project Assistant!<br> I’m going to help you set up your project.</p>
                        </div>
                        <div class="f-name-projct">

                            <label for="" class="font-wap2 active">What’s the Title of your project?</label>
                            <span class="icon-wap-i tooltip-wap" >
                                    <i class="material-icons">info</i>
                                        <span class="tooltiptext-wap">Add the title to your project.  i.e. “Wedding Crashers,”  “Lethal Weapon 2,” “Borat: Cultural Learnings Of America For Make Benefit Glorious Nation Of Kazakhstan”</span>
                                </span>
                            <!--<input type="hidden" id="key" value="">-->
                            <input type="text"   name="assistantTitleProject" id="assistantTitleProject" value="" placeholder="Add Title" class="" style="margin: 10px 0px; text-align: center;">
                            <p class="requiered-f msg-title">* Required Fields</p>
                            <div class="margin-wap2">
                                <a href="{{route('home')}}" style="cursor: pointer;">
                                    <button type="button" name="button" class="btn-wap1 clear-popup"> Back to Dashboard</button>
                                </a>

                                <a id="modalCreateProjectAssistentTitleSubmit" class="btn-next-step" goto="two" currentpage="one">
                                    <button type="button" name="" class="btn-wap2 links" id="modalCreateProjectAssistentTitleSubmit" > Next </button>
                                </a>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>