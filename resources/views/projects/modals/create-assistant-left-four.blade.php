<div class="assistent-flow-left assistent-flow-left-four">
    <div class="assistent-flow-left-content">

        <form class="form-project row" id="modalCreateProjectAssistentFormFour" name="modalCreateProjectAssistentFormFour" action="" method="post">

            <div class="assistent-entity-four">
                <div class="assistent-entity-four-content">
                    <div class="area-title-m-cp">
                        <!--<div class="area-title-m-cp">
                            <h4 class="title-m-cp">Select Rating</h4>
                        </div>-->
                        <div class="col s12 m12 l12 xl12 steps-form mar-cap">
                            <div class="a-lbl">
                                <label class="lbl-m-cp">Registering Entity</label>
                                <!--<span class="icon-wap-i tooltip-wap"><i class="material-icons">info</i>
                                    <span class="tooltiptext-wap">If you are crowd funding for your project and have an established funding platform, you can link it here. Add the name of the platform and copy and paste the link. Then press "Save."</span>
                                </span>-->
                                <button type="button" name="button" class="wap-btn3  id-plst u-add-elements" id="btnAddEntityFour">Add Entity</button>
                            </div>
                            <div class="mar-cap2 list-entityform">
                            </div>
                        </div>
                    </div>

                    <form class="form-project row" method="post" id="frm-reiting">
                        
                        <div class="col s12 m12 l12 xl12 steps-form">
                            <div class="a-lbl">
                                <label class="lbl-m-cp">Rating</label>
                                <!-- <span class="icon-wap-i tooltip-wap"><i class="material-icons">info</i>-->
                                <!--<span class="tooltiptext-wap">If you are crowd funding for your project and have an established funding platform, you can link it here. Add the name of the platform and copy and paste the link. Then press "Save."</span>-->
                                <!-- </span>-->
                            </div>
                            <div class="">
                                <ul class="reiting-r">
                                    <li>
                                        <a class="assistent-four-square btm-rea-r btm-rea color-active-days-r" data-id="1"><label>G</label></a>
                                    </li>
                                    <li>
                                        <a class="assistent-four-square btm-rea-r btm-rea color-active-days-r" data-id="2"><label>PG</label></a>
                                    </li>
                                    <li>
                                        <a class="assistent-four-square btm-rea-r btm-rea color-active-days-r" data-id="3"><label>PG-13</label></a>
                                    </li>
                                    <li>
                                        <a class="assistent-four-square btm-rea-r btm-rea color-active-days-r" data-id="4"><label>R</label></a>
                                    </li>
                                    <li>
                                        <a class="assistent-four-square btm-rea-r btm-rea color-active-days-r" data-id="5"><label>NC-17</label></a>
                                    </li>
                                </ul>
                                <div class="rati-container">
                                    <p id="rati-1" class="list-reating font-reating-r hide"><strong>G — General Audiences.</strong> All Ages Admitted. A G-rated motion picture contains nothing in theme, language, nudity, sex, violence or other matters that, in the view of the Rating Board, would offend parents whose younger children view the motion</p>
                                    <p id="rati-2" class="list-reating font-reating-r hide"><strong>PG — Parental Guidance Suggested.</strong> Some Material May Not Be Suitable For Children. A PG-rated motion picture should be investigated by parents before they let their younger children attend. The PG rating indicates, in the view of the Rating Board, that parents may consider some material unsuitable for their children, and parents should make that decision.</p>
                                    <p id="rati-3" class="list-reating font-reating-r hide"><strong>PG-13 — Parents Strongly Cautioned</strong> Some Material May Be Inappropriate For Children Under 13. A PG-13 rating is a sterner warning by the Rating Board to parents to determine whether their children under age 13 should view the motion picture, as some material might not be suited for them. A PG-13 motion picture may go beyond the PG rating in theme, violence, nudity, sensuality, language, adult activities or other elements, but does not reach the restricted R category.</p>
                                    <p id="rati-4" class="list-reating font-reating-r hide"><strong>R — Restricted.</strong> Children Under 17 Require Accompanying Parent or Adult Guardian. An R-rated motion picture, in the view of the Rating Board, contains some adult material. An R-rated motion picture may include adult themes, adult activity, hard language, intense or persistent violence, sexually-oriented nudity, drug abuse or other elements, so that parents are counseled to take this rating very seriously. Children under 17 are not allowed to attend R-rated motion pictures unaccompanied by a parent or adult guardian.</p>
                                    <p id="rati-5" class="list-reating font-reating-r hide"><strong>NC-17 — No One 17 and Under Admitted.</strong> An NC-17 rated motion picture is one that, in the view of the Rating Board, most parents would consider patently too adult for their children 17 and under. No children will be admitted. NC-17 does not mean "obscene" or "pornographic" in the common or legal meaning of those words, and should not be construed as a negative judgment in any sense. The rating simply signalsd</p>
                                </div>

                            </div>

                        </div>
                        <div class="clearfix"></div>
                        <input class="required hide" id="info-reiting" value="">
                        <a  href="#" goto="genres" currentpage="four"  class="btn-next-step-left btn-nex-m-cp next-1 links next-views" currentvalidate="true" >Continue</a>
                        <a  href="#" class="btn-next-step-left btn-nex-m-cp return-back bk"  goto="tree" currentpage="four">Back</a>
                    </form>
                </div>
                <!-- hide contents -->
                <div class="assistent-entity-four-content-form" id="addEntityFour">
                    <div class="assistent-charanter-three-content-form-contet">
                        <div class="hiden-elements" id="_add-writer">
                            <div class="element-conten">
                                <div class="wap-back items-returns" data-id="_add-writer">
                                    <i class="material-icons">keyboard_backspace</i>
                                </div>
                                <div class="area-title-m-cp">
                                    <h4 class="title-m-cp">Additional Writer/s</h4>
                                    <p class="font-m-cp">If you are the only writer to this project, just leave this blank.</p>
                                </div>
                                <form class="form-project row" method="post" id="frm-writer4" autocomplete="off">
                                    <div class="col s12 m12 l12 xl12 steps-form">
                                        <div class="a-lbl">
                                            <label class="lbl-m-cp">Name of Writer *</label>
                                        </div>
                                        <div class="select-m-cp-n3 u-relative">
                                            <input type="text" name="assistentNameWrite4" value="" placeholder="Add Name" id="assistentNameWrite4" data-name="coauthor" class="text-left writer-input">
                                            <div class="resultado hide"></div>
                                        </div>
                                        <p class="requiered-f">* Required Fields</p>
                                    </div>
                                    <div class="col s12 m12 l12 xl12 steps-form">
                                        <div class="a-lbl">
                                            <label class="lbl-m-cp">Email of Writer *</label>
                                        </div>
                                        <div class="select-m-cp-n3">
                                            <input type="text" value="" placeholder="Add Email" class="input-mail text-left" name="assistentEmailWrite4" id="assistentEmailWrite4">
                                            <input type="hidden" name="couter" id="couter-writer4" value="0">
                                            <input type="hidden" name="group" id="group" value="0">
                                        </div>
                                        <p class="requiered-f">* Required Fields</p>
                                        <input type="hidden" id="tokm4" value="0">
                                    </div>
                                    <div class="clearfix"></div>
                                    <button class="btn-nex-m-cp add-elements-writer" data-class="list-writes" >Add Writer

                                    </button></form></div>
                        </div>
                    </div>
                </div>
                <!-- hide contents end-->
            </div>

        </form>

    </div>
</div>