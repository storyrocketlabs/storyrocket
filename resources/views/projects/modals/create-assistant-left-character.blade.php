<div class="assistent-flow-left assistent-flow-left-character">
    <div class="assistent-flow-left-content">
        <form class="form-projct row" id="modalCreateProjectAssistentFormCharacter" name="modalCreateProjectAssistentFormCharacter" method="post">
            <div class="assistent-location">
                <div class="assistent-location-content">
                    <div class="col s12 m12 l12 xl12 steps-form">
                        <div class="area-title-m-cp">
                            <h4 class="title-m-cp">You must include at least one Character to continue!</h4>
                            <p class="font-m-cp">Enter the name of your first Character. And now you can add the name of the actor you'd like to see in that role. Repeat for each additional Character in your story that you want listed.</p>
                        </div>
                    </div>
                    
                    <form class="form-location row" method="post" id="frm-character">
                        <div class="col s12 m12 l12 xl12 steps-form">
                            <div class="a-lbl">
                                <label class="lbl-m-cp">Character</label>
                                <span class="icon-wap-i tooltip-wap-2"><i class="material-icons">info</i>
                                    <span class="tooltiptext-wap-2">You must include at least one character.Enter the name of your first character, and add the name of the actor you’d like to see in that role (this is not mandatory).  Then select the gender, add the age and copy and paste or type a short description.  Once complete, press “Save and Continue.” Repeat for each additional character in your story that you want listed. The name(s) of each writer will appear below.</span>
                                </span>
                            </div>
                            <div class="btn-addchcter">
                                <!--<a href="" class="btn-add-chcter">Add a character</a>-->
                                <button type="button" name="button" class="wap-btn3 add-elements" data-id="_add-character" id="addCharacterButton">Add Character</button>
                            </div> 
                        </div>
                        <div class="col s12 m12 l12 xl12 steps-form">
                            <p class="font-m-cp2 font-info-content">Repeat for each additional Character in your story that you want listed. The name(s) of each Character will appear below.</p>
                            <p class="requiered-f">* Required Fields</p>
                            <div class="area-add-character">
                                <div class="row">
                                    <div class="col s12 m12 l12 xl12" id="list-characters">
                                        <div class="items-added-chcter" id="chart-12" data-id="12">
                                            <a href="#" class="char-getty-small left">
                                                <img src="https://www.storyrocket.com//public/images/icon-caract-getty.svg" alt="" id="img-12">
                                            </a>
                                            <p class="wap-font-name truncate" id="name-12">CharacterName</p>
                                            <div class="flex-container">
                                                <div class="_edit-item-chcter">
                                                    <a href="#" class="vd-icon-2 characters-list add-elements" data-id="_add-character" data-id="_add-character" data-type="edit" data-cod="12">
                                                        <i class="material-icons " >edit</i>
                                                        Edit
                                                    </a>
                                                    <a href="#" class="vd-icon remove-characters" data-type="remove" data-cod="12">
                                                        <i class="material-icons " >delete</i>
                                                        Remove
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <a  href="#" goto="video" currentpage="character"  class="btn-next-step-left btn-nex-m-cp next-1 links next-views" currentvalidate="true" >Continue</a>
                        <a  href="#" class="btn-next-step-left btn-nex-m-cp return-back bk"  goto="genre" currentpage="character">Back</a>
                    </form>
                </div>
            </div>
            <!--hide contents-->
            <div class="assistent-charanter-content-form assistent-content-hide" id="addCharacter">
            <div class="wap-back items-returns" data-id="_add-character">
                <i class="material-icons">keyboard_backspace</i>
            </div>
            <form class="form-project row padding-pro-l" method="POST" id="frm-gettyn" data-find="storyrocket"><br><br>
                <div class="col s12 m12 l12 xl12 ">
                    <div class="steps-form-alone">
                        <div class="a-lbl">
                            <label class="lbl-m-cp">Character</label>
                        </div>
                        <!--<div class="btn-addchcter">
                            <a href="javascript:void(0);" class="btn-add-chcter upload-characters">Add a character</a>
                            <p class="requiered-f">* Required Fields</p>
                        </div>-->
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="area-add-chcter">
                    <div class="row">
                        <div class="col s12 m4 l4 xl4  center">
                            <a href="javascript:void(0);" class="char-getty upload-characters "  data-getty="true" data-count="10">
                                <img src="https://www.storyrocket.com/public/images/icon-caract-getty.svg" alt="" class="src-gettyn src-gettyn-add" >
                                <span>select image</span>
                                <input type="hidden" id="ima-gettyn-input" data-name="images" value="">
                                <input type="hidden" id="update-getty-image" value="i" data-name="update-getty-image">
                            </a>
                            <p class="char-txt-getty">Use Getty images photo</p>
                        </div>
                        <div class="col s12 m8 l8 xl8">
                            <input type="hidden" value='storyrocket' name="srctype" data-name='srctype' id="srctype">
                            <input class="margin-last-l input-cast  input-profile1 inpt-alone-mb add-req required" type="name" id="character" name="" placeholder="Enter name of Character*" data-name='nameCharacter'>
                            <!--<input class="input-profile1 inpt-alone-mb" type="text" placeholder="" value="">-->
                            <input class="margin-last-l input-profile1 inpt-alone-mb" id="actor" type="text" placeholder="Enter name of actor" value="" data-name='nameActor'>
                            <div class="col s12 m6 no-padding-l" style=" padding-left: 0px;">
                                <!--<input class="input-profile1 inpt-alone-mb" type="text" placeholder="Age" value="">-->
                                <input class="input-profile1 inpt-alone-mb add-req required" type="name" name="" placeholder="Enter age" data-name='age' id='age'>
                            </div>
                            <div class="col s12 m6 no-padding-r inpt-alone-mb" style="padding-right: 0px;">
                                <div class="select-m-cp ">
                                    <select class="browser-default " data-name='genre' id="genre">
                                        <option value="0"  selected="" class="" data-gender='genre'>Gender*</option>
				                        <option value="Male">Male</option>
				                        <option value="Female">Female</option>
				                        <option value="Other">Other</option>
                                    </select>
                                </div>
                                <p class="requiered-f">* Required Fields</p>
                            </div>
                        </div>
                        <div class="col s12 m12 l12 xl12 steps-form">
                            <div class="select-m-cp-n">
                                <!--<textarea name="name" rows="8" cols="80" placeholder="Copy &amp; paste or type your description"></textarea>-->
                                <textarea class="text-pas-cast add-req required" name="message" rows="8" cols="80" id='desc' data-name='descrit' placeholder="Copy and paste or type your Character description"></textarea>
                                <input type="hidden" id="hidden-cod" data-name="cod" value="0">
                                <input type="hidden" id="keyGettyImageCharacter" data-name="keyGettyImageCharacter" value="123">
                            </div>
                            <p class="requiered-f">* Required Fields</p>
                        </div>
                    </div>
                    <!--<a href="javascript:void(0);" class="add-chr-btn2 bottom-charac ">Add</a>-->
                    <button class="add-chr-btn2 bottom-charac ">Add Character</button>

                </div>
            </form>
            </div>
            <!--end hide contents-->
        </form>
    </div>
</div>