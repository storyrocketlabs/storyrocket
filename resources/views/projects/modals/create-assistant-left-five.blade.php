<div class="assistent-flow-left assistent-flow-left-five">
    <div class="assistent-flow-left-content">

        <form class="form-project row" id="modalCreateProjectAssistentFormFive" name="modalCreateProjectAssistentFormFive" action="" method="post">

            <div class="assistent-entity-five">
                <div class="assistent-entity-five-content">
                    <div class="area-title-m-cp">
                        <h4 class="title-m-cp">Select up to 3 Genres that best represent your project!</h4>
                        <p class="font-m-cp">This is very important because it allows your project to be found in an Advance Search.</p>
                    </div>
                    <form id="frm-genre">
                        <div class="row">
                            <div class="col s12 m12 l12 xl12 steps-form">
                                <br><br>
                                <div class="a-lbl">
                                    <label class="lbl-m-cp">Genres</label>
                                    <span class="icon-wap-i tooltip-wap-16"><i class="material-icons">info</i>
                                        <span class="tooltiptext-wap-16">Select a genre that best describes your project. Choosing at least one genre is required to submit. Adding a second genre will increase the times your project may be seen. However listing your project in an inappropriate category is not advised, as it will annoy members searching for category specific projects.</span>
                                    </span>
                                </div>
                                <p class="all-btns-genres">
                                    <!-- <button class="btn-genres color-active-days">Action</button>-->
                                    <button class="btn-genres" id="1" data-key="1462" data-dell="i">Action</button>
                                    <button class=" bnt-no-genres color-active-days " id="2" data-key="1462" data-dell="i">Adventure</button>
                                    <button class=" bnt-no-genres color-active-days " id="34" data-key="1462" data-dell="i">Animals</button>
                                    <button class="btn-genres" id="3" data-key="1462" data-dell="i">Animation</button>
                                    <button class=" bnt-no-genres color-active-days " id="35" data-key="1462" data-dell="i">Anime / manga</button>
                                    <button class="btn-genres" id="4" data-key="1462" data-dell="i">Biography</button>
                                    <button class="btn-genres" id="31" data-key="1462" data-dell="i">Children</button>
                                    <button class="btn-genres" id="5" data-key="1462" data-dell="i">Comedy</button>
                                    <button class="btn-genres" id="29" data-key="1462" data-dell="i">Comic book</button>
                                    <button class="btn-genres" id="6" data-key="1462" data-dell="i">Crime</button>
                                    <button class="btn-genres" id="26" data-key="1462" data-dell="i">Dark comedy</button>
                                    <button class="btn-genres" id="7" data-key="1462" data-dell="i">Documentary</button>
                                    <button class="btn-genres" id="8" data-key="1462" data-dell="i">Drama</button>
                                    <button class="btn-genres" id="24" data-key="1462" data-dell="i">Erotic</button>
                                    <button class="btn-genres" id="9" data-key="1462" data-dell="i">Faith</button>
                                    <button class="btn-genres" id="10" data-key="1462" data-dell="i">Family</button>
                                    <button class="btn-genres" id="11" data-key="1462" data-dell="i">Fantasy</button>
                                    <button class="btn-genres" id="12" data-key="1462" data-dell="i">Film noir</button>
                                    <button class="btn-genres" id="36" data-key="1462" data-dell="i">Graphics novels</button>
                                    <button class="btn-genres" id="13" data-key="1462" data-dell="i">History</button>
                                    <button class="btn-genres" id="14" data-key="1462" data-dell="i">Horror</button>
                                    <button class="_upper btn-genres" id="33" data-key="1462" data-dell="i">Lgbtq</button>
                                    <button class="btn-genres" id="25" data-key="1462" data-dell="i">Mafia</button>
                                    <button class="btn-genres" id="27" data-key="1462" data-dell="i">Monster</button>
                                    <button class="btn-genres" id="16" data-key="1462" data-dell="i">Musical</button>
                                    <button class="btn-genres" id="15" data-key="1462" data-dell="i">Mystery</button>
                                    <button class="btn-genres" id="17" data-key="1462" data-dell="i">Reality</button>
                                    <button class="btn-genres" id="18" data-key="1462" data-dell="i">Romance</button>
                                    <button class="btn-genres" id="19" data-key="1462" data-dell="i">Sci fi</button>
                                    <button class="btn-genres" id="20" data-key="1462" data-dell="i">Sport</button>
                                    <button class="btn-genres" id="28" data-key="1462" data-dell="i">Spy</button>
                                    <button class="btn-genres" id="30" data-key="1462" data-dell="i">Super hero</button>
                                    <button class="btn-genres" id="21" data-key="1462" data-dell="i">Thriller</button>
                                    <button class="btn-genres" id="22" data-key="1462" data-dell="i">War</button>
                                    <button class="btn-genres" id="23" data-key="1462" data-dell="i">Western</button>
                                    <button class="btn-genres" id="32" data-key="1462" data-dell="i">Young adult</button>
                                </p>
                            </div>
                        </div>
                        <input type="hidden" class="required _tag" value="1">
                        <div class="clearfix"></div>
                        <!--   <a href="#" class="btn-nex-m-cp">Continue</a>  -->
                        <a href="#" class="btn-nex-m-cp links" data-action="prev" data-rel="location" data-type="u" data-key="14-18-19-90">Continue</a>
                        <a href="#" class="btn-nex-m-cp bk links" data-action="next" data-type="u" data-key="14-18-19-90" data-rel="reiting">Back</a>
                    </form>

                </div>


            </div>

        </form>

    </div>
</div>