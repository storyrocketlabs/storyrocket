<div class="assistent-flow-left assistent-flow-left-three">
    <div class="assistent-flow-left-content">

        <form class="form-project row" id="modalCreateProjectAssistentFormThree" name="modalCreateProjectAssistentFormThree" action="" method="post">

            <div class="assistent-vide-three">
                <div class="assistent-vide-three-content">
                    <h4 class="title-m-cp">Video uploading made easy!</h4>
                    <p class="font-m-cp">Add as many videos, related to your project, as you would like.
                        The Video will appear in your Pitch Package.</p>

                    <div class="form-project-2 row" action="" method="" id="wap-form">
                        <div class="col s12 m12 l12 xl12 steps-form mar-cap broder-wap">
                            <div class="a-lbl">
                                <label class="lbl-m-cp">Video</label>
                                <span class="icon-wap-i tooltip-wap-11"><i class="material-icons">info</i>
                                    <span class="tooltiptext-wap-11">  To add a video, click the "Add Video" button. Type the title of your video and in the box below, copy and paste your link from YouTube or Vimeo, then press "Save." Repeat for each additional video you want listed. Add as many videos, related to your project, as you would like. The video links will appear below. </span>
                                </span>
                                <button type="button" name="button" class="wap-btn3 add-elements" id="btnAddVideoThree" data-id="_add-video" style="width: 180px;">Add another Video</button>

                            </div>
                        </div>
                        <div class="col s12 m12 l12 xl12">
                            <p class="font-url">Copy and paste your link from YouTube or Vimeo</p>
                        </div>
                        <div class="col s12 m12 l12 xl12">
                            <ul id="list-video">
                                <li class="card-wap">
                                    <div class="card-wap-video">
                                        <span class="wap-icon-play"><i class="material-icons">play_circle_outline</i></span>
                                        <span class="wap-font-v _v-font">Hello</span>
                                        <div class="wap-video-p">

                                            <span class="wap-icon-dele let-dif" data-dif="397" data-type="remove"><i class="material-icons">close</i></span>
                                        </div>
                                    </div>
                                    <a href="!#mAKsZ26SabQ" data-event="youtube" class="wap-font-link let-dif _preview-video" data-dif="397" data-type="edit">Preview video</a>
                                </li>
                            </ul>
                        </div>

                        <div class="clearfix"></div>
                        <div class="cont-btn">
                            <a id="modalCreateProjectAssistentThreeSubmit" href="#" goto="four" currentpage="three" class="btn-next-step-left btn-nex-m-cp next-1 links" >Continue</a>
                            <a id="modalCreateProjectAssistentThreeBack" href="#" class="btn-next-step-left btn-nex-m-cp return-back bk"  goto="two" currentpage="three">Back</a>
                        </div>

                    </div>
                </div>
                <div class="assistent-vide-three-content-form">
                    <div class="assistent-vide-three-content-form-content">
                        <div class="hiden-elements" id="_add-video" style="left: 0px;">
                            <div class="element-conten">
                                <div class="wap-back items-returns items-returns-three" data-id="_add-video">
                                    <i class="material-icons">keyboard_backspace</i>
                                </div>
                                <div class="area-title-m-cp">
                                    <h4 class="title-m-cp">Add Video</h4>
                                </div>
                                <form class="form-project row" method="post" id="frm-video-project">
                                    <div class="col s12 m12 l12 xl12 steps-form">
                                        <div class="a-lbl">
                                            <label class="lbl-m-cp">Video Title</label>
                                        </div>
                                        <div class="select-m-cp-n3">
                                            <input type="text" name="" value="" placeholder="Video Title, i.e. ‘Who Am I’ or ‘Trailer’" id="titleVideo" data-name="title">
                                            <input type="hidden" id="key" data-name="key" value="14-18-16-93">
                                        </div>
                                    </div>
                                    <div class="col s12 m12 l12 xl12 steps-form">
                                        <div class="a-lbl">
                                            <label class="lbl-m-cp">Video Link</label>
                                        </div>
                                        <div class="select-m-cp-n3">
                                            <input type="text" name="" value="" placeholder="Copy and paste link from YouTube or Vimeo" id="urlVideo" data-name="url-vidio">
                                        </div>
                                    </div>
                                    <div class="col s12 m12 l12 xl12">

                                    </div>
                                    <div class="clearfix"></div>
                                    <button class="btn-nex-m-cp">Add Video</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </form>

    </div>
</div>