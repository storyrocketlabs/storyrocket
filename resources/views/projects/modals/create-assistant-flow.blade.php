<div class="assistent-flow assistent-flow-section">
    <div class="assistent-flow-two-content">
        <div class="modal-create-project-assistent-header">
            <h2><span class="icon-projects icon-nav"></span>Storyrocket Project Assistant</h2>
            <a href="{{route('home')}}" class="btn-back-dash clear-popup">Back to Dashboard</a>
        </div>
        <div class="modal-create-project-assistent-header-error">
            <div class="scope-togle-error">
                <i class="material-icons">error</i> Opps! Please fill out this fields. We've marked them for you to modify or add.
            </div>
        </div>
        <div class="modal-create-project-assistent-body">
            <div class="assistent-flow-form">
                <div class="assistent-flow-form-left-content">
                    @include('projects.modals.create-assistant-left-one')
                    @include('projects.modals.create-assistant-left-two')
                    @include('projects.modals.create-assistant-left-three')
                    @include('projects.modals.create-assistant-left-four')
                    @include('projects.modals.create-assistant-left-genres')
                    @include('projects.modals.create-assistant-left-location')
                    @include('projects.modals.create-assistant-left-character')
                    @include('projects.modals.create-assistant-left-video')
                    @include('projects.modals.create-assistant-left-pdf')
                    @include('projects.modals.create-assistant-left-tags')
                    @include('projects.modals.create-assistant-left-poster')
                    @include('projects.modals.create-assistant-left-congratulation')
                </div>
                <div class="assistent-flow-right">
                    <div class="assistent-flow-right-content">
                        <p class="tit-project-scope">PROJECT SCOPE</p>
                        <div class="row">
                            <div class="col s12 m6 l6 xl6">
                                <div class="items-m-cp items-m-cp-title">
                                    <div class="items-m-cp-selected-row-header-1">
                                        <div class="items-m-cp-selected-row-header-1-content">
                                            <div class="items-m-cp-selected-row-header-content-name">
                                                Title
                                            </div>
                                            <div class="items-m-cp-selected-row-header-content-link btn-back-step-title" currentpage="two" goto="one">
                                                <a >Edit</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="items-m-cp-selected-row-data-1">
                                        <div class="items-m-cp-selected-row-data-content">
                                        </div>
                                    </div>
                                </div>

                                <div class="items-m-cp items-m-cp-relation-to-the-project hide">
                                    <div class="items-m-cp-selected-row-header-2">
                                        <div class="items-m-cp-selected-row-header-2-content">
                                            <div class="items-m-cp-selected-row-header-content-name items-m-cp-selected-row-header-2-content-name">
                                                Relation to the project
                                            </div>
                                            <div class="items-m-cp-selected-row-header-content-link btn-back-step" currentpage="three" goto="one">
                                                <a >Edit</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="items-m-cp-selected-row-data-1">
                                        <div class="items-m-cp-selected-row-data-content">
                                        </div>
                                    </div>
                                </div>
                                <div class="items-m-cp items-m-cp-intended-medium hide">
                                    <div class="items-m-cp-selected-row-header-2">
                                        <div class="items-m-cp-selected-row-header-2-content">
                                            <div class="items-m-cp-selected-row-header-content-name items-m-cp-selected-row-header-2-content-name">
                                                Intended Medium
                                            </div>
                                            <div class="items-m-cp-selected-row-header-content-link btn-back-step" currentpage="three" goto="one">
                                                <a >Edit</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="items-m-cp-selected-row-data-1">
                                        <div class="items-m-cp-selected-row-data-content">
                                        </div>
                                    </div>
                                </div>
                                <div class="items-m-cp items-m-cp-intended-material-type hide">
                                    <div class="items-m-cp-selected-row-header-2">
                                        <div class="items-m-cp-selected-row-header-2-content">
                                            <div class="items-m-cp-selected-row-header-content-name items-m-cp-selected-row-header-2-content-name">
                                                Material Type
                                            </div>
                                            <div class="items-m-cp-selected-row-header-content-link btn-back-step" currentpage="three" goto="one">
                                                <a >Edit</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="items-m-cp-selected-row-data-1">
                                        <div class="items-m-cp-selected-row-data-content">

                                        </div>
                                    </div>
                                </div>
                                <div class="items-m-cp items-m-cp-intended-lenguage-of-project hide">
                                    <div class="items-m-cp-selected-row-header-2">
                                        <div class="items-m-cp-selected-row-header-2-content">
                                            <div class="items-m-cp-selected-row-header-content-name items-m-cp-selected-row-header-2-content-name">
                                                Language of project
                                            </div>
                                            <div class="items-m-cp-selected-row-header-content-link btn-back-step" currentpage="three" goto="one">
                                                <a >Edit</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="items-m-cp-selected-row-data-1">
                                        <div class="items-m-cp-selected-row-data-content">
                                            Arabic
                                        </div>
                                    </div>
                                </div>
                                <div class="items-m-cp items-m-cp-logline hide">
                                    <div class="items-m-cp-selected-row-header-2">
                                        <div class="items-m-cp-selected-row-header-2-content">
                                            <div class="items-m-cp-selected-row-header-content-name items-m-cp-selected-row-header-2-content-name">
                                                Logline
                                            </div>
                                            <div class="items-m-cp-selected-row-header-content-link btn-back-step" currentpage="three" goto="two">
                                                <a >Edit</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="items-m-cp-selected-row-data-1">
                                        <div class="items-m-cp-selected-row-data-content">
                                            Logline
                                        </div>
                                    </div>
                                </div>
                                <div class="items-m-cp items-m-cp-tagline hide">
                                    <div class="items-m-cp-selected-row-header-2">
                                        <div class="items-m-cp-selected-row-header-2-content">
                                            <div class="items-m-cp-selected-row-header-content-name items-m-cp-selected-row-header-2-content-name">
                                                Tagline
                                            </div>
                                            <div class="items-m-cp-selected-row-header-content-link btn-back-step" currentpage="three" goto="two">
                                                <a >Edit</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="items-m-cp-selected-row-data-1">
                                        <div class="items-m-cp-selected-row-data-content">
                                            Tagline
                                        </div>
                                    </div>
                                </div>
                                <div class="items-m-cp items-m-cp-synopsis hide">
                                    <div class="items-m-cp-selected-row-header-2">
                                        <div class="items-m-cp-selected-row-header-2-content">
                                            <div class="items-m-cp-selected-row-header-content-name items-m-cp-selected-row-header-2-content-name">
                                                Synopsis
                                            </div>
                                            <div class="items-m-cp-selected-row-header-content-link btn-back-step" currentpage="three" goto="two">
                                                <a >Edit</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="items-m-cp-selected-row-data-1">
                                        <div class="items-m-cp-selected-row-data-content">
                                            Synopsis
                                        </div>
                                    </div>
                                </div>
                                <div class="items-m-cp items-m-cp-intended-writer hide">
                                    <div class="items-m-cp-selected-row-header-2">
                                        <div class="items-m-cp-selected-row-header-2-content">
                                            <div class="items-m-cp-selected-row-header-content-name items-m-cp-selected-row-header-2-content-name">
                                                Writer(s)
                                            </div>
                                            <div class="items-m-cp-selected-row-header-content-link btn-back-step" currentpage="three" goto="two">
                                                <a >Edit</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="items-m-cp-selected-row-data-1">
                                        <div class="items-m-cp-selected-row-data-content">
                                            Writer(s)
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col s12 m6 l6 xl6">
                                <div class="items-m-cp items-m-cp-intended-rating hide">
                                    <div class="items-m-cp-selected-row-header-2">
                                        <div class="items-m-cp-selected-row-header-2-content">
                                            <div class="items-m-cp-selected-row-header-content-name items-m-cp-selected-row-header-2-content-name">
                                                Rating
                                            </div>
                                            <div class="items-m-cp-selected-row-header-content-link btn-back-step" currentpage="three" goto="two">
                                                <a >Edit</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="items-m-cp-selected-row-data-1">
                                        <div class="items-m-cp-selected-row-data-content">
                                            Rating
                                        </div>
                                    </div>
                                </div>
                                <div class="items-m-cp items-m-cp-intended-genres hide">
                                    <div class="items-m-cp-selected-row-header-2">
                                        <div class="items-m-cp-selected-row-header-2-content">
                                            <div class="items-m-cp-selected-row-header-content-name items-m-cp-selected-row-header-2-content-name">
                                                Genres
                                            </div>
                                            <div class="items-m-cp-selected-row-header-content-link btn-back-step" currentpage="three" goto="two">
                                                <a >Edit</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="items-m-cp-selected-row-data-1">
                                        <div class="items-m-cp-selected-row-data-content">
                                            <!--Genres-->
                                        </div>
                                    </div>
                                </div>
                                <div class="items-m-cp items-m-cp-intended-era hide">
                                    <div class="items-m-cp-selected-row-header-2">
                                        <div class="items-m-cp-selected-row-header-2-content">
                                            <div class="items-m-cp-selected-row-header-content-name items-m-cp-selected-row-header-2-content-name">
                                                Era
                                            </div>
                                            <div class="items-m-cp-selected-row-header-content-link btn-back-step" currentpage="three" goto="two">
                                                <a >Edit</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="items-m-cp-selected-row-data-1">
                                        <div class="items-m-cp-selected-row-data-content">
                                            Era
                                        </div>
                                    </div>
                                </div>
                                <div class="items-m-cp items-m-cp-intended-location hide">
                                    <div class="items-m-cp-selected-row-header-2">
                                        <div class="items-m-cp-selected-row-header-2-content">
                                            <div class="items-m-cp-selected-row-header-content-name items-m-cp-selected-row-header-2-content-name">
                                                Location
                                            </div>
                                            <div class="items-m-cp-selected-row-header-content-link btn-back-step" currentpage="three" goto="two">
                                                <a >Edit</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="items-m-cp-selected-row-data-1">
                                        <div class="items-m-cp-selected-row-data-content">
                                            Location
                                        </div>
                                    </div>
                                </div>
                                <div class="items-m-cp items-m-cp-intended-character hide">
                                    <div class="items-m-cp-selected-row-header-2">
                                        <div class="items-m-cp-selected-row-header-2-content">
                                            <div class="items-m-cp-selected-row-header-content-name items-m-cp-selected-row-header-2-content-name">
                                                Character (2)
                                            </div>
                                            <div class="items-m-cp-selected-row-header-content-link btn-back-step" currentpage="three" goto="two">
                                                <a >Edit</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="items-m-cp-selected-row-data-1">
                                        <div class="items-m-cp-selected-row-data-content">
                                            Character (2)
                                        </div>
                                    </div>
                                </div>
                                <div class="items-m-cp items-m-cp-intended-video hide">
                                    <div class="items-m-cp-selected-row-header-2">
                                        <div class="items-m-cp-selected-row-header-2-content">
                                            <div class="items-m-cp-selected-row-header-content-name items-m-cp-selected-row-header-2-content-name">
                                               Video
                                            </div>
                                            <div class="items-m-cp-selected-row-header-content-link btn-back-step" currentpage="three" goto="two">
                                                <a >Add</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="items-m-cp-selected-row-data-1">
                                        <div class="items-m-cp-selected-row-data-content">
                                            Video
                                        </div>
                                    </div>
                                </div>
                                <div class="items-m-cp items-m-cp-intended-video hide">
                                    <div class="items-m-cp-selected-row-header-2">
                                        <div class="items-m-cp-selected-row-header-2-content">
                                            <div class="items-m-cp-selected-row-header-content-name items-m-cp-selected-row-header-2-content-name">
                                                Tag
                                            </div>
                                            <div class="items-m-cp-selected-row-header-content-link btn-back-step" currentpage="three" goto="two">
                                                <a >Edit</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="items-m-cp-selected-row-data-1">
                                        <div class="items-m-cp-selected-row-data-content">
                                            Tag
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>