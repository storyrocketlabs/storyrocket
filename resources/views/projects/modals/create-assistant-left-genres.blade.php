<div class="assistent-flow-left assistent-flow-left-genres">
    <div class="assistent-flow-left-content">
        <form class="form-projct row" id="modalCreateProjectAssistentFormGenre" name="modalCreateProjectAssistentFormGenre" method="post">
            <div class="assistent-entity-genre">
                <div class="assistent-genre-content">
                    <div class="area-title-m-cp">
                        <h4 class="title-m-cp">Select up to 3 Genres that best represent your project!</h4>
                        <p class="font-m-cp">This is very important because it allows your project to be found in an Advance Search.</p>
                    </div>
                    <form class="form-project row" method="post" id="frm-genre">
                        <div class="a-lbl">
                            <label class="lbl-m-cp">Genres</label>
                            <span class="icon-wap-i tooltip-wap-2">
                                <i class="material-icons">info</i>
                                <span class="tooltiptext-wap-2">Select a genre that best describes your project. Choosing at least one genre is required to submit. Adding a second genre will increase the times your project may be seen. However listing your project in an inappropriate category is not advised, as it will annoy members searching for category specific projects.</span>
                            </span>
                        </div>
                        <!--lista de tag de generos seleccionados-->
                        <p class="all-btns-genres">
                            <button class="btn-genres">Action</button>
                            <button class="btn-genres">Action</button>
                            <button class="btn-genres">Action</button>
                            <button class="btn-genres">Action</button>
                            <button class="btn-genres">Action</button>
                        </p>
                        <input type="hidden" class="required _tag" value="">
                        <div class="clearfix"></div>
                        <a  href="#" goto="location" currentpage="genres"  class="btn-next-step-left btn-nex-m-cp next-1 links next-views" currentvalidate="true" >Continue</a>
                        <a  href="#" class="btn-next-step-left btn-nex-m-cp return-back bk"  goto="four" currentpage="genres">Back</a>
                    </form>
                </div>
            </div>
        </form>
    </div>
</div>