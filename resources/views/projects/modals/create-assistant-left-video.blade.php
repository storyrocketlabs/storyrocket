<div class="assistent-flow-left assistent-flow-left-video">
    <div class="assistent-flow-left-content">
        <form class="form-projct row" id="modalCreateProjectAssistentFormVideo" name="modalCreateProjectAssistentFormVideo" method="post">
            <div class="assistent-video">
                <div class="assistent-video-content">
                    <div class="col s12 m12 l12 xl12 steps-form">
                        <div class="area-title-m-cp space-video">
                            <h4 class="title-m-cp">Video uploading made easy!</h4>
                            <p class="font-m-cp">Add as many videos, related to your project, as you would like.<br/>The Video will appear in your Pitch Package.</p>
                        </div>
                    </div>
                    <form class="form-vifro row" method="post" id="frm-video">
                        <div class="col s12 m12 l12 xl12 steps-form">
                            <div class="a-lbl">
                                <label class="lbl-m-cp">Video</label>
                                <span class="icon-wap-i tooltip-wap-11"><i class="material-icons">info</i>
                                    <span class="tooltiptext-wap-11">  To add a video, click the "Add Video" button. Type the title of your video and in the box below, copy and paste your link from YouTube or Vimeo, then press "Save." Repeat for each additional video you want listed. Add as many videos, related to your project, as you would like. The video links will appear below. </span>
                                </span>
                                <button type="button" name="button" class="wap-btn3 add-elements " id="addVideoButton" data-id="_add-video" style="width: 180px;">Add Video</button>
                            </div>
                            <p class="font-url">Copy and paste your link from YouTube or Vimeo</p>
                        </div>
                        <div class="col s12 m12 l12 xl12 steps-form">
                            <ul id="list-video">
                                <li class="card-wap">
                                    <div class="card-wap-video">
                                        <span class="wap-icon-play"><i class="material-icons">play_circle_outline</i></span>
                                        <span class="wap-font-v _v-font">title video</span>
                                        <div class="wap-video-p">
                                            <span class="wap-icon-dele let-dif" data-dif="12" data-type="remove"><i class="material-icons">close</i></span>
                                        </div>
                                    </div>
                                    <a href="!#qswerdsadd23" data-event='12' class="wap-font-link let-dif _preview-video" data-dif="12" data-type="edit">Preview video</a>
                                </li>
                            </ul>
                        </div>
                        <div class="clearfix"></div>
                        <a  href="#" goto="pdf" currentpage="video"  class="btn-next-step-left btn-nex-m-cp next-1 links next-views" currentvalidate="true" >Continue</a>
                        <a  href="#" class="btn-next-step-left btn-nex-m-cp return-back bk"  goto="character" currentpage="video">Back</a>
                    </form>
                </div>
            </div>
            <div class="assistent-video-content-form assistent-content-hide" id="addVideo">
                <div class="wap-back items-returns" data-id="_add-video">
                    <i class="material-icons">keyboard_backspace</i>
                </div>
                
                <form class="form-project row" method="post" id="frm-video-project">
                    <div class="col s12 m12 l12 xl12 steps-form">
                        <div class="area-title-m-cp">
                            <h4 class="title-m-cp">Add Video</h4>
                        </div>
                    </div>
                    <div class="col s12 m12 l12 xl12 steps-form">
                        <div class="a-lbl">
                            <label class="lbl-m-cp">Video Title</label>
                        </div>
                        <div class="select-m-cp-n3">
                            <input type="text" class="text-left" name="" value="" placeholder="Video Title, i.e. ‘Who Am I’ or ‘Trailer’" id="titleVideo" data-name="title">
                            <input type="hidden" id="keyVideo" data-name="key" value="12">
                        </div>
                    </div>
                    <div class="col s12 m12 l12 xl12 steps-form">
                        <div class="a-lbl">
                            <label class="lbl-m-cp">Video Link</label>
                        </div>
                        <div class="select-m-cp-n3">
                            <input type="text" name="" class="text-left" value="" placeholder="Copy and paste link from YouTube or Vimeo" id="urlVideo" data-name='url-vidio'>
                        </div>
                    </div>
                    <div class="col s12 m12 l12 xl12">
                       
                    </div>
                    <div class="clearfix"></div>
                    <button class="btn-nex-m-cp">Add Video</button>
                </form>
            </div>
        </form>
    </div>
</div>