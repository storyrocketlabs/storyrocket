<div class="assistent-flow-left assistent-flow-left-congratulation">
    <div class="assistent-flow-left-content">
        <div class="assistent-congratulation">
            <div class="assistent-congratulation-content">
                <div class="wapper-cont-new" id="wap">
                    <div class="wap-icon-c">
                        <div class="wap-icon clear-popup">
                            <i class="material-icons">clear</i>
                        </div>
                    </div>
                    <div class="cont-new-p">
                        <div class="cont-new-p-2">
                            <div class="">
                                <figure>
                                    <img src="https://www.storyrocket.com/public/images/ilustracion-f.svg" alt="icon" class="responsive-img">
                                </figure>
                            </div>
                            <div class="margin-wap1">
                                <p class="font-wap-2-2"> Great Job NOMBRE DEL PROJECTO. You’re done!</p>
                                <p class="font-wap1"> To make your project public and share it with the World,<br> click "Upload my Project.”</p>
                            </div>
                            <div class="" style="text-align: center;display: block;position:relative;">
                                <button type="button" name="button" class="btn-wap2 btn-nex-m-cp links bottom-publish" style="width: 332px;font-size: 20px;display: block;position: relative;float: inherit;margin: 9px auto;" data-rel="loadingprojects" data-type="u" data-key="23">Upload my Project</button>
                                <div style="position: relative;font-size: 13px;margin-bottom: 8px;">You accept our Terms of Service and Privacy Policy.</div>  
                                <a href="#" class="wap-back items-returns bk links" style="background: #e5e9f2 !important;margin-bottom: 0; position: relative;" data-action="next" data-type="u" data-key="23" data-rel="poster"><i class="material-icons">keyboard_backspace</i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>