
<div class="assistent-flow-left assistent-flow-left-one">
    <div class="assistent-flow-left-content">
        <form class="form-project row" id="modalCreateProjectAssistentFormOne" name="modalCreateProjectAssistentFormOne" action="" method="post">
            <div class="col s12 m12 l12 xl12 steps-form">   
                <h4 class="title-m-cp">Excellent!</h4>
                    <p class="font-m-cp">If you need to update your selection at any time, click Edit on the section within your Project Scope to the right. Now, tell me more or provide a summary of your project?</p>

                    <input type="hidden" name="asistantIntendedMediumCounter" id="asistantIntendedMediumCounter" value="">
                    <input type="hidden" name="assistantTitleProjectValue" id="assistantTitleProjectValue" value="">
            </div>
            <div class="col s12 m12 l12 xl12 steps-form">
                <div class="a-lbl">
                    <label class="lbl-m-cp">Relation to the project</label>
                    <span class="icon-wap-i tooltip-wap-2"><i class="material-icons">info</i>
                                                    <span class="tooltiptext-wap-2">Select the relation you have to the project.  If you are the only writer, select “writer” and continue.  If there are additional writers, you can add them in the next section.  If your relation to the project is agent, publisher or rightsholder, select it and you must add at least one writer and email in the next section.</span>
                                                </span>
                </div>
                <div class="select-m-cp">
                    <select class="browser-default"  data-ids="text-relation" id="assistantRelationToTheProject" name="assistantRelationToTheProject"  >
                        <option value="" selected="">Select</option>
                        <option value="45">Writer</option>
                        <option value="2">Agent</option>
                        <option value="137">Publisher</option>
                        <option value="139">Rightsholder</option>
                    </select>
                </div>
                <p class="requiered-f">* Required Fields</p>
            </div>
            <div class="col s12 m12 l12 xl12 steps-form">
                <div class="a-lbl">
                    <label class="lbl-m-cp">Intended Medium</label>
                    <span class="icon-wap-i tooltip-wap-3"><i class="material-icons">info</i>
                                                    <span class="tooltiptext-wap-3">Select the intended medium that best describes your project. NOTE: It may aide in categorizing your project for searches specific to your submission.</span>
                                                </span>
                </div>
                <div class="select-m-cp">
                    <select   class="browser-default "  id="asistantIntendedMedium" name="asistantIntendedMedium"     >
                        <option value="" selected="">Select</option>
                        <option value="9">Gaming-Interactive</option>
                        <option value="10">Literary</option>
                        <option value="11">Movie</option>
                        <option value="12">Web Series</option>
                        <option value="13">Radio-Podcast</option>
                        <option value="15">Stage-Theatre</option>
                        <option value="35">Television</option>
                    </select>
                </div>
                <p class="requiered-f">* Required Fields</p>
                <div id="intendedBubbleError">
                    <div class="intended-medium-project-bubble-error">
                        <div class="intended-medium-project-bubble-error-content hide">
                            You can choose up to 2
                        </div>
                    </div>
                </div>
                <ul class="list-chip-m-cp" id="intendedMediumProjectBubble">

                </ul>

            </div>
            <div class="col s12 m12 l12 xl12 steps-form">
                <div class="a-lbl">
                    <label class="lbl-m-cp">Material Type</label>
                    <span class="icon-wap-i tooltip-wap-4"><i class="material-icons">info</i>
                                                    <span class="tooltiptext-wap-4">Select the material type that best describes your project. NOTE: It may aide in categorizing your project for searches specific to your submission.</span>
                                                </span>
                </div>
                <div class="select-m-cp">
                    <select data-ids="text-material" class="browser-default"  id="asistantIntendedMaterialType" name="asistantIntendedMaterialType"  >
                        <option value="" selected="">Select</option>
                        <option value="1">Book</option>
                        <option value="7">Play</option>
                        <option value="11">Script</option>
                        <option value="13">Story</option>
                        <option value="15">Treatment</option>
                        <option value="44">Manuscript</option>
                    </select>
                </div>
                <p class="requiered-f">* Required Fields</p>
            </div>
            <div class="col s12 m12 l12 xl12 steps-form">
                <div class="a-lbl">
                    <label class="lbl-m-cp">Language of project</label>
                    <span class="icon-wap-i tooltip-wap"><i class="material-icons">info</i>
                                                    <span class="tooltiptext-wap">Select the language in which your project is written. You may complete this form in one language and submit your project in its original version. NOTE: In this case, spend extra time detailing the Synopsis</span>
                                                </span>
                </div>
                <div class="select-m-cp">
                    <select  class="browser-default"  id="asistantLenguage" name="asistantLenguage"  >
                        <option value="" selected="">Select</option>
                        <option value="1">English</option>
                        <option value="2">Spanish</option>
                        <option value="3">French</option>
                        <option value="4">Arabic</option>
                        <option value="5">Bulgarian</option>
                        <option value="6">Chinese</option>
                        <option value="7">Croatian</option>
                        <option value="8">Danish</option>
                        <option value="9">Dutch</option>
                        <option value="10">Farsi</option>
                        <option value="11">Filipino</option>
                        <option value="12">Finnish</option>
                        <option value="13">German</option>
                        <option value="14">Greek</option>
                        <option value="15">Hebrew</option>
                        <option value="16">Hindi</option>
                        <option value="17">Hungarian</option>
                        <option value="18">Icelandic</option>
                        <option value="19">Indonesian</option>
                        <option value="20">Italian</option>
                        <option value="21">Japanese</option>
                        <option value="22">Korean</option>
                        <option value="23">Norwegian</option>
                        <option value="24">Polish</option>
                        <option value="25">Portuguese</option>
                        <option value="26">Punjabi</option>
                        <option value="27">Romanian</option>
                        <option value="28">Russian</option>
                        <option value="29">Swedish</option>
                        <option value="30">Turkish</option>
                        <option value="31">Ukrainian </option>
                    </select>
                </div>
                <p class="requiered-f">* Required Fields</p>
            </div>
            <div class="clearfix"></div>
            <div class="cont-btn">
                <a id="modalCreateProjectAssistentOneSubmit" href="#" goto="two" currentpage="one" class="btn-next-step-left btn-nex-m-cp next-1 links" >Continue</a>
                <a id="modalCreateProjectAssistentOneBack" href="#" class="btn-next-step-left btn-nex-m-cp return-back bk"  goto="two" currentpage="one">Back</a>
            </div>
        </form>
    </div>
</div>