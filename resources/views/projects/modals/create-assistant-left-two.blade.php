<div class="assistent-flow-left assistent-flow-left-two">
    <div class="assistent-flow-left-content">

        <form class="form-project row" id="modalCreateProjectAssistentFormTwo" name="modalCreateProjectAssistentFormTwo" action="" method="post">
            <div class="col s12 m12 l12 xl12 steps-form">
                <h4 class="title-m-cp">Nice! You’re doing great.</h4>
                <p class="font-m-cp">Now tell us more. These sections are very important because they describe your project.</p>
            </div>

            <!-- input type="hidden" name="asistantIntendedMediumCounter" id="asistantIntendedMediumCounter" value="">
            <input type="hidden" name="assistantTitleProjectValue" id="assistantTitleProjectValue" value="" -->
            <div class="col s12 m12 l12 xl12 steps-form">
                <div class="a-lbl">
                    <label class="lbl-m-cp">Logline</label>
                    <span class="icon-wap-i tooltip-wap-2">
                        <i class="material-icons">info</i>
                        <span class="tooltiptext-wap-2">
                            The logline conveys the dramatic essence of your story in the most abbreviated manner possible. It presents the major storyline of the dramatic narrative without character intricacies and sub-plots. It summarizes your story in 1 to 2 sentences, though some can be as long as 3 to 4 sentences. i.e. “A Southern simpleton has a bumbling hand in some of the 20th century’s biggest events in this touching story of love and courage over adversity. Set during the tumultuous Sixties counterculture, Vietnam and Watergate.” – Forrest Gump.
                        </span>
                    </span>
                </div>
                <div class="select-m-cp-n">
                    <textarea rows="8" name="assistantLogline" id="assistantLogline" cols="80" placeholder="Add Logline" maxlength="120"></textarea>
                </div>
                <p class="requiered-f">* Required Fields</p>
            </div>

            <div class="col s12 m12 l12 xl12 steps-form">
                <div class="a-lbl">
                    <label class="lbl-m-cp">Tagline</label>
                    <span class="icon-wap-i tooltip-wap-3">
                        <i class="material-icons">info</i>
                        <span class="tooltiptext-wap-3">
                            A tagline is a catchphrase or slogan. It’s the key phrase that identifies your project by capturing its essence. i.e. “Earth, it was fun while it lasted.” – Armageddon 1998 “A long time ago in a galaxy far, far away.” – Star Wars 1977 “The longer you wait, the harder it gets” – The 40-Year Old Virgin 2005
                        </span>
                    </span>
                </div>
                <div class="select-m-cp-n">
                    <textarea rows="8" name="assistantTagline" id="assistantTagline" cols="80" placeholder="Add Tagline" maxlength="120"></textarea>
                </div>
                <p class="requiered-f">* Required Fields</p>
                <div id="intendedBubbleError">
                    <div class="intended-medium-project-bubble-error">
                        <div class="intended-medium-project-bubble-error-content hide">
                            You can choose up to 2
                        </div>
                    </div>
                </div>

            </div>
            <div class="col s12 m12 l12 xl12 steps-form">
                <div class="a-lbl">
                    <label class="lbl-m-cp">Synopsis</label>
                    <span class="icon-wap-i tooltip-wap-4">
                        <i class="material-icons">info</i>
                        <span class="tooltiptext-wap-4">
                            Select the material type that best describes your project. NOTE: It may aide in categorizing your project for searches specific to your submission.
                        </span>
                    </span>
                </div>
                <div class="select-m-cp-n">
                    <textarea rows="8" name="assistantSynopsis" id="assistantSynopsis" cols="80" placeholder="Add Synopsis" maxlength="120"></textarea>
                </div>
                <p class="requiered-f">* Required Fields</p>
            </div>

            <div class="clearfix"></div>
            <div class="cont-btn">
                <a href="#" goto="three" currentpage="two" class="btn-next-step-left btn-nex-m-cp next-1 links next-views" currentvalidate="true" >Continue</a>
                <a id="modalCreateProjectAssistentTwoBack" href="#" class="btn-back-step btn-nex-m-cp return-back bk"  goto="one" currentpage="two">Back</a>
            </div>
        </form>

    </div>
</div>