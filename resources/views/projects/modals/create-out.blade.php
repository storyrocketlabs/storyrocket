<div id="modalCreateProjectOut" class="modal-create-project modal modal-index" style="z-index: 1003;">
    <div class="modal-content center-align">
        <span class="icon-close modal-close bg-blck"></span>
        <h1 class="title-modal mr-bootom">Welcome!</h1>
        <p class="text-modal-sub title-modal">
            <span>Plans start as low as $8.33</span>
            <span>a month for up to 10 projects.</span>
        </p>
        <p class="txt-modal">For members with a code  its easy as 1-2-3!</p>
        <p class="text-modal-fot txt-modal">
            <span><span class="bg-blck in-line">1-</span> Pick your desired plan</span>
            <span><span class="bg-blck in-line">2-</span> Press “Buy now”</span>
            <span><span class="bg-blck in-line">3-</span> Apply the code upon checkout</span>
        </p>
        <div class=""><a href="{{route('plans')}}" class="btn-modal go-home line-5btn">Take me to plans</a></div>
        <p class="txt-modal text-fonts-ft">The World's Best Platform to Showcase &amp; Discover Content.</p>
    </div>
</div>