<section class="assistent-flow-left assistent-flow-left-location">
   <div class="assistent-flow-left-content">
        <form class="form-projct row" id="modalCreateProjectAssistentFormLocation" name="modalCreateProjectAssistentFormLocation" method="post">
            <div class="assistent-location">
                <div class="assistent-location-content">
                    <div class="col s12 m12 l12 xl12 steps-form">
                        <div class="area-title-m-cp">
                            <h4 class="title-m-cp"></h4>
                            <p class="font-m-cp">Select the Era and Main Location that best describes your project or gives the reader specific information about your story.</p>
                        </div>
                    </div>
                    <form class="form-location row" method="post" id="frm-location">
                        <div class="col s12 m12 l12 xl12 steps-form">
                            <div class="a-lbl">
                                <label class="lbl-m-cp">Era</label>
                                <span class="icon-wap-i tooltip-wap-2"><i class="material-icons">info</i>
                                    <span class="tooltiptext-wap-2">Select the ERA that best describes your project. If your story does not take place in our world, you will be able to choose “Fictitious World” as an options. Once you make a selection, you will be able to define or date the ERA in the box below. i.e. “The Great Depression,” “The Roman Empire,” “The Middle Ages,” “1000 years into the future,” "My story is told in flashbacks to a year ago when the characters were on vacation and the present day"</span>
                                </span>
                            </div>
                        </div>    
                        <div class="col s12 m12 l12 xl12 steps-form">
                            <div class="select-m-cp">
                                <select class="browser-default cbo-add _cc required items-era" data-ids="text-era" id="EraID" name="EraID" data-type="ajax_insrt" data-action="add_projects" data-ids="text-era">
                                    <option value="" selected="">Select</option>
                                    <option value="1">select era</option>
                                </select>
                            </div>
                        </div>
                        <div class="col s12 m12 l12 xl12 steps-form">
                            <div class="select-m-cp-n">
                                <textarea class="text-pas-1 add-element _add-lat" data-id="text-era-desc" data-type="ajax_insrt" data-action="add_projects" id="Example" name="Example" rows="8" cols="30" placeholder="Add description of the Era"></textarea>
                            </div>
                        </div>
                        <div class="col s12 m12 l12 xl12 steps-form">
                            <div class="a-lbl">
                                <label class="lbl-m-cp">Location</label>
                                <span class="icon-wap-i tooltip-wap-2"><i class="material-icons">info</i>
                                    <span class="tooltiptext-wap-2">Select the main location of your project. If there are multiple locations, use the "Describe your location" section to detail secondary locations and settings or to give your reader specific information about your locations.</span>
                                </span>
                            </div>
                            <div class="col s12 m4 l4 xl4 pad-i">
                                <label class="lbl-m-cp-1">Country</label>
                                <div class="select-m-cp alter-position-row">
                                    <select class="browser-default cbo-select _cc required items-contry" data-ids="text-location" data-type="contry" data-id="state" data-status="project" data-ip="">
                                        <option value="" selected="">Select</option>
                                        <option value="0">contry</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col s12 m4 l4 xl4 ">
                                <label class="lbl-m-cp-1">State/Province</label>
                                <div class="select-m-cp alter-position-row">
                                    <select class="browser-default cbo-select _cc " data-ids="text-location" id="state" data-type="state" data-id="city" data-status="project">
                                        <option value="" selected="">Select</option>
                                        <option value="1">state/province</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col s12 m4 l4 xl4 pad-d mar-wap-1">
                                <label class="lbl-m-cp-1">City</label>
                                <div class="">
                                    <input type="text" data-id="text-location" id="City" class="add-element" data-status="project" data-type="ajax_insrt" data-action="add_projects" >
                                </div>
                            </div>
                        </div>
                        <div class="col s12 m12 l12 xl12 steps-form">
                            <textarea class="text-pas-loca add-element _add-lat " data-id="text-location-desc" id="City2" data-type="ajax_insrt" data-action="add_projects" name="message" rows="8" cols="80" required="" placeholder="Add description of the Location"></textarea>
                            <p class="requiered-f">* Required Fields</p>
                        </div>
                        <div class="clearfix"></div>
                        <a  href="#" goto="character" currentpage="location"  class="btn-next-step-left btn-nex-m-cp next-1 links next-views" currentvalidate="true" >Continue</a>
                        <a  href="#" class="btn-next-step-left btn-nex-m-cp return-back bk"  goto="genres" currentpage="location">Back</a>
                    </form>
                </div>
            </div>
        </form>
   </div>
</section>