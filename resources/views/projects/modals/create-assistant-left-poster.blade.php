<div class="assistent-flow-left assistent-flow-left-poster">
    <div class="assistent-flow-left-content">
        <div class="col s12 m12 l12 xl12 ">
            <div class="area-title-m-cp">
                <h4 class="title-m-cp">You must upload a Poster for your project.</h4>
                <p class="font-m-cp">If you have an image, click the "Upload Poster" icon, then select your Poster from your computer. Now drag your Banner to reposition it.</p>
            </div>
            <div class="steps-form-alone">
                <div class="a-lbl">
                    <label class="lbl-m-cp"> Upload Poster</label>
                    <span class="icon-wap-i tooltip-wap-2"><i class="material-icons">info</i>
                        <span class="tooltiptext-wap-2">You must upload an image (poster) for your project. If you have an image, click the “Upload your poster” icon, then select your poster from your computer.</span>
                    </span>
                </div>
            </div>
        </div>
        <form class="form-poster row" id="modalCreateProjectAssistentFormPoster" name="modalCreateProjectAssistentFormPoster" method="post">
            <div class="col s12 m12 l12 xl12 ">
                <div class="assistent-poster">
                    <div class="assistent-poster-content">
                        <form class="form-project row" id="frm-poster" method="">
                            <a class="drag-b u-absolute">
                                <i class="material-icons">menu</i>
                                Drag to Reposition Banner
                            </a>
                            <div class="clearfix"></div>
                            <div class="row">
                                <div class="col s12 m12 l12 xl12 splig poster-up" style="height: 175px;" id="upload-header-layout">
                                    <div class="area-add-banner-m-cp" id="upload-header-layout">
                                        <img src="" id="img-drag" alt="" class="cropit-preview-image u-relative j-image-g _xleft" style="top:0px">
                                        <input type="hidden" id="txtTop" name="" value="">
                                    </div>
                                </div>
                                <input type="hidden" class="required input-poster" value="0">
                                <div class="show-poster-box-m-cp center u-cursor-default" id="upload-header-layout-vertical" >
                                    <div class="overlay-poster-up ">
                                        <button class="poster-cancel">Delete</button>
                                    </div>
                                    <div class=" hide loadings-btn" style="position: absolute;z-index: 3;height: 100%; width: 100%;background: #286EBF; padding: 6rem 4rem;">
                                        <img src="{{asset("images/rolling.gif")}}" alt="" style="vertical-align: middle;">
                                    </div>
                                    <div class="btn-uploadposter  " style="padding: 5rem 0;">
                                        <a href="javascript:void(0);" class="center btn-ups">
                                            <i class="material-icons">cloud_upload</i>
                                            <p class="center">Upload Poster</p>
                                        </a>
                                        <div class="small-line-uploadposter"></div>
                                        <a href="javascript:void(0);" class="pre-made-poster">or Select Pre-made Poster</a>
                                    </div>
                                    <p class="min-size-ps">Min.size: 800 x 1067px</p>
                                </div>
                            </div>
                            <p class="right drag-txt" style="    margin-bottom: 7rem;"></p>
                            <div class="clearfix" style="display: block; margin-bottom: 11rem;"></div>
                            <br>
                            <div class="mar-btn u-relative " style="padding: 0 1rem;">
                                <button class="bottom-primary-save bottom-poster hide btn-salva-poster wap-btn3" style="float: inherit; padding: 6px 17px;">Save Poster & Banner</button>
                            </div>
                            <div class="clearfix"></div>
                            <a  href="#" goto="congratulation" currentpage="poster"  class="btn-next-step-left btn-nex-m-cp next-1 links next-views" currentvalidate="true" >Continue</a>
                            <a  href="#" class="btn-next-step-left btn-nex-m-cp return-back bk"  goto="tag" currentpage="poster">Back</a>
                        </form>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>