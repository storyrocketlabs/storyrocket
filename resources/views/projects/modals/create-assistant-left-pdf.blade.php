<div class="assistent-flow-left assistent-flow-left-pdf">
    <div class="assistent-flow-left-content">
        <form class="form-projct row" id="modalCreateProjectAssistentFormPdf" name="modalCreateProjectAssistentFormPdf" method="post">
            <div class="assistent-location">
                <div class="area-title-m-cp">
                    <div class="_xhide">
                        <h4 class="title-m-cp">Now add a PDF of the project!</h4>
                        <p class="font-m-cp"> The site will only display 5 consecutive pages, beginning with the pages you choose.</p>
                    </div>
                    <p class="font-m-cp text-url-pdf" >Review to make sure these are the pages you want the public to see.</p>
                    <form class="form-pdf row" method="post" id="frm-pdf">
                        <div class="a-lbl center">
                            <label class="lbl-m-cp link-uplad-pdf text-lineal">Click here </label>to upload your PDF
                            <span class="icon-wap-i tooltip-wap-2"><i class="material-icons">info</i>
                                <span class="tooltiptext-wap-2">Upload a PDF file of the project from your computer. After your file loads, click the "VIEW PDF" icon to select the pages you want visible, on the site. The system will only display the 3 consecutive pages beginning with the page you choose. Review the pages to make sure these are the pages you want the public to see.</span>
                            </span>
                        </div>
                        <span class="space-lineal-or">or</span>
                        <input type="file" class="uploaderPDF hide" id="upload-pdf" accept="aapplication/pdf">
                        <div class="select-m-cp-pdf">
                            <div class="drag-pdf-n drag-pdf" id="upload-file">
                                <a href="javascript:void(0);" class="icon-up-pdf-n link-uplad-pdf">
                                <i class="pdf-icon"></i>
                                <p class="wap-icon-font"><strong>Drag and Drop your PDF</strong></p>
                                </a>
                            </div>
                        </div>
                        <div class="content-pdf-n conte-view-pdf ">
                            <div class="progress progress-form-n progress-form">
                                <div class="determinate determinate-r-n determinate-r" ></div>
                            </div>
                            <div class="content-text-pdf-n">
                                <span class="wap-icono-form24">     
                                    <i class="material-icons">picture_as_pdf</i>
                                </span>
                                <span class="span-name-pdf-n">
                                    <strong class="_title">Project - electo.pdf</strong>
                                    <span class="_calc"><?php //echo $this->jsonProjectsAll[0]['sizeFile']10.4 MB; ?></span>
                                </span>
                                <a href="javascript:void(0);" class="right close-pdf-n close-pdf" data-cod="12">
                                    <i class="material-icons">clear</i>
                                </a>
                                <div class="carga-pdf ">
                                <p class="text-porcentaje">0%</p>
                                </div>
                                <div class="carga-pdf-last-n carga-pdf-last >">
                                    <label class="label-pdf-n">Select  first page of PDF</label>
                                    <input type="text" name="" class="page-pdf-n page-pdf" value="1">
                                    <a href="urlpdf" class="right link-pre-n link-pre" target="_blank">Preview full PDF</a>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <a  href="#" goto="tags" currentpage="pdf"  class="btn-next-step-left btn-nex-m-cp next-1 links next-views" currentvalidate="true" >Continue</a>
                        <a  href="#" class="btn-next-step-left btn-nex-m-cp return-back bk"  goto="video" currentpage="pdf">Back</a>
                    </form>
                </div>
            </div>
        </form>
    </div>
</div>