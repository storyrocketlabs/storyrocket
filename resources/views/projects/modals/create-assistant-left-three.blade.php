<div class="assistent-flow-left assistent-flow-left-three">
    <div class="assistent-flow-left-content">

        <form class="form-project row" id="modalCreateProjectAssistentFormThree"
              name="modalCreateProjectAssistentFormThree" action="" method="post">

            <div class="assistent-character-three">
                <div class="assistent-character-three-content">
                    <form class="form-project row" action="" method="" id="wap-form">
                        <div class="col s12 m12 l12 xl12 steps-form">
                            <div class="area-title-m-cp">
                                <h4 class="title-m-cp">Now let's see if you have other relationships with respect to
                                    your project.</h4>
                                <p class="font-m-cp">If you don't know, just leave this blank.</p>
                            </div>
                        </div>
                        <input type="hidden" id="key" data-name="key" value="14-18-16-81">
                        <div class="col s12 m12 l12 xl12 steps-form mar-cap">
                            <div class="a-lbl">
                                <label class="lbl-m-cp text-required">Writer(s)</label>
                                <span class="icon-wap-i tooltip-wap-8"><i class="material-icons">info</i>
                                    <span class="tooltiptext-wap-8">To add a writer, type the name and email address and press “Add a Writer.” Repeat for each additional writer you want listed. The name(s) of each writer will appear below.  If your relation to the project is an agent, publisher or rightsholder you must add at least one writer and email to continue.</span>
                                </span>
                                <input class="required hide _add-text text-writer" value="1">
                                <button type="button" name="button" class="wap-btn3 add-elements" data-id="_add-writer"
                                        id="addWriteThreeButton">Add Writer
                                </button>
                                <ul class="list-chip-m-cp" id="add-writers-list">

                                </ul>
                            </div>
                        </div>
                        <div class="col s12 m12 l12 xl12 steps-form mar-cap">
                            <div class="a-lbl">
                                <label class="lbl-m-cp">Publisher or Writing Association</label>
                                <span class="icon-wap-i tooltip-wap-9"><i class="material-icons">info</i>
                                    <span class="tooltiptext-wap-9">To add a Publishers or Writing associations, type the name and email address and press “Save.”</span>
                                </span>
                                <button type="button" name="button" class="wap-btn3  _01x add-elements"
                                        id="addPublisherThreeButton" data-id="_add-publisher">Add Publisher
                                </button>
                            </div>
                            <ul class="list-chip-m-cp" id="add-publishers-list">

                            </ul>
                        </div>
                        <div class="col s12 m12 l12 xl12 steps-form mar-cap">
                            <div class="a-lbl">
                                <label class="lbl-m-cp">Agent</label>
                                <span class="icon-wap-i tooltip-wap-10"><i class="material-icons">info</i>
                                    <span class="tooltiptext-wap-10">To add an agent, type the name, name of the agency, email address and the agent(s) phone number and press “Save.”</span>
                                </span>
                                <button type="button" name="button" class="wap-btn3  __id-agn add-elements"
                                        data-id="_add-agent" id="addAgentThreeButton">Add Agent
                                </button>
                            </div>
                            <div class="mar-cap2 list-agent">
                            </div>
                        </div>
                        <div class="col s12 m12 l12 xl12 steps-form mar-cap">
                            <div class="a-lbl">
                                <label class="lbl-m-cp">Funding Platform</label>
                                <span class="icon-wap-i tooltip-wap"><i class="material-icons">info</i>
                                    <span class="tooltiptext-wap">If you are crowd funding for your project and have an established funding platform, you can link it here. Add the name of the platform and copy and paste the link. Then press "Save."</span>
                                </span>
                                <button type="button" id="addPlatformThreeButton" name="button"
                                        class="wap-btn3  id-plst add-elements" data-id="_id-plataform">
                                    Add Platform
                                </button>
                            </div>
                            <div class="mar-cap2 list-plataform">
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <a href="#" class="btn-nex-m-cp links" id="modalCreateProjectAssistentThreeSubmit">Continue</a>
                        <a href="#" class="btn-nex-m-cp btn-back-step bk links" goto="two" currentpage="three"
                           id="modalCreateProjectAssistentThreeBack">Back</a>
                    </form>
                </div>


                <!-- hide contents -->
                <div class="assistent-charanter-three-content-form" id="addWriteThree">
                    <div class="assistent-charanter-three-content-form-contet">
                        <div class="hiden-elements">
                            <div class="element-conten">
                                <div class="wap-back items-returns" data-id="_add-writer">
                                    <i class="material-icons">keyboard_backspace</i>
                                </div>
                                <div class="area-title-m-cp">
                                    <h4 class="title-m-cp">Additional Writer/s</h4>
                                    <p class="font-m-cp">If you are the only writer to this project, just leave this
                                        blank.</p>
                                </div>
                                <form class="form-project row" method="post" id="frm-writer3" autocomplete="off">
                                    <div class="col s12 m12 l12 xl12 steps-form">
                                        <div class="a-lbl">
                                            <label class="lbl-m-cp">Name of Writer *</label>
                                        </div>
                                        @include('under-score-templates.item')
                                        <div class="select-m-cp-n3 u-relative">
                                            <input type="text" name="assistentNameWrite3" value=""
                                                   placeholder="Add Name" id="assistentNameWrite3" data-name="coauthor"
                                                   class="text-left writer-input">
                                            <div class="resultado" id="writers-results">
                                            </div>
                                        </div>
                                        <p class="requiered-f">* Required Fields</p>
                                    </div>
                                    <div class="col s12 m12 l12 xl12 steps-form">
                                        <div class="a-lbl">
                                            <label class="lbl-m-cp">Email of Writer *</label>
                                        </div>
                                        <div class="select-m-cp-n3">
                                            <input type="text" value="" placeholder="Add Email"
                                                   class="text-left input-mail" name="assistentEmailWrite3"
                                                   id="assistentEmailWrite3">
                                            <input type="hidden" name="couter" id="couter-writer3" value="0">
                                            <input type="hidden" name="groupWriter3" id="groupWriter3" value="0">
                                        </div>
                                        <p class="requiered-f">* Required Fields</p>
                                        <input type="hidden" id="tokmEmail3" value="0">
                                    </div>
                                    <div class="clearfix"></div>
                                    <button class="btn-nex-m-cp add-elements-writer cursor-pointer"
                                            data-class="list-writes" id="btn-add-writer">Add Writer
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="assistent-charanter-three-content-form" id="addPublisherThree">
                    <div class="assistent-charanter-three-content-form-contet">
                        <div class="hiden-elements" id="_add-publisher">
                            <div class="element-conten">
                                <div class="wap-back items-returns" data-id="_add-publisher">
                                    <i class="material-icons">keyboard_backspace</i>
                                </div>
                                <div class="area-title-m-cp">
                                    <h4 class="title-m-cp">Add Publisher or Writing Association</h4>
                                    <p class="font-m-cp">If you don't know, just leave this blank.</p>
                                </div>
                                <form class="form-project row from-writer-publisher" method="post" id="frm-publishers3"
                                      autocomplete="off">
                                    <div class="col s12 m12 l12 xl12 steps-form">
                                        <div class="a-lbl">
                                            <label class="lbl-m-cp">Name of Publisher or Writing Association</label>
                                        </div>
                                        <div class="select-m-cp-n3 u-relative">
                                            <input data-name="publishers" type="text" name="name_writer" value=""
                                                   id="name_writer" placeholder="Add Name"
                                                   class="text-left writer-inputp">
                                            <div class="resultado" id="publishers-results">
                                            </div>
                                        </div>
                                        <p class="requiered-f">* Required Fields</p>
                                    </div>
                                    <div class="col s12 m12 l12 xl12 steps-form">
                                        <div class="a-lbl">
                                            <label class="lbl-m-cp">Email of Publisher or Writing Association</label>
                                        </div>
                                        <div class="select-m-cp-n3">
                                            <input type="text" name="mail_writer" id="mail_writer"
                                                   class="mailp text-left " value="" placeholder="Add Email">

                                        </div>
                                        <input type="hidden" name="couter" id="couter" value="1">
                                        <input type="hidden" name="groupWrite3" id="groupWrite3" class="groupp"
                                               value="">

                                        <input type="hidden" id="tokmeEmailPublisher3" value="">
                                        <p class="requiered-f">* Required Fields</p>
                                    </div>
                                    <div class="clearfix"></div>
                                    <button class="btn-nex-m-cp add-elements-publihs cursor-pointer"
                                            id="btn-add-publisher">Add Publisher or Writing Association
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="assistent-charanter-three-content-form" id="addAgentThree">
                    <div class="assistent-charanter-three-content-form-contet">
                        <div class="hiden-elements" id="_add-agent">
                            <div class="element-conten">
                                <div class="wap-back2 items-returns" data-id="_add-agent">
                                    <i class="material-icons">keyboard_backspace</i>
                                </div>
                                <div class="area-title-m-cp">
                                    <h4 class="title-m-cp">Add Agent</h4>
                                    <p class="font-m-cp">If you don't know, just leave this blank.</p>
                                </div>
                                <form class="form-project row" method="post" id="agente-proms" autocomplete="off">
                                    <div class="col s12 m12 l12 xl12 steps-form">
                                        <div class="a-lbl">
                                            <label class="lbl-m-cp">Name of Agent *</label>
                                        </div>
                                        <div class="select-m-cp-n3">
                                            <input type="text" name="agent" id="agent" value="" placeholder="Add Name"
                                                   class="input-add text-left">
                                        </div>
                                        <p class="requiered-f">* Required Fields</p>
                                    </div>
                                    <div class="col s12 m12 l12 xl12 steps-form">
                                        <div class="a-lbl">
                                            <label class="lbl-m-cp">Name of Agency *</label>
                                        </div>
                                        <div class="select-m-cp-n3">
                                            <input type="text" name="agency" id="agency" value=""
                                                   placeholder="Add Agency" class="input-add text-left">
                                        </div>
                                        <p class="requiered-f">* Required Fields</p>
                                    </div>
                                    <div class="col s12 m12 l12 xl12 steps-form">
                                        <div class="a-lbl">
                                            <label class="lbl-m-cp">Email Contact *</label>
                                        </div>
                                        <div class="select-m-cp-n3">
                                            <input type="text" name="contactMail" id="contactMail" data-mail="mail"
                                                   value="" placeholder="Add Email" class="input-add text-left">
                                        </div>
                                        <p class="requiered-f">* Required Fields</p>
                                    </div>
                                    <div class="col s12 m12 l12 xl12 steps-form">
                                        <div class="a-lbl">
                                            <label class="lbl-m-cp">Phone Contact *</label>
                                        </div>
                                        <div class="select-m-cp-n3">
                                            <input type="text" name="cantacPhone" id="cantacPhone" value=""
                                                   placeholder="Add Phone Number" class="input-add text-left">
                                        </div>
                                        <p class="requiered-f">* Required Fields</p>
                                    </div>
                                    <div class="col s12 m12 l12 xl12 steps-form">
                                        <p class="marg-input">
                                            <input type="checkbox" class="filled-in blue input-add" id="filled-in-box"
                                                   x="" data-name="check">
                                            <label for="filled-in-box" class="font-check">This information is private on
                                                Storyrocket.com. By checking the box you will make your agent’s
                                                information public to ALL Storyrocket members.</label>
                                        </p>
                                    </div>
                                    <input type="hidden" id="id" value="0" class="input-add" name="id">
                                    <input type="hidden" id="tag" name="tag" class="input-add" value="14-18-16-81">
                                    <div class="clearfix"></div>
                                    <button class="btn-nex-m-cp _element-add">Add Agent</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="assistent-charanter-three-content-form" id="addPlatformThree">
                    <div class="assistent-charanter-three-content-form-contet">
                        <div class="hiden-elements" id="_id-plataform">
                            <div class="element-conten">
                                <div class="wap-back items-returns" data-id="_id-plataform">
                                    <i class="material-icons ">keyboard_backspace</i>
                                </div>
                                <div class="area-title-m-cp">
                                    <h4 class="title-m-cp">Add Funding Platform</h4>
                                    <p class="font-m-cp">If you don't know, just leave this blank.</p>
                                </div>
                                <form class="form-project row" method="post" id="frm-platforms" autocomplete="off">
                                    <div class="col s12 m12 l12 xl12 steps-form">
                                        <div class="a-lbl">
                                            <label class="lbl-m-cp">Platform</label>
                                        </div>
                                        <div class="select-m-cp-n3">
                                            <input type="text" class="text-left" name="" value="" id="platform"
                                                   data-name="platform"
                                                   placeholder="Name of Platform, i.e. Kickstarter or other">
                                        </div>
                                        <p class="requiered-f">* Required Fields</p>
                                    </div>
                                    <div class="col s12 m12 l12 xl12 steps-form">
                                        <div class="a-lbl">
                                            <label class="lbl-m-cp">Link</label>
                                        </div>
                                        <div class="select-m-cp-n3">
                                            <input type="text" id="link-platform" class="text-left" data-name="link"
                                                   name="" value="" placeholder="Copy and paste or type the link">
                                        </div>
                                        <p class="requiered-f">* Required Fields</p>
                                    </div>
                                    <div class="clearfix"></div>
                                    <input type="hidden" data-name="key" value="14-18-16-81">
                                    <input type="hidden" data-name="id" id="id-plaft" value="0">
                                    <button class="btn-nex-m-cp">Add Platform</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- hide contents end-->
            </div>

        </form>

    </div>
</div>