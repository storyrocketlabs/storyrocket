@extends('layouts.app-main')

@section('content')
    <div class="cover-page cover-page-in cover-page-1 s-subscription">
        <div class="cover-page-content">
            <!--<div class="cover-accordion-container">-->
            <div class="">
                <!-- slide left start -->
                    @auth
                        @include('profile.partials.slideleft')
                    @endauth
                <!-- slide left end -->



                <div class="page-content-wrapper" >


                    <article class="container">
                    </article>
                    <section class="area-payment-st">
                        <div class="row">
                            <div class="col s12 m7 l7 area-subcription">
                                <h1 class="title-subcription">Subscription</h1>
                                <!--<div class="row subscription-items">
                                    <div class="col s12 m5 l4 xl4 clr-2plans">Site:</div>
                                    <div class="col s12 m7 l8 xl8">Storyrocket</div>
                                </div>-->
                                <div class="row subscription-items">
                                    <div class="col s12 m5 l4 xl4 clr-2plans">Plan:</div>
                                    <div class="col s12 m7 l8 xl8 txt-uppers">{{$plan->name ?? 'N/A'}}</div>
                                </div>
                                <div class="row subscription-items">
                                    <div class="col s12 m5 l4 xl4 clr-2plans">Billing Cycle:</div>
                                    <div class="col s12 m7 l8 xl8 _txtTransforms">{{$plan->cycle ?? 'N/A'}}</div>
                                </div>
                                <div class="row subscription-items">
                                    <div class="col s12 m5 l4 xl4 clr-2plans">Status:</div>
                                    <div class="col s12 m7 l8 xl8">{{auth()->user()->isActive() ? 'Activo' : 'N/A'}}</div>
                                </div>
                                <div class="row subscription-items">
                                    <div class="col s12 m5 l4 xl4 clr-2plans">Start Date:</div>
                                    <div class="col s12 m7 l8 xl8">2018-10-14 14:56:28</div>
                                </div>
                                <div class="row subscription-items">
                                    <div class="col s12 m5 l4 xl4 clr-2plans">End Date:</div>
                                    <div class="col s12 m7 l8 xl8">2018-12-14 14:56:28</div>
                                </div>
                                <!-- div class="row subscription-items">
                                    <div class="col s12 m5 l4 xl4 clr-2plans">Projects Posted:</div>
                                    <div class="col s12 m7 l8 xl8">1</div>
                                </div -->


                            </div>
                            <div class="col s12 m5 l5 no-padding img-subscription-back">
                                <div class="front-title-subscrit">

                                    <h4>Plus</h4>
                                    <p>License</p>

                                </div>
                                <img src="public/images/bg.png" data-src="{{asset("images/plans-back-2.png")}}" alt=""  class="full-img lazy-image">
                            </div>
                            <div class="clearfix">

                            </div>
                            <div class="receipts">

                                <a href="{{route("plans")}}" class="btn-join-st waves-effect waves-light btn-button-sub">SEE PLANS</a>

                                <div class="red-view">
                                    <div class="flex-container">
                                        <p class="clr-1plans font-3plans" style="font-size:14px !important">Send receipts to:</p>
                                    </div>
                                    <p class="font-1plans crl-3plans font-4plans"><span class="clr-2plans">alberto@storyrocket.com </span>  </p>
                                </div>
                                <!--<a href="#!" class="update-subscription">Update Payment Method </a>
                                <div class="update-subscription">Update Payment Method</div>-->
                            </div>
                        </div>
                    </section>

                </div>

            </div>
        </div>
    </div>
    </div>

    <div class="load-asistent-project"></div>

@endsection

