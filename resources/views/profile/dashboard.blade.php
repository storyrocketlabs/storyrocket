@extends('layouts.app-main')

@section('content')
<div class="cover-page @auth cover-page-in cover-page-1 @endauth cover-page-project s-dashboard">
    <div class="cover-page-content">
        <div class="cover-accordion-container">
            <!-- slide left start -->
            @auth
                @include('profile.partials.slideleft')
            @endauth
            <!-- slide left end -->

            <section class="row">
                <div class="s-dashboard-header">
                    <div class="publici-1">
                        <a href="{{url('/')}}/plans" class="modal-trigger ">
                            <img src="https://res.cloudinary.com/storyrocket-llc/image/fetch/dpr_2,f_auto/https://www.storyrocket.com/public/images/banner-dasboradhs.jpg" data-src="https://www.storyrocket.com/public/images/banner-dasboradhs.jpg" alt="banner" class="responsive-img real-img lazy-image">
                        </a>
                    </div>
                </div>
            </section>

            <section>
                <div class="s-dashboard-body">
                    <div class="s-dashboard-body-content">
                        <div class="text-title-d">
                        <div class="boder-hr"></div>
                        <p class="target-future">LAUNCH PAD</p>
                        <div class="boder-hr  "></div>
                    </div>
                        <div class="new-warp-dash">
                            <div class="new-warp-dash-content">
                                <div style="display: none;" class="icon-dash1 padding-dash3">
                                    <div class="padding-dash3-content">
                                        <div class="area-icon-dash">
                                        <span class="icon30-icon-firts"></span>
                                        <a href="https://www.storyrocket.com/projects?section=my-projects&amp;read-list" class="font-dahs1">My Read Later List</a>
                                        <i class="material-icons">keyboard_arrow_right</i>
                                    </div>
                                        <p class="font-grupo3 quantity-cardmembers _explore-realid">Add a project to your "Read Later List" and discover incredible stories. <a href="https://www.storyrocket.com/#genres">Explore</a></p>

                                        <!-- first card start -->
                                        <div class="marg-prov">
                                            <div class="area-icon-dash">
                                                <span class="icon30-new-project"></span>
                                                <a href="{{route("search")}}" class="font-dahs1">New Projects</a>
                                                <i class="material-icons">keyboard_arrow_right</i>
                                            </div>
                                            <div class="">
                                                <div class="newer-card-project">
                                                    <div class="show-poster-card">
                                                        <a href="https://www.storyrocket.com/galdin--el-resurgir-del-mal" target="_blank">
                                                            <div class="overlay-card-project"></div>
                                                        </a>
                                                        <div class="head-of-project">
                                                <span class="truncate font-autor-card">
                                                Submited by <a href="https://www.storyrocket.com/vera almazan.g1t4c0ae" target="_blank" title="Victor Manuel  Vera Almazan">Victor Manuel  Vera Almazan</a>
                                                </span>
                                                            <div class="circle-on-card">
                                                                <a href="https://www.storyrocket.com/vera almazan.g1t4c0ae" target="_blank"><img src="https://res.cloudinary.com/storyrocket-llc/image/fetch/dpr_2,f_auto/https://storyrocket-aws3.s3.amazonaws.com/full5bb762391c0771538744889sr_jpg" data-src="https://storyrocket-aws3.s3.amazonaws.com/full5bb762391c0771538744889sr_jpg" alt="Victor Manuel  Vera Almazan" class="responsive-img lazy-image"></a>
                                                            </div>
                                                        </div>
                                                        <a href="https://www.storyrocket.com/galdin--el-resurgir-del-mal" target="_blank">
                                                            <div class="new-info-card-project">
                                                                <p class="font-p-title-card truncate">
                                                                    Galdin - El resurgir del mal        </p>
                                                                <p class="gender-on-card">
                                                                    ADVENTURE | ACTION | FANTASY                                    </p>
                                                                <p class="logline-card">
                                                                    After thousands of years of peace and harmony, a war between two races unleashes the desolation in a territory that will remain barren. After so much hatred, death and a residue of arcane power, an evil will arise that will cause the races that live in Rahaylimu to unite again to fight it.

                                                                    Tras miles de años de paz y armonía, una guerra entre dos razas desata la desolación en un territorio que quedará yermo. Tras tanto odio, muertes y residuo del poder arcano, surgirá un mal que hará que las razas que viven en Rahaylimu se unan de nuevo para combatirlo.                                    </p>
                                                            </div>
                                                        </a>
                                                        <img src="https://res.cloudinary.com/storyrocket-llc/image/fetch/dpr_2,f_auto/https://storyrocket-aws3.s3.amazonaws.com/thumb_5bb77617564d0.png" data-src="https://storyrocket-aws3.s3.amazonaws.com/thumb_5bb77617564d0.png" alt="Galdin - El resurgir del mal" class="responsive-img lazy-image">
                                                    </div>
                                                    <div class="down-part-cardproject">
                                                        <div class="icon-on-card">
                                                <span>
                                                    <i class="material-icons">remove_red_eye</i>44                                </span>
                                                            <span>
                                                    <i class="material-icons">favorite</i>2                                </span>
                                                            <div class="add-collections icon-marc icon-marc-newcard" data-users="1370" data-pro="" data-read="remove " data-list="invited">
                                                                <i class="icon-icono-project4 font-18"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="loaders" style="display: none;" id="">load....</div>
                                            </div>
                                        </div>
                                        <!-- first card start -->
                                        <!-- fourth card start -->
                                        <div class="marg-prov">
                                        <div class="area-icon-dash">
                                            <span class="icon30-spotllight"></span>
                                            <a href="{{route("search")}}" class="font-dahs1">Project Spotlight</a>
                                            <i class="material-icons">keyboard_arrow_right</i>
                                        </div>
                                        <div class="">
                                            <div class="newer-card-project">
                                                <div class="show-poster-card">
                                                    <a href="/{{$project ? $project->url : 'owl-manor--the-dawning'}}" target="_blank">
                                                        <div class="overlay-card-project"></div>
                                                    </a>
                                                    <div class="head-of-project">
                                            <span class="truncate font-autor-card">
                                                Submited by <a href="/{{$project ? $project->user->profile->url : 'Zita.immkiay0'}}" target="_blank" title="Zita Harrison">{{$project ? ($project->user->profile->full_name.' '.$project->user->profile->last_name) : 'Zita Harrison'}}</a>
                                            </span>
                                                        <div class="circle-on-card">
                                                            <a href="https://www.storyrocket.com/Zita.immkiay0" target="_blank"><img src="https://res.cloudinary.com/storyrocket-llc/image/fetch/dpr_2,f_auto/https://www.storyrocket.com/public/images/avatar_default_user.png" data-src="https://www.storyrocket.com/public/images/avatar_default_user.png" alt="Zita Harrison" class="responsive-img lazy-image"></a>
                                                        </div>
                                                    </div>
                                                    <a href="/{{$project ? $project->url : 'owl-manor--the-dawning'}}" target="_blank">
                                                        <div class="new-info-card-project">
                                                            <p class="font-p-title-card truncate">
                                                                {{$project ? $project->project_name : 'Owl Manor - the Dawning'}}                                    </p>
                                                            <p class="gender-on-card">
                                                                DRAMA | HORROR | THRILLER                                    </p>
                                                            <p class="logline-card">
                                                                Stifled by the repression of women in the 1800s, trapped in a loveless marriage, Eva lives a life of dissatisfaction and frustration. The tide sweeps her to the Rocky Mountains during the gold rush in 1859, where she finds unexpected hope at Owl Manor, a strange, dark place with owls in the very fabric of its walls.

                                                                But the stakes are perilous. Shadows wander the dim corridors. The owner of the manor is moody, volatile. Does she dare trust him?                                    </p>
                                                        </div>
                                                    </a>
                                                    <img src="{{asset('images/bg.png')}}" data-src="{{$project ? $project->poster : 'https://storyrocket-aws3.s3.amazonaws.com/741021d4.png'}}" alt="Owl Manor - the Dawning" class="responsive-img lazy-image">
                                                </div>
                                                <div class="down-part-cardproject">
                                                    <div class="icon-on-card">
                                            <span>
                                                <i class="material-icons">remove_red_eye</i>27                                </span>
                                                        <span>
                                                <i class="material-icons">favorite</i>0                                </span>
                                                        <div class="add-collections icon-marc icon-marc-newcard" data-users="1422" data-pro="" data-read="remove " data-list="invited">
                                                            <i class="icon-icono-project4 font-18"></i>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="loaders" style="display: none;" id="">load....</div>
                                        </div>
                                    </div>
                                    </div>
                                </div>


                                <div class="dashboard-notification ">
                                    <div class="dashboard-notification-content w-notification">

                                        <div class="dashboard-notification-header">
                                            <div class="dashboard-notification-header-content">
                                                <div class="area-icon-dash"> <span class="icon30-blog"></span> <a href="https://www.storyrocket.com/blog/" class="font-dahs1">Activity</a> <i class="material-icons">keyboard_arrow_right</i> </div>
                                            </div>
                                        </div>


                                        <div class="dashboard-notification-body">
                                            <div class="dashboard-notification-body-content">
                                                <ul class="w-notification-element"> <div class="triangle-up-noti"></div> <li class="rel-tag "> <a href="http://storyrocket.test/Alex.jau0s0jm"> <div class="tag-link-notification"> <div class="notification-col1 images-profile"> <img src="http://graph.facebook.com/10102115934497352/picture?type=large" alt="foto" class="responsive-img img-border1"> </div> <div class="notification-col2 cont-noti-1"> <div class="notification-col2-content"> <div class="font-noti-principal"> <div class="font-noti-principal-row"> <div class="font-noti-principal-row-content"> <div class="font-acti-a2">Jennie Jarvis</div> <span>Likes your project </span> </div> </div> <div class="font-noti-principal-row"> <div class="font-noti-principal-row-content"> <div class="ic-noti-2"> <i class="material-icons">thumb_up</i> </div> <div class="font-noti-principal-time"> 1 minute ago </div> </div> </div> </div> </div> </div> </div> </a> </li> <li class="rel-tag "> <a href="http://storyrocket.test/Alex.jau0s0jm"> <div class="tag-link-notification"> <div class="notification-col1 images-profile"> <img src="http://graph.facebook.com/10102115934497352/picture?type=large" alt="foto" class="responsive-img img-border1"> </div> <div class="notification-col2 cont-noti-1"> <div class="notification-col2-content"> <div class="font-noti-principal"> <div class="font-noti-principal-row"> <div class="font-noti-principal-row-content"> <div class="font-acti-a2">Jennie Jarvis</div> <span>Added your project </span> </div> </div> <div class="font-noti-principal-row"> <div class="font-noti-principal-row-content"> <div class="ic-noti-2"> <i class="material-icons">assistant</i> </div> <div class="font-noti-principal-time"> 2 minute ago </div> </div> </div> </div> </div> </div> </div> </a> </li> <li class="rel-tag "> <a href="http://storyrocket.test/Alex.jau0s0jm"> <div class="tag-link-notification"> <div class="notification-col1 images-profile"> <img src="http://graph.facebook.com/10102115934497352/picture?type=large" alt="foto" class="responsive-img img-border1"> </div> <div class="notification-col2 cont-noti-1"> <div class="notification-col2-content"> <div class="font-noti-principal"> <div class="font-noti-principal-row"> <div class="font-noti-principal-row-content"> <div class="font-acti-a2">Jennie Jarvis</div> <span>Added your profile </span> </div> </div> <div class="font-noti-principal-row"> <div class="font-noti-principal-row-content"> <div class="ic-noti-2"> <i class="material-icons">supervisor_account</i> </div> <div class="font-noti-principal-time"> 2 minute ago </div> </div> </div> </div> </div> </div> </div> </a> </li>  <li class="rel-tag "> <a href="http://dev.storyrocket.com/jonathan.pcpsuqpy"> <div class="tag-link-notification"> <div class="notification-col1 images-profile"> <img src="https://storyrocket-aws3.s3.amazonaws.com/fc0914f6.jpg" alt="foto" class="responsive-img img-border1"> </div> <div class="notification-col2 cont-noti-1"> <div class="notification-col2-content"> <div class="font-noti-principal"> <div class="font-noti-principal-row"> <div class="font-noti-principal-row-content"> <div class="font-acti-a2">jonathan schork</div> <span>viewed your profile</span> </div> </div> <div class="font-noti-principal-row"> <div class="font-noti-principal-row-content"> <div class="ic-noti-2"> <i class="material-icons">visibility</i> </div> <div class="font-noti-principal-time"> 3 weeks ago </div> </div> </div> </div> </div> </div> </div> </a> </li>  <li class="rel-tag "> <a href="http://dev.storyrocket.com/Alex.jau0s0jm"> <div class="tag-link-notification"> <div class="notification-col1 images-profile"> <img src="https://storyrocket-aws3.s3.amazonaws.com/28c0985f.jpg" alt="foto" class="responsive-img img-border1"> </div> <div class="notification-col2 cont-noti-1"> <div class="notification-col2-content"> <div class="font-noti-principal"> <div class="font-noti-principal-row"> <div class="font-noti-principal-row-content"> <div class="font-acti-a2">Alex Davis</div> <span>viewed your profile</span> </div> </div> <div class="font-noti-principal-row"> <div class="font-noti-principal-row-content"> <div class="ic-noti-2"> <i class="material-icons">visibility</i> </div> <div class="font-noti-principal-time"> 3 weeks ago </div> </div> </div> </div> </div> </div> </div> </a> </li>  <li class="rel-tag "> <a href="http://dev.storyrocket.com/Stanley.ekvp1hm9"> <div class="tag-link-notification"> <div class="notification-col1 images-profile"> <img src="http://dev.storyrocket.com/images/avatar_default_user.png" alt="foto" class="responsive-img img-border1"> </div> <div class="notification-col2 cont-noti-1"> <div class="notification-col2-content"> <div class="font-noti-principal"> <div class="font-noti-principal-row"> <div class="font-noti-principal-row-content"> <div class="font-acti-a2">Stanley Brown</div> <span>viewed your profile</span> </div> </div> <div class="font-noti-principal-row"> <div class="font-noti-principal-row-content"> <div class="ic-noti-2"> <i class="material-icons">visibility</i> </div> <div class="font-noti-principal-time"> 3 weeks ago </div> </div> </div> </div> </div> </div> </div> </a> </li>  </ul>
                                            </div>
                                        </div>



                                    </div>
                                </div>

                                <div class="icon-dash1 padding-dash3">
                                    <div class="padding-dash3-content">
                                        <div class="area-icon-dash">
                                            <span class="icon30-blog"></span>
                                            <a href="https://www.storyrocket.com/blog/" class="font-dahs1">Blog</a>
                                            <i class="material-icons">keyboard_arrow_right</i>
                                        </div>
                                        <div class="card-medium-blo">
                                    <a href="https://www.storyrocket.com/blog/writing-technique-the-zoom/" target="_blank">
                                        <div class="content-img-dsh">
                                            <img src="https://res.cloudinary.com/storyrocket-llc/image/fetch/dpr_2,f_auto/https://www.storyrocket.com/blog/wp-content/uploads/2017/07/FORMAT-BLOG-F9B2.jpg" data-src="https://www.storyrocket.com/blog/wp-content/uploads/2017/07/FORMAT-BLOG-F9B2.jpg" alt="" class="responsive-img lazy-image" style="height: 100%">
                                            <div class="overlay-dash">
                                                <div class="content-dash3">

                                                    <p class="title-dash-blog">
                                                        Writing Technique: the “Zoom”                                    </p>
                                                    <p class="title2-dash-blog">
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                    <div class="padding-dsh-img">
                                        <div class="content-dash2">
                                            <p class="dash-tipe">Writing</p>
                                            <p class="truncate font-dash1">by <a href="https://www.storyrocket.com/blog/author/fabia-scali/" class="">Fabia Scali</a></p>
                                        </div>
                                    </div>
                                </div>

                                        <!-- third card start -->

                                        <div class="marg-prov">
                                            <div class="area-icon-dash">
                                                <span class="icon30-trending"></span>
                                                <a href="{{route("search")}}" class="font-dahs1">Trending Projects</a>
                                                <i class="material-icons">keyboard_arrow_right</i>
                                            </div>
                                            <div class="">
                                                <div class="newer-card-project">
                                                    <div class="show-poster-card">
                                                        <a href="https://www.storyrocket.com/atlantes" target="_blank">
                                                            <div class="overlay-card-project"></div>
                                                        </a>
                                                        <div class="head-of-project">
                                                <span class="truncate font-autor-card">
                                                    Submited by <a href="https://www.storyrocket.com/gilabert.og5q5l0v" target="_blank" title="Ivan Gilabert">Ivan Gilabert</a>
                                                </span>
                                                            <div class="circle-on-card">
                                                                <a href="https://www.storyrocket.com/gilabert.og5q5l0v" target="_blank"><img src="https://res.cloudinary.com/storyrocket-llc/image/fetch/dpr_2,f_auto/https://storyrocket-aws3.s3.amazonaws.com/full5bc06477538f51539335287sr_jpg" data-src="https://storyrocket-aws3.s3.amazonaws.com/full5bc06477538f51539335287sr_jpg" alt="Ivan Gilabert" class="responsive-img lazy-image"></a>
                                                            </div>
                                                        </div>
                                                        <a href="https://www.storyrocket.com/atlantes" target="_blank">
                                                            <div class="new-info-card-project">
                                                                <p class="font-p-title-card truncate">
                                                                    ATLANTES        </p>
                                                                <p class="gender-on-card">
                                                                    THRILLER | SCI FI | ADVENTURE                                    </p>
                                                                <p class="logline-card">
                                                                    La civilización Atlante ha permanecido en la sombra desde tiempos remotos dirigiendo y controlando la evolución del ser humano. Tan solo algunos hombres afortunados han tenido la suerte de interactuar con ellos, de aprender de sus increíbles adelantos y conocimientos, pasando así a ser personajes importantes de la historia.

                                                                    A pesar de ser una raza mucho más evolucionada que la humana, una serie de acontecimientos les han hecho salir a la luz y pedir ayuda a un grupo muy especial de científicos humanos para averiguar dónde sucederá lo que ellos llevan miles de años esperando, un suceso que tendrá lugar en menos de cuarenta horas y que será un acontecimiento único que sólo sucede cada 75 000 años.

                                                                    Durante esas cuarenta horas, el ser humano averiguará su verdadero pasado y la batalla épica que desde hace miles de años lleva produciéndose entre el Bien y el Mal.

                                                                    Además, conocerá su falso presente, construido bajo las mentiras de unos pocos que reescribieron la historia para ocultar la verdad en beneficio propio.

                                                                    Y, sobre todo, si el hombre consigue sobrevivir y superar este día, descubrirá su incierto futuro.                                    </p>
                                                            </div>
                                                        </a>
                                                        <img src="https://res.cloudinary.com/storyrocket-llc/image/fetch/dpr_2,f_auto/https://storyrocket-aws3.s3.amazonaws.com/thumb_5bc0874abca8d.png" data-src="https://storyrocket-aws3.s3.amazonaws.com/thumb_5bc0874abca8d.png" alt="ATLANTES" class="responsive-img lazy-image">
                                                    </div>
                                                    <div class="down-part-cardproject">
                                                        <div class="icon-on-card">
                                                <span>
                                                    <i class="material-icons">remove_red_eye</i>28                                </span>
                                                            <span>
                                                    <i class="material-icons">favorite</i>2                                </span>
                                                            <div class="add-collections icon-marc icon-marc-newcard" data-users="1387" data-pro="" data-read="remove " data-list="invited">
                                                                <i class="icon-icono-project4 font-18"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="loaders" style="display: none;" id="">load....</div>
                                            </div>
                                        </div>

                                        <!-- third card end -->

                                        <div class="marg-prov">
                                    <div class="area-icon-dash">
                                        <span class="icon30-members"></span>
                                        <a href="{{route("search")}}" class="font-dahs1">New Member</a>
                                        <i class="material-icons">keyboard_arrow_right</i>
                                    </div>
                                    <div class="card-member-on-dash">
                                        <div class="new-card-members link-div" href="https://www.storyrocket.com/chenoweth.2b1zjm1d">
                                            <div class="top-part-carmember">
                                                <p class="card-ocupation"></p>
                                                <div class="flex"><i class="material-icons">location_on</i>
                                                    <p class="country-card-member">United States</p>
                                                </div>
                                            </div>
                                            <div class="show-member-card">
                                                <img src="https://res.cloudinary.com/storyrocket-llc/image/fetch/dpr_2,f_auto/https://storyrocket-aws3.s3.amazonaws.com/full5bc357d956acd1539528665sr_jpg" data-src="https://storyrocket-aws3.s3.amazonaws.com/full5bc357d956acd1539528665sr_jpg" alt="Alison K Chenoweth" class="full-img-card link-div lazy-image" href="https://www.storyrocket.com/chenoweth.2b1zjm1d">
                                            </div>
                                            <div class="down-part-cardmember center">
                                                <p class="name-on-newcardmember">Alison K Chenoweth</p>
                                                <p class="quantity-cardmembers"><span>0 Following </span> I <span>1 Followers</span></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                    </div>
                                </div>


                                <div class="icon-dash1 padding-dash3" id="colum-ocul">
                                    <div class="padding-dash3-content">

                                        <div class="marg-prov-n" id="version-movil-1">
                                            <div class="area-icon-dash">
                                                <span class="icon30-projects"></span>
                                                <a href="{{route("search")}}" class="font-dahs1">Most Viewed Projects</a>
                                                <i class="material-icons">keyboard_arrow_right</i>
                                            </div>
                                            <div class="">
                                                <div class="newer-card-project">
                                                    <div class="show-poster-card">
                                                        <a href="https://www.storyrocket.com/oh--george" target="_blank">
                                                            <div class="overlay-card-project"></div>
                                                        </a>
                                                        <div class="head-of-project">
                                                <span class="truncate font-autor-card">
                                                    Submited by <a href="https://www.storyrocket.com/Ron.82c040rm" target="_blank" title="Ronald Karasz">Ronald Karasz</a>
                                                </span>
                                                            <div class="circle-on-card">
                                                                <a href="Ronald Karasz" target="_blank"><img src="https://res.cloudinary.com/storyrocket-llc/image/fetch/dpr_2,f_auto/https://storyrocket-aws3.s3.amazonaws.com/35a82d60.jpg" data-src="https://storyrocket-aws3.s3.amazonaws.com/35a82d60.jpg" alt="Ronald Karasz" class="responsive-img lazy-image"></a>
                                                            </div>
                                                        </div>
                                                        <a href="https://www.storyrocket.com/single-and-looking-daisy-secret-lives-of-sisters" target="_blank">
                                                            <div class="new-info-card-project">
                                                                <p class="font-p-title-card truncate">
                                                                    Oh! George        </p>
                                                                <p class="gender-on-card">
                                                                    COMEDY                                    </p>
                                                                <p class="logline-card">
                                                                    “O! George” is uttered from time to time by wife, family and friends, and is the title of the TV show he stars in. George Orbea is the host of a Saturday morning kids show on a U.S. Spanglish network called “Unimundo.” At every turn he’s trying to attain the “American Dream” of stardom and prosperity, without realizing that he’s closer than he knows. Along with his “best” friend, Ron Kay, they always seem to be working on one scheme or another, a business deal, or the next great project that will launch  George into the general market and place Ron among the producing/managing elite like Speilberg and Spelling. Ron used to work at Unimundo’s Sports Department, but as the token “gringo,” he felt it would be better to leave and pursue “business opportunities,” in which he’s always trying to convince George to take part of. George and Ron, who think alike, are very different in public. George becomes the consummate extrovert; bubbly, boisterous and gregarious while Ron is the prankster and reserved one. Both George and Ron are married and their wives are good friends. George and his wife Isa, “Isabel” have been trying to have children of their own, but so far have been unable, and are about to begin extreme measures. Ron and his wife, Ana, are just about to announce that they’re expecting their first born. This will put George’s life into the greatest turmoil he has ever experienced.                                    </p>
                                                            </div>
                                                        </a>
                                                        <img src="https://res.cloudinary.com/storyrocket-llc/image/fetch/dpr_2,f_auto/https://storyrocket-aws3.s3.amazonaws.com/8c51da46.jpg" data-src="https://storyrocket-aws3.s3.amazonaws.com/8c51da46.jpg" alt="Oh! George" class="responsive-img lazy-image">
                                                    </div>
                                                    <div class="down-part-cardproject">
                                                        <div class="icon-on-card">
                                                <span>
                                                    <i class="material-icons">remove_red_eye</i>954                                </span>
                                                            <span>
                                                    <i class="material-icons">favorite</i>2                                </span>
                                                            <div class="add-collections icon-marc icon-marc-newcard" data-users="11" data-pro="" data-read="remove " data-list="invited">
                                                                <i class="icon-icono-project4 font-18"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="loaders" style="display: none;" id="">load....</div>
                                            </div>
                                        </div>

                                        <div class="marg-prov" id="version-movil-2">
                                            <div class="area-icon-dash">
                                                <span class="icon30-members"></span>
                                                <a href="{{route("search")}}" class="font-dahs1">Member Spotlight</a>
                                                <i class="material-icons">keyboard_arrow_right</i>
                                            </div>
                                            <div class="card-member-on-dash">
                                                <div class="new-card-members link-div" href="{{$profile_dashboard ? '/'.$profile_dashboard->url : 'https://www.storyrocket.com/vera almazan.g1t4c0ae'}}">
                                                    <div class="top-part-carmember">
                                                        <p class="card-ocupation"></p>
                                                        <div class="flex"><i class="material-icons">location_on</i>
                                                            <p class="country-card-member">Spain</p>
                                                        </div>
                                                    </div>
                                                    <div class="show-member-card">
                                                        <img src="{{asset('images/bg.png')}}" data-src="{{$profile_dashboard ? ($profile_dashboard->photo) : 'https://storyrocket-aws3.s3.amazonaws.com/full5bb762391c0771538744889sr_jpg'}}" alt="Victor Manuel  Vera Almazan" class="full-img-card link-div lazy-image" href="https://www.storyrocket.com/vera almazan.g1t4c0ae">
                                                    </div>
                                                    <div class="down-part-cardmember center">
                                                        <p class="name-on-newcardmember">{{$profile_dashboard ? ($profile_dashboard->full_name.' '.$profile_dashboard->last_name) : 'Daniel Dawson'}}</p>
                                                        <p class="quantity-cardmembers"><span>0 Following </span> I <span>0 Followers</span></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="marg-prov" id="version-movil-3">
                                            <div class="area-icon-dash">
                                                <span class="icon30-group"></span>
                                                <a href="https://www.storyrocket.com/groups" class="font-dahs1">Group</a>
                                                <i class="material-icons">keyboard_arrow_right</i>
                                            </div>
                                            <div class="card-medium-blo-group link-div" href="https://www.storyrocket.com/groups/new-york-writing-club">
                                                <div class="card-conten-group">
                                                    <img src="https://res.cloudinary.com/storyrocket-llc/image/fetch/dpr_2,f_auto/https://storyrocket-aws3.s3.amazonaws.com/banner-211x1925ad8e67872a441524164216sr_jpg" data-src="https://storyrocket-aws3.s3.amazonaws.com/banner-211x1925ad8e67872a441524164216sr_jpg" alt="" class="responsive-img img-comple lazy-image">
                                                    <div class="overlay-group-dash">
                                                        <div class="group-dash1">
                                                            <a href="https://www.storyrocket.com/groups/new-york-writing-club" class="truncate font-grupo1">New York Writing Club</a>
                                                            <p class="truncate font-grupo2">Create by <a href="https://www.storyrocket.com/Damien.l52sw2c2">Damien Tillman</a></p>
                                                            
                                                        </div>
                                                        <div class="conte-parra-g">
                                                            <p class="font-grupo3">Hello everyone,

                                                                The New York Writing Club was made to create a community of writers from different walks of life who come together in their interest and passion for writing. With this in mind, I've been pushing to build our social media presence in hopes to continue to grow this great group. We are looking for people who have some professional background in the literary world who can offer services to our members, be it editing, proofreading, copyright, web design, graphic arts, book reviews,</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="join-grupo">
                                                    <div class="member-dash-g">
                                                        <img src="https://res.cloudinary.com/storyrocket-llc/image/fetch/dpr_2,f_auto/https://storyrocket-aws3.s3.amazonaws.com/797ae509.jpg" data-src="https://storyrocket-aws3.s3.amazonaws.com/797ae509.jpg" alt="" class="responsive-img lazy-image">
                                                    </div>
                                                    <div class="cont-menber-dash">
                                                        <span>+1</span>
                                                    </div>
                                                    <!--btns-->

                                                    <button type="button" name="button" class="new-buttom-dash-g" onclick="location.href='https://www.storyrocket.com/groups/new-york-writing-club';">Join Group</button>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="icon-dash1 padding-dash3 new-ad-sr" id="avertacid">

                                    <div class="card-meidum-aver">
                                        <a href="https://www.storyrocket.com/bookgosocial">
                                            <img src="https://res.cloudinary.com/storyrocket-llc/image/fetch/dpr_0.8999999761581421,f_auto/https://www.storyrocket.com/public/images/publicidad-aside-1.1.jpg" data-src="https://www.storyrocket.com/public/images/publicidad-aside-1.1.jpg" alt="" class="responsive-img lazy-image">
                                        </a>
                                    </div>
                                    <div class="card-large-pro-publi">
                                        <a href="https://www.storyrocket.com/writers" target="_blank">
                                            <img src="https://res.cloudinary.com/storyrocket-llc/image/fetch/dpr_0.8999999761581421,f_auto/https://www.storyrocket.com/public/images/publicidad-aside-2.jpg" data-src="https://www.storyrocket.com/public/images/publicidad-aside-2.jpg" alt="" class="responsive-img lazy-image">
                                        </a>
                                    </div>
                                    <div class="card-meidum-aver margin-publici">
                                        <a href="https://www.storyrocket.com/producers">
                                            <img src="https://res.cloudinary.com/storyrocket-llc/image/fetch/dpr_0.8999999761581421,f_auto/https://www.storyrocket.com/public/images/publicidad-aside-3.jpg" data-src="https://www.storyrocket.com/public/images/publicidad-aside-3.jpg" alt="" class="responsive-img lazy-image">
                                        </a>
                                    </div>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
    </div>
</div>
@endsection
