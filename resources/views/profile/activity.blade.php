@extends('layouts.app-main')

@section('content')
<div class="cover-page @auth cover-page-in cover-page-1 @endauth cover-page-activity s-activity">
    <div class="cover-page-content">
        <!--<div class="cover-accordion-container">-->
        <div class="">
            <!-- slide left start -->
            @auth
                @include('profile.partials.slideleft')
            @endauth
            <!-- slide left end -->

            <div class="page-content-wrapper page-content-wrapper-plans" style="background-color: #e6ecf0;">
            <section class="cont-general">
                <article class="padding-p-3 text-pc">
                    <h2 class="font-price">Activity</h2>
                    <!-- h3 class="font-body">Choose the plan that best meets your needs.</h3 -->
                </article>

                <article class="container">

                    <!-- div class="row">
                        <div class="card-panel grey lighten-4 collection">This is a card panel with a teal lighten-2 class</div>
                        <div class="card-panel grey lighten-4 collection">This is a card panel with a teal lighten-2 class</div>

                    </div -->

                    <!-- div id="chatapp" class="col s12 active" style="">
                        <div class="collection border-none">
                            <a href="#!" class="collection-item avatar border-none">
                                <img src="../../images/avatar/avatar-1.png" alt="" class="circle cyan">
                                <span class="line-height-0">Elizabeth Elliott </span>
                                <span class="medium-small right blue-grey-text text-lighten-3">5.00 AM</span>
                                <p class="medium-small blue-grey-text text-lighten-3">Thank you </p>
                            </a>
                            <a href="#!" class="collection-item avatar border-none">
                                <img src="../../images/avatar/avatar-2.png" alt="" class="circle deep-orange accent-2">
                                <span class="line-height-0">Mary Adams </span>
                                <span class="medium-small right blue-grey-text text-lighten-3">4.14 AM</span>
                                <p class="medium-small blue-grey-text text-lighten-3">Hello Boo </p>
                            </a>
                        </div>

                        <br><br><br><br><br>
                    </div -->



                    <div  class="col s10 m4 l4 card-panel z-depth-1">
                        @if(!empty($profileActivity))
                            <ul class="collection">
                                @foreach($profileActivity as $profileActivityVal)
                                <li class="collection-item avatar">
                                    <a href="{{$profileActivityVal->url}}" class="collection-item-link">
                                        <img src="{{$profileActivityVal->photo_profile}}" alt="" class="circle">
                                        <span class="collections-title small"><strong>{{$profileActivityVal->full_name}}</strong></span> <span class="collections-content small">viewed your profile</span>
                                        <p class="truncate grey-text">
                                            <i class="material-icons green-text" style="font-size: 21px;display: inline-block;float: left;margin-right: 3px;">visibility</i>
                                            <span class="ultra-small" style="float: left;display: inline-block;">{{$profileActivityVal->time_elapsed}}</span>
                                        </p>
                                    </a>
                                </li>
                                @endforeach
                            </ul>
                        @endif

                        @if(empty($profileActivity))
                                No New Notifications
                        @endif
                    </div>

                </article>
                <br><br><br>
            </section>
        </div>
    </div>
    </div>
</div>
@endsection
