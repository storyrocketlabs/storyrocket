@extends('layouts.app-main')

@section('content')
<div class="cover-page cover-page-in cover-page-1 cover-page-profile-photo s-profile-photo">
    <div class="cover-page-content">
        <!--<div class="cover-accordion-container">-->
        <div class="">
            <!-- slide left start -->
            @auth
                @include('profile.partials.slideleft')
            @endauth
            <!-- slide left end -->

            <div class="container page-content-wrapper page-content-wrapper-profile-photo" style="background-color: #e6ecf0;">


                <section id="profile-1">
                    <article class="row">
                        <article class="container">
                            <div class="paddig-pro2">
                                <a href='{{url('/')}}/{{$profile->url}}' class="right button-back">Back to Profile</a>
                            </div>
                        </article>
                    </article>
                    <article class="row pro-mar2">
                        <article class="container">
                            <div class="col s12 m12 l3 xl3">
                                <div class="tab2">
                                    <button class="tabs-profile ic-9 active2" data-id="photos" >
                                        <span>Photos</span>
                                        <div class="indice"></div>
                                    </button>
                                    <button class="tabs-profile " data-id="headshot" >
                                        <span>Headshots</span>
                                    </button>
                                    <button class="tabs-profile" data-id="artworkd" >
                                        <span>Artworks</span>
                                    </button>
                                </div>
                                <div class="box-fq">
                                    <p class="box-color">Need help? See our <a href="{{route("home.faqs")}}" target="_blank">FAQ →</a></p>
                                </div>
                            </div>
                            <div class="col s12 m12 l9 xl9">
                                <div id="photos" class="tabcontent col s12 m12 l12 xl12 openDefault">
                                    <p class="title-images left-align">Photos</p>
                                    <form class="form-images" enctype="multipart/form-data" id='fileUploadForm' acton='settings/uploadphotos'>
                                        <div class="file-field input-field">
                                            <div class="btn-upload-img">
                                                <span>Add Images</span>
                                                <input type="file" multiple name='file[]' class='upload-images' data-type='photo' id='photo' accept="image/*">
                                            </div>
                                            <div class="file-path-wrapper">
                                                <input class="file-path validate height-camp-file" type="text">
                                            </div>
                                        </div>
                                        <p class="message-photos">JPG, GIF or PNG. Max size of 800K</p>
                                        <!---->
                                        <div class="pross hide">
                                            <div class="progress progress-form">
                                                <div class="determinate determinate-r indeterminate "></div>
                                            </div>
                                            <p class="message-photos text-porcentaje">0%</p>
                                        </div>
                                        <!---->
                                        <div class='cnt-btn hide' id='photo'>
                                            <input type="submit" name="photos" value="Upload Now" class="form-sub-images"  data-id='photo' >
                                            <a class="form-sub-cancel-images" id="cancel-avatar" href="#canel-avatar">Cancel</a>
                                        </div>
                                    </form>
                                    <div class="area-all-images">
                                        <div class="row " id='list-photo'>
                                            <div class="row " id="list-photo">
                                                    <span class="notings-images notings-images-photo">No picture available</span>
                                            </div>
                                            <!-- span class="notings-images notings-images-photo">No picture available</span -->
                                        </div>
                                    </div>
                                </div>
                                <div id="headshot" class="tabcontent col s12 m12 l12 xl12" style="display: none;">
                                    <p class="title-images left-align">Headshots</p>
                                    <form class="form-images" id='fileUploadForm' enctype="multipart/form-data" id='fileUploadForm'>
                                        <div class="file-field input-field">
                                            <div class="btn-upload-img">
                                                <span>Add Images</span>
                                                <input type="file" multiple name='file[]' class='upload-images' data-type='headshots' id='headshots' accept="image/*">
                                            </div>
                                            <div class="file-path-wrapper">
                                                <input class="file-path validate height-camp-file" type="text">
                                            </div>
                                        </div>
                                        <p class="message-photos">JPG, GIF or PNG. Max size of 800K</p>
                                        <!---->
                                        <div class="pross hide">
                                            <div class="progress progress-form">
                                                <div class="determinate determinate-r indeterminate "></div>
                                            </div>
                                            <p class="message-photos text-porcentaje">0%</p>
                                        </div>
                                        <!---->
                                        <div class='cnt-btn hide' id='headshots'>
                                            <input type="submit" name="headshots" value="Upload Now" class="form-sub-images"  data-id='headshots' >
                                            <a class="form-sub-cancel-images" id="cancel-avatar" href="#canel-avatar">Cancel</a>
                                        </div>
                                    </form>
                                    <div class="area-all-images">
                                        <div class="row " id='list-headshots'>

                                            <div class="col s6 m13 l3 xl3 space-img-me lis-images-headshop" id='photo-item-1'>
                                                <div class="show-me-photos">
                                                    <div class="overlay-photo"></div>
                                                    <div class="items-images">
                                                        <!--<i class="material-icons">drag_handle</i>-->
                                                        <a href="#!" class='delete-images' data-cod="1" data-type='headshots'><i class="material-icons">delete</i></a>

                                                    </div>

                                                    <img src="public/images/bg.png" data-src="https://storyrocket-aws3.s3.amazonaws.com/dev/a013989ffb.png" alt="" class="images-me lazy-image">
                                                </div>
                                            </div>

                                            <span class="notings-images notings-images-headshots">No picture available</span>

                                        </div>
                                    </div>
                                </div>
                                <div id="artworkd" class="tabcontent col s12 m12 l12 xl12" style="display: none;">
                                    <p class="title-images left-align">Artworks</p>
                                    <form class="form-images" enctype="multipart/form-data" id='fileUploadForm'>
                                        <div class="file-field input-field">
                                            <div class="btn-upload-img">
                                                <span>Add Images</span>
                                                <input type="file" multiple name='file[]' class='upload-images' data-type='artworks' id='artworks' accept="image/*">
                                            </div>
                                            <div class="file-path-wrapper">
                                                <input class="file-path validate height-camp-file" type="text">
                                            </div>
                                        </div>
                                        <p class="message-photos">JPG, GIF or PNG. Max size of 800K</p>
                                        <div class="pross hide">
                                            <div class="progress progress-form">
                                                <div class="determinate determinate-r indeterminate "></div>
                                            </div>
                                            <p class="message-photos text-porcentaje">0%</p>
                                        </div>
                                        <div class='cnt-btn hide' id='artworks'>
                                            <input type="submit" name="artworks" value="Upload Now" class="form-sub-images"  data-id='artworks' >
                                            <a class="form-sub-cancel-images" id="cancel-avatar" href="#canel-avatar">Cancel</a>
                                        </div>
                                    </form>
                                    <div class="area-all-images">
                                        <div class="row " id='list-artworks'>

                                            <div class="col s6 m13 l3 xl3 space-img-me list-images-artwork" id='photo-item-1'>
                                                <div class="show-me-photos">
                                                    <div class="overlay-photo"></div>
                                                    <div class="items-images">
                                                        <!--<i class="material-icons">drag_handle</i>-->
                                                        <a href="#!" class='delete-images' data-cod="1" data-type='artworks'><i class="material-icons">delete</i></a>
                                                    </div>

                                                    <img src="public/images/bg.png" data-src="https://storyrocket-aws3.s3.amazonaws.com/dev/a013989ffb.png" alt="" class="images-me lazy-image">
                                                </div>
                                            </div>

                                            <!-- span class="notings-images notings-images-artworks">No picture available</span -->

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </article>
                </section>

            </div>
        </div>
    </div>
</div>
@endsection
