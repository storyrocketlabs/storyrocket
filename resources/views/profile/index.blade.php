@extends('layouts.app-main')
@section('content')

    <div class="cover-page cover-page-in cover-page-1 cover-page-profile">
        <div class="cover-page-content">
            <!--<div class="cover-accordion-container">-->
            <div class="cover-accordion-container">
                <!-- slide left start -->
            @auth
                @include('profile.partials.slideleft')
            @endauth
            <!-- slide left end -->

                <div class="container page-content-wrapper page-content-wrapper-profile" style="background-color: #e6ecf0;">
        <div class="row">
            <div class="col s12 m4 l4">
                <div class="row">
                    <div class="col s12 u-relative center">
                        <input type="file" name="avatar" id="profile-avatar" class="upload" accept="image/*">
                        <div class="pro-img">
                            <figure class="pro_bloque">
                                <img src="{{$profile->photo}}" alt="perfil" class="responsive-img image-avatar-user new-user-img-f" id="profile-avatar-src">
                            </figure>
                            <div class="over-upload-photo-f profile-link-upload">
                                <div class="icon-upload-photo" id="uplad-avatar">
                                    <i class="material-icons">camera_alt</i>
                                    <p class="font-upload-photo">Upload photo</p>
                                </div>
                            </div>
                        </div>

                        <p class="u-center-c lineal-or new-or">OR</p>
                        <p class="u-center-c text-here">
                            <a href="javascript:void(0)" class="hrf-upload clic-here profile-link-upload">Click here</a> to upload your Photo</p>
                    </div>
                </div>
                <div class="tab-list-profile center">
                    <ul>
                        <li><button class="tabs-btn tab-active" data-id="form-profiles">Basic Information</button></li>
                        <li><button class="tabs-btn" data-id="form-bios">Bio</button></li>
                        <li><button class="tabs-btn" data-id="form-locations">Location</button></li>
                        <li><button class="tabs-btn" data-id="form-email">Email / Password</button></li>
                        <li><button class="tabs-btn" data-id="form-agente">Agent</button></li>
                        <li><button class="tabs-btn" data-id="form-credits">Credits</button></li>
                        <li><button class="tabs-btn" data-id="form-awards">Awards</button></li>
                        <li><button class="tabs-btn" data-id="form-videos">Videos</button></li>
                    </ul>
                    <div class="box-fq">
                        <p class="box-color">Need help? See our <a href="https://www.storyrocket.com/faqs" target="_blank">FAQ →</a></p>
                    </div>
                </div>
            </div>
            <div class="col s12 m8 l8">
                <div class="col s12 m12 l9 xl9">
                    @include('profile.partials.main')
                    @include('profile.partials.bio')
                    @include('profile.partials.location')
                    @include('profile.partials.email')
                    @include('profile.partials.agent')
                    @include('profile.partials.credits')
                    @include('profile.partials.awards')
                    @include('profile.partials.videos')
                </div>
               <!-- <ul class="collapsible" id="profile-collapsible">
                    <li>
                    </li>
                </ul>-->
            </div>
        </div>
    </div>
    <div class="overlay-main box">
        <div class="overlay-body">
            <div class="overlay-body-cont">
                <div class="body-main">
                    <div class="wapper-mod-img-drag ">
                        <div class="w-cont-img-drag ">
                            <div class="part1-img-drag">
                                <div class="left-aling">
                                    <i class="material-icons">insert_photo</i>
                                    <span class="font-part1-img">Update Profile Picture</span>
                                </div>
                                <div class="part1-img-2">
                                    <i class="material-icons close-r">close</i>
                                </div>
                            </div>
                            <div class="pad-img-drag">
                                <div class="img-drag">
                                    <div class="image-editor">
                                        <div class="cropit-preview">
                                        </div>
                                        <input type="range" class="cropit-image-zoom-input">
                                    </div>
                                </div>
                            </div>
                            <div class="pad-img-drag pop-alin-drag borde-btn-drag">
                                <button class="btn-drag-2 close-r" id="_cl-upload-avatar-profile">Cancel</button>
                                <button class="btn-drag-1" id="upload-avatar-profile">Upload Photo</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
            </div>
        </div>
    </div>

    <div class="over-light box"></div>

    <script>
        window.profile_value = {{$profile->id}} ;
    </script>
@endsection