<script type="text/html" id="profile-loader">
    <div class="triangle-up-noti"></div>
    <div class="preloader-notification-menu">
        <div class="preloader-notification-menu-content">
            <div class="preloader-wrapper x-small active">
                <div class="spinner-layer spinner-green-only">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div><div class="gap-patch">
                        <div class="circle"></div>
                    </div><div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</script>