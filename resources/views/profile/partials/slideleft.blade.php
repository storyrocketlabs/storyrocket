<div class="sidebar-wrapper " style="">
    <div class="sidebar-nav">
        <div class="small-icons box-blank"></div>
        <ul class="collapsible collapsible-accordion collapsible-none" data-collapsible="accordion">

            <li class="few-top-nav">
                <div class="box-empty-2 small-icons"></div>
                <a href="#" class="tooltip-ico">
                </a>
                <a href="{{url('/')}}/{{$profile->url}}" class="see-profile-sidebar">
                    <div class="circle-seeprofiel-sidebar">
                        <img src="{{$profile->photo}}" alt="" class="full-img media-img">
                    </div>
                    <p>My Profile</p>
                </a>
            </li>
            <li class="few-regular-nav">
                <a href="{{route("dashboard.index")}}" class="tooltip-ico">
                    <div class="small-icons side-icons">
                        <span class="icon-home"></span>
                    </div>
                </a>
                <a href="{{route("dashboard.index")}}">Dashboard</a>
            </li>
            <li class="few-regular-nav">
                <a href="{{url('/')}}/{{$profile->url}}" class="tooltip-ico">
                    <div class="small-icons side-icons">
                        <span class="icon-edit-profile"></span>
                    </div>
                </a>
                <a href="{{url('/')}}/{{$profile->url}}" >My Profile</a>

            </li>
            <li class="few-regular-nav">
                <a href="{{route("profile.photo")}}" class="tooltip-ico">
                    <div class="small-icons side-icons">
                        <span class="icon-pictures"></span>
                    </div>
                </a>
                <div class="collapsible-header coll-header-add"><i class="material-icons">arrow_drop_down</i>Pictures</div>
                <div class="collapsible-body collapsible-body-none">
                    <ul>
                        <li class="first">
                            <div class="box-empty small-icons" style=""></div>
                            <a href="{{route("profile.photo")}}">Photos</a></li>
                        <li>
                            <div class="box-empty small-icons" style=""></div>
                            <a href="{{route("profile.headshots")}}">Headshots</a></li>
                        <li>
                            <div class="box-empty small-icons" style=""></div>
                            <a href="{{route("profile.artworks")}}">Artwork</a></li>
                    </ul>
                </div>
            </li>
            <li class="few-regular-nav">
                <a href="{{route("profile.my-projects")}}" class="tooltip-ico">
                    <div class="small-icons side-icons">
                        <span class="icon-projects"></span>
                    </div>
                </a>
                <div class="collapsible-header default coll-header-add"><i class="material-icons">arrow_drop_down</i>Projects</div>
                <div class="collapsible-body collapsible-body-none">
                    <ul>
                        <li class="first">
                            <div class="box-empty small-icons" style=""></div>
                            <a href="{{route("profile.my-projects")}}">Projects</a></li>
                        <li>
                            <div class="box-empty small-icons" style=""></div>
                            <a href="{{route("profile.co-authored")}}">Co-authored</a></li>
                        <li>
                            <div class="box-empty small-icons" style=""></div>
                            <a href="{{route("profile.readlist")}}">Read List</a></li>
                    </ul>
                </div>
            </li>
            <!-- li class="few-regular-nav">
                <a href="https://www.storyrocket.com/mail" class="tooltip-ico">
                    <div class="small-icons side-icons">
                        <span class="icon-mailbox"></span>
                    </div>
                </a>
                <a href="https://www.storyrocket.com/mail">Mailbox</a>
            </li -->

            <!-- li class="few-regular-nav">
                <a href="https://www.storyrocket.com/settings?section=network&amp;followme" class="tooltip-ico">
                    <div class="small-icons side-icons">
                        <span class="icon-contacts"></span>
                    </div>
                </a>
                <div class="collapsible-header default coll-header-add"><i class="material-icons">arrow_drop_down</i>Contacts</div>
                <div class="collapsible-body collapsible-body-none">
                    <ul>
                        <li>
                            <div class="box-empty small-icons" style=""></div>
                            <a href="https://www.storyrocket.com/settings?section=network&amp;followme">Followers</a></li>
                        <li>
                            <div class="box-empty small-icons" style=""></div>
                            <a href="https://www.storyrocket.com/settings?section=network&amp;following">Following</a></li>
                        <li class="first">
                            <div class="box-empty small-icons" style=""></div>
                            <a href="https://www.storyrocket.com/settings?section=network&amp;friends">Friends</a></li>
                    </ul>
                </div>
            </li -->
            <li class="few-regular-nav">
                <a href="https://www.storyrocket.com/blog/" target="_blank" class="tooltip-ico">
                    <div class="small-icons side-icons">
                        <span class="icon-blog"></span>
                    </div>
                </a>
                <a href="https://www.storyrocket.com/blog/" target="_blank">Blog</a>
            </li>
            <li class="few-regular-nav">
                <a href="{{route("group-index")}}" class="tooltip-ico">
                    <div class="small-icons side-icons">
                        <span class="icon-group"></span>
                    </div>
                </a>
                <a href="{{route("group-index")}}">Groups</a>
            </li>
            <!-- li class="few-regular-nav">
                <a href="https://www.storyrocket.com/events" class="tooltip-ico">
                    <div class="small-icons side-icons" style="">
                        <span class="icon-events"></span>
                    </div>
                </a>
                <a href="https://www.storyrocket.com/events">Events</a>
            </li -->
            <li class="few-regular-nav">
                <a href="{{route("search")}}" class="tooltip-ico">
                    <div class="small-icons side-icons">
                        <span class="icon-search"></span>
                    </div>
                </a>
                <a href="{{route("search")}}">Search</a>
            </li>
            <li class="few-regular-nav">
                <a href="{{route("subscription")}}" class="tooltip-ico">
                    <div class="small-icons side-icons">
                        <span class="icon-subcription"></span>
                    </div>
                </a>
                <a href="{{route("subscription")}}">Subscription</a>
            </li>
            <li class="few-regular-nav">
                <a href="{{route("settings.index")}}" class="tooltip-ico">
                    <div class="small-icons side-icons">
                        <span class="icon-settings"></span>
                    </div>
                </a>
                <a href="{{route("settings.index")}}">Settings</a>
            </li>

            <li class="few-regular-nav">
                <a href="{{route("auth.logout")}}" class="tooltip-ico">
                    <div class="small-icons side-icons">
                        <span class="icon-logout"></span>
                    </div>
                </a>
                <a href="{{route("auth.logout")}}">Logout</a>
            </li>
        </ul>
    </div>
</div>