<script type="text/html" id="profile-views-notification-template-empty">
    <ul class="w-notification-element ">
        <div class="triangle-up-noti"></div>
        <li class="rel-tag">
            <div class="tag-link-notification" >
                <div class="notification-col-complete">
                    <div class="notification-col2-content">
                        <div class="font-noti-principal">
                            <div class="font-noti-principal-row">
                                <div class="font-noti-principal-row-content">
                                    <div class="font-acti-a2">No New Notifications</div>
                                </div>
                            </div>
                            <div class="font-noti-principal-row">
                                <div class="font-noti-principal-row-content">
                                    <div class="font-noti-principal-time">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </li>
    </ul>
</script>