<div class="tabcontent-info-profile col s12 m12 l9 xl12 hide" id="form-awards">
<p class="font-title">Awards</p>
    <form method="POST"
          action="{{old('award_id') ? route('profile.update-award', old('award_id')) :route('profile.store-award')}}"
          id="form-save-award" style="display: none">
        @csrf
        <input type="hidden" id="award_id" name="award_id">
        <div class="row">
            <div class="col s12">
                <label class="font-label" for="title_award">Title</label>
                <input class="input-profile1 inpt-alone-mb _01-i" type="text" name="title_award" id="title_award"
                       placeholder="Title" value="{{old('title_award')}}">
                @if ($errors->has('title_award'))
                    <script>
                        window.open_collapsible = 6;
                        window.errors_award = true;
                    </script>
                    <span class="has-error" data-error="wrong"
                          data-success="right">{{ $errors->first('title_award') }}</span>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col s6">
                <label class="font-label" for="year_award">Year</label>
                {!! Form::selectYear('year_award',date('Y'),1900,old('year_award') ,['class'=>'browser-default', 'placeholder'=>'Year', 'id'=>'year_award']) !!}

                @if ($errors->has('year_award'))
                    <script>
                        window.open_collapsible = 6;
                        window.errors_award = true;
                    </script>
                    <span class="has-error" data-error="wrong"
                          data-success="right">{{ $errors->first('year_award') }}</span>
                @endif
            </div>
            <div class="col s6">
                <label class="font-label" for="nominate_award">Nominate Award</label>
                <input class="input-profile1 inpt-alone-mb _01-i" type="text" name="nominate_award" id="nominate_award"
                       placeholder="Nominate Award" value="{{old('nominate_award')}}">
                @if ($errors->has('nominate_award'))
                    <script>
                        window.open_collapsible = 6;
                        window.errors_award = true;
                    </script>
                    <span class="has-error" data-error="wrong"
                          data-success="right">{{ $errors->first('nominate_award') }}</span>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col s6">
                <label class="font-label" for="category_award">Category</label>
                <input class="input-profile1 inpt-alone-mb _01-i" type="text" name="category_award" id="category_award"
                       placeholder="Category Award" value="{{old('category_award')}}">
                @if ($errors->has('category_award'))
                    <script>
                        window.open_collapsible = 6;
                        window.errors_award = true;
                    </script>
                    <span class="has-error" data-error="wrong"
                          data-success="right">{{ $errors->first('category_award') }}</span>
                @endif
            </div>
            <div class="col s6">
                <label class="font-label" for="result_award">Result</label>
                <input class="input-profile1 inpt-alone-mb _01-i" type="text" name="result_award" id="result_award"
                       placeholder="Category Award" value="{{old('result_award')}}">
                @if ($errors->has('result_award'))
                    <script>
                        window.open_collapsible = 6;
                        window.errors_award = true;
                    </script>
                    <span class="has-error" data-error="wrong"
                          data-success="right">{{ $errors->first('result_award') }}</span>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col s12">
                <label class="font-label" for="description_award">Copy and paste or type a brief description of your credit</label>
                <textarea class="input-profile1 inpt-alone-mb _01-i text-awards" name="description_award" id="description_award"
                          placeholder="Category Award" value="{{old('description_award')}}"></textarea>

                @if ($errors->has('description_award'))
                    <script>
                        window.open_collapsible = 6;
                        window.errors_award = true;
                    </script>
                    <span class="has-error" data-error="wrong"
                          data-success="right">{{ $errors->first('description_award') }}</span>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col s12">
                <button type="submit" class="right button-back">
                    SAVE
                </button>
                <a href="#" class="right button-back" id="cancel-add-award">
                    Cancel
                </a>
            </div>
        </div>
    </form>

    <div class="row" id="add-award-container">
        <div class="col s12">
            <a href="#" class="right button-back" id="add-award">
                Add Award
            </a>
        </div>
    </div>

    @if(count($awards) > 0)
        <ul class="collection list-items" id="list-awards-profile">
            @foreach($awards as $award)
                <li class="collection-item">
                    {{$award->title_award}}
                    <a href="#" class="text-red right delete-award" data-id="{{$award->id}}">
                        <i class="material-icons">delete</i>
                    </a>

                    <a href="#" class="right update-award" data-id="{{$award->id}}">
                        <i class="material-icons">edit</i>
                    </a>
                </li>
            @endforeach
        </ul>
    @endif
</div>

<script>
    window.list_awards = @json($awards);
</script>

