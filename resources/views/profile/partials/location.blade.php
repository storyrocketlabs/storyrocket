<div class="tabcontent-info-profile col s12 m12 l9 xl9 hide" id="form-locations">
    <p class="font-title">Location</p>
    <form method="POST" action="{{route('profile.update')}}" id="form-location">
        @csrf
        <div class="row">
            <div class="col s12">
                <label >Country *</label>
                {!! Form::select('country_id', $countries, $profile->country_id, ['placeholder' => 'Search ..', 'class'=>'browser-default cbo-profesion', 'id'=>'country']) !!}
            </div>
            <br>
            <br>
            <br>
            <div class="col s6 space-top">
                <label>State / Province</label>
                {!! Form::select('state', [], $profile->state, ['placeholder' => 'State/Province', 'class'=>'browser-default cbo-profesion', 'id'=>'state_m']) !!}
            </div>
            <div class="col s6 space-top">
                <label>City</label>
                <input class="input-profile1 inpt-alone-mb _01-i" data-name="city" type="text" name="city"
                       placeholder="City" value="{{$profile->city}}" id="city_m">
            </div>
            <p class="text-required text-required-space">* Required Fields</p>
        </div>
        <div class="row">
            <div class="col s12">
                <a href="#" class="right button-back" id="save-location">SAVE</a>
            </div>
        </div>
    </form>
</div>

<script>
    window.state_value = {{$profile->state}};
</script>