<!--<div class="collapsible-header">
    Basic Information
</div>-->
<div class="tabcontent-info-profile col s12 m12 l9 xl12 openDefault" id="form-profiles">
<p class="font-title">Basic Information</p>
    <form method="POST" action="{{route('profile.update')}}" id="form-profile">
        @csrf
        <input type="hidden" name="list-occupations" id="list-occupations">
        <input type="hidden" name="list-languages" id="list-languages">
        <div class="row">
            <div class="col s12 m12 l6 xl6 space-bottom">
                <label for="full_name">First Name</label>
                <input id="full_name" type="text" class="validate" name="full_name" placeholder="Enter your First name"
                       value="{{$profile->full_name}}">
                
            </div>
            <div class="col s12 m12 l6 xl6 space-bottom">
            <label for="last_name">Last Name</label>
                <input id="last_name" type="text" name="last_name" class="validate" placeholder="Enter your last name"
                       value="{{$profile->last_name}}">
                
            </div>
            <div class="col s12 m12 l12 xl12 space-bottom">
                <label for="website">Website</label>
                <input id="website" type="text" name="website" class="validate" placeholder="http://"
                       value="{{$profile->website}}">
                
            </div>

            <div class="col s12 m12 l12 xl12 space-bottom">
                <label for="languages">Language(s) spoken (at least one)*</label>
                {!! Form::select('languages', $languages, null, ['placeholder' => 'Select your Language', 'class'=>'browser-default', 'id'=>'languages']) !!}
                <ul class="list-chip-m-languages">
                    @foreach($profile_languages as $language)
                        <li class="chip tag-active tag-icon"
                            data-id="{{$language->id}}">{{$language->language}}
                            <i class="close material-icons close-icons"
                               data-type="{{$language->language}}">close</i>
                        </li>
                    @endforeach
                </ul>
            </div>

            <div class="col s12 m12 l12 xl12 space-bottom">
                <label for="occupations">What type of work do you do ( max three) *</label>
                {!! Form::select('occupations', $occupations, null, ['placeholder' => 'Select', 'class'=>'browser-default', 'id'=>'occupations']) !!}
                <ul class="list-chip-m-cp" id="prof-ocup">
                    @foreach($profile_occupations as $occupation)
                        <li class="chip tag-active tag-icon"
                            data-id="{{$occupation->id}}">{{$occupation->occupation}}
                            <i class="close material-icons close-icons"
                               data-type="{{$occupation->occupation}}">close</i>
                        </li>
                    @endforeach
                </ul>
            </div>
            <div class="col s12 m12 l12 xl12 space-bottom">
            <label for="company">Your  companyj name</label>
                <input id="company" type="text" name="company" class="validate" placeholder="My company"
                       value="{{$profile->company}}">
                
            </div>
            <div class="col s7">
                <label for="company">Birthday</label>
                <div class="row">
                    <div class="col s4">
                        {!! Form::selectRange('day', 1, 31,$profile->day ,['class'=>'browser-default sl-default', 'placeholder'=>'day']) !!}
                    </div>
                    <div class="col s4">
                        {!! Form::selectMonth('month', $profile->month ,['class'=>'browser-default sl-default', 'placeholder'=>'month']) !!}
                    </div>
                    <div class="col s4">
                        {!! Form::selectYear('year', 1900, date('Y'),$profile->year ,['class'=>'browser-default sl-default', 'placeholder'=>'year']) !!}
                    </div>
                </div>
            </div>
            <div class="col s5">
                <label for="company">Gender</label>
                {!! Form::select('gender', ['Male', 'Female'], $profile->gender, ['placeholder' => 'Gender', 'class'=>'browser-default', 'id'=>'gender']) !!}
            </div>
        </div>
        <div class="row">
            <div class="col s12">
                <a href="#" class="right button-back" id="save-profile">SAVE</a>
            </div>
        </div>
    </form>
</div>