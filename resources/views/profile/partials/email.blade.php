<div class="tabcontent-info-profile col s12 m12 l9 xl12 hide" id="form-email">
    <div class="container">
        <p class="font-for-tittle">Edit Email</p>
        <form method="POST" action="{{route('profile.email')}}">
            @csrf
            <div class="row">
                <div class="col s12">
                    <label>Enter New Email</label>
                    <label class="font-lebel right-align right grey-text">Current email:{{$profile->email}}</label>
                    <input class="input-profile1 inpt-alone-mb _01-i" type="text" name="email"
                           placeholder="Enter New Email" value="">
                    @if ($errors->has('email'))
                        <script>
                            window.open_collapsible = 3;
                        </script>
                        <span class="has-error" data-error="wrong" data-success="right">{{ $errors->first('email') }}</span>
                    @endif
                </div>
                <br>
                <br>
                <br>
                <br>
                <div class="col s12">
                    <label>Confirm New Email</label>
                    <input class="input-profile1 inpt-alone-mb _01-i" type="text" name="confirm_email"
                           placeholder="Confirm Email" value="">
                    @if ($errors->has('confirm_email'))
                        <script>
                            window.open_collapsible = 3;
                        </script>
                        <span class="has-error" data-error="wrong" data-success="right">{{ $errors->first('confirm_email') }}</span>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col s12">
                    <button type="submit" class="right button-back">
                        SAVE
                    </button>
                </div>
            </div>
        </form>
    </div>

    <div class="container">
        <p class="font-for-tittle">Change Password</p>
        <form method="POST" action="{{route('profile.password')}}">
            @csrf
            <div class="row">
                <div class="col s12">
                    <label>Old Password</label>
                    <input class="input-profile1 inpt-alone-mb _01-i" type="password" name="old_password"
                           placeholder="Old Password" >
                    @if ($errors->has('old_password'))
                        <script>
                            window.open_collapsible = 3;
                        </script>
                        <span class="has-error" data-error="wrong" data-success="right">{{ $errors->first('old_password') }}</span>
                    @endif
                </div>
                <br>
                <br>
                <br>
                <br>
                <div class="col s12">
                    <label>Password</label>
                    <input class="input-profile1 inpt-alone-mb _01-i" type="password" name="password"
                           placeholder="Password" >
                    @if ($errors->has('password'))
                        <script>
                            window.open_collapsible = 3;
                        </script>
                        <span class="has-error" data-error="wrong" data-success="right">{{ $errors->first('password') }}</span>
                    @endif
                </div>
                <br>
                <br>
                <br>
                <br>
                <div class="col s12">
                    <label>Confirm new Password</label>
                    <input class="input-profile1 inpt-alone-mb _01-i" type="password" name="password_confirmation"
                           placeholder="Confirm Password" >
                    @if ($errors->has('password_confirmation'))
                        <script>
                            window.open_collapsible = 3;
                        </script>
                        <span class="has-error" data-error="wrong" data-success="right">{{ $errors->first('password_confirmation') }}</span>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col s12">
                    <button type="submit" class="right button-back">
                        SAVE
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>

