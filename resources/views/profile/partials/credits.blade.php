<div class="tabcontent-info-profile col s12 m12 l9 xl12 hide" id="form-credits">
    <p class="font-title">Credits</p>
    <form method="POST" action="{{old('credit_id') ? route('profile.update-credit', old('credit_id')) :route('profile.store-credit')}}" id="form-save-credit" style="display: none">
        @csrf
        <input type="hidden" id="credit_id" name="credit_id">
        <div class="row">
            <div class="col s6">
                <label class="font-label" for="title">Title of show or book</label>
                <input class="input-profile1 inpt-alone-mb _01-i" type="text" name="title" id="title"
                       placeholder="Title" value="{{old('title')}}">
                @if ($errors->has('title'))
                    <script>
                        window.open_collapsible = 5;
                        window.errors_credit = true;
                    </script>
                    <span class="has-error" data-error="wrong"
                          data-success="right">{{ $errors->first('title') }}</span>
                @endif
            </div>
            <div class="col s6">
                <label class="font-label" for="company-credit">Company</label>
                <input class="input-profile1 inpt-alone-mb _01-i" type="text" name="company" id="company-credit"
                       placeholder="Company" value="{{old('company')}}">
                @if ($errors->has('company'))
                    <script>
                        window.open_collapsible = 5;
                        window.errors_credit = true;
                    </script>
                    <span class="has-error" data-error="wrong"
                          data-success="right">{{ $errors->first('company') }}</span>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col s6">
                <label class="font-label" for="position">Position</label>
                {!! Form::select('position', ['Writer', 'Producer', 'Director', 'Company'], old('position'), ['placeholder' => 'Choise Position', 'class'=>'browser-default', 'id'=>'position']) !!}

                @if ($errors->has('position'))
                    <script>
                        window.open_collapsible = 5;
                        window.errors_credit = true;
                    </script>
                    <span class="has-error" data-error="wrong"
                          data-success="right">{{ $errors->first('position') }}</span>
                @endif
            </div>
            <div class="col s6">
                <label class="font-label" for="year">Year</label>
                {!! Form::selectYear('year',date('Y'),1900,old('year') ,['class'=>'browser-default', 'placeholder'=>'Year', 'id'=>'year']) !!}

                @if ($errors->has('year'))
                    <script>
                        window.open_collapsible = 5;
                        window.errors_credit = true;
                    </script>
                    <span class="has-error" data-error="wrong"
                          data-success="right">{{ $errors->first('year') }}</span>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col s12">
                <button type="submit" class="right button-back">
                    SAVE
                </button>
                <a href="#" class="right button-back" id="cancel-add-credit">
                    Cancel
                </a>
            </div>
        </div>
    </form>

    <div class="row" id="add-credit-container">
        <div class="col s12">
            <a href="#" class="right button-back" id="add-credit">
                Add Credit
            </a>
        </div>
    </div>

    @if(count($credits) > 0)
        <ul class="collection list-items" id="list-credits-profile">
            @foreach($credits as $credit)
                <li class="collection-item">
                    {{$credit->title}}
                    <a href="#" class="text-red right delete-credit" data-id="{{$credit->id}}">
                        <i class="material-icons">delete</i>
                    </a>

                    <a href="#" class="right update-credit" data-id="{{$credit->id}}">
                        <i class="material-icons">edit</i>
                    </a>
                </li>
            @endforeach
        </ul>
    @endif
</div>

<script>
    window.list_credits = @json($credits);
</script>

