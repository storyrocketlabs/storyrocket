<script type="text/html" id="profile-views-notification-template">
    <ul class="w-notification-element">
        <div class="triangle-up-noti"></div>

        <li class="rel-tag ">
            <a href="http://storyrocket.test/Alex.jau0s0jm">
                <div class="tag-link-notification">
                    <div class="notification-col1 images-profile">
                        <img src="http://graph.facebook.com/10102115934497352/picture?type=large" alt="foto" class="responsive-img img-border1">
                    </div>
                    <div class="notification-col2 cont-noti-1">
                        <div class="notification-col2-content">
                            <div class="font-noti-principal">
                                <div class="font-noti-principal-row">
                                    <div class="font-noti-principal-row-content">
                                        <div class="font-acti-a2">Jennie Jarvis</div>
                                        <span>Likes your project

                                        </span>
                                    </div>
                                </div>
                                <div class="font-noti-principal-row">
                                    <div class="font-noti-principal-row-content">
                                        <div class="ic-noti-2">
                                            <i class="material-icons">thumb_up</i>
                                        </div>
                                        <div class="font-noti-principal-time"> 1 minute ago </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </li>


        <li class="rel-tag ">
            <a href="http://storyrocket.test/Alex.jau0s0jm">
                <div class="tag-link-notification">
                    <div class="notification-col1 images-profile">
                        <img src="http://graph.facebook.com/10102115934497352/picture?type=large" alt="foto" class="responsive-img img-border1">
                    </div>
                    <div class="notification-col2 cont-noti-1">
                        <div class="notification-col2-content">
                            <div class="font-noti-principal">
                                <div class="font-noti-principal-row">
                                    <div class="font-noti-principal-row-content">
                                        <div class="font-acti-a2">Jennie Jarvis</div>
                                        <span>Added your project

                                        </span>
                                    </div>
                                </div>
                                <div class="font-noti-principal-row">
                                    <div class="font-noti-principal-row-content">
                                        <div class="ic-noti-2">
                                            <i class="material-icons">assistant</i>
                                        </div>
                                        <div class="font-noti-principal-time"> 2 minute ago </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </li>


        <li class="rel-tag ">
            <a href="http://storyrocket.test/Alex.jau0s0jm">
                <div class="tag-link-notification">
                    <div class="notification-col1 images-profile">
                        <img src="http://graph.facebook.com/10102115934497352/picture?type=large" alt="foto" class="responsive-img img-border1">
                    </div>
                    <div class="notification-col2 cont-noti-1">
                        <div class="notification-col2-content">
                            <div class="font-noti-principal">
                                <div class="font-noti-principal-row">
                                    <div class="font-noti-principal-row-content">
                                        <div class="font-acti-a2">Jennie Jarvis</div>
                                        <span>Added your profile

                                        </span>
                                    </div>
                                </div>
                                <div class="font-noti-principal-row">
                                    <div class="font-noti-principal-row-content">
                                        <div class="ic-noti-2">
                                            <i class="material-icons">supervisor_account</i>
                                        </div>
                                        <div class="font-noti-principal-time"> 2 minute ago </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </li>

        <% _.forEach(profile_visitor, function(profile_visitor) { %>
        <li class="rel-tag <%- profile_visitor.mark_css %>"   >
            <a href="<%- profile_visitor.url %>" >
                <div class="tag-link-notification" >

                    <div class="notification-col1 images-profile">
                        <img src="<%- profile_visitor.photo_profile %>" alt="foto" class="responsive-img img-border1"/>
                    </div>
                    <div class="notification-col2 cont-noti-1">
                        <div class="notification-col2-content">
                            <div class="font-noti-principal">
                                <div class="font-noti-principal-row">
                                    <div class="font-noti-principal-row-content">
                                        <div class="font-acti-a2"><%- profile_visitor.full_name %></div>
                                        <span>viewed your profile</span>
                                    </div>
                                </div>
                                <div class="font-noti-principal-row">
                                    <div class="font-noti-principal-row-content">
                                        <div class="ic-noti-2">
                                            <i class="material-icons">visibility</i>
                                        </div>
                                        <div class="font-noti-principal-time">
                                            <%- profile_visitor.time_elapsed %>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </li>

        <% }); %>




    </ul>
    <ul>
        <li class="btn-all-activity">
            <div class="btn-all-activity-content">
                <a href="activity">See all activity</a>
            </div>
        </li>
    </ul>
</script>