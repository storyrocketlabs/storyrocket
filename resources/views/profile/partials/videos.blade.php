<div class="tabcontent-info-profile col s12 m12 l9 xl12 hide" id="form-videos">
<p class="font-title">Videos</p>
    <form method="POST"
          action="{{old('video_id') ? route('profile.update-video', old('video_id')) :route('profile.store-video')}}"
          id="form-save-video" style="display: none">
        @csrf
        <input type="hidden" id="video_id" name="video_id">
        <div class="row">
            <div class="col s12">
                <label class="font-label" for="title-video">Video title</label>
                <input class="input-profile1 inpt-alone-mb _01-i" type="text" name="title" id="title-video"
                       placeholder="Video title" value="{{old('title')}}">
                @if ($errors->has('title'))
                    <script>
                        window.open_collapsible = 7;
                        window.errors_video = true;
                    </script>
                    <span class="has-error" data-error="wrong"
                          data-success="right">{{ $errors->first('title') }}</span>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col s12">
                <label class="font-label" for="link-video">URL</label>
                <input class="input-profile1 inpt-alone-mb _01-i" type="text" name="link" id="link-video"
                       placeholder="Copy and paste your link from Youtube or Vimeo" value="{{old('link')}}">
                @if ($errors->has('link'))
                    <script>
                        window.open_collapsible = 7;
                        window.errors_video = true;
                    </script>
                    <span class="has-error" data-error="wrong"
                          data-success="right">{{ $errors->first('link') }}</span>
                @endif
            </div>
        </div>

        <div class="row">
            <div class="col s12">
                <button type="submit" class="right button-back">
                    SAVE
                </button>
                <a href="#" class="right button-back" id="cancel-add-video">
                    Cancel
                </a>
            </div>
        </div>
    </form>

    <div class="row" id="add-video-container">
        <div class="col s12">
            <a href="#" class="right button-back" id="add-video">
                Add Video
            </a>
        </div>
    </div>

    @if(count($videos) > 0)
        <ul class="collection list-items" id="list-videos-profile">
            @foreach($videos as $video)
                <li class="collection-item">
                    <i class="icon8-video u-icon"></i>
                    <p>{{$video->title}}</p>
                    <a href="#" class="text-red right delete-video" data-id="{{$video->id}}">
                        <i class="material-icons">delete</i>
                    </a>
                    <a href="#" class="right update-video" data-id="{{$video->id}}">
                        <i class="material-icons">edit</i>
                    </a>
                </li>
            @endforeach
        </ul>
    @endif
</div>

<script>
    window.list_videos = @json($videos);
</script>

