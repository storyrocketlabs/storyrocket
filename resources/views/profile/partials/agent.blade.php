<div class="tabcontent-info-profile col s12 m12 l9 xl12 hide" id="form-agente">
<p class="font-title">Agent</p>
    @if($agent->id > 0)
        <form method="POST" action="{{route('profile.store-agent', $agent->id)}}">

            @else
                <form method="POST" action="{{route('profile.store-agent')}}">

                    @endif
                    @csrf
                    <div class="row">
                        <div class="col s12 m12 l12 xl12">
                        <label for="nam_agent">Name of Agent</label>
                            <input id="nam_agent" type="text" name="nam_agent" placeholder="Name of agente" value="{{$agent->nam_agent ? $agent->nam_agent : old('nam_agent')}}">
                            @if ($errors->has('nam_agent'))
                                <script>
                                    window.open_collapsible = 4;
                                </script>
                                <span class="has-error" data-error="wrong"
                                      data-success="right">{{ $errors->first('nam_agent') }}</span>
                            @endif
                        </div>

                        <div class="col s12 m12 l12 xl12">
                        <label for="lit_agent">Name of agency</label>
                            <input id="lit_agent" type="text" placeholder="Name of agency" name="lit_agent" value="{{$agent->lit_agent ? $agent->lit_agent : old('lit_agent') }}">
                            
                            @if ($errors->has('lit_agent'))
                                <script>
                                    window.open_collapsible = 4;
                                </script>
                                <span class="has-error" data-error="wrong"
                                      data-success="right">{{ $errors->first('lit_agent') }}</span>
                            @endif
                        </div>

                        <div class="col s12 m12 l12 xl12">
                            <label for="ema_agent">Email</label>
                            <input id="ema_agent" placeholder="Email" type="text" name="ema_agent" value="{{$agent->ema_agent ? $agent->ema_agent : old('ema_agent')}}">
                            
                            @if ($errors->has('ema_agent'))
                                <script>
                                    window.open_collapsible = 4;
                                </script>
                                <span class="has-error" data-error="wrong"
                                      data-success="right">{{ $errors->first('ema_agent') }}</span>
                            @endif
                        </div>
                        <div class="col s12 m12 l12 xl12">
                            <label for="pho_agent">Phone</label>
                            <input id="pho_agent" placeholder="Phone" type="text" name="pho_agent" value="{{$agent->pho_agent ? $agent->pho_agent : old('pho_agent')}}">
                            
                            @if ($errors->has('pho_agent'))
                                <script>
                                    window.open_collapsible = 4;
                                </script>
                                <span class="has-error" data-error="wrong"
                                      data-success="right">{{ $errors->first('pho_agent') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="row">
                        <div class="col s12">
                            <button type="submit" class="right button-back">
                                SAVE
                            </button>
                        </div>
                    </div>
                </form>
</div>

