@extends('layouts.app-main')

@section('content')


<div class="cover-page @auth cover-page-in cover-page-1 @endauth cover-page-project s-profile">
    <div class="cover-page-content">
        <!--<div class="cover-accordion-container">-->
        <div class="">
            <!-- slide left start -->
            @auth
                @include('profile.partials.slideleft')
            @endauth
            <!-- slide left end -->
            <div class="container">
                <div class="had-container clearfix">
                    <!-- main area -->
                    <div class="main-area" id="profile-1">
                        <section class="row">
                            <!-- columna A-->
                            <article class="col s12 m4 l3 xl3 pro_pad1">
                                <!-- content-info-->
                                <div class="pro-1 center-align">
                                    <div class="pro-img">
                                        <figure class="pro_bloque">
                                            <img src="{{$profileCurrent->photo}}" alt="perfil" class="responsive-img image-avatar-user new-user-img lazy-image">
                                        </figure>
                                        <div class="over-upload-photo ">
                                            <div class="icon-upload-photo" id="uplad-avatar">
                                                <i class="material-icons">camera_alt</i>
                                                <p class="font-upload-photo">Upload photo</p>
                                            </div>
                                            <form id="frm-file">
                                                <input type="file" name="avatar" id="file" class="upload" accept="image/*">
                                            </form>
                                        </div>
                                    </div>
                                    <div>
					                    <span class="pro-font1 pro-color1">{{$profileCurrent->full_name}}</span>
                                        <div>
                                            <span class="pro-font2 pro-color2">Writer, Manager</span>
                                        </div>
                                        <!-- info ubicacion-->
                                        <div class="pro-marg6">
                                            <span class="map-icon-ll"><i class="icon-icono-profile1 font-15"></i></span>
                                            <span class="pro-font3 pro-color4">Miami, Florida, United States</span>
                                        </div>
                                        <!-- fin info ubicacion-->
                                        @if(0)
                                        <div class="pro-marg5 pro-boton">
                                            <!-- boton editar profile-->
                                            <a href="{{route('profile-index')}}" class="  bottom-primary-edit  bottom-g2-edit">
                                                <i class="material-icons vert-bls">edit</i>
                                                <span class="font-edit-p ">Edit Profile</span>
                                            </a>
                                            <!-- fin boton editar profile-->
                                        </div>
                                        @endif

                                        @if(1)

                                        <div class="pro-marg5 pro-boton">
                                            <!-- boton enviar mensaje-->
                                            <a href="#modal-userMessage" id="frind-user-id" class="icon-g  bottom-primary bottom-g2 tooltip3 modal-trigger" data-friends="{{$profileCurrent->user_id}}">
                                                <span class="icon-icono-profile2 font-15"></span>
                                                <span class="font-msn">Message</span>
                                                <!-- span class="tooltiptext3">Send a Message</span -->
</a>
                                            <!-- fin boton enviar mensaje-->
                                            <!-- boton add network-->
                                            <button class="btn-friend botom-net-f-p" data-type="Follow" data-id="{{$profileCurrent->user_id}}" data-rel="add">
                                                <i class="icon-icono-profile3 font-15"></i>
                                                <span>Add</span>
                                            </button>
                                            <!-- fin boton add-->
                                        </div>
                                        @endif


                                        <!-- esto varia si el usuario pone agente o no -->
                                        <ul class="collapsible coll-1" id="profile-informations" data-collapsible="accordion">
                                            <li class="agent-infor">
                                                <!--<div class="collapsible-header coll-2 no-hovers">
                                                    <i class="icon-card icon-icono-profile5 pro-padi font-15 pdg-15 vert-grid"></i>
                                                    <span class="pro-font4 text-pos4 vert-base-middle dis-flex-p bottom-i">Agent Information</span>
                                                    <div class="acor-1 mrg-lef">
                                                        <span class="icon-icono-profile17 font-11"></span>
                                                    </div>
                                                </div>-->
                                                <div class="collapsible-header coll-2">
                                                    <i class="icon-card icon-icono-profile5 pro-padi font-15 pdg-15 vert-grid"></i>
                                                    <span class="pro-font4 text-pos4 vert-base-middle dis-flex-p bottom-i">Agent Information</span>
                                                    <div class="acor-1 mrg-lef">
                                                        <span class="icon-icono-profile17 font-11"></span>
                                                    </div>
								                </div>
								                <div class="collapsible-body collapsible-w" style="display: none;">
                                                    <p class="font-agent"><strong class="color-agent3">Agent: </strong>agencia devos</p>
                                                    <p class="font-agent"><strong class="color-agent3">Name of Agency: </strong>servideks</p>
                                                    <p class="font-agent"><strong class="color-agent3">Email: </strong>felix_1108@hotmail.com</p>
                                                    <p class="font-agent"><strong class="color-agent3">Contact: </strong>956201245</p>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="collapsible-header dis-flex-i coll-3">
                                                    <i class="material-icons vert-botn dis-flex-p">add</i>
                                                    <span class="pro-font4 text-pos4 vert-base-middle dis-flex-p bottom-i">More Information</span>
                                                    <div class="acor-1 dis-flex-p" style="display: inline-block !important;">
                                                        <span class="icon-icono-profile17 font-11"></span>
                                                    </div>
                                                </div>
                                                <div class="collapsible-body collapsible-w">
                                                    <!-- redes sociales-->
                                                    <div class="center-align margin-more">
                                                        <ul>

                                                        </ul>
                                                    </div>
                                                    <!-- fin redes sociales-->
                                                    <!-- follow y following-->
                                                    <div class="col s12 m12 fomateo-foll" id="conte-foll">
                                                        <div class="col s6 m6 center-align foll-border">
                                                            <p class="font-foll1">Following</p>
                                                            <p class="font-foll2">0</p>
                                                        </div>
                                                        <div class="col s6 m6 center-align">
                                                            <p class="font-foll1">Followers</p>
                                                            <p class="font-foll2">0</p>
                                                        </div>
                                                    </div>
                                                    <!-- fin redes sociales-->
                                                    <div class="left-align">
                                                        <span class="icon-icono-profile15 ic-nolik pro-padi"></span>
                                                        <span class="pro-font4 pro-color4 inline-idioma">No language</span>
                                                    </div>
                                                    <div class="pro-marg6 cont-link-url">
                                                        <span class="icon-icono-profile16 ic-nolik pro-padi"></span>
                                                        <span class="pro-font4 pro-color4 link-pos truncate">Url</span>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                        <div class="">
                                            <span class="pro-font4 pro-color4">Member since Aug 08, 2018</span>
                                        </div>
                                    </div>
                                </div>
                                <!-- fin content-info-->
                                <!-- content read later list-->
                                <div class="pro-marg4" id="read-list-aside">
                                    <div class="border-pro3 pro_marg1 u-flex1 u-flexTop u-justifyContentSpaceBetween">
                                        <span class="pro-font9 pro-color1 u-boder">Read Later List (<span class="pro-font5 pro-color1 list-count-read">3</span>)</span>
                                    </div>
                                    <div id="list-read-collection">
                                        <!--- card read list-->
                                        <div class="pro-marg5" id="1318">
                                            <div class="card-read-new">

                                                <div class="show-img-on-newreadlist">
                                                    <a href="https://www.storyrocket.com/an-unlikely-beginning" title="An Unlikely Beginning">
                                                        <img src="{{empty($profile->photo) ? (empty($profile->facebook_photo) ? asset('images/avatar_default_user.png')  : $profile->facebook_photo ) : $profile->photo}}" alt="An Unlikely Beginning" class="img-on-newreadlist lazy-image">
                                                    </a>
                                                </div>

                                                <div class="info-box-newreadlist">

                                                    <a href="https://www.storyrocket.com/an-unlikely-beginning" title="An Unlikely Beginning">
                                                        <span class="font-card2 truncate">An Unlikely Beginning</span>
                                                    </a>
                                                    <span class="read-font4 pro-color7 truncate read-bloque ">by</span>
                                                    <span class="couponcode tooltip-load truncate _xltext" id="tooltip-load" data-key="1216" data-type="users">
														<a href="https://www.storyrocket.com/patty_wiseman1966@yahoo.com.eo849un8" class="read-bloque read-font4 pro-color5 ">
															Patty Wiseman														</a>
                                                        <!-- tool-tip -->
														<div class="tol-dinac"></div>
                                                        <!--  fin tooltip-->
													</span>
                                                    <div class="pro-font8 pro-color7">
                                                        <a href="https://www.storyrocket.com/searchs/genre?q=search&amp;history&amp;espv=1" class="pro-font8 pro-color7 cm-tag" title="History">History</a>
                                                        <a href="https://www.storyrocket.com/searchs/genre?q=search&amp;romance&amp;espv=1" class="pro-font8 pro-color7 cm-tag" title="Romance">Romance</a>
                                                        <a href="https://www.storyrocket.com/searchs/genre?q=search&amp;mystery&amp;espv=1" class="pro-font8 pro-color7 cm-tag" title="Mystery">Mystery</a>
                                                    </div>
                                                    <span class="btnico right marg-read tooltip" data-users="1256" data-pro="1318" data-read="remove" data-list="users">
																															<i class="icon-icono-project5 font-18"></i>

																													</span>

                                                </div>

                                            </div>
                                        </div>
                                        <div class="pro-marg5" id="1260">
                                            <div class="card-read-new">

                                                <div class="show-img-on-newreadlist">
                                                    <a href="https://www.storyrocket.com/winters-verdict-book-2-grace-restored-series" title="WINTER'S VERDICT (Book 2, Grace Restored Series)">
                                                        <img src="{{empty($profile->photo) ? (empty($profile->facebook_photo) ? asset('images/avatar_default_user.png')  : $profile->facebook_photo ) : $profile->photo}}" alt="WINTER'S VERDICT (Book 2, Grace Restored Series)" class="img-on-newreadlist lazy-image">
                                                    </a>
                                                </div>

                                                <div class="info-box-newreadlist">

                                                    <a href="https://www.storyrocket.com/winters-verdict-book-2-grace-restored-series" title="WINTER'S VERDICT (Book 2, Grace Restored Series)">
                                                        <span class="font-card2 truncate">WINTER'S VERDICT (Book 2, Grace Restored Series)</span>
                                                    </a>
                                                    <span class="read-font4 pro-color7 truncate read-bloque ">by</span>
                                                    <span class="couponcode tooltip-load truncate _xltext" id="tooltip-load" data-key="1210" data-type="users">
														<a href="https://www.storyrocket.com/C.J..1aaa86s6" class="read-bloque read-font4 pro-color5 ">
															C.J. Peterson														</a>
                                                        <!-- tool-tip -->
														<div class="tol-dinac"></div>
                                                        <!--  fin tooltip-->
													</span>
                                                    <div class="pro-font8 pro-color7">
                                                        <a href="https://www.storyrocket.com/searchs/genre?q=search&amp;action&amp;espv=1" class="pro-font8 pro-color7 cm-tag" title="Action">Action</a>
                                                        <a href="https://www.storyrocket.com/searchs/genre?q=search&amp;faith&amp;espv=1" class="pro-font8 pro-color7 cm-tag" title="Faith">Faith</a>
                                                        <a href="https://www.storyrocket.com/searchs/genre?q=search&amp;young adult&amp;espv=1" class="pro-font8 pro-color7 cm-tag" title="Young Adult">Young Adult</a>
                                                    </div>
                                                    <span class="btnico right marg-read tooltip" data-users="1256" data-pro="1260" data-read="remove" data-list="users">
																															<i class="icon-icono-project5 font-18"></i>

																													</span>

                                                </div>

                                            </div>
                                        </div>
                                        <div class="pro-marg5" id="1260">
                                            <div class="card-read-new">

                                                <div class="show-img-on-newreadlist">
                                                    <a href="https://www.storyrocket.com/winters-verdict-book-2-grace-restored-series" title="WINTER'S VERDICT (Book 2, Grace Restored Series)">
                                                        <img src="https://res.cloudinary.com/storyrocket-llc/image/fetch/dpr_2,f_auto/https://storyrocket-aws3.s3.amazonaws.com/faf00152.png" data-src="https://storyrocket-aws3.s3.amazonaws.com/faf00152.png" alt="WINTER'S VERDICT (Book 2, Grace Restored Series)" class="img-on-newreadlist lazy-image">
                                                    </a>
                                                </div>

                                                <div class="info-box-newreadlist">

                                                    <a href="https://www.storyrocket.com/winters-verdict-book-2-grace-restored-series" title="WINTER'S VERDICT (Book 2, Grace Restored Series)">
                                                        <span class="font-card2 truncate">WINTER'S VERDICT (Book 2, Grace Restored Series)</span>
                                                    </a>
                                                    <span class="read-font4 pro-color7 truncate read-bloque ">by</span>
                                                    <span class="couponcode tooltip-load truncate _xltext" id="tooltip-load" data-key="1210" data-type="users">
														<a href="https://www.storyrocket.com/C.J..1aaa86s6" class="read-bloque read-font4 pro-color5 ">
															C.J. Peterson														</a>
                                                        <!-- tool-tip -->
														<div class="tol-dinac"></div>
                                                        <!--  fin tooltip-->
													</span>
                                                    <div class="pro-font8 pro-color7">
                                                        <a href="https://www.storyrocket.com/searchs/genre?q=search&amp;action&amp;espv=1" class="pro-font8 pro-color7 cm-tag" title="Action">Action</a>
                                                        <a href="https://www.storyrocket.com/searchs/genre?q=search&amp;faith&amp;espv=1" class="pro-font8 pro-color7 cm-tag" title="Faith">Faith</a>
                                                        <a href="https://www.storyrocket.com/searchs/genre?q=search&amp;young adult&amp;espv=1" class="pro-font8 pro-color7 cm-tag" title="Young Adult">Young Adult</a>
                                                    </div>
                                                    <span class="btnico right marg-read tooltip" data-users="1256" data-pro="1260" data-read="remove" data-list="users">
																															<i class="icon-icono-project5 font-18"></i>

																													</span>

                                                </div>

                                            </div>
                                        </div>
                                        <!--- fin card read list-->
                                    </div>
                                </div>
                                <!--- fin read list-->
                            </article>
                            <!-- fin columna A-->
                            <!-- columna B -->
                            <article class="col s12 m8 l9 xl9" id="main-boxe">
                                <article class="col s12 m12 l12 xl12" id="profile-2">
                                    <div class="main-tabs">
                                        <ul id="tabs-swipe-demo" class="tabs border-pro3 pro_marg1">
                                            <li class="tab col s2 marg-tabs"><a class="active" href="#test-swipe-1">Biography</a></li>
                                            <li class="tab col s2 marg-tabs"><a href="#test-swipe-2">Credits</a></li>
                                            <li class="tab col s2 marg-tabs"><a href="#test-swipe-3" class="">Awards</a></li>
                                            <li class="tab col s2 marg-tabs"><a href="#test-swipe-4" class="">Videos</a></li>
                                            <li class="tab col s2 marg-tabs"><a href="#test-swipe-5" class="">Images</a></li>
                                            <li class="indicator" style="right: 673px; left: 0px;"></li></ul>
                                        <div id="test-swipe-1" class="col s12 active" style="padding: 0px !important; display: block;">
                                            <div class="content-bio pro_marg1">
                                                <div class="box-no-profile">
                                                    <div class="collapsible-header-u credit-padding-no car-credit-no curzor-none">
                                                        <i class="material-icons">subject</i>
                                                        <p class="font-add-credit">
                                                            informacion de la web
                                                            {{$profileCurrent->full_name}}
                                                        </p>
                                                        <a class="btn-go-credit" href="https://www.storyrocket.com/settings?section=account&amp;about=about">Add Bio</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="test-swipe-2" class="col s12" style="display: none;">
                                            <ul class="box-no-profile" data-collapsible="accordion">
                                                <li>
                                                    <div class="collapsible-header-u credit-padding-no car-credit-no curzor-none">
                                                        <i class="material-icons">view_list</i>
                                                        <p class="font-add-credit">Let the Storyrocket Community know more about you.</p>
                                                        <a class="btn-go-credit" href="https://www.storyrocket.com/settings?section=account&amp;credits=credits">Add credit</a>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                        <div id="test-swipe-3" class="col s12" style="display: none;">
                                            <ul class="box-no-profile" data-collapsible="accordion">
                                                <li>
                                                    <div class="collapsible-header aw-padding-no car-aw-no">
                                                        <div class="add-credit inline">
                                                            <span class="icon-icono-profile29 verti1"></span>
                                                        </div>
                                                        <p class="font-add-credit inline">You’re a winner? Tell us more!</p>

                                                            <a class="btn-go-credit" href="https://www.storyrocket.com/settings?section=account&amp;awards=awards">Add Award</a>

                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                        <div id="test-swipe-4" class="col s12 pro-padg-3" style="display: none;">
                                            <div class="col s12 m12 l12 xl12  pro-padg-3" id="conte-videos-profiles">
                                                @foreach($profilePhoto as $video)
                                                    <div class="col s6 m6 l4 xl4 view-thmbil-video" data-link="{{$video->link}}">
                                                        <div class="card-video mediun thumbil-image-video" >
                                                            <div class="video-image">
                                                                <div class="view-thumbli-images"></div>
                                                                <a  class="btn-v link-vidio-drop " data-type="" data-self="12-22-17">
                                                                    <span class="icon-icono-profile26 font-20 items-vidio"></span>
                                                                </a>
                                                            </div>
                                                            <div class="conten-video">
                                                                <a href="" class="truncate font-video">dsadsad</a>
                                                                <p class="font-video2"><span class="cnt-video">0</span> Views</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                        <div id="test-swipe-5" class="col s12 pro-padg-3" style="display: none;">
                                            <div class="col s12 m12 l12 xl12 pro-padg-3 margin-fotos" id="photos">
                                                <div id="cards-photo">
                                                    <div class="col s6 m6 l3 xl3 padding-last image-thumbil-view">
                                                        <div class="card-foto mediun2">
                                                            <div class="foto-image">
                                                                <a href="https://storyrocket-aws3.s3.amazonaws.com/full5c0055f9e8cc11543525881sr_.jpg" class="photo link-photo-drop" data-name="photos" data-type="1" data-source="1313">
                                                                    <img src="https://storyrocket-aws3.s3.amazonaws.com/thum-250x2505c0055f9e8cc11543525881sr_.jpg" alt="foto">
                                                                </a>
                                                                <div class="overlay-fotos open-overlay"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col s6 m6 l3 xl3 padding-last image-thumbil-view">
                                                        <div class="card-foto mediun2">
                                                            <div class="foto-image">
                                                                <a href="https://storyrocket-aws3.s3.amazonaws.com/full5c005773792ce1543526259sr_.png" class="photo link-photo-drop" data-name="photos" data-type="1" data-source="1313">
                                                                    <img src="https://storyrocket-aws3.s3.amazonaws.com/thum-250x2505c005773792ce1543526259sr_.png" alt="foto">
                                                                </a>
                                                                <div class="overlay-fotos open-overlay"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <!--<div class="collapsible box-no-profile" data-collapsible="accordion">
                                                    <div class="collapsible-header-u credit-padding-no car-credit-no curzor-none">
                                                        <i class="material-icons">camera_alt</i>
                                                        <p class="font-add-credit">Upload your images and let everyone know more about you.</p>
                                                        <a class="btn-go-credit" href="https://www.storyrocket.com/settings?section=images&amp;photo">Add image</a>
                                                    </div>
                                                </div>-->
                                            </div>
                                        </div>
                                    </div>
                                </article>
                                <article class="col s12 m12 l12 xl12" id="profile-3">
                                    <div class="main-tabs">
                                        <a href="https://www.storyrocket.com/Ivan.0a1qftxt/project?projects" class="pro-font6 link-position hrf-view-project hide">View all</a>
                                        <ul id="tabs-swipe-demo" class="tabs border-pro3 pro_marg1">
                                            <li class="tab col s2 marg-tabs">
                                                <a data-list="projects" class="active tag-project" data-num="" href="#test-swipe-6">Projects (0)</a>
                                            </li>
                                            <li class="indicator" style="right: 673px; left: 0px;"></li></ul>
                                        <div id="test-swipe-6" class="col s12 active" style="padding: 0px !important;">
                                            <div class="pro_marg1 limit-area-result">

                                            </div>
                                        </div>
                                    </div>
                                    <!--<a href="" class="pro-font6 link-position-res">View all</a>-->
                                    <div class="newer-card-project">
  <div class="show-poster-card">
    <a href="https://www.storyrocket.com/seasons-of-change-book-1-grace-restored-series" target="_blank">
      <div class="overlay-card-project"></div>
    </a>
    <div class="head-of-project">
      <span class="truncate font-autor-card">
                        Submited by <a href="https://www.storyrocket.com/C.J..1aaa86s6" target="_blank" title="C.J. Peterson">C.J. Peterson</a>
                    </span>
      <div class="circle-on-card">
        <a href="https://www.storyrocket.com/C.J..1aaa86s6" target="_blank"><img src="https://res.cloudinary.com/storyrocket-llc/image/fetch/dpr_1,f_auto/https://storyrocket-aws3.s3.amazonaws.com/3df100d4.jpg" data-src="https://storyrocket-aws3.s3.amazonaws.com/3df100d4.jpg" alt="C.J. Peterson" class="responsive-img lazy-image"></a>
      </div>
    </div>
    <a href="https://www.storyrocket.com/seasons-of-change-book-1-grace-restored-series" target="_blank">
      <div class="new-info-card-project">
        <p class="font-p-title-card truncate">
          SEASONS OF CHANGE (Book 1, Grace Restored Series)        </p>
        <p class="gender-on-card">
                    <span>Faith</span>
                    <span>Action</span>
                    <span>Young Adult</span>
                  </p>
        <p class="logline-card">
          Katie MacKenna experienced one storm after another in her life. When Leukemia stole                                                                                                                                                                                            ...        </p>
      </div>
    </a>
    <img src="https://res.cloudinary.com/storyrocket-llc/image/fetch/dpr_1,f_auto/https://storyrocket-aws3.s3.amazonaws.com/9741814b.png" data-src="https://storyrocket-aws3.s3.amazonaws.com/9741814b.png" alt="SEASONS OF CHANGE (Book 1, Grace Restored Series)" class="responsive-img lazy-image">
  </div>
  <div class="down-part-cardproject">
    <div class="icon-on-card">
      <span>
                        <i class="material-icons">remove_red_eye</i>965                    </span>
      <span>
                        <i class="material-icons">favorite</i>4                    </span>
            <div class="
                            add-collections                             icon-marc icon-marc-newcard" data-users="1313" data-pro="" data-read="remove " data-list="invited">
                <i class="icon-icono-project4 font-18"></i>
        <!--<span class="tooltiptext">Add to List</span>-->
              </div>
      <!--<div class="icon-marc">
                        <i class="material-icons style-ico-turned">turned_in_not</i>
                    </div>-->
    </div>
  </div>
</div>
                                </article>
                                <article class="col s12 m12 l12 xl12" id="profile-2">
                                    <div class="main-tabs">
                                        <a href="https://www.storyrocket.com/Ivan.0a1qftxt/social?groups" class="pro-font6 link-position hrf-view hide">View all</a>
                                        <ul id="tabs-swipe-demo" class="tabs border-pro3 pro_marg1">
                                            <li class="tab col s2 marg-tabs"><a class="active tab-social" data-num="0" href="#test-swipe-8" data-list="groups">Groups (0)</a></li>
                                            <li class="tab col s2 marg-tabs">
                                                <a class="tab-social" data-num="0" href=" #test-swipe-9" data-list="events">Events (0)</a>
                                            </li>
                                            <li class="tab col s2 marg-tabs"><a class="tab-social" data-num="0" href="#test-swipe-10" data-list="network">Network (0)</a></li>
                                            <li class="indicator" style="right: 673px; left: 0px;"></li></ul>
                                        <div id="test-swipe-8" class="col s12 active">
                                            <div class="col s12 m12 l12 xl12 pro-padg-3">
                                                <div class="card-group-no ">
                                                    <i class="material-icons">group_add</i>
                                                    <div class="group-no">
                                                        <p class="font-group-no ">Join a group and become part of a Storyrocket Community.</p>
                                                        <a href="https://www.storyrocket.com/groups" class="btn-go-credit btn-reactj">Search Group</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="test-swipe-9" class="col s12" style="display: none;">
                                            <div class="col s12 m12 l12 xl12 pro-padg-3">
                                                <div class="card-event-no">
                                                    <i class="material-icons">event_note</i>
                                                    <div class="event-no">
                                                        <p class="font-event-no ">Would you like to attend an event in your area?</p>
                                                        <a href="https://www.storyrocket.com/events" class="btn-go-credit btn-reactj"> Join now</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="test-swipe-10" class="col s12" style="display: none;">
                                            <div class="col s12 m12 l12 xl12 pro-padg-3">
                                                <div class="card-friend-no">
                                                    <i class="material-icons">people</i>
                                                    <div class="friend-no">
                                                        <p class="font-friend-no">You haven't followed anyone yet.</p>
                                                        <a href="https://www.storyrocket.com/search" class="btn-go-credit btn-reactj">Search members</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--<a href="" class="pro-font6 link-position-res">View all</a>-->
                                </article>
                                <article class="col s12 m12 l12 xl12" id="profile-last">
                                    <!-- content read later list-->
                                    <div class="pro-marg4-r" id="read-list-aside">
                                        <div class="span-text border-pro3 pro_marg1 u-flex1 u-flexTop u-justifyContentSpaceBetween">
                                            <span class="pro-font9 pro-color1 u-boder">Read Later List <span class="pro-font5 pro-color1">(3)</span></span>
                                            <a href="" class="pro-font6 pro-color5">View all</a>
                                        </div>
                                        <div class="pro-marg5">
                                            <!--- card read list-->
                                            <div class="card-read">
                                                <a href="" title="If It Ain’t Broke, Don’t Fix it">
                                                    <span class="font-card2 truncate">If It Ain’t Broke, Don’t Fix it</span>
                                                </a>
                                                <span class="read-font4 pro-color7 truncate read-bloque ">By</span>
                                                <span class="couponcode">
												<a href="" class="read-bloque read-font4 pro-color5 truncate"> Juan Lagoa-gonzalez</a>
                                                    <!-- tool-tip -->
													<div class="coupontooltip">
														<div class="card-tool">
															<div class="arrow-tol"></div>
															<div class="tol-horizontal">
																<div class="tol-images">
																	<figure>
																		<img src="{{asset("images/prueba-2.jpg")}}" alt="tool">
																	</figure>
																</div>
																<div class="tol-content">
																	<a href="" class="font-tol">Juan Lagoa-gonzalez</a>
																	<p class="font-tol2 truncate">
																		<span class="font-tol2">Director (Film/TV)</span>
																	</p>
																	<p class="font-tol3 truncate">San Fransico, California, US</p>
																</div>
																<div class="btn-pos">
																	<!-- boton follow-2 toltip-->
																		<button href="" class="botoni botom-net-f SeeMore3">
																			<i class="icon-icono-profile3"></i>
																		</button>
                                                                    <!--fin boton-2-->
																</div>
															</div>
															<div class="tol-content2">
																<div class="truncate">
																	<span class="font-tol4">
																	I’m a writer, editor, and mountain guide living in Denver, Colorado. I did my undergraduate work at the University of Saint Andrews (in Scotland) and at the University of Washington (in Seattle), where I earned a Bachelor’s of Arts in...
																	<a href="" class="font-tol7">Read More</a>
																	</span>
																</div>
															</div>
															<div class="col s4 tol-margin">
																<span class="tol-float">
																	<svg style="width:13px;height:24px" viewBox="0 0 24 24">
																		<path fill="#000000" d="M18,22A2,2 0 0,0 20,20V4C20,2.89 19.1,2 18,2H12V9L9.5,7.5L7,9V2H6A2,2 0 0,0 4,4V20A2,2 0 0,0 6,22H18Z"></path>
																	</svg>
																</span>
																<span class="font-tol3">
																	<strong class="color-tip">5</strong> Projects
																</span>
															</div>
															<div class="col s4 tol-margin">
																<span class="tol-float">
																	<svg style="width:14px;height:24px" viewBox="0 0 24 24">
																		<path fill="#000000" d="M12,9A3,3 0 0,0 9,12A3,3 0 0,0 12,15A3,3 0 0,0 15,12A3,3 0 0,0 12,9M12,17A5,5 0 0,1 7,12A5,5 0 0,1 12,7A5,5 0 0,1 17,12A5,5 0 0,1 12,17M12,4.5C7,4.5 2.73,7.61 1,12C2.73,16.39 7,19.5 12,19.5C17,19.5 21.27,16.39 23,12C21.27,7.61 17,4.5 12,4.5Z"></path>
																	</svg>
															 	</span>
															 	<span class="font-tol3"><strong class="color-tip">100</strong> View</span>
															</div>
															<div class="col s4 tol-margin">
																<span class="tol-float">
																	<svg width="11px" height="11px" viewBox="0 0 11 11" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
																		<g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																			<g id="folower" fill="#011523">
																				<path d="M4.17928879,5.08398787 C5.45700271,5.08398787 6.4811143,3.63506625 6.4811143,2.34496328 C6.4811143,1.05486031 5.44521073,0.0089559387 4.16749681,0.0089559387 C2.88978289,0.0089559387 1.85387931,1.05486031 1.85387931,2.34496328 C1.85402858,3.63506625 2.90157487,5.08398787 4.17928879,5.08398787 Z" id="Shape"></path>
																				<path d="M5.03577506,6.11675686 L3.31518997,6.11675686 C1.48429757,6.11675686 0,7.59896472 0,9.42746887 L0.00791107918,9.81899266 C0.00791107918,10.3297797 0.422571041,10.7439919 0.934253672,10.7439919 L7.43268279,10.7439919 C7.94421616,10.7439919 8.35902538,10.3297797 8.35902538,9.81899266 L8.3511143,9.42746887 C8.35096504,7.59896472 6.86681673,6.11675686 5.03577506,6.11675686 Z" id="Shape"></path>
																				<path d="M9.61882743,5.31833493 C9.50225096,5.20175846 9.50225096,5.01278815 9.61882743,4.89621169 L10.6596568,3.85538234 C10.7762332,3.73880587 10.7762332,3.54983557 10.6596568,3.4332591 L10.4485951,3.22204821 C10.3320187,3.10547174 10.1430484,3.10547174 10.0264719,3.22204821 L8.35230843,4.89606242 C8.23573196,5.01263889 8.23573196,5.2016092 8.35230843,5.31818566 L10.0264719,6.99234914 C10.1430484,7.10892561 10.3320187,7.10892561 10.4485951,6.99234914 L10.6596568,6.78128752 C10.7762332,6.66471105 10.7762332,6.47574074 10.6596568,6.35916427 L9.61882743,5.31833493 Z" id="Shape"></path>
																			</g>
																		</g>
																	</svg>
															 	</span>
															 	<span class="font-tol3"><strong class="color-tip">650</strong> Followers</span>
															</div>
														</div>
													</div>
                                                    <!--  fin tooltip-->
											</span>
                                                <div class="pro-font8 pro-color7">
                                                    <a href="" class="pro-font8 pro-color7" title="drama">Drama</a>,
                                                    <a href="" class="pro-font8 pro-color7" title="comedy"> Comedy</a>
                                                </div>
                                                <span class="btnico right marg-read tooltip"><i class="icon-icono-project5"></i>
												<span class="tooltiptext">Add to Read Later List</span>
											</span>
                                            </div>
                                            <!--- fin card read list-->
                                            <div class="pro-marg5">
                                                <!--- card read list-->
                                                <div class="card-read">
                                                    <a href="" title="If It Ain’t Broke, Don’t Fix it">
                                                        <span class="font-card2 truncate">If It Ain’t Broke, Don’t Fix it</span>
                                                    </a>
                                                    <span class="read-font4 pro-color7 truncate read-bloque ">By</span>
                                                    <span class="couponcode">
											<a href="" class="read-bloque read-font4 pro-color5 truncate"> Juan Lagoa-gonzalez</a>
                                                        <!-- tool-tip -->
												<div class="coupontooltip">
													<div class="card-tool">
														<div class="arrow-tol"></div>
														<div class="tol-horizontal">
															<div class="tol-images">
																<figure>
																	<img src="{{asset("images/prueba-2.jpg")}}" alt="tool">
																</figure>
															</div>
															<div class="tol-content">
																<a href="" class="font-tol">Juan Lagoa-gonzalez</a>
																<p class="font-tol2 truncate">
																	<span class="font-tol2">Director (Film/TV)</span>
																</p>
																<p class="font-tol3 truncate">San Fransico, California, US</p>
															</div>
															<div class="btn-pos">
															 	<!-- boton follow-2 toltip-->
																	<button href="" class="botoni botom-net-f SeeMore3">
																		<i class="icon-icono-profile3"></i>
																	</button>
                                                                <!--fin boton-2-->
															</div>
														</div>
														<div class="tol-content2">
															<div class="truncate">
																<span class="font-tol4">
																I’m a writer, editor, and mountain guide living in Denver, Colorado. I did my undergraduate work at the University of Saint Andrews (in Scotland) and at the University of Washington (in Seattle), where I earned a Bachelor’s of Arts in...
																<a href="" class="font-tol7">Read More</a>
																</span>
															</div>
														</div>
														<div class="col s4 tol-margin">
															<span class="tol-float">
																<svg style="width:13px;height:24px" viewBox="0 0 24 24">
																	<path fill="#000000" d="M18,22A2,2 0 0,0 20,20V4C20,2.89 19.1,2 18,2H12V9L9.5,7.5L7,9V2H6A2,2 0 0,0 4,4V20A2,2 0 0,0 6,22H18Z"></path>
																</svg>
															</span>
															<span class="font-tol3"><strong class="color-tip">5</strong> Projects</span>
														</div>
														<div class="col s4 tol-margin">
															<span class="tol-float">
																<svg style="width:14px;height:24px" viewBox="0 0 24 24">
																	<path fill="#000000" d="M12,9A3,3 0 0,0 9,12A3,3 0 0,0 12,15A3,3 0 0,0 15,12A3,3 0 0,0 12,9M12,17A5,5 0 0,1 7,12A5,5 0 0,1 12,7A5,5 0 0,1 17,12A5,5 0 0,1 12,17M12,4.5C7,4.5 2.73,7.61 1,12C2.73,16.39 7,19.5 12,19.5C17,19.5 21.27,16.39 23,12C21.27,7.61 17,4.5 12,4.5Z"></path>
																</svg>
															</span>
															<span class="font-tol3"><strong class="color-tip">100</strong> View</span>
														</div>
														<div class="col s4 tol-margin">
															<span class="tol-float">
																<svg width="11px" height="11px" viewBox="0 0 11 11" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
																		<g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																			<g id="folower" fill="#011523">
																				<path d="M4.17928879,5.08398787 C5.45700271,5.08398787 6.4811143,3.63506625 6.4811143,2.34496328 C6.4811143,1.05486031 5.44521073,0.0089559387 4.16749681,0.0089559387 C2.88978289,0.0089559387 1.85387931,1.05486031 1.85387931,2.34496328 C1.85402858,3.63506625 2.90157487,5.08398787 4.17928879,5.08398787 Z" id="Shape"></path>
																				<path d="M5.03577506,6.11675686 L3.31518997,6.11675686 C1.48429757,6.11675686 0,7.59896472 0,9.42746887 L0.00791107918,9.81899266 C0.00791107918,10.3297797 0.422571041,10.7439919 0.934253672,10.7439919 L7.43268279,10.7439919 C7.94421616,10.7439919 8.35902538,10.3297797 8.35902538,9.81899266 L8.3511143,9.42746887 C8.35096504,7.59896472 6.86681673,6.11675686 5.03577506,6.11675686 Z" id="Shape"></path>
																				<path d="M9.61882743,5.31833493 C9.50225096,5.20175846 9.50225096,5.01278815 9.61882743,4.89621169 L10.6596568,3.85538234 C10.7762332,3.73880587 10.7762332,3.54983557 10.6596568,3.4332591 L10.4485951,3.22204821 C10.3320187,3.10547174 10.1430484,3.10547174 10.0264719,3.22204821 L8.35230843,4.89606242 C8.23573196,5.01263889 8.23573196,5.2016092 8.35230843,5.31818566 L10.0264719,6.99234914 C10.1430484,7.10892561 10.3320187,7.10892561 10.4485951,6.99234914 L10.6596568,6.78128752 C10.7762332,6.66471105 10.7762332,6.47574074 10.6596568,6.35916427 L9.61882743,5.31833493 Z" id="Shape"></path>
																			</g>
																		</g>
																</svg>
															</span>
															<span class="font-tol3">
																<strong class="color-tip">650</strong> Followers
															</span>
														</div>
													</div>
												</div>
                                                        <!--  fin tooltip-->
										</span>
                                                    <div class="pro-font8 pro-color7">
                                                        <a href="" class="pro-font8 pro-color7" title="drama">Drama</a>,
                                                        <a href="" class="pro-font8 pro-color7" title="comedy"> Comedy</a>
                                                    </div>
                                                    <span class="btnico right marg-read tooltip"><i class="icon-icono-project5"></i>
											<span class="tooltiptext">Add to Read Later List</span>
										</span>
                                                </div>
                                                <!--- fin card read list-->
                                                <div class="pro-marg5">
                                                    <!--- card read list-->
                                                    <div class="card-read">
                                                        <a href="" title="If It Ain’t Broke, Don’t Fix it">
                                                            <span class="font-card2 truncate">If It Ain’t Broke, Don’t Fix it</span>
                                                        </a>
                                                        <span class="read-font4 pro-color7 truncate read-bloque ">By</span>
                                                        <span class="couponcode">
											<a href="" class="read-bloque read-font4 pro-color5 truncate"> Juan Lagoa-gonzalez</a>
                                                            <!-- tool-tip -->
											<div class="coupontooltip">
												<div class="card-tool">
													<div class="arrow-tol"></div>
													<div class="tol-horizontal">
												 		<div class="tol-images">
													 		<figure>
														 		<img src="{{asset("images/prueba-2.jpg")}}" alt="tool">
													 		</figure>
													 	</div>
												 		<div class="tol-content">
												 			<a href="" class="font-tol">Juan Lagoa-gonzalez</a>
													 		<p class="font-tol2 truncate">
													 			<span class="font-tol2">Director (Film/TV)</span>
													 		</p>
													 		<p class="font-tol3 truncate">San Fransico, California, US</p>
													 	</div>
														<div class="btn-pos">
														 	<!-- boton follow-2 toltip-->
																<button href="" class="botoni botom-net-f SeeMore3">
																	<i class="icon-icono-profile3"></i>
																</button>
                                                            <!--fin boton-2-->
													 	</div>
													</div>
													<div class="tol-content2">
														<div class="truncate">
															<span class="font-tol4">
															I’m a writer, editor, and mountain guide living in Denver, Colorado. I did my undergraduate work at the University of Saint Andrews (in Scotland) and at the University of Washington (in Seattle), where I earned a Bachelor’s of Arts in...
															<a href="" class="font-tol7">Read More</a>
															</span>
														</div>
													</div>
													<div class="col s4 tol-margin">
												 		<span class="tol-float">
													 		<svg style="width:13px;height:24px" viewBox="0 0 24 24">
																<path fill="#000000" d="M18,22A2,2 0 0,0 20,20V4C20,2.89 19.1,2 18,2H12V9L9.5,7.5L7,9V2H6A2,2 0 0,0 4,4V20A2,2 0 0,0 6,22H18Z"></path>
															</svg>
													 	</span>
													 	<span class="font-tol3"><strong class="color-tip">5</strong> Projects</span>
													</div>
												 	<div class="col s4 tol-margin">
													 	<span class="tol-float">
															<svg style="width:14px;height:24px" viewBox="0 0 24 24">
																<path fill="#000000" d="M12,9A3,3 0 0,0 9,12A3,3 0 0,0 12,15A3,3 0 0,0 15,12A3,3 0 0,0 12,9M12,17A5,5 0 0,1 7,12A5,5 0 0,1 12,7A5,5 0 0,1 17,12A5,5 0 0,1 12,17M12,4.5C7,4.5 2.73,7.61 1,12C2.73,16.39 7,19.5 12,19.5C17,19.5 21.27,16.39 23,12C21.27,7.61 17,4.5 12,4.5Z"></path>
															</svg>
													 	</span>
												 		<span class="font-tol3"><strong class="color-tip">100</strong> View</span>
													</div>
												 	<div class="col s4 tol-margin">
												 		<span class="tol-float">
															<svg width="11px" height="11px" viewBox="0 0 11 11" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
																<g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																	<g id="folower" fill="#011523">
																		<path d="M4.17928879,5.08398787 C5.45700271,5.08398787 6.4811143,3.63506625 6.4811143,2.34496328 C6.4811143,1.05486031 5.44521073,0.0089559387 4.16749681,0.0089559387 C2.88978289,0.0089559387 1.85387931,1.05486031 1.85387931,2.34496328 C1.85402858,3.63506625 2.90157487,5.08398787 4.17928879,5.08398787 Z" id="Shape"></path>
																		<path d="M5.03577506,6.11675686 L3.31518997,6.11675686 C1.48429757,6.11675686 0,7.59896472 0,9.42746887 L0.00791107918,9.81899266 C0.00791107918,10.3297797 0.422571041,10.7439919 0.934253672,10.7439919 L7.43268279,10.7439919 C7.94421616,10.7439919 8.35902538,10.3297797 8.35902538,9.81899266 L8.3511143,9.42746887 C8.35096504,7.59896472 6.86681673,6.11675686 5.03577506,6.11675686 Z" id="Shape"></path>
																		<path d="M9.61882743,5.31833493 C9.50225096,5.20175846 9.50225096,5.01278815 9.61882743,4.89621169 L10.6596568,3.85538234 C10.7762332,3.73880587 10.7762332,3.54983557 10.6596568,3.4332591 L10.4485951,3.22204821 C10.3320187,3.10547174 10.1430484,3.10547174 10.0264719,3.22204821 L8.35230843,4.89606242 C8.23573196,5.01263889 8.23573196,5.2016092 8.35230843,5.31818566 L10.0264719,6.99234914 C10.1430484,7.10892561 10.3320187,7.10892561 10.4485951,6.99234914 L10.6596568,6.78128752 C10.7762332,6.66471105 10.7762332,6.47574074 10.6596568,6.35916427 L9.61882743,5.31833493 Z" id="Shape"></path>
																	</g>
																</g>
															</svg>
													 	</span>
													 	<span class="font-tol3"><strong class="color-tip">650</strong> Followers</span>
													</div>
												</div>
											</div>
                                                            <!--  fin tooltip-->
										</span>
                                                        <div class="pro-font8 pro-color7">
                                                            <a href="" class="pro-font8 pro-color7" title="drama">Drama</a>,
                                                            <a href="" class="pro-font8 pro-color7" title="comedy"> Comedy</a>
                                                        </div>
                                                        <span class="btnico right marg-read tooltip">
									 		<i class="icon-icono-project5"></i>
											<span class="tooltiptext">Add to Read Later List</span>
									 	</span>
                                                    </div>
                                                    <!--- fin card read list-->
                                                </div></div></div></div></article>
                            </article>
                            <!-- fin columna b-->

                        </section>
                        <!-- fin seection all-->
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</div>


<div id="modal-userMessage" class="modal modal-message-users">
    <div id="popup-fg">
        <div class="conteten-ms">
            <span class="font-pop">Send a Messages</span>
        </div>
        <form class="row padding-pop " id="frm-mjs">
            <div class="col s12 m2 l2 pop-padding">
                <div class="send-pop">
                    <img 
                        src="" 
                        data-src="{{empty($profile->photo) ? (empty($profile->facebook_photo) ? asset('images/avatar_default_user.png')  : $profile->facebook_photo ) : $profile->photo}}" class="responsive-img lazy-image">
                </div>
            </div>
            <div class="col s10 m10 l10 pop-padding2">
                <label class="font-lebel">Subject:</label>
                <input type="text" class="msn-input" id="txt_name_obs">
            </div>
            <div class="col s12 m12 l12 pop-padding3">
                <textarea class="text-area-pop" placeholder="Enter your message" id="txt_name_descrip"></textarea>
            </div>   
        </form>
        <div class="actions act-2">
        <button id="close-modal-userMessage" class="btn-cancel">Cancel</button>
        <button id="submit-message" class="btn-send">Send</button>
        </div>
    </div>
</div>
@endsection
