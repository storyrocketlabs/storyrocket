@extends('layouts.app-main')

@section('content')
    <div class="cover-page cover-page-in cover-page-1 s-projects">
        <div class="cover-page-content">
            <!--<div class="cover-accordion-container">-->
            <div class="">
                <!-- slide left start -->
                    @auth
                        @include('profile.partials.slideleft')
                    @endauth
                <!-- slide left end -->
                <section id="groups">
                    <article class="row">
                        <article class="" style="    margin-left: -7px;">
                            <p class="title-all-projects">PROJECTS </p>
                            <div class="col s12 m12 l12 xl12 project-mar">
                                <ul class="tabs tab-demo tab-reset lit-tag project-space-tabs2 align-all-projects">
                                    <li class="tab"><a href="#test50" class="active">My Projects</a></li>
                                    <li class="tab"><a class="" href="#test51">My Co-Authored</a></li>
                                    <li class="tab"><a href="#test52" class="">My Read Later List</a></li>
                                    <!--<li class="tab"><a href="#feature-projects" class="">Most Viewed</a></li>
                                    <li class="tab"><a href="#trending-projects" class="">New Projects</a></li>-->

                                    <!--<li class="indicator" style="right: 642px; left: 335px;"></li>-->
                                </ul>
                                <div id="test50" class="col s12 active" style="">
                                    <div class="area-max-show-allprojects limit-area-result" id="cont-prj">
                                        @if(count($projects) > 0)
                                            @foreach($projects as $project)
                                                @include('search.partials.project-card')
                                            @endforeach
                                        @else
                                            <div class="add-create elemts-create">
                                                <span class="icon-icono-profile28"></span>
                                                <div class="margin-crea">
                                                    <div class="row">
                                                        <span class="font-create">Create a project and share it with the World. </span>
                                                    </div>

                                                    <div class="row">
                                                        <a  class="btn-go-create btn-reactj w130">Create a project</a>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                <div id="test51" class="col s12 active" style="display: none;">
                                    <div class="area-max-show-allprojects card-read-no elemts-create">
                                        <div class="no-cou-p">
                                            <p class="font-cou-p">No projects to display</p>
                                        </div>
                                    </div>
                                </div>
                                <div id="test52" class="col s12 active" style="display: none;">
                                    <div class="area-max-show-allprojects">
                                        <div id="list-read-collection">
                                            <div class="card-read-no elemts-create">
                                                <i class="material-icons">view_list</i>
                                                <div class="read-no">
                                                    <p class="font-read-no">Add a project to your “Read Later List” and
                                                        discover incredible stories.</p>
                                                    <a href="https://www.storyrocket.com/#genres"
                                                       class="btn-go-credit btn-reactj">Explore</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col s12 m12 l12 xl12">
                                        <!-- img src="images/prealoder/three-dots.svg" width="60" alt="" class="post-preol" -->
                                    </div>
                                </div>
                            </div>
                        </article>
                    </article>


                </section>
            </div>
        </div>
    </div>
    </div>

    <div class="load-asistent-project"></div>

@endsection

