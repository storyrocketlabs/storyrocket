<script type="text/html" id="item-autocomplete">
    <% _.forEach(results, function(result) { %>
    <li class="result-search-writter menbers-gr <%-className%>" data-mail="<%-result.email %>" data-id="<%-result.id %>" data-name="<%-result.full_name %> <%-result.last_name %>">
        <div class="display-search-writter">
            <img src="<%-result.photo %>" alt="<%-result.full_name %> <%-result.last_name %>">
        </div>
        <div class="search-txt-writter" style="padding-top:15px">
            <p><%-result.full_name %> <%-result.last_name %></p>
        </div>
    </li>
    <% } ) %>
</script>

<script type="text/html" id="template-tag-list">
    <% _.forEach(writers, function(writer, index) { %>
    <li class="intended-medium-project-bubble-10" style="display: inline-block;">
        <div class="chip tag-active intended-medium-project-bubble-div10">
            <%-writer.name %><i class="close material-icons <%-className%>" data-index="<%-index %>">close</i>
        </div>
    </li>
    <% } ) %>
</script>