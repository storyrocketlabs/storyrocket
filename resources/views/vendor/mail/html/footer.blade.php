<tr>
    <td colspan="2">
        <table class="footer" align="center" width="570" cellpadding="0" cellspacing="0">
            <tr>
                <td class="footer-content">
                    <a href="https://www.storyrocket.com/" target="_blank">
                        <img src="{{asset('images/ISO-STORYROCKET.png')}}" alt="storyrocket" width="225">
                    </a>
                </td>
            </tr>
            <tr>
                <td class="footer-content">
                    <a href="https://www.facebook.com/storyrocketweb/?fref=ts" target="_blank">
                        <img src="{{asset('images/email/facebook.png')}}" alt="storyrocket">
                    </a>
                    <a href="https://twitter.com/storyrocketweb" target="_blank">
                        <img src="{{asset('images/email/instagram.png')}}" alt="storyrocket">
                    </a>
                    <a href="https://www.instagram.com/storyrocketweb/" target="_blank">
                        <img src="{{asset('images/email/twitter.png')}}" alt="storyrocket">
                    </a>
                </td>
            </tr>
            <tr>
                <td class="footer-content title-footer">
                    Storyrocket, LLC
                </td>
            </tr>
            <tr>
                <td class="footer-content email-footer">
                    Email: support@storyrocket.com
                </td>
            </tr>
            <tr>
                <td class="footer-content blank-space footer-space">

                </td>
            </tr>
            <tr>
                <td class="footer-content blank-space">

                </td>
            </tr>
            <tr>
                <td class="footer-terms">
                    Any information that we collect is subject to the <a href="https://www.storyrocket.com/privacy-policy" target="_blank">Privacy Policy</a> in effect at the time such information is collected. This Privacy Statement applies to all users, visitors and others who access the Service and does not apply to any third party websites, services or applications, even if they are accessible through our Service. Also, please note that, unless we define a term in this Privacy Policy, all words used in this Privacy Policy shall have the same meanings as in our Terms of Use, accessible at
                    <a href="https://www.storyrocket.com/terms-of-service" target="_blank">Terms</a>.
                </td>
            </tr>
            <tr>
                <td class="content-cell" align="center">
                    {{ Illuminate\Mail\Markdown::parse($slot) }}
                </td>
            </tr>
        </table>
    </td>
</tr>
