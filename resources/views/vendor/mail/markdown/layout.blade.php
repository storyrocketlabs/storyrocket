{!! strip_tags($header) !!}

{!! strip_tags($slot) !!}


{!! strip_tags($footer) !!}

@isset($subcopy)

    {!! strip_tags($subcopy) !!}
@endisset
