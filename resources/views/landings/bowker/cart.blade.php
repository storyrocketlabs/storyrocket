@extends('landings.bowker.app-main')

@section('content')
    <div class="sr-landings sr-landing-bowker s-complete-this-purchase">
        <div class="sr-landings-content">


            <div class="landing-bowker-container-steps-container">
                <div class="landing-bowker-container-steps">

                    <div class="landing-bowker-container-steps-content">

                        <div class="payment-cart-title">
                            <div class="payment-cart-title-content">
                                Shopping Cart
                            </div>
                        </div>

                        <!-- payment cart data start -->
                        <div class="payment-cart-data">

                            <div class="payment-cart-data-left">
                                <!-- grid start -->
                                <div class="payment-cart-grid">
                                    <div class="payment-cart-grid-content">

                                        <div class="payment-cart-grid-rows payment-cart-grid-title">
                                            <div class="payment-cart-grid-rows-content">
                                                <div class="payment-cart-grid-col payment-cart-grid-col1">
                                                    <div class="payment-cart-grid-col1-content">
                                                        Plan
                                                    </div>
                                                </div>

                                                <div class="payment-cart-grid-col payment-cart-grid-col2">
                                                    <div class="payment-cart-grid-col2-content">
                                                        Price
                                                    </div>
                                                </div>

                                                <div class="payment-cart-grid-col payment-cart-grid-col3">
                                                    <div class="payment-cart-grid-col3-content">
                                                        Quantity
                                                    </div>
                                                </div>

                                                <div class="payment-cart-grid-col payment-cart-grid-col4">
                                                    <div class="payment-cart-grid-col4-content">

                                                    </div>
                                                </div>
                                            </div>
                                        </div>



                                        <div class="payment-cart-grid-rows">
                                            <div class="payment-cart-grid-rows-content">

                                                <div class="payment-cart-grid-col payment-cart-grid-col1">
                                                    <div class="payment-cart-grid-col1-content text-left">
                                                        <p><b>Professional Plan</b> with 20 projects <b>$195.00</b></p>
                                                        <p><b>50 Getty Images</b> for the Cast Wish List <b>$50.00</b></p>
                                                        <p><span>$245.00</span> $149.99 </p>
                                                    </div>
                                                </div>

                                                <div class="payment-cart-grid-col payment-cart-grid-col2">
                                                    <div class="payment-cart-grid-col2-content">
                                                        $149.99
                                                    </div>
                                                </div>

                                                <div class="payment-cart-grid-col payment-cart-grid-col3">
                                                    <div class="payment-cart-grid-col3-content">
                                                        1
                                                    </div>
                                                </div>

                                                <!-- div class="payment-cart-grid-col payment-cart-grid-col3">
                                                    <div class="payment-cart-grid-col3-content">
                                                        <a href="#">Delete</a>
                                                    </div>
                                                </div -->

                                            </div>

                                        </div>




                                    </div>
                                </div>
                                <!-- grid end -->
                            </div>

                            <div class="payment-cart-data-right">
                                <div class="payment-cart-data-right-content">

                                    <!-- subtotal start -->
                                    <div class="payment-cart-subtotal formatter">
                                        <div class="payment-cart-subtotal-content">
                                            <div class="payment-cart-subtotal-line formatter">
                                                <div class="payment-cart-subtotal-line-content">
                                                    Subtotal (1 item): <b>$149.99</b>
                                                </div>
                                            </div>

                                            <div class="payment-cart-subtotal-button formatter">
                                                <div class="payment-cart-subtotal-button-content">
                                                    <a href="{{route('bowker.checkout')}}">
                                                        <button type="button" name="button" class="btn-primary-small-2 btn-mar-p-1 order-link btn-size order-link herlper-btn">Place your order</button>
                                                    </a>

                                                </div>
                                            </div>


                                            <!-- order details start -->
                                            <div class="order-details formatter">
                                                <div class="order-details-content">
                                                    <div class="order-details-title">
                                                        <div class="order-details-title-content">
                                                            <b>Order Summary</b>
                                                        </div>
                                                    </div>

                                                    <div class="order-details-row">
                                                        <div class="order-details-row-content">
                                                            <div class="order-details-col order-details-col-1">
                                                                <div class="order-details-col-content text-left">
                                                                    Items
                                                                </div>
                                                            </div>
                                                            <div class="order-details-col order-details-col-2">
                                                                <div class="order-details-col-content text-right">
                                                                    $149.99
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>


                                                    <div class="order-details-row order-details-row-total">
                                                        <div class="order-details-row-content">
                                                            <div class="order-details-col order-details-col-1">
                                                                <div class="order-details-col-content text-left">
                                                                    <h6><b>Order Total</b></h6>
                                                                </div>
                                                            </div>
                                                            <div class="order-details-col order-details-col-2">
                                                                <div class="order-details-col-content text-right">
                                                                    <h6><b>$149.99</b></h6>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>


                                                </div>
                                            </div>

                                            <!-- order details end -->


                                        </div>

                                    </div>
                                    <!-- subtotal end -->



                                    <!-- div class="payment-cart-suggestions">
                                        <div class="payment-cart-suggestions-content">
                                            <div class="payment-cart-suggestions-row">
                                                <div class="payment-cart-suggestions-row-content">
                                                    <div class="payment-cart-suggestions-colleft">
                                                        <div class="payment-cart-suggestions-img">
                                                            <div class="payment-cart-suggestions-img-content">
                                                                <img src="{{asset('images/cart-logo-getty.png')}}">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="payment-cart-suggestions-colright">

                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div -->



                                </div>
                            </div>


                        </div>

                        <!-- payment cart data end -->


                        <!-- payment cart footer start -->

                        <div class="payment-cart-footer">
                            <div class="payment-cart-footer-content">
                                <br>
                                <div class="payment-cart-footer-title">
                                    <div class="payment-cart-footer-title-content">
                                        Auto Renewal
                                    </div>
                                </div>


                                <div class="payment-cart-footer-description">
                                    <div class="payment-cart-footer-description-content">
                                        Plans are set to Auto Renewal. You will be notified 30 days before your plan expires to cancel or to make changes. Once a billing period begins, no refunds will be made.
                                    </div>

                                    <br>
                                </div>
                            </div>
                        </div>



                        <!-- payment cart footer end -->




                    </div>

                </div>
            </div>

        </div>
    </div>
@endsection