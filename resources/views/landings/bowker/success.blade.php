@extends('landings.bowker.app-main')

@section('content')
    <div class="sr-landings sr-landing-bowker s-complete-this-purchase">
        <div class="sr-landings-content">


            <div class="landing-bowker-container-steps-container">
                <div class="landing-bowker-container-steps">

                    <div class="landing-bowker-container-steps-content">

                        <div class="landing-bowker-success">
                            <div class="landing-bowker-success-content success-text">
                                <br><br><br><br><br><br><br>
                                <h4 >
                                    Thank you for signing up with Storyrocket. We’re happy you’re here!
                                </h4>
                                <p>
                                    You’re annual <b>PRO PLAN</b> allows you to post up to <b>20 projects</b> and includes <b>50 Getty Images.</b>
                                </p>

                                <h5 class="success-text" style="font-weight: bolder; color: red;">
                                    Important! check your email to activate your account.
                                </h5>
                                <p>
                                    We sent you a confirmation email with a link to activate
                                    your account. Please check your email and click the link.
                                    This helps to ensure that your (and our) inbox remains free of spam.

                                </p>
                                <br><br><br><br><br>

                            </div>
                        </div>



                    </div>

                    <div class="payment-cart-form-row text-right">
                        <img src="{{asset('images/logo-SR.png')}}" width="220" style="display: inline-block; padding-right: 30px;" alt="">
                    </div>
                    <br><br>

                </div>
            </div>

        </div>
    </div>
@endsection