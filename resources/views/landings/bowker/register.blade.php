@extends('landings.bowker.app-main')

@section('content')
    <div class="sr-landings sr-landing-bowker s-complete-this-purchase">
        <div class="sr-landings-content">


            <div class="landing-bowker-container-steps-container landing-bowker-register-container-steps-container">
                <div class="landing-bowker-container-steps">
                    <br>
                    <div class="landing-bowker-container-steps-content">

                        <div class="complete-this-purchase-title">
                            <div class="complete-this-purchase-title-content">
                                To complete this purchase you<br>need to enter your email address.
                            </div>

                        </div>

                        <div class="complete-this-purchase-form">
                            {{ Form::open(array('route' => 'bowker.register.save')) }}
                                <div class="complete-this-purchase-form-content">
                                    <div class="complete-this-purchase-form-input">
                                        <input class="input-profile1 add-element required margin-helper-hero1" type="text" name="email" id="email" placeholder="name@email.com">
                                    </div>
                                    <div class="complete-this-purchase-form-button">
                                        <button type="submit" name="button" class="btn-primary-medium btn-size marg-footer margin-helper-hero2">Next</button>
                                    </div>

                                    <div class="complete-this-purchase-form-input">
                                        @if($errors->any())
                                            <div class="has-error" style="color:red;">{{$errors->first()}}</div>
                                        @endif
                                    </div>
                                </div>
                            {{ Form::close() }}

                        </div>
                        <div class="complete-this-purchase-text">
                            <div class="complete-this-purchase-text-content">
                                <!--<p class="complete-this-purchase-text-top">
                                    <span>Are you a member?</span>
                                    <a  href="{{route('login')}}" class="color-f-4"> Sign in to your account</a>
                                </p> -->
                                <p class="complete-this-purchase-text-bottom">
                                    <span>By signing up, I agree to the Storyrocket</span>
                                    <a href="https://www.storyrocket.com/privacy-policy" target="_blank" class="color-f-4"> Privacy Policy</a>
                                    <span> and </span>
                                    <a href="https://www.storyrocket.com/terms-of-service"  target="_blank" class="color-f-4"> Terms of Service.</a>
                                </p>
                            </div>
                        </div>

                    </div>
                    <br><br><br><br>
                </div>
            </div>

        </div>
    </div>
@endsection
