<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
</head>
<body>
<style>
    @media only screen and (max-width: 600px) {
        .inner-body {
            width: 100% !important;
        }

        .footer {
            width: 100% !important;
        }
    }

    @media only screen and (max-width: 500px) {
        .button {
            width: 100% !important;
        }
    }
</style>

<table class="wrapper" width="100%" cellpadding="0" cellspacing="0">
    <tr>
        <td align="center">
            <table class="content" width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td class="space" colspan="2">

                    </td>
                </tr>
                <tr>
                    <td class="blank-space" colspan="2" style="text-align: center">
                        <img src="http://dev.storyrocket.com/images/mails/logo.png?v=1" width="110" height=110"
                             alt="alt_text" border="0"
                             style="height: auto; background: #ffffff; font-family: sans-serif; font-size: 15px; line-height: 15px; color: #555555;">
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <br>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="text-align: center" align="center">
                        <table style="width: 700px; margin: 0 auto;background-color:#fff; padding: 10px">
                            <tr>
                                <td colspan="4">
                                    <br>
                                    <br>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: center" colspan="4">
                                    <h1 style="font-family:sans-serif; font-weight:bolder; line-height:27px; display:inline-block">
                                        Thank you for signing up with Storyrocket. <br>
                                        We’re happy you’re here!
                                    </h1>
                                </td>
                            </tr>
                            <tr>
                                <td style="font-family:sans-serif;font-size:15px;line-height:15px;text-align:center;color:#0d0e10;background-color:#fff"
                                    colspan="4">
                                    <br>
                                    <br>
                                    Activate your account below and create a password. <br><br>
                                    From there you will be taken directly to your member profile, click "Edit Profile"
                                    to add your information. Complete as much as you can, and remember you can edit as
                                    often as you want.
                                    <br><br>
                                    Be on the lookout for a series of onboarding videos in your email. These emails will
                                    get you navigating Storyrocket like a pro in no time.
                                    <br><br>
                                    Welcome to Storyrocket!
                                </td>
                            </tr>
                            <tr>
                                <td align="center" style="width: 25%">
                                </td>
                                <td colspan="2" style="width: 50%" valign="middle">
                                    <br>
                                    <br>
                                    <div style="background: #FFBC00; width: 100%; border-radius: 4px;border: 1px solid #D5AB55; margin: 0 auto; height: 40px; text-align: center">
                                        <a href="{{route('bowker.formMailRegisterPassword', $user[0]->UserID)}}"
                                           style="color: #0D0E10;
                                   font-family: sans-serif;
                                   font-size: 15px;
                                   line-height: 40px;
                                   text-decoration: none;
                                   display: block;
                                   width: 300px;
                                   height: 20px;
                                    margin: 0 auto;
                                    text-align: center;">
                                            Click here to activate your account
                                        </a>
                                    </div>
                                    <br>
                                    <br>
                                </td>
                                <td align="center" width="25%">

                                </td>
                            </tr>
                        </table>
                        <table style="width: 320px; margin: 0 auto;background-color:#FFF">
                            <tr>
                                <td colspan="4">
                                    <br>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: center" colspan="4" align="center">
                                    <img src="http://dev.storyrocket.com/images/mails/logo-mail-1.png" width="300"
                                         style="margin: 0 auto">
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: center; width: 25%">
                                    <a href="https://www.facebook.com/storyrocketweb/" target="_blank"
                                       style="padding: 5px; text-decoration: none">
                                        <img src="http://dev.storyrocket.com/images/mails/facebook.png?v1" width="30">
                                    </a>
                                </td>
                                <td style="text-align: center; width: 25%">
                                    <a href="https://twitter.com/storyrocketweb" target="_blank"
                                       style="padding: 5px;text-decoration: none">
                                        <img src="http://dev.storyrocket.com/images/mails/twitter.png" width="30">
                                    </a>
                                </td>
                                <td style="text-align: center; width: 25%">
                                    <a href="https://www.instagram.com/storyrocketweb/" target="_blank"
                                       style="padding: 5px; text-decoration: none">
                                        <img src="http://dev.storyrocket.com/images/mails/instagram.png" width="30">
                                    </a>
                                </td>
                                <td style="text-align: center; width: 25%">
                                    <a href="http://www.storyrocket.com" target="_blank"
                                       style="padding: 5px; text-decoration: none">
                                        <img src="http://dev.storyrocket.com/images/mails/link.png" width="30">
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: center; font-family: sans-serif;
                                   font-size: 13px;" colspan="4">
                                    <br>
                                    Copyright © 2019 Storyrocket.com, All rights reserved.
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>
</html>
