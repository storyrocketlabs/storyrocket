@extends('landings.bowker.app-main')

@section('content')
    <div class="sr-landings sr-landing-bowker s-complete-this-purchase">
        <div class="sr-landings-content">


            <div class="landing-bowker-container-steps-container">
                <div class="landing-bowker-container-steps">

                    <div class="landing-bowker-container-steps-content">

                        <div class="payment-cart-title">
                            <div class="payment-cart-title-content">
                                Payment method
                            </div>
                        </div>


                        <div class="payment-cart-methods formatter">
                            <div class="payment-cart-methods-content formatter text-left">
                                <div class="method-pay ">
                                    <div>
                                        <img width="30" src="{{asset('images/pay1-l.svg')}}" alt="">
                                    </div>
                                    <div>
                                        <img width="30" src="{{asset('images/pay2-l.svg')}}" alt="">
                                    </div>
                                    <div>
                                        <img width="30" src="{{asset('images/pay3-l.svg')}}" alt="">
                                    </div>
                                    <div>
                                        <img width="30" src="{{asset('images/pay4-l.svg')}}" alt="">
                                    </div>
                                    <div>
                                        <img width="30" src="{{asset('images/pay5-l.svg')}}" alt="">
                                    </div>
                                    <div>
                                        <img width="30" src="{{asset('images/pay6-l.svg')}}" alt="">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- payment cart data start -->
                        <div class="payment-cart-data">

                            <div class="payment-cart-data-left">
                                <!-- payment form start -->
                                <div class="payment-cart-form">
                                    <div class="payment-cart-form">

                                        <!-- row start -->
                                        <div class="payment-cart-form-row formatter" >
                                            <div class="payment-cart-form-50">
                                                <div class="payment-cart-form-50-content">
                                                    <div class="payment-cart-form-field-title">
                                                        <div class="payment-cart-form-field-title-content text-left">
                                                            First Name
                                                        </div>
                                                    </div>
                                                    <div class="payment-cart-form-field">
                                                        <div class="payment-cart-form-field-content">
                                                            <input placeholder="First Name" id="name" name="name">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="payment-cart-form-50">
                                                <div class="payment-cart-form-50-content">

                                                    <div class="payment-cart-form-field-title">
                                                        <div class="payment-cart-form-field-title-content text-left">
                                                            Last Name
                                                        </div>
                                                    </div>
                                                    <div class="payment-cart-form-field">
                                                        <div class="payment-cart-form-field-content">
                                                            <input placeholder="Last Name" id="last_name" name="last_name">
                                                        </div>

                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <!-- row end  -->

                                        <!-- row start -->
                                        <div class="payment-cart-form-row formatter" >
                                            <div class="payment-cart-form-100 formatter">
                                                <div class="payment-cart-form-100-content formatter">
                                                    <div class="payment-cart-form-field-title">
                                                        <div class="payment-cart-form-field-title-content text-left">
                                                            Card Number
                                                        </div>
                                                    </div>
                                                    <div class="payment-cart-form-field">
                                                        <div class="payment-cart-form-field-content">
                                                            <input placeholder="Enter credit card number" id="name_card" name="name_card">
                                                        </div>

                                                    </div>

                                                </div>
                                            </div>

                                        </div>
                                        <!-- row end  -->


                                        <!-- row start -->
                                        <div class="payment-cart-form-row formatter" >
                                            <div class="payment-cart-form-30">
                                                <div class="payment-cart-form-30-content">
                                                    <div class="payment-cart-form-field-title">
                                                        <div class="payment-cart-form-field-title-content text-left">
                                                            Expiration date
                                                        </div>
                                                    </div>
                                                    <div class="payment-cart-form-field">
                                                        <div class="payment-cart-form-field-content">
                                                            <input placeholder="MM/YY" id="expiration_date" name="expiration_date">
                                                        </div>

                                                    </div>

                                                </div>
                                            </div>

                                            <div class="payment-cart-form-30">
                                                <div class="payment-cart-form-30-content">

                                                    <div class="payment-cart-form-field-title">
                                                        <div class="payment-cart-form-field-title-content text-left">
                                                            CVV
                                                        </div>
                                                    </div>
                                                    <div class="payment-cart-form-field">
                                                        <div class="payment-cart-form-field-content">
                                                            <input placeholder="CVV" id="cvv" name="cvv">
                                                        </div>

                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <!-- row end  -->

                                        <!-- row start -->
                                        <div class="payment-cart-form-row formatter" >
                                            <div class="payment-cart-form-50">
                                                <div class="payment-cart-form-50-content">
                                                    <div class="payment-cart-form-field-title">
                                                        <div class="payment-cart-form-field-title-content text-left">
                                                            Country
                                                        </div>
                                                    </div>
                                                    <div class="payment-cart-form-field">
                                                        <div class="payment-cart-form-field-content">
                                                            <input placeholder="Country" id="name" name="name">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="payment-cart-form-50">
                                                <div class="payment-cart-form-50-content">

                                                    <div class="payment-cart-form-field-title">
                                                        <div class="payment-cart-form-field-title-content text-left">
                                                            Postal Code
                                                        </div>
                                                    </div>
                                                    <div class="payment-cart-form-field">
                                                        <div class="payment-cart-form-field-content">
                                                            <input placeholder="Postal Code" id="last_name" name="last_name">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- row end  -->

                                        <div class="payment-cart-form-row formatter" style="width: 250px">
                                            <button type="button" name="button" class="btn-primary-small-2 btn-mar-p-1 order-link btn-size order-link herlper-btn">Use this payment method</button>
                                        </div>

                                    </div>
                                </div>

                                <!-- payment form end -->
                            </div>

                            <div class="payment-cart-data-right">
                                <div class="payment-cart-data-right-content">

                                    <!-- subtotal start -->
                                    <div class="payment-cart-subtotal formatter">
                                        <div class="payment-cart-subtotal-content">
                                            <div class="payment-cart-subtotal-line formatter">
                                                <div class="payment-cart-subtotal-line-content">
                                                    Subtotal (1 item): <b>$149.99</b>
                                                </div>
                                            </div>

                                            <div class="payment-cart-subtotal-button formatter">
                                                <div class="payment-cart-subtotal-button-content">
                                                    <button type="button" name="button" class="btn-primary-small-2 btn-mar-p-1 order-link btn-size order-link herlper-btn">Use this payment method</button>
                                                </div>
                                            </div>


                                            <!-- order details start -->
                                            <div class="order-details formatter">
                                                <div class="order-details-content">
                                                    <div class="order-details-title">
                                                        <div class="order-details-title-content">
                                                            <b>Order Summary</b>
                                                        </div>
                                                    </div>

                                                    <div class="order-details-row">
                                                        <div class="order-details-row-content">
                                                            <div class="order-details-col order-details-col-1">
                                                                <div class="order-details-col-content text-left">
                                                                    Items
                                                                </div>
                                                            </div>
                                                            <div class="order-details-col order-details-col-2">
                                                                <div class="order-details-col-content text-right">
                                                                    $149.99
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>


                                                    <div class="order-details-row order-details-row-total">
                                                        <div class="order-details-row-content">
                                                            <div class="order-details-col order-details-col-1">
                                                                <div class="order-details-col-content text-left">
                                                                    <h6><b>Order Total</b></h6>
                                                                </div>
                                                            </div>
                                                            <div class="order-details-col order-details-col-2">
                                                                <div class="order-details-col-content text-right">
                                                                    <h6><b>$149.99</b></h6>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>


                                                </div>
                                            </div>

                                            <!-- order details end -->


                                        </div>
                                    </div>
                                    <!-- subtotal end -->



                                <!-- div class="payment-cart-suggestions">
                                        <div class="payment-cart-suggestions-content">
                                            <div class="payment-cart-suggestions-row">
                                                <div class="payment-cart-suggestions-row-content">
                                                    <div class="payment-cart-suggestions-colleft">
                                                        <div class="payment-cart-suggestions-img">
                                                            <div class="payment-cart-suggestions-img-content">
                                                                <img src="{{asset('images/cart-logo-getty.png')}}">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="payment-cart-suggestions-colright">

                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div -->



                                </div>
                            </div>


                        </div>

                        <!-- payment cart data end -->


                        <!-- payment cart footer start -->

                        <div class="payment-cart-footer">
                            <div class="payment-cart-footer-content">
                                <div class="payment-cart-footer-title">
                                    <div class="payment-cart-footer-title-content">
                                        Auto Renewal
                                    </div>
                                </div>


                                <div class="payment-cart-footer-description">
                                    <div class="payment-cart-footer-description-content">
                                        Plans are set to Auto Renewal. You will be notified 30 days before your plan expires to cancel or to make changes. Once a billing period begins, no refunds will be made.

                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- payment cart footer end -->




                    </div>

                </div>
            </div>

        </div>
    </div>
@endsection
