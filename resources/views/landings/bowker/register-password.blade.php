@extends('landings.bowker.app-main')

@section('content')
    <div class="sr-landings sr-landing-bowker s-complete-this-purchase s-register-password">
        <div class="sr-landings-content">


            <div class="register-password-logo">
                <div class="register-password-logo-content">
                    <img src="{{asset('images/story-ima.png')}}">
                </div>
            </div>

        </div>
        <div class="landing-bowker-container-steps-container landing-bowker-register-container-steps-container">
            <div class="landing-bowker-container-steps">
                <div class="register-password-title formatter">
                    <div class="register-password-title-content">
                        Choose a password for signing in to Storyrocket!
                    </div>
                </div>

                <div class="register-password-subtitle formatter">
                    <div class="register-password-subtitle-content">
                        Your password must contain 8 or more characters, at least 1 uppercase
                        & 1 lowercase letter and at least one number.
                    </div>
                </div>

                {{ Form::open(array('route' => 'bowker.savePassword')) }}
                <input type="hidden" value="{{$id}}" name="user_id">
                <div class="register-password-form formatter">
                    <div class="register-password-form-content">
                        <div class="register-password-form-title formatter">
                            <div class="register-password-form-title-content">
                                Password
                            </div>
                        </div>

                        <div class="register-password-form-field formatter">
                            <div class="register-password-form-field-content">
                                <input type="password" placeholder="Enter password" name="password" id="password">
                                <div class="register-password-form-field-eye register-password-form-field-eye1">
                                    <a class="register-password-form-field-eye-content noselect">
                                        <input type="checkbox" name="register-password-form-field-eye-check1" id="register-password-form-field-eye-check1" style="display: none;">
                                        <i class="material-icons">remove_red_eye</i>
                                    </a>
                                </div>
                            </div>
                            @if ($errors->has('password'))
                                <!-- span class="has-error" data-error="wrong"
                                      data-success="right" style="    margin-top: -27px;
    text-align: left;">{{ $errors->first('password') }}</span -->

                                <span  class="has-error" data-error="wrong"
                                      data-success="right" style="    margin-top: -27px;
    text-align: left;">This is not a valid password </span>
                                <br>
                            @endif

                            <span  class="has-error message-v1" data-error="wrong"
                                   data-success="right" style="    margin-top: -27px;
    text-align: left;">

                            </span>
                        </div>

                        <div class="register-password-form-title formatter">
                            <div class="register-password-form-title-content">
                                Confirm Password
                            </div>
                        </div>

                        <div class="register-password-form-field formatter">
                            <div class="register-password-form-field-content" style="    padding-bottom: 25px;">
                                <input type="password" placeholder="Confirm password" name="password_confirmation"
                                       id="password_confirmation">

                                <div class="register-password-form-field-eye register-password-form-field-eye2">
                                    <a class="register-password-form-field-eye-content noselect">
                                        <input type="checkbox" name="register-password-form-field-eye-check2" id="register-password-form-field-eye-check2" style="display: none;">
                                        <i class="material-icons">remove_red_eye</i>
                                    </a>
                                </div>

                            </div>
                            @if ($errors->has('password_confirmation'))
                                <span class="has-error" data-error="wrong"
                                      data-success="right" style="    margin-top: -27px;
    text-align: left;">{{ $errors->first('password_confirmation') }}</span>
                                <br>
                            @endif

                            <span  class="has-error message-v2" data-error="wrong"
                                   data-success="right" style="    margin-top: -27px;
    text-align: left;">

                            </span>
                        </div>

                        <div class="register-submit-form-field formatter">
                            <div class="register-submit-form-field-content">
                                <input style="width: 200px" type="submit"
                                       class="btn-primary-small-2 btn-mar-p-1 order-link btn-size order-link herlper-btn">
                            </div>
                            <br><br><br><br><br>
                        </div>

                        <!-- div class="register-password-footer">
                            <div class="register-password-footer-content">
                                This email contains private information for your Storyrocket account - please don't forward it.
                                Questions about setting up Storyrocket?
                                Email us at <a href="mailto:helpdesk@storyrocket.com">helpdesk@storyrocket.com</a>
                            </div>
                        </div -->


                    </div>
                </div>
                {{ Form::close() }}

            </div>
        </div>

    </div>
    </div>
@endsection
