@extends('landings.bowker.app-main')

@section('content')
    <div class="sr-landings sr-landing-bowker s-complete-this-purchase">
        <div class="sr-landings-content">


            <div class="landing-bowker-container-steps-container">
                <div class="landing-bowker-container-steps">
                    <div class="overlay-checkout-bowker">
                        <div class="animate-flicker preloader-icon" id="overlay-bowker">
                            <img src="{{asset('images/preloader.svg')}}">
                        </div>
                    </div>
                    <div class="landing-bowker-container-steps-content">
                        <div class="payment-cart-title">
                            <div class="payment-cart-title-content">
                                Payment Method
                            </div>
                        </div>


                        <div class="payment-cart-methods formatter">
                            <div class="payment-cart-methods-content formatter text-left">
                                <div class="method-pay ">
                                    <div>
                                        <img width="30" src="{{asset('images/pay1-l.svg')}}" alt="">
                                    </div>
                                    <div>
                                        <img width="30" src="{{asset('images/pay2-l.svg')}}" alt="">
                                    </div>
                                    <div>
                                        <img width="30" src="{{asset('images/pay3-l.svg')}}" alt="">
                                    </div>
                                    <div>
                                        <img width="30" src="{{asset('images/pay4-l.svg')}}" alt="">
                                    </div>
                                    <div>
                                        <img width="30" src="{{asset('images/pay5-l.svg')}}" alt="">
                                    </div>
                                    <div>
                                        <img width="30" src="{{asset('images/pay6-l.svg')}}" alt="">
                                    </div>
                                </div>
                            </div>
                        </div>

                    {{ Form::open(array('route' => 'bowker.savePayment', 'id'=>'form-checkout')) }}

                    <!-- payment cart data start -->
                        <div class="payment-cart-data">

                            <div class="payment-cart-data-left">
                                <!-- payment form start -->
                                <div class="payment-cart-form">
                                    <div class="payment-cart-form">

                                        <!-- row start -->
                                        <div class="payment-cart-form-row  formatter">
                                            <div class="payment-cart-form-50">
                                                <div class="payment-cart-form-50-content">
                                                    <div class="payment-cart-form-field-title">
                                                        <div class="payment-cart-form-field-title-content text-left">
                                                            First Name
                                                        </div>
                                                    </div>
                                                    <div class="payment-cart-form-field">
                                                        <div class="payment-cart-form-field-content">
                                                            <input placeholder="First Name" id="name" name="name" value="{{old('name')}}">
                                                        </div>
                                                    </div>
                                                    @if ($errors->has('name'))
                                                        <div class="payment-cart-form-field"
                                                             style="display: inline-block; text-align: left; width: 100%;">
                                                            <!-- span class="has-error" data-error="wrong"
                                                                  data-success="right">{{ $errors->first('name') }}</span -->

                                                            <span class="has-error" data-error="wrong"
                                                                  data-success="right">Name is required.</span>


                                                        </div>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="payment-cart-form-50">
                                                <div class="payment-cart-form-50-content">

                                                    <div class="payment-cart-form-field-title">
                                                        <div class="payment-cart-form-field-title-content text-left">
                                                            Last Name
                                                        </div>
                                                    </div>
                                                    <div class="payment-cart-form-field">
                                                        <div class="payment-cart-form-field-content">
                                                            <input placeholder="Last Name" id="last_name"
                                                                   name="last_name" value="{{old('last_name')}}">
                                                        </div>
                                                    </div>
                                                    @if ($errors->has('last_name'))
                                                        <div class="payment-cart-form-field"
                                                             style="display: inline-block; text-align: left; width: 100%;">
                                                            <!-- span class="has-error" data-error="wrong"
                                                                  data-success="right">{{ $errors->first('last_name') }}</span -->

                                                                <span class="has-error" data-error="wrong"
                                                                      data-success="right">Last Name is required.</span>



                                                        </div>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <!-- row end  -->

                                        <!-- row start -->
                                        <div class="payment-cart-form-row formatter payment-cart-form-row-cardname">
                                            <div class="payment-cart-form-100 formatter">
                                                <div class="payment-cart-form-100-content formatter">
                                                    <div class="payment-cart-form-field-title">
                                                        <div class="payment-cart-form-field-title-content text-left">
                                                            Card Number
                                                        </div>
                                                    </div>
                                                    <div class="payment-cart-form-field">
                                                        <div class="payment-cart-form-field-content">
                                                            <input placeholder="Enter credit card number" id="name_card"
                                                                   name="name_card" value="{{old('name_card')}}">
                                                        </div>
                                                    </div>
                                                    @if ($errors->has('name_card'))
                                                        <div id="id_name_card_error" class="name_card_error payment-cart-form-field"
                                                             style="display: inline-block; text-align: left; width: 100%;">
                                                            <!-- span class="has-error" data-error="wrong"
                                                                  data-success="right">{{ $errors->first('name_card') }}</span -->

                                                            <span class="has-error" data-error="wrong"
                                                                  data-success="right">The card number is required.</span>
                                                        </div>
                                                    @endif

                                                    @if (session()->has('error_payment'))
                                                        <div class="payment-cart-form-field"
                                                             style="display: inline-block; text-align: left; width: 100%;">
                                                            <span class="has-error" data-error="wrong"
                                                                  data-success="right">{{session()->get('error_payment')}}</span>
                                                        </div>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <!-- row end  -->

                                        <!-- row start -->
                                        <div class="payment-cart-form-row formatter">
                                            <div class="payment-cart-form-30">
                                                <div class="payment-cart-form-30-content">
                                                    <div class="payment-cart-form-field-title">
                                                        <div class="payment-cart-form-field-title-content text-left">
                                                            Expiration Date
                                                        </div>
                                                    </div>
                                                    <div class="payment-cart-form-field">
                                                        <div class="payment-cart-form-field-content">
                                                            <input placeholder="MM/YY" id="expedition_date"
                                                                   name="expedition_date" value="{{old('expedition_date')}}">
                                                        </div>
                                                    </div>
                                                    @if ($errors->has('expedition_date'))
                                                        <div class="payment-cart-form-field"
                                                             style="display: inline-block; text-align: left; width: 100%;">
                                                            <!-- span class="has-error" data-error="wrong"
                                                                  data-success="right">{{ $errors->first('expedition_date') }}</span -->
                                                            <span class="has-error" data-error="wrong"
                                                                  data-success="right">The expiration date is required.</span>
                                                        </div>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="payment-cart-form-30">
                                                <div class="payment-cart-form-30-content">
                                                    <div class="payment-cart-form-field-title">
                                                        <div class="payment-cart-form-field-title-content text-left">
                                                            CVV
                                                        </div>
                                                    </div>
                                                    <div class="payment-cart-form-field">
                                                        <div class="payment-cart-form-field-content">
                                                            <input placeholder="CVV" id="cvv" name="cvv" value="{{old('cvv')}}">
                                                        </div>
                                                    </div>
                                                        @if ($errors->has('cvv'))
                                                        <div class="payment-cart-form-field"
                                                             style="display: inline-block; text-align: left; width: 100%;">
                                                            <!-- span class="has-error" data-error="wrong"
                                                                  data-success="right">{{ $errors->first('cvv') }}</span -->

                                                            <span class="has-error" data-error="wrong"
                                                                  data-success="right">The CVV is required.</span>

                                                        </div>
                                                        @endif
                                                </div>
                                            </div>
                                        </div>
                                        <!-- row end  -->

                                        <!-- row start -->
                                        <div class="payment-cart-form-row formatter">
                                            <div class="payment-cart-form-100">
                                                <div class="payment-cart-form-100-content">
                                                    <div class="payment-cart-form-field-title">
                                                        <div class="payment-cart-form-field-title-content text-left">
                                                            Street Address
                                                        </div>
                                                    </div>
                                                    <div class="payment-cart-form-field">
                                                        <div class="payment-cart-form-field-content">
                                                            <input placeholder="Enter street address"
                                                                   id="street_address" name="street_address" value="{{old('street_address')}}">
                                                        </div>
                                                    </div>
                                                    @if ($errors->has('street_address'))
                                                        <div class="payment-cart-form-field"
                                                             style="display: inline-block; text-align: left; width: 100%;">
                                                            <span class="has-error" data-error="wrong"
                                                                  data-success="right">{{ $errors->first('street_address') }}</span>
                                                        </div>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <!-- row end  -->

                                        <!-- row start -->
                                        <div class="payment-cart-form-row formatter">
                                            <div class="payment-cart-form-50">
                                                <div class="payment-cart-form-50-content">
                                                    <div class="payment-cart-form-field-title">
                                                        <div class="payment-cart-form-field-title-content text-left">
                                                            Country
                                                        </div>
                                                    </div>
                                                    <div class="payment-cart-form-field">
                                                        <div class="payment-cart-form-field-content">
                                                            {!! Form::select('country', $countries, old('street_address'), ['placeholder' => 'Select your Country', 'class'=>'browser-default cbo-profesion', 'id'=>'country', 'style'=>'width: 89%']) !!}
                                                        </div>
                                                    </div>
                                                        @if ($errors->has('country'))
                                                        <div class="payment-cart-form-field"
                                                             style="display: inline-block; text-align: left; width: 100%;">
                                                            <!-- span class="has-error" data-error="wrong"
                                                                  data-success="right">{{ $errors->first('country') }}</span -->

                                                            <span class="has-error" data-error="wrong"
                                                                  data-success="right">Country is required.</span>
                                                        </div>
                                                        @endif
                                                </div>
                                            </div>
                                            <div class="payment-cart-form-50">
                                                <div class="payment-cart-form-50-content">
                                                    <div class="payment-cart-form-field-title">
                                                        <div class="payment-cart-form-field-title-content text-left">
                                                            State
                                                        </div>
                                                    </div>
                                                    <div class="payment-cart-form-field">
                                                        <div class="payment-cart-form-field-content">
                                                            {!! Form::select('state', [], old('state'), ['placeholder' => 'State/Province', 'class'=>'browser-default cbo-profesion', 'id'=>'state_m', 'style'=>'width: 89%']) !!}
                                                        </div>
                                                        @if ($errors->has('state'))
                                                            <div class="payment-cart-form-field"
                                                                 style="display: inline-block; text-align: left; width: 100%;">
                                                                <!-- span class="has-error" data-error="wrong"
                                                                      data-success="right">{{ $errors->first('state') }}</span -->

                                                                <span class="has-error" data-error="wrong"
                                                                      data-success="right">State is required.</span>
                                                            </div>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- row end  -->
                                        <!-- row start -->
                                        <div class="payment-cart-form-row formatter">
                                            <div class="payment-cart-form-50">
                                                <div class="payment-cart-form-50-content">
                                                    <div class="payment-cart-form-field-title">
                                                        <div class="payment-cart-form-field-title-content text-left">
                                                            City
                                                        </div>
                                                    </div>
                                                    <div class="payment-cart-form-field">
                                                        <div class="payment-cart-form-field-content">
                                                            <input placeholder="City" id="city" name="city" value="{{old('city')}}">
                                                        </div>
                                                    </div>
                                                    @if ($errors->has('city'))
                                                        <div class="payment-cart-form-field"
                                                             style="display: inline-block; text-align: left; width: 100%;">
                                                            <!-- span class="has-error" data-error="wrong"
                                                                  data-success="right">{{ $errors->first('city') }}</span -->

                                                                <span class="has-error" data-error="wrong"
                                                                      data-success="right">City is required.</span>
                                                        </div>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="payment-cart-form-50">
                                                <div class="payment-cart-form-50-content">
                                                    <div class="payment-cart-form-field-title">
                                                        <div class="payment-cart-form-field-title-content text-left">
                                                            Postal Code
                                                        </div>
                                                    </div>
                                                    <div class="payment-cart-form-field">
                                                        <div class="payment-cart-form-field-content">
                                                            <input placeholder="Postal Code" id="postal_code"
                                                                   name="postal_code" value="{{old('postal_code')}}">
                                                        </div>
                                                    </div>
                                                    @if ($errors->has('postal_code'))
                                                        <div class="payment-cart-form-field"
                                                             style="display: inline-block; text-align: left; width: 100%;">
                                                            <!-- span class="has-error" data-error="wrong"
                                                                  data-success="right">{{ $errors->first('postal_code') }}</span -->
                                                                <span class="has-error" data-error="wrong"
                                                                      data-success="right">Postal Code is required.</span>
                                                        </div>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <!-- row end  -->

                                        <div class="payment-cart-form-row formatter" style="width: 250px">
                                            <a href="{{route('bowker.success')}}">
                                                <button type="submit" name="button"
                                                        class="btn-primary-small-2 btn-mar-p-1 order-link btn-size order-link herlper-btn">
                                                    Use this payment method
                                                </button>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <!-- payment form end -->
                            </div>

                            <div class="payment-cart-data-right">
                                <div class="payment-cart-data-right-content">
                                    <!-- subtotal start -->
                                    <div class="payment-cart-subtotal formatter">
                                        <div class="payment-cart-subtotal-content">
                                            <div class="payment-cart-subtotal-line formatter">
                                                <div class="payment-cart-subtotal-line-content">
                                                    Subtotal (1 item): <b>$149.99</b>
                                                </div>
                                            </div>

                                            <div class="payment-cart-subtotal-button formatter">
                                                <div class="payment-cart-subtotal-button-content">
                                                    <a href="{{route('bowker.success')}}">
                                                        <button type="submit" name="button"
                                                                class="btn-primary-small-2 btn-mar-p-1 order-link btn-size order-link herlper-btn">
                                                            Use this payment method
                                                        </button>
                                                    </a>
                                                </div>
                                            </div>

                                            <!-- order details start -->
                                            <div class="order-details formatter">
                                                <div class="order-details-content">
                                                    <div class="order-details-title">
                                                        <div class="order-details-title-content">
                                                            <b>Order Summary</b>
                                                        </div>
                                                    </div>

                                                    <div class="order-details-row">
                                                        <div class="order-details-row-content">
                                                            <div class="order-details-col order-details-col-1">
                                                                <div class="order-details-col-content text-left">
                                                                    Items
                                                                </div>
                                                            </div>
                                                            <div class="order-details-col order-details-col-2">
                                                                <div class="order-details-col-content text-right">
                                                                    $149.99
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>


                                                    <div class="order-details-row order-details-row-total">
                                                        <div class="order-details-row-content">
                                                            <div class="order-details-col order-details-col-1">
                                                                <div class="order-details-col-content text-left">
                                                                    <h6><b>Order Total</b></h6>
                                                                </div>
                                                            </div>
                                                            <div class="order-details-col order-details-col-2">
                                                                <div class="order-details-col-content text-right">
                                                                    <h6><b>$149.99</b></h6>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>


                                                </div>
                                            </div>

                                            <!-- order details end -->


                                        </div>
                                    </div>
                                    <!-- subtotal end -->


                                <!-- div class="payment-cart-suggestions">
                                            <div class="payment-cart-suggestions-content">
                                                <div class="payment-cart-suggestions-row">
                                                    <div class="payment-cart-suggestions-row-content">
                                                        <div class="payment-cart-suggestions-colleft">
                                                            <div class="payment-cart-suggestions-img">
                                                                <div class="payment-cart-suggestions-img-content">
                                                                    <img src="{{asset('images/cart-logo-getty.png')}}">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="payment-cart-suggestions-colright">

                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div -->


                                </div>
                            </div>


                        </div>
                        <!-- payment cart data end -->

                    {{ Form::close() }}


                    <!-- payment cart footer start -->

                        <div class="payment-cart-footer">
                            <div class="payment-cart-footer-content">
                                <div class="payment-cart-footer-title">
                                    <div class="payment-cart-footer-title-content">
                                        Auto Renewal
                                    </div>
                                </div>


                                <div class="payment-cart-footer-description">
                                    <div class="payment-cart-footer-description-content">
                                        Plans are set to Auto Renewal. You will be notified 30 days before your plan
                                        expires to cancel or to make changes. Once a billing period begins, no refunds
                                        will be made.

                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- payment cart footer end -->


                    </div>

                </div>
            </div>

        </div>
    </div>

    <script type="text/javascript" >
        window.onload = function () {
            var cleave = new Cleave('#name_card', {
                creditCard: true
            });

            var cleave_1 = new Cleave('#expedition_date', {
                date: true,
                datePattern: ['m', 'y']
            });

            var cleave_2 = new Cleave('#cvv', {
                numeral: true,
                stripLeadingZeroes: false,
                numeralIntegerScale: 4,
                delimiter: ''
            });

            $("#form-checkout").on("submit", function(){
                $('.overlay-checkout-bowker').css('display', 'block');
            })

        } ;
    </script>
@endsection
