<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
    <meta name="robots" content="noodp">
    <meta name="googlebot" content="index, follow">

    <meta name="Author" content="Storyrocket">

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset(mix('css/app.css')) }}" rel="stylesheet">

    <link rel="icon" href="{{asset('images/favicon.ico')}}" type="image/x-icon"/>
</head>
<body class="{{$profileView ?? ''}}">
<header class="s-header s-header--home">
    <nav class="s-wrap-header">
        <div class="nav-wrapper @guest  container @endguest ">
            @auth
                <a href="#">
                    <div class="father-burger">
                        <div class="burger">
                            <div class="line-burger1"></div>
                            <div class="upper"></div>
                            <div class="line-burger2"></div>
                            <div class="downer"></div>
                            <div class="line-burger3"></div>
                        </div>
                    </div>
                </a>
                <!--mobile-->
                <div id="nav-hamburger-mobile" class="button-collapse" data-activates="slide-out">
                    <i class="material-icons">menu</i>
                </div>
                <!--end-mobile-->
                <a id="logo-container" href="{{route('home')}}" class="left logo-dspl">
                    <img src="{{asset('images/logo-nav.svg')}}?v=1" alt="" class="logo-dash">
                </a>
            @else
                <a id="logo-container" href="{{route('home')}}" class="brand-logo">
                    <img src="{{asset('images/logo-nav.svg')}}?v=1" alt="" class="logo-on-nav">
                </a>
            @endauth

            @guest
                <ul id="nav-web" class="right hide-on-med-and-down">
                    <li>
                        <a href="#register-modal-1" class="nav-register modal-trigger">BECOME A MEMBER</a>
                    </li>
                    <li class="clr4">|</li>
                    <li>
                        <a class="nav-login" href="{{route('login')}}">SIGN IN</a>
                    </li>
                </ul>
                <!--mobil-->
                <ul id="slide-out" class="sidenav">
                    <div class="user-view">
                        <a href="#!user" class="center-align"><img class="circle-menu-mob"
                                                                   src="{{asset('images/logo-nav.svg')}}"></a>
                    </div>
                    <li><a href="#seekers" class="click-hr">WATCH VIDEO</a></li>
                    <li><a href="#faqs" class="click-hr">FAQ</a></li>
                    <br>
                    <li><a href="javascript:void(0);" class="_sin-here" data-type="welcom">BECOME A MEMBER</a></li>
                    <li><a href="{{route('login')}}">SIGN IN</a></li>
                </ul>
                <a href="#" data-activates="slide-out" class="button-collapse"><i class="material-icons">menu</i></a>
                <!--end-mobil-->
            @else
            <!-- logged start -->
                <div class="select-yourplan left mr-top">
                    <a href="{{route('plans')}}"
                       class="left-plans hover-search select-yourplan-link">
                        <div class="hover-search-content">
                            <span class="icon-plans icon-nav"></span>
                            <span class="txt-nav">SELECT YOUR PLAN</span>
                        </div>
                    </a>
                </div>
                <ul id="nav-web" class="right">

                    <li class="hover-search">
                        <a href="{{route('projects.create')}}?modal-create=1" class="btn-create-projects" data-tab="2">
                            <span class="icon-create icon-nav"></span>
                            <span class="txt-nav">CREATE A PROJECT</span>
                        </a>
                    </li>

                    <li class="hover-search">
                        <a href="{{route("search")}}">
                            <div class="hover-search-content">
                                <span class="icon-search2 icon-nav"></span>
                                <span class="txt-nav">ADVANCED SEARCH</span>
                            </div>
                        </a>
                    </li>
                    <li class="u-relative nav-web-icon">
                        <a href="https://www.storyrocket.com/mail">
                            <span class="icon-message2 alerts-nav"></span>
                        </a>
                    </li>
                    <li class="u-relative nav-web-icon">

                        <a href="javascript:void(0);" class="notifications-alert">
                            <div id="notf" style="/* visibility: hidden; */">
                                <span class="Notifications not-alert">0</span>
                            </div>
                            <div id="notificationBellAlert" style="display: none;">
                                <span class="Notifications not-alert">0</span>
                            </div>
                            <span class="icon-notifications alerts-nav" id="bell"></span>
                        </a>
                        <div class="notifications-alert-displayed w-notification hide" id="noti-1">

                        </div>
                    </li>
                    <li class="align-thumb-profile">

                        <a class="profileicon-minimenu"  href="javascript:void(0);"
                           data-target='dropdown-trigger-profileicon-content'>
                            <img src="{{$profile->photo}}"  alt="" class="thumb-profile image-avatar-user">
                        </a>
                        <div class="profileicon-minimenu-displayed hide">
                            <div class="triangle-up-noti"></div>
                            <ul class="w-notification-element">
                                <li class="box-profile-item">
                                    <a href="{{route('profile-show', $profile->url)}}" class="_ho-active font-14">My
                                        Profile</a>
                                </li>
                                <li class="box-profile-item">
                                    <a href="{{route('profile-index')}}"
                                       class="_ho-active font-14">Edit Profile</a>
                                </li>
                                <li class="box-profile-item">
                                    <a href="#"
                                       onclick="event.preventDefault();document.getElementById('logout-form').submit();"
                                       class="title _ho-active font-14">Logout</a>
                                </li>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                      style="display: none;">
                                    @csrf
                                </form>
                            </ul>
                        </div>
                        <!-- ul id='dropdown-trigger-profileicon-content' class='dropdown-content'>
                            <div class="triangle-up"></div>
                            <li class="box-profile-item">
                                <a href="{{route('profile-show', $profile->url)}}" class="_ho-active font-14">My
                                    Profile</a>
                            </li>
                            <li class="box-profile-item">
                                <a href="{{route('profile-index')}}"
                                   class="_ho-active font-14">Edit Profile</a>
                            </li>
                            <li class="box-profile-item">
                                <a href="#"
                                   onclick="event.preventDefault();document.getElementById('logout-form').submit();"
                                   class="title _ho-active font-14">Logout</a>
                            </li>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                  style="display: none;">
                                @csrf
                            </form>
                        </ul -->

                    </li>
                </ul>
                <!-- logged end -->

            @endguest

        </div>

    </nav>

</header>

<div class="s-header-bg">

</div>

<!-- modals start -->
@auth
    @include('projects.modals.create-out')
@endauth
<!-- modals end -->
<section>
    @yield('content')
</section>
<footer>
    <div class="container center-footer-area">
        <div class="row">
            <div class="col s12 m6 l10 txt-cnt">
                <div class="col s12 l4 txt-mobile">
                    <h4 class="title-footer">About Storyrocket</h4>
                    <p>
                    <p class="items-footer no-hover"><a href="{{route("home.aboutus")}}">About Us</a></p>
                    </a>
                    <!--<a href="mailto:helpdesk@Storyrocket.com?subject=Support">
                      <p class="items-footer">Support</p>
                    </a>-->
                    <!--<a href="investors" target="_blank">
                        <p class="items-footer">Investor Page</p>
                    </a>-->
                </div>
                <div class=" col s12 l4 no-pdg-left txt-mobile">
                    <h4 class="title-footer">Support</h4>
                    <a href="{{route("home.faqs")}}">
                        <p class="items-footer">FAQs</p>
                    </a>
                    <a href="mailto:helpdesk@Storyrocket.com?subject=Open a ticket">
                        <p class="items-footer">Help Desk</p>
                    </a>
                    <a href="{{route("home.tutorials")}}">
                        <p class="items-footer">Tutorials</p>
                    </a>
                </div>
                <div class=" col s12 l4 no-pdg-left txt-mobile">
                    <h4 class="title-footer">Legal</h4>
                    <a href="{{route("home.privacyPolicy")}}">
                        <p class="items-footer">Privacy Policy and GDPR</p>
                    </a>
                    <a href="{{route("home.termOfService")}}">
                        <p class="items-footer">Terms of Service</p>
                    </a>
                    <a href="{{route("home.copyright")}}">
                        <p class="items-footer">Copyright</p>
                    </a>
                </div>
            </div>

            <div class="no-pdg-left col s12 m6 l2 txt-cnt footer-socialmedia">
                <div class=" col s12 l12">
                    <h4 class="title-footer txt-mobile">Follow us</h4>
                    <div class="flex-container txt-mobile no-flex-mobile list-footer-redes">
                        <a href="https://www.facebook.com/storyrocketweb/" target="_blank"><i
                                    class="icon-footer-facebook icons-redes"></i></a>
                        <a href="https://twitter.com/storyrocketweb" target="_blank"><i
                                    class="icon-footer-twitter icons-redes"></i></a>
                        <a href="https://www.instagram.com/storyrocketweb/" target="_blank"><i
                                    class="icon-footer-instagram icons-redes"></i></a>
                    </div>
                </div>
            </div>

            <div class="col s12 m12 l7 xl7" id="footer-res1">

                <ul class="collapsible" data-collapsible="accordion" id="acord-1-nw">
                    <li>
                        <div class="collapsible-header acor-1-nw">About Storyrocket <i class="material-icons">keyboard_arrow_down</i>
                        </div>
                        <div class="collapsible-body">
                            <p class="nw-fot-1">
                                <a href="{{route("home.aboutus")}}">About Us</a>
                            </p>
                            <p class="nw-fot-1">
                                <a href="mailto:helpdesk@Storyrocket.com?subject=Support">Support</a>
                            </p>
                            <p class="nw-fot-1">
                                <a href="investors">Innversor Page</a>
                            </p>
                        </div>
                    </li>
                    <li>
                        <div class="collapsible-header acor-1-nw">Support<i
                                    class="material-icons">keyboard_arrow_down</i></div>
                        <div class="collapsible-body">
                            <p class="nw-fot-1">
                                <a href="{{route("home.faqs")}}">FAQs</a>
                            </p>
                            <p class="nw-fot-1">
                                <a href="mailto:helpdesk@Storyrocket.com?subject=Open a ticket">Open a ticket</a>
                            </p>
                            <p class="nw-fot-1">
                                <a href="{{route("home.tutorials")}}">Tutorials</a>
                            </p>
                        </div>
                    </li>
                    <li>
                        <div class="collapsible-header acor-1-nw">Legal<i class="material-icons">keyboard_arrow_down</i>
                        </div>
                        <div class="collapsible-body">
                            <p class="nw-fot-1">
                                <a href="{{route("home.privacyPolicy")}}">Privacy Policy</a>
                            </p>
                            <p class="nw-fot-1">
                                <a href="{{route("home.termOfService")}}">Terms of Service</a>
                            </p>
                            <p class="nw-fot-1">
                                <a href="{{route("home.copyright")}}">Copyright</a>
                            </p>
                        </div>
                    </li>
                    <li>
                        <div class="collapsible-header acor-1-nw">Follow Us <i class="material-icons">keyboard_arrow_down</i>
                        </div>
                        <div class="collapsible-body">
                            <p class="nw-fot-1">
                                <a href="https://www.facebook.com/storyrocketweb/">Facebook</a>
                            </p>
                            <p class="nw-fot-1">
                                <a href="https://twitter.com/storyrocketweb">Twitter</a>
                            </p>
                            <p class="nw-fot-1">
                                <a href="https://www.instagram.com/storyrocketweb/">Instagram</a>
                            </p>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="col s12 m12 " id="fotter-last">
                <p class="">© 2018 Storyrocket</p>
            </div>

        </div>
    </div>
    <br>
    <div class="footer-below">
        <div class="container">
            <div class="by-storyrocket2">
                <p style="margin: 0 auto;">© 2018 Storyrocket</p>
            </div>
        </div>
    </div>
</footer>
    <!--<div id="modal111" class="modal modal-main-video-index toggle-close">
    <div class="modal-content modal-video-popup">
  		<iframe width="100%" height="490" class="embedly-embed" src="https://www.youtube.com/embed/EATZOfw1H94?rel=0&amp;autoplay=0&amp;showinfo=0&amp;enablejsapi=1" scrolling="no" frameborder="0" allowfullscreen="" id="popup-youtube-player"></iframe>
    </div> -->
  </div>
<script>
    window.auth = {{ filter_var(auth()->check(), FILTER_VALIDATE_BOOLEAN) ? 'true' : 'false'}};

    window.production = {{ filter_var(env('PRODUCTION'), FILTER_VALIDATE_BOOLEAN) ? 'true' : 'false'}};
</script>
<!-- Scripts -->
<script src="{{ asset(mix('js/manifest.js')) }}"></script>
<script src="{{ asset(mix('js/vendor.js')) }}"></script>
<script src="{{ asset(mix('js/app.js')) }}"></script>
<script src="{{ asset(mix('js/lib.js')) }}"></script>
</body>
</html>

@include('profile.partials.profile-view-notification-t')
@include('profile.partials.profile-view-notification-empty-t')
@include('profile.partials.loader')

