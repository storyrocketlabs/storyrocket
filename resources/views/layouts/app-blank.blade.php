<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset(mix('css/app.css')) }}" rel="stylesheet">

    <link rel="icon" href="{{asset('images/favicon.ico')}}" type="image/x-icon" />
</head>
<body class="bck-gris">
<div id="app">
    @yield('content')
</div>
<!-- Scripts -->
<script src="{{ asset(mix('js/manifest.js')) }}" defer></script>
<script src="{{ asset(mix('js/vendor.js')) }}" defer></script>
<script src="{{ asset(mix('js/app.js')) }}" defer></script>
<script src="{{ asset(mix('js/lib.js')) }}" defer></script>

<script>
    window.production = {{ filter_var(env('PRODUCTION'), FILTER_VALIDATE_BOOLEAN) ? 'true' : 'false'}};
</script>
</body>
</html>
<!--<div id="modal111" class="modal modal-main-video-index toggle-close">
    <div class="modal-content modal-video-popup">
  		<iframe width="550" height="550" class="embedly-embed" src="https://www.youtube.com/embed/EATZOfw1H94?rel=0&amp;autoplay=0&amp;showinfo=0&amp;enablejsapi=1" scrolling="no" frameborder="0" allowfullscreen="" id="popup-youtube-player"></iframe>
    </div>
</div>-->
