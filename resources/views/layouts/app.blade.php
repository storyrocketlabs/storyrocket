<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
    <meta name="robots" content="noodp">
    <meta name="googlebot" content="index, follow">

    <meta name="Author" content="Storyrocket">

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset(mix('css/app.css')) }}" rel="stylesheet">

    <link rel="icon" href="{{asset('images/favicon.ico')}}" type="image/x-icon"/>
</head>
<body class="{{$profileView ?? ''}}">
<header class="s-header s-header--home">
    <nav class="s-wrap-header">
        <div class="nav-wrapper @guest  container @endguest ">
            @auth
                <a href="https://www.storyrocket.com">
                    <div class="father-burger">

                        <div class="burger">
                            <div class="line-burger1"></div>
                            <div class="upper"></div>
                            <div class="line-burger2"></div>
                            <div class="downer"></div>
                            <div class="line-burger3"></div>
                        </div>
                    </div>
                </a>
                <a id="logo-container" href="{{route('home')}}" class="left logo-dspl">
                    <img src="{{asset('images/logo-nav.svg')}}" alt="" class="logo-dash">
                </a>
            @else
                <a id="logo-container" href="https://www.storyrocket.com" class="brand-logo">
                    <img src="{{asset('images/logo-nav.svg')}}" alt="" class="logo-on-nav">
                </a>
            @endauth
            @guest
                <ul id="nav-web" class="right">
                    <li>
                        <a href="#register-modal-1" class="nav-register modal-trigger">BECOME A MEMBER</a>
                    </li>
                    <li class="clr4">|</li>
                    <li>
                        <a class="nav-login" href="{{route('login')}}">SIGN IN</a>
                    </li>
                </ul>
            @else
                @if(isset($profile))
                    @if(!empty($profile))
                    <!-- logged start -->
                        <div class="select-yourplan left mr-top">
                            <a href="{{route('plans')}}"
                               class="left-plans hover-search select-yourplan-link">
                                <div class="hover-search-content">
                                    <span class="icon-plans icon-nav"></span>
                                    <span class="txt-nav">SELECT YOUR PLAN</span>
                                </div>
                            </a>
                        </div>

                        <ul id="nav-web" class="right">
                            <li class="hover-search">
                                <a href="/projects/create" class="modal-trigger ">
                                    <div class="hover-search-content">
                                        <span class="icon-create icon-nav"></span>
                                        <span class="txt-nav">CREATE A PROJECT</span>
                                    </div>
                                </a>
                            </li>
                            <li class="hover-search">
                                <a href="{{route("search")}}">
                                    <div class="hover-search-content">
                                        <span class="icon-search2 icon-nav"></span>
                                        <span class="txt-nav">ADVANCED SEARCH</span>
                                    </div>
                                </a>
                            </li>
                            <li class="u-relative nav-web-icon">
                                <a href="https://www.storyrocket.com/mail">
                                    <div id="notf" style="/* visibility: hidden; */">
                                        <span class="mail-notifications">{{count($count_inbox)}}</span>
                                    </div>
                                    <span class="icon-message2 alerts-nav"></span>
                                </a>
                            </li>
                            <li class="u-relative nav-web-icon">
                                <a href="javascript:void(0);" class="notifications-alert">
                                    
                                    <span class="icon-notifications alerts-nav" id="bell"></span>
                                </a>
                                <ul class="w-notification hide" id="noti-1">
                                    <div class="triangle-up-noti"></div>
                                </ul>
                            </li>
                            <li class="align-thumb-profile">

                                <a class="dropdown-trigger-profileicon" href="{{$profile->url}}"
                                   data-target='dropdown-trigger-profileicon-content'>
                                    <img src="{{empty($profile->Photo) ? (empty($profile->FacebookPhoto) ? asset('images/avatar_default_user.png')  : $profile->FacebookPhoto ) : $profile->Photo}}"
                                         alt="" class="thumb-profile image-avatar-user">
                                </a>
                                <ul id='dropdown-trigger-profileicon-content' class='dropdown-content'>
                                    <div class="triangle-up"></div>
                                    <li class="box-profile-item">
                                        <a href="{{url("/")}}{{$profile->url}}" class="_ho-active font-14">My
                                            Profile</a>
                                    </li>
                                    <li class="box-profile-item">
                                        <a href="{{url("/")}}{{$profile->url}}"
                                           class="_ho-active font-14">Edit Profile</a>
                                    </li>
                                    <li class="box-profile-item">
                                        <a href="https://www.storyrocket.com/signin/close" class="title _ho-active font-14">Logout</a>
                                    </li>
                                    <form id="logout-form" action="https://www.storyrocket.com/signin/close" method="POST"
                                          style="display: none;">
                                        @csrf
                                    </form>
                                </ul>

                            </li>
                        </ul>

                        <!-- logged end -->

                    @endif
                @endif
            @endif

        </div>
    </nav>
</header>

<div class="s-header-bg">

</div>

<!-- modals start -->
@auth

    @include('projects.modals.create-out')
    @include('projects.modals.create-assistant')
@endauth
<!-- modals end -->
<section>
    @yield('content')
</section>
<footer>
    <div class="container center-footer-area">
        <div class="row">
            <div class="col s12 m6 l10 txt-cnt">
                <div class="col s12 l4 txt-mobile">
                    <h4 class="title-footer">About Storyrocket</h4>
                    <p>
                    <p class="items-footer no-hover"><a href="{{route("home.aboutus")}}">About Us</a></p>
                    </a>
                    <!--<a href="mailto:helpdesk@Storyrocket.com?subject=Support">
                      <p class="items-footer">Support</p>
                    </a>-->
                    <!--<a href="investors/index.html" target="_blank">
                        <p class="items-footer">Investor Page</p>
                    </a>-->
                </div>
                <div class=" col s12 l4 no-pdg-left txt-mobile">
                    <h4 class="title-footer">Support</h4>
                    <a href="faqs">
                        <p class="items-footer">FAQs</p>
                    </a>
                    <a href="mailto:helpdesk@Storyrocket.com?subject=Open a ticket">
                        <p class="items-footer">Help Desk</p>
                    </a>
                    <a href="/tutorials">
                        <p class="items-footer">Tutorials</p>
                    </a>
                </div>
                <div class=" col s12 l4 no-pdg-left txt-mobile">
                    <h4 class="title-footer">Legal</h4>
                    <a href="privacy-policy">
                        <p class="items-footer">Privacy Policy and GDPR</p>
                    </a>
                    <a href="terms-of-service">
                        <p class="items-footer">Terms of Service</p>
                    </a>
                    <a href="copyright">
                        <p class="items-footer">Copyright</p>
                    </a>
                </div>
            </div>

            <div class="no-pdg-left col s12 m6 l2 txt-cnt footer-socialmedia">
                <div class=" col s12 l12">
                    <h4 class="title-footer txt-mobile">Follow us</h4>
                    <div class="flex-container txt-mobile no-flex-mobile list-footer-redes">
                        <a href="https://www.facebook.com/storyrocketweb/" target="_blank"><i
                                    class="icon-footer-facebook icons-redes"></i></a>
                        <a href="https://twitter.com/storyrocketweb" target="_blank"><i
                                    class="icon-footer-twitter icons-redes"></i></a>
                        <a href="https://www.instagram.com/storyrocketweb/" target="_blank"><i
                                    class="icon-footer-instagram icons-redes"></i></a>
                    </div>
                </div>
            </div>

            <div class="col s12 m12 l7 xl7" id="footer-res1">

                <ul class="collapsible" data-collapsible="accordion" id="acord-1-nw">
                    <li>
                        <div class="collapsible-header acor-1-nw">About Storyrocket <i class="material-icons">keyboard_arrow_down</i>
                        </div>
                        <div class="collapsible-body">
                            <p class="nw-fot-1">
                                <a href="https://www.storyrocket.com/about">About Us</a>
                            </p>
                            <p class="nw-fot-1">
                                <a href="mailto:helpdesk@Storyrocket.com?subject=Support">Support</a>
                            </p>
                            <p class="nw-fot-1">
                                <a href="https://www.storyrocket.com/investors/index.html">Innversor Page</a>
                            </p>
                        </div>
                    </li>
                    <li>
                        <div class="collapsible-header acor-1-nw">Support<i
                                    class="material-icons">keyboard_arrow_down</i></div>
                        <div class="collapsible-body">
                            <p class="nw-fot-1">
                                <a href="https://www.storyrocket.com/faqs">FAQs</a>
                            </p>
                            <p class="nw-fot-1">
                                <a href="mailto:helpdesk@Storyrocket.com?subject=Open a ticket">Open a ticket</a>
                            </p>
                            <p class="nw-fot-1">
                                <a href="/tutorials">Tutorials</a>
                            </p>
                        </div>
                    </li>
                    <li>
                        <div class="collapsible-header acor-1-nw">Legal<i class="material-icons">keyboard_arrow_down</i>
                        </div>
                        <div class="collapsible-body">
                            <p class="nw-fot-1">
                                <a href="https://www.storyrocket.com/privacy-policy">Privacy Policy</a>
                            </p>
                            <p class="nw-fot-1">
                                <a href="https://www.storyrocket.com/terms-of-service">Terms of Service</a>
                            </p>
                            <p class="nw-fot-1">
                                <a href="https://www.storyrocket.com/copyright">Copyright</a>
                            </p>
                        </div>
                    </li>
                    <li>
                        <div class="collapsible-header acor-1-nw">Follow Us <i class="material-icons">keyboard_arrow_down</i>
                        </div>
                        <div class="collapsible-body">
                            <p class="nw-fot-1">
                                <a href="https://www.facebook.com/storyrocketweb/">Facebook</a>
                            </p>
                            <p class="nw-fot-1">
                                <a href="https://twitter.com/storyrocketweb">Twitter</a>
                            </p>
                            <p class="nw-fot-1">
                                <a href="https://www.instagram.com/storyrocketweb/">Instagram</a>
                            </p>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="col s12 m12 " id="fotter-last">
                <p class="">© 2018 Storyrocket</p>
            </div>

        </div>
    </div>
    <br>
    <div class="footer-below">
        <div class="container">
            <div class="by-storyrocket2">
                <p style="margin: 0 auto;">© 2018 Storyrocket</p>
            </div>
        </div>
    </div>
</footer>

<!-- Scripts -->
<script src="{{ asset(mix('js/manifest.js')) }}"></script>
<script src="{{ asset(mix('js/vendor.js')) }}"></script>
<script src="{{ asset(mix('js/app.js')) }}"></script>

<script src="{{ asset(mix('js/lib.js')) }}"></script>

<script>
    window.production = {{ filter_var(env('PRODUCTION'), FILTER_VALIDATE_BOOLEAN) ? 'true' : 'false'}};
</script>
</body>
</html>
