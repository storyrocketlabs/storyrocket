@extends('layouts.app-main')

@section('content')
    <div class="cover-page cover-page-in cover-page-1 cover-page-groups s-groups">
        <div class="cover-page-content">
            <div class="cover-accordion-container">
                <!-- slide left start -->
                    @auth
                        @include('profile.partials.slideleft')
                    @endauth
                <!-- slide left end -->

                <div class="page-content-wrapper page-content-wrapper-groups" style="background-color: #e6ecf0;">
                    <section class="cont-general">
                        <!--inicio grupos-->
                            <section id="groups">
                                <article class="row">
                                    <article class="container">
                                        <div class="col s12 m12 l3 xl3">
                                            <div class="center-align group-mr">
                                                <button class="btm-creat-g-e btn-create-groups modal-trigger" href="#modal1" data-type="one" data-id="0">
                                                    <span>CREATE A GROUP</span>
                                                </button>
                                            </div>
                                            <div class="center-align new-d-search">
                                                <div class="search-g-3">
                                                    <form action="#" class="cont-sort">
                                                        <p class="title-font-g">SORT BY</p>
                                                        <p>
                                                            <label>
                                                                <input class="with-gap check-groups" name="group1" type="radio" id="test2" data-type="my-grups" data-text="My Groups">
                                                                <span class="label-font-g">My Groups</span>
                                                            </label>
                                                           
                                                        </p>
                                                        <p>
                                                            <label>
                                                                <input class="with-gap check-groups" name="group1" type="radio" id="test1" data-type="popular-groups" data-text="Popular Groups">
                                                                <span class="label-font-g">Popular Groups</span>
                                                            </label>
                                                        </p>
                                                        <p>
                                                            <label>
                                                                <input class="with-gap check-groups" name="group1" type="radio" id="test3" data-type="new-groups" data-text="New Groups">
                                                                <span class="label-font-g">New Groups</span>
                                                            </label>
                                                        </p>
                                                        <!--<div class="left-align group-mr2">
                                                        <label class="title-font-g">LOCATION</label>
                                                        <input class="input-g" type="text" name="" placeholder="All">
                                                        <p class="sub-font">Enter a city, state, or country</p>
                                                        </div>
                                                        <button class="btn-search-g">Search</button>-->
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col s12 m12 l9 xl9">
                                            <div class="group-mr5">
                                                <span class="font-title-2" id="title-searchs">Popular Groups</span>
                                            </div>
                                            <div class="cnt-grs-all">
                                                <div class="row cont-general-group">
                                                    <div class="col s12 m12 l3 xl3">
                                                        <div class="img-g-cont">
                                                            <a href="https://www.storyrocket.com/groups/beating-windward-press" alt="Beating Windward Press" title="Beating Windward Press">
                                                                <img src="https://res.cloudinary.com/storyrocket-llc/image/fetch/dpr_1,f_auto/https://www.storyrocket.com/public/images/beating-windward-press/thumb_banner-59134e84a06ab.jpg" data-src="https://www.storyrocket.com/public/images/beating-windward-press/thumb_banner-59134e84a06ab.jpg" alt="Beating Windward Press" class="responsive-img lazy-image">
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="col s12 m12 l8 xl8 group-mr4">
                                                        <div class="gi-1 truncate">
                                                            <span class="font-g-title1 truncate">
                                                                <a href="https://www.storyrocket.com/groups/beating-windward-press" 
                                                                    title="Beating Windward Press" alt="Beating Windward Press">
                                                                        Beating Windward Press
                                                                </a>
                                                            </span>
                                                        </div>
                                                        <div class="gi-2">
                                                            <i class="material-icons">location_on</i>
                                                            <span class="font-loca-g">San Francisco, California</span>
                                                        </div>
                                                        <p class="font-g-p">
                                                            Books for Books’ Sake.Beating Windward Press is an independent publisher of novels, 
                                                            short story collections, and non-fiction by emerging authors. Our books reflect the 
                                                            individual tastes of our staff – mostly cross-genre fiction with a literary edge. 
                                                            Beating Windward offers books with distinct voices and perspectives that we hope 
                                                            brings pleasure, knowledge, and entertainment to our readers.
                                                        </p>
                                                        <span class="font-c-t">
                                                            Create by 
                                                            <strong>
                                                                <a href="https://www.storyrocket.com/Matthew.fkrekgdw" 
                                                                    title="Beating Windward Press" alt="Beating Windward Press">
                                                                    Beating Windward Press
                                                                </a>
                                                            </strong>
                                                        </span>
                                                        <div class="group-mr3">
                                                            <div class="user-event bloque-event">
                                                                <figure class="not-margin couponcode tooltip-load" data-type="users" data-key="10-17-19">
                                                                    <img src="https://res.cloudinary.com/storyrocket-llc/image/fetch/dpr_1,f_auto/https://storyrocket-aws3.s3.amazonaws.com/2110ad32.jpg" 
                                                                    data-src="https://storyrocket-aws3.s3.amazonaws.com/2110ad32.jpg" alt="Christina Crall-Reed" 
                                                                    class="responsive-img lazy-image">
                                                                    <div class="tol-dinac"></div>
                                                                </figure>
                                                            </div>
                                                            <div class="user-event bloque-event">
                                                            <figure class="not-margin couponcode tooltip-load" data-type="users" data-key="10-21-20">
                                                                <img src="https://res.cloudinary.com/storyrocket-llc/image/fetch/dpr_1,f_auto/https://storyrocket-aws3.s3.amazonaws.com/0e4d24c2.jpg" 
                                                                    data-src="https://storyrocket-aws3.s3.amazonaws.com/0e4d24c2.jpg" alt="Peter M. Gordon" class="responsive-img lazy-image">
                                                                <div class="tol-dinac"></div>
                                                            </figure>
                                                        </div>
                                                        <div class="user-group2">
                                                            <p class="font-group3 no-margin">+17</p>
                                                        </div>
                                                        <button class="boton-join SeeMore4" data-list="13">
                                                            <span>Join Group</span>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row cont-general-group">
                                                <div class="col s12 m12 l3 xl3">
                                                    <div class="img-g-cont">
                                                        <a href="https://www.storyrocket.com/groups/indie-beacon" alt="Indie Beacon" title="Indie Beacon">
                                                            <img src="https://res.cloudinary.com/storyrocket-llc/image/fetch/dpr_1,f_auto/https://storyrocket-aws3.s3.amazonaws.com/banner-211x1925a99993195ade1520015665sr_jpg" data-src="https://storyrocket-aws3.s3.amazonaws.com/banner-211x1925a99993195ade1520015665sr_jpg" alt="Indie Beacon" class="responsive-img lazy-image">
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="col s12 m12 l8 xl8 group-mr4">
                                                    <div class="gi-1 truncate">
                                                        <span class="font-g-title1 truncate"><a href="https://www.storyrocket.com/groups/indie-beacon" title="Indie Beacon" alt="Indie Beacon">Indie Beacon</a></span>
                                                    </div>
                                                    <div class="gi-2">
                                                        <i class="material-icons">location_on</i>
                                                        <span class="font-loca-g">San Francisco, California</span>
                                                    </div>
                                                    <p class="font-g-p">Indie Authors supporting each other helping each member grow stronger and have their voices heard. </p>
                                                    <span class="font-c-t">Create by <strong><a href="https://www.storyrocket.com/B.pw82kr1c" title="B Alan Bourgeois" alt="B Alan Bourgeois">B Alan Bourgeois</a></strong></span>
                                                    <div class="group-mr3">
                                                        <div class="user-event bloque-event">
                                                            <figure class="not-margin couponcode tooltip-load" data-type="users" data-key="7-16-23">
                                                                <img src="https://res.cloudinary.com/storyrocket-llc/image/fetch/dpr_1,f_auto/https://storyrocket-aws3.s3.amazonaws.com/704e9ba3.jpg" data-src="https://storyrocket-aws3.s3.amazonaws.com/704e9ba3.jpg" alt="B Alan Bourgeois" class="responsive-img lazy-image">
                                                                <div class="tol-dinac"></div>
                                                            </figure>
                                                        </div>
                                                        <div class="user-group2">
                                                            <p class="font-group3">+1</p>
                                                        </div>
                                                        <button class="btn-default-request">
                                                            <span>Request Send</span>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </article>
                                </article>
                            </section>
                        <!--end grupos-->
                    </section>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal Structure -->
  <div id="modal1" class="modal modal-initial">
    <div class="modal-content">
    <div class="w-event-create">
		 	<div class="part1-event">
			 	<i class="material-icons">group</i>
			 	<span class="font-part1">
			 					 			Create Group
			 					 	</span>
			 	<div class="icon-close-evnt modal-close">
				 	<i class="material-icons">close</i>
			 	</div>
		 	</div>
		 	<div class="part1-event-1">
				<form action="#" id="frm-groups">
					<div class="conte-one">
						<div>
							<label class="lebel-co">Group Name*</label>
							<input class="input-event-f required" type="text" value="" name="title" id="namegroup" data-name="title">
						</div>
					 	<div>
							<label class="lebel-co">Location *</label>
							<input class="input-event-f required" type="text" value="" name="address" id="address" data-name="address" placeholder=" " autocomplete="off">
						
						</div>
													<div class="u-relative _gr_x">
							 	<label class="lebel-co">Invite Member</label>
							 	<div class="losders-menbers ">
						 									 			<input class="input-event-f " type="text" id="member_name" name="" placeholder="Add Name">
							 	</div>
								<ul class="resultado"></ul>
							 	<input type="hidden" id="menbers" data-name="idusers">
						 	</div>
						 						<div>
							<label class="lebel-co">Description (500 characters)</label>
							<textarea class="input-event-t required" id="descript" name="descript" data-length="500" data-name="descript"></textarea>
						</div>
						<input type="hidden" name="op" data-name="op" value="i">
						<button class="btn-create-e-p ic-new-g btnCreateGroup _xspar ladda-button" data-style="slide-right" data-op="i">			
								<span class="ladda-label">CREATE GROUP</span>
							</button>
			            					</div>
					<div class="conte-two hide">
						<div class="col s6 m6 cont-check mar-aux-form">
              <p>
              	<input name="group1" type="radio" id="test29" data-name="public" value="1">
                <label class="line-h" for="test29"><i class="material-icons">lock_open</i> <span class="check-font"><strong class="s-font txt-uppers">Public group</strong><br> Anyone can see the group, its members and their posts.</span></label>
              </p>
            </div>
            <div class="col s6 m6 cont-check">
              <p>
                <input name="group1" type="radio" id="test30" data-name="close" value="2">
                <label class="line-h" for="test30"><i class="material-icons">lock_outline</i> <span class="check-font"><strong class="s-font txt-uppers">Closed  group</strong><br> Anyone can find the group. Only members can see posts.</span></label>
              </p>
            </div>
            <div class="col s6 m6 cont-check">
              <p>
                <input name="group1" type="radio" id="test31" data-name="secret" value="3">
                <label class="line-h" for="test31"><i class="material-icons ico-sercts">vpn_key</i> <span class="check-font"><strong class="s-font txt-uppers">Secret  group</strong><br> Only members can find the group and see posts.</span></label>
              </p>
            </div>
            	<input type="hidden" name="op" data-name="op" value="i">
				<button class="btn-create-e-p ic-new-g btn-createGroups" data-op="i">
	              <span class="spn-create">CREATE GROUP</span>
	            </button>

					</div>
				</form>
		 </div>
	 </div>
    </div>
  </div>
@endsection
