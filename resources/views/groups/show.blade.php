@extends('layouts.app-main')

@section('content')
    <div class="cover-page cover-page-in cover-page-1 cover-page-groups s-groups">
        <div class="cover-page-content">
            <div class="cover-accordion-container">
                <!-- slide left start -->
                    @auth
                        @include('profile.partials.slideleft')
                    @endauth
                <!-- slide left end -->

                <div class="page-content-wrapper page-content-wrapper-groups" style="background-color: #e6ecf0;">
                    <div class="page-content-wrapper idpage" data-type="groups">
                        <section class="row" id="view-group-1">
                            <article class="had-container-g row">
                                <input type="file" id="crop-banner" style="left: 0;top: 0;height: 0;" data-action="image_banner" data-title="Upload image banner" class="cropit-image-poster u-none upload" name="btn-crop[]" required="" accept="image/*">
                                <div id="upload-header-layout">
                                    <img src="https://res.cloudinary.com/storyrocket-llc/image/fetch/dpr_0.8999999761581421,f_auto/https://www.storyrocket.com/public/images/mariachi-paraiso-writers/banner-58e4001a08e26.jpg" data-src="https://www.storyrocket.com/public/images/mariachi-paraiso-writers/banner-58e4001a08e26.jpg" alt="Mariachi Paraiso Writers" class="u-relative j-image-g head-drag _xleft lazy-image" id="img-drag" style="top:-46px">
                                    <input type="hidden" id="txtTop" name="" value="">
                                </div>
                                <article class="row g-barr-view">
                                    <div class="col s12 m12 l12 xl12">
                                        <div class="col s12 m12 l4 xl4 ">
                                            <div class="col s12 m12 l12 xl12 gi-10 pad-group1 space-left_x3">
                                                <h5 class="title-group-v">Mariachi Paraiso Writers</h5>
                                                <i class="material-icons ico-typs hide">lock_open</i>
                                                <span class="font-p-1 text-typs hide">Public Group</span>
                                            </div>
                                        </div>
                                        <div class="col s12 m12 l8 xl8 center-align">
                                            <div class="col s4 m4 l5 xl5 pad-group2"></div>
                                            <div class="col s4 m4 l3 xl3">
                                                <div class="menber-cont-g">
                                                    <div id="cnts-usrs" class="inline">
                                                        <div class="perfil-g-view-g" id="members-14-23">
                                                            <figure class="not-margin">
                                                                <img src="https://res.cloudinary.com/storyrocket-llc/image/fetch/dpr_0.8999999761581421,f_auto/https://storyrocket-aws3.s3.amazonaws.com/35a82d60.jpg" data-src="https://storyrocket-aws3.s3.amazonaws.com/35a82d60.jpg" class="responsive-img lazy-image" alt="Ronald Karasz">
                                                            </figure>
                                                        </div>
                                                    </div>
                                                    <div class="menber-dis">
                                                        <p><strong class="num-menbers">1</strong> members</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col s4 m4 l4 xl4 ">
                                                <div class="pad-group3 ">
                                                    <div class="uploader-btn hide">
                                                        <button class="btn-send-1 save-banner-gbv" data-type="group" data-ip="7">Save</button>
                                                        <button class="btn-cancel-1-1">Cancel</button>
                                                    </div>
                                                    <button class="add-grb btn-join-g " ajaxify="groups/new_member_group?rel=7&amp;tpl=groups">
                                                        <i class="material-icons">add</i><span>Join Group</span>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </article>
                            </article>
                        </section>
                    </div>
                    <section class="row sec-nav-g" id="">
                        <article class="container">
                            <ul class="tabs tab-demo no-shadow text-nav-g">
                                <li class="tab padding-tb-a"><a href="#test16" class="active">Group Posts (<span class="post-count">3</span>)</a></li>
                                <li class="tab padding-tb-a"><a href="#test17" class="u-relative">Members (1)</a></li>
                                <li class="tab padding-tb-a"><a href="#test18" class="">Events (0)</a></li>
                                <li class="tab padding-tb-a"><a href="#test19" class="">Photos (1)</a></li>
                                <li class="indicator" style="right: 1100px; left: 0px;"></li>
                            </ul>
                        </article>
                    </section>
                    <section id="ideo-1">
                        <section class="row mar-group1">
                            <section class="container">
                                <article class="col s12 m12 l8 xl9" id="page-cont-1">
                                    <div id="test16" class="col s12 active" style="display: block;">
                                        <div class="file-fields ">
                                            <button class="btn-w">
                                                <i class="material-icons">create</i>
                                                <span>Post Something</span>
                                            </button>
                                            <button class="btn-add add-photos u-relative">
                                                <i class="material-icons">insert_photo</i>
                                                <span>Add Photo</span>
                                                <input class="upload-images-groups" id="btnAddPhotos" type="file" name="photos[]" multiple="multiple" accept="image/jpeg,image/png,image/gif">
                                            </button>
                                        </div>
                                        <form id="frm-comment-group" method="post" enctype="multipart/form-data">
                                            <div class="border-text-w conte-previews">
                                                <textarea class="text-w required" name="post-text" id="post-text"></textarea>
                                            </div>
                                            <div class="left-align mar-mod-1">
                                                <div class="pross hide">
                                                    <div class="progress progress-form">
                                                        <div class="determinate determinate-r indeterminate "></div>
                                                    </div>
                                                    <p class="message-photos text-porcentaje">0%</p>
                                                </div>
                                                <button type="button" class="btn-type-1 ladda-button" data-spinner-color="#246eb9" data-color="blue" data-style="slide-right" id="btnPost" data-type="groups" data-rel="13-20-23">
                                                    <span class="ladda-label"><i class="material-icons">send</i> Post</span>
                                                </button>
                                            </div>
                                        </form>
                                        <div class="msj-groups hide">
                                            Join this public group, you can see the discussion, post and coments
                                            <button onclick="window.location.reload()" class="add-grb btn-join-g " ajaxify="groups/new_member_group?rel=7&amp;tpl=groups">
                                                <i class="material-icons">add</i><span>Join Group</span>
                                            </button>
                                        </div>
                                        <!-- primer modulo-->
                                        <div class="loader-module-groups ">
                                            <div class="mar-mod-2 view-post" id="13-22-20-95-68-91-90-65-92-86-227-128-91-74-116-115-121-119-119">
                                                <div class="card-mod-1">
                    	                            <div class="card-mod-1-1">
                                                        <div class="card-mod-img">
                                                            <a href="https://www.storyrocket.com/Ron.82c040rm" alt="Ronald Karasz">
                                                                <img src="https://storyrocket-aws3.s3.amazonaws.com/35a82d60.jpg" alt="Ronald Karasz" class="responsive-img">
                                                            </a>
                                                        </div>
                                                        <div class="card-mod-p">
                                                            <p class="font-mod-1">
                         	                                  <a href="https://www.storyrocket.com/Ron.82c040rm" title="Ronald Karasz" alt="Ronald Karasz">Ronald Karasz</a>
                                                            </p>
                                                            <span class="font-mod-2">One year ago</span>
                                                        </div>
                                                    </div>
                                                    <div class="card-mod-1-2">
                                                        <p class="font-mod-3">Hi Michael, wanna join the group</p>
                                                        <figure class="img-post open-images">
                                                            <a href="https://storyrocket-aws3.s3.amazonaws.com/full5a1048f12abf31511016689sr_.png" class="img-prints" data-type="one" id="13-19-16">
                                                                <img src="https://storyrocket-aws3.s3.amazonaws.com/thumb-671x4475a1048f12abf31511016689sr_.png" alt="poster" class="responsive-img img-prin ">
                                                            </a>
                                                        </figure>
                                                    </div>
                                                    <div class="card-mod-1-3 right-align ">
                                                        <div class="display-new-gr gi-12 card-likes">
                    	                                   <i class="icon10-heart"></i>
                    	                                   <span class="fo-icon1  _1x5- _5x1_ _50-k"><span class="_cont">0</span></span>
                    	                                   <i class="icon15-icon-chat1"></i>
                    	                                   <span class="fo-icon2 counter-coments">0</span>
                                                        </div>
                                                        <a class="icon-comet1 icon-m1 SRLikeLink" data-key="14-21-20-91" post-key="13-19-16">
                                                            <i class=" icon10-heart-outline"></i>
                                                            <span class="font-l-new">Like</span>
                                                        </a>
                                                        <a class="icon-comet1-c icon-m1">
                                                            <i class="icon15-icon-chat2"></i>
                                                            <span class="font-l-new">Comment</span>
                                                        </a>
                    	                           </div>
                                                </div>
                                                <div class="mod-coment">
                                                        <ul id="list-coments-7-23-20">
                                                                                                                                                                                                <li id="13-22-20-80-68-91-89-65-92-92-227-128-90-74-114-115-121-119-124">
                                                                        <div class="mod-img-coment">
                                                                            <img src="https://storyrocket-aws3.s3.amazonaws.com/full5c06b0bdadfdb1543942333sr_jpg" alt="coment" class="responsive-img">
                                                                        </div>
                                                                        <div class="mod-comet-1">
                                                                            <p class="font-comet-1">
                                                                                <a class="font-profile" href="">alberto luis magallanes</a> comentnado                                                                            </p>
                                                                            <p class="font-comet-2">
                                                                                <span class="font-comet-2">3 seconds ago</span>
                                                                                                                                                                    -
                                                                                    <span class="font-comet-2">
                                                                                        <a href="#!" class="a-dele colse-comment-post" data-type="cmt" data-key="13-22-20-80-68-91-89-65-92-92-227-128-90-74-114-115-121-119-124">Delete</a>
                                                                                    </span>
                                                                                                                                                            </p>
                      	                                                 </div>
                                                                    </li>
                                                                                                                                                                                        <li>
                                                                <div class="mod-img-coment">
                                                                    <img src="https://storyrocket-aws3.s3.amazonaws.com/full5c06b0bdadfdb1543942333sr_jpg" alt="coment" class="responsive-img">
                                                                </div>
                                                                <div class="mod-comet-1-send">
                                                                    <form id="frm-coments-7-23-20">
                                                                        <input class="input-g-c required" type="text" name="">
                                                                        <button class="btn-post-comment ladda-button _7-23-20" data-style="expand-left" data-post="7-23-20" data-ip="13-20-23" data-type="pbl"><span class="ladda-label">Post Comment</span></button>
                                                                    </form>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                            </div>
                                            <div class="mar-mod-2 view-post" id="13-22-20-95-68-91-90-65-92-91-227-131-95-74-114-119-121-112-118">
                                                <div class="card-mod-1">
                    	                            <div class="card-mod-1-1">
                                                        <div class="card-mod-img">
                                                            <a href="https://www.storyrocket.com/Ron.82c040rm" alt="Ronald Karasz">
                                                                <img src="https://storyrocket-aws3.s3.amazonaws.com/35a82d60.jpg" alt="Ronald Karasz" class="responsive-img">
                                                            </a>
                                                        </div>
                                                        <div class="card-mod-p">
                                                            <p class="font-mod-1">
                         	                                  <a href="https://www.storyrocket.com/Ron.82c040rm" title="Ronald Karasz" alt="Ronald Karasz">Ronald Karasz</a>
                                                            </p>
                                                            <span class="font-mod-2">One year ago</span>
                                                        </div>
                                                    </div>
                                                    <div class="card-mod-1-2">
                                                        <p class="font-mod-3">Meeting next Monday (11/20/17) to continue writing</p>
                                                    </div>
                                                    <div class="card-mod-1-3 right-align ">
                                                        <div class="display-new-gr gi-12 card-likes">
                    	                                   <i class="icon10-heart"></i>
                    	                                   <span class="fo-icon1  _1x5- _5x1_ _50-k"><span class="_cont">0</span></span>
                    	                                   <i class="icon15-icon-chat1"></i>
                    	                                   <span class="fo-icon2 counter-coments">0</span>
                                                        </div>
                                                        <a class="icon-comet1 icon-m1 SRLikeLink" data-key="14-21-20-91" post-key="13-19-22">
                                                            <i class=" icon10-heart-outline"></i>
                                                            <span class="font-l-new">Like</span>
                                                        </a>
                                                        <a class="icon-comet1-c icon-m1">
                                                            <i class="icon15-icon-chat2"></i>
                                                            <span class="font-l-new">Comment</span>
                                                        </a>
                    	                           </div>
                                                </div>
                                            </div>
                                            <div class="mar-mod-2 view-post" id="13-22-20-95-68-90-95-65-93-90-227-131-95-74-115-119-121-117-118">
                                                <div class="card-mod-1">
                    	                            <div class="card-mod-1-1">
                                                        <div class="card-mod-img">
                                                            <a href="https://www.storyrocket.com/Ron.82c040rm" alt="Ronald Karasz">
                                                                <img src="https://storyrocket-aws3.s3.amazonaws.com/35a82d60.jpg" alt="Ronald Karasz" class="responsive-img">
                                                            </a>
                                                        </div>
                                                        <div class="card-mod-p">
                                                            <p class="font-mod-1">
                         	                                  <a href="https://www.storyrocket.com/Ron.82c040rm" title="Ronald Karasz" alt="Ronald Karasz">Ronald Karasz</a>
                                                            </p>
                                                            <span class="font-mod-2">2 years ago</span>
                                                        </div>
                                                    </div>
                                                    <div class="card-mod-1-2">
                                                        <p class="font-mod-3">Looking for writers to finish a long form treatment or script about 5 down-on-their-luck mariachis that get lost at sea and end up in the Dominican Republic.</p>
                                                    </div>
                                                    <div class="card-mod-1-3 right-align ">
                                                        <div class="display-new-gr gi-12 card-likes">
                    	                                   <i class="icon10-heart"></i>
                    	                                   <span class="fo-icon1  _1x5- _5x1_ _50-k"><span class="_cont">1</span></span>
                    	                                   <i class="icon15-icon-chat1"></i>
                    	                                   <span class="fo-icon2 counter-coments">0</span>
                                                        </div>
                                                        <a class="icon-comet1 icon-m1 SRLikeLink" data-key="14-21-20-91" post-key="14-19">
                                                            <i class=" icon10-heart-outline"></i>
                                                            <span class="font-l-new">Like</span>
                                                        </a>
                                                        <a class="icon-comet1-c icon-m1">
                                                            <i class="icon15-icon-chat2"></i>
                                                            <span class="font-l-new">Comment</span>
                                                        </a>
                    	                           </div>
                                                </div>
                                            </div>
                                        </div>
                                                        <!-- fin primer modulo-->
                                    </div>
                                    <div id="test17" class="col s12" style="display: none;">
                                        <div class="row border-text-m">
                                            <div class="col s12 m12 l6 xl6 pro-padg-2" id="members-14-23">
                                                <div class="card horizontal-net card-menber">
                                                    <div class="card-image-net net-card">
                                                        <a href="https://www.storyrocket.com/Ron.82c040rm"><img src="https://storyrocket-aws3.s3.amazonaws.com/35a82d60.jpg" alt="Ronald Karasz"></a>
                                                    </div>
                                                    <div class="card-content-net">
                                                        <a href="https://www.storyrocket.com/Ron.82c040rm" class="font-net truncate">Ronald Karasz</a>
                                                        <p class="font-net2 truncate">
                                                            <span class="font-net2">Writer</span>
                                                        </p>
                                                        <p class="font-net3 truncate">United States</p>
                                                        <div class="boton-marg">
                                                            <!-- boton follow-->
                                                            <button class="botom-net-f btn-friend" data-type="Follow" data-friends="11" data-rel="user">
                                                                <i class="icon-icono-profile3"></i>
                                                                <span>Follow</span>
                                                            </button>                                                                                             <!--fin boton-->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="test18" class="col s12" style="display: none;">
                                        <div class="row border-text-m">
                                            <div class="not-post">
                                                <img src="https://www.storyrocket.com/public/images/calendar-today.svg" alt="">
                                                <p>This group has no events to show.</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="test19" class="col s12" style="display: none;">
                                        <div class="row border-text-m">
                                            <div class="col s6 m6 l3 xl3 padding-last u-relative">
                                                <div class="card-foto mediun2">
                                                    <div class="foto-image">
                                                        <a href="https://storyrocket-aws3.s3.amazonaws.com/full5a1048f12abf31511016689sr_.png" class="" data-rel="13-19-16"><img src="https://storyrocket-aws3.s3.amazonaws.com/thumb-165x1555a1048f12abf31511016689sr_.png" alt="foto"></a>
                                                        <div class="overlay-fotos link-over-gbr" data-type="all"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </article>
                                <article class="col s12 m12 l4 xl3">
                                    <div class="cont-g-2">
                                        <div class="mar-invi">
                                            <button class="btn-invit-g" data-type="groups">
                                                <i class="material-icons">mail</i>
                                                <span> Invite friends via Email</span>
                                            </button>
                                        </div>
                                        <div class="mar-b-2">
                                            <span class="font-d-1">Add Member</span>
                                            <div class="cont-sec-1">
                                                <input class="inpunt-g-t" id="add-member" data-type="groups" type="" name="" placeholder="+ Enter the name to add">
                                                <div class="loaders"><ul class="list-menbers list-searchmgb"></ul></div>
                                            </div>
                                        </div>
                                        <div class="mar-b-2">
                                            <p class="font-d-1">Description</p>
                                            <div class="cont-sec-1">
                                                <p class="font-decrip">
                                                    I'm looking for a couple of comedy writers to co-write a long form treatment or script about 5 mariachis that get lost at sea and end up in the Dominican Republic.Interested or if you want to know
                                                </p>
                                            </div>
                                        </div>
                                        <div class="mar-b-3">
                                            <span class="font-d-1">Members</span>
                                            <div class="cont-sec-1 hiden-members">
                                                <div class="menber-profile-box" id="members-14-23">
                                                    <a href="https://www.storyrocket.com/Ron.82c040rm">
                                                        <div class="menber-box">
                                                            <img src="https://storyrocket-aws3.s3.amazonaws.com/35a82d60.jpg" alt="perfil" class="responsive-img">
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="mar-b-4">
                                            <span class="font-d-1">Recent Group Photos</span>
                                            <div class="cont-sec-1">
                                                <div class="u-relative" style="padding: 0px 2px 0px 2px; display: inline-block;">
                                                    <a href="https://storyrocket-aws3.s3.amazonaws.com/full5a1048f12abf31511016689sr_.png" data-rel="13-19-16">
                                                        <div class="box-p">
                                                            <img src="https://storyrocket-aws3.s3.amazonaws.com/thumb-80x805a1048f12abf31511016689sr_.png" alt="photos" class="responsive-img">
                                                        </div>
                                                    </a>
                                                    <div class="overlay-fotos link-over-gbr x95" data-type="all"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="cont-sec-1"></div>
                                        <div class="cont-sec-1">
                                            <div class="mar-b-1">
                                                <p class="font-b-1">Create April 04, 2017</p>
                                                <span class="font-b-2">Aministrator <a href="https://www.storyrocket.com/Ron.82c040rm" class="color-b-1">Ronald Karasz</a></span>
                                            </div>
                                        </div>
                                    </div>
                                </article>
                            </section>
                        </section>
                    </section>
                </div>
            </div>
        </div>
    </div>
@endsection