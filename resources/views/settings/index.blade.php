@extends('layouts.app-main')

@section('content')

    <script type="text/javascript">
        var settings = @json($settings);
    </script>
<div class="cover-page cover-page-in cover-page-1 cover-page-plans s-settings">
    <div class="cover-page-content">
        <!--<div class="cover-accordion-container">-->
        <div class="">
            <!-- slide left start -->
            @auth
                @include('profile.partials.slideleft')
            @endauth
            <!-- slide left end -->

            <div class="page-content-wrapper page-content-wrapper-plans" style="background-color: #e6ecf0;">
            <section class="cont-general">
                <article >
                    <form id="frm-privacity">

                        <!-- article class="row">
                            <article class="container">
                                <div class="paddig-pro2">
                                    <input type="submit" value="SAVE &amp; RETURN" class="right btn-salva-input">
                                </div>
                            </article>
                        </article -->

                        <article class="row pro-mar2">
                            <article class="container">
                                <div class="col s12 m12">

                                    <div id="privacy" class="box-setting u-cursor-default tabcontent col s12 m12 l12 xl12 items-settings openDefault" style="">
                                        <h1 class="title-settings left-align">Privacy Settings</h1>
                                        <p>
                                            <label>
                                                <input  type="checkbox" class="settings-check check-ios" name="ps_show_other_members_that_you_looked_at_their_profile" id="ps_show_other_members_that_you_looked_at_their_profile" />
                                            </label>
                                            <span>Show other members that you looked at their profile</span>
                                        </p>
                                        <p class="ps_addme_to_profile_spotlight_container">
                                            <label>
                                                <input  type="checkbox" class="settings-check check-ios" name="ps_add_me_to_profile_potlight" id="ps_addme_to_profile_spotlight" />
                                            </label>
                                            <span>Add me to "Profile Spotlight</span>
                                        </p>
                                        <p class="ps_add_my_projects_to_project_spotlight_container">
                                            <label>
                                                <input  type="checkbox" class="settings-check check-ios" name="ps_add_my_projects_to_project_spotlight" id="ps_add_my_projects_to_project_spotlight" />
                                            </label>
                                            <span>Add my projects to "Project Spotlight"</span>
                                        </p>
                                        <br>
                                        <div class="settings-hr"></div>
                                        <br>

                                        <br>
                                        <h1 class="title-settings left-align">Notify me when</h1>
                                        <p>
                                            <label>
                                                <input  type="checkbox" class="settings-check check-ios" name="nm_someone_looks_you_at_my_profile" id="nm_someone_looks_you_at_my_profile" />
                                            </label>
                                            <span>Someone looks you at my profile</span>
                                        </p>


                                        <!-- block start -->
                                        <div class="check-parent-depend">
                                            <p>
                                                <label>
                                                    <input  type="checkbox" class="settings-check check-ios" name="nm_someone_adds_you_to_his_network" id="nm_someone_adds_you_to_his_network" />
                                                </label>
                                                <span>Someone adds you to his Network as a friend</span>
                                            </p>
                                            <p class="second-check-setting">
                                                <label>
                                                    <input type="checkbox" class="filled-in" id="nm_someone_adds_you_to_his_network_email"   name="nm_someone_adds_you_to_his_network_email" >
                                                    <span>Send to my email</span>
                                                </label>
                                            </p>
                                        </div>
                                        <!-- block end -->



                                        <!-- block start -->
                                        <div class="check-parent-depend">
                                            <p >
                                                <label>
                                                    <input type="checkbox" class="settings-check check-ios settings-check-parent" name="nm_someone_adds_one_of_my_projects_to_their_read_later_list" id="nm_someone_adds_one_of_my_projects_to_their_read_later_list" />
                                                </label>
                                                <span>Someone adds one of my projects to their "Read Later List"</span>
                                            </p>
                                            <p class="second-check-setting">
                                                <label>
                                                    <input type="checkbox" class="filled-in" id="nm_someone_adds_one_of_my_projects_to_their_read_later_list_email"  name="nm_someone_adds_one_of_my_projects_to_their_read_later_list_email" >
                                                    <span>Send to my email</span>
                                                </label>
                                            </p>
                                        </div>
                                        <!-- block end -->


                                        <!-- block start -->
                                        <div class="check-parent-depend">
                                            <p>
                                                <label>
                                                    <input type="checkbox" class="settings-check  check-ios settings-check-parent" name="nm_someone_likes_one_of_my_projects" id="nm_someone_likes_one_of_my_projects" />
                                                    <span>Someone "Likes" one of my projects</span>
                                                </label>
                                            </p>
                                            <p class="second-check-setting">
                                                <label>
                                                    <input type="checkbox" class="filled-in" id="nm_someone_likes_one_of_my_projects_email" name="nm_someone_likes_one_of_my_projects_email">
                                                    <span>Send to my email</span>
                                                </label>
                                            </p>
                                        </div>
                                        <!-- block end -->


                                        <!-- block start -->
                                        <div class="check-parent-depend">
                                            <p>
                                                <label>
                                                    <input type="checkbox" class="settings-check check-ios settings-check-parent" name="nm_someone_comment_one_of_my_projects" id="nm_someone_comment_one_of_my_projects" />
                                                    <span>Someone makes a comment on one of my projects</span>
                                                </label>

                                            </p>
                                            <p class="second-check-setting">
                                                <label>
                                                    <input type="checkbox" id="nm_someone_comment_one_of_my_projects_email" name="nm_someone_comment_one_of_my_projects_email"  class="filled-in">
                                                    <span>Send to my email</span>
                                                </label>
                                            </p>
                                        </div>
                                        <!-- block end -->


                                        <?php
                                        /*
                                        <p>

                                            <label>
                                                <input type="checkbox" class="settings-check" name="nm_adds_a_new_project" id="nm_adds_a_new_project" />
                                                <span>Adds a new project</span>
                                            </label>

                                            <!-- input type="checkbox" class=settings-check" checked="checked" id="test15" name="notifywhenNewProject" data-name="notifywhenNewProject">
                                            <label for="test15" class="u-cursor-default">Adds a new project</label -->
                                        </p>
                                        <p>
                                            <label>
                                                <input type="checkbox" class="settings-check" name="nm_updates_a_project" id="nm_updates_a_project" />
                                                <span>Updates a project</span>
                                            </label>

                                            <!-- input type="checkbox" class=settings-check" checked="checked" id="test16" name="notifywhenUpdateProject" data-name="notifywhenUpdateProject">
                                            <label for="test16" class="u-cursor-default">Updates a project</label -->
                                        </p>
                                        <p>
                                            <label>
                                                <input type="checkbox" class="settings-check" name="nm_updates_their_profile" id="nm_updates_their_profile" />
                                                <span>Updates a project</span>
                                            </label>

                                            <!-- input type="checkbox" class=settings-check" checked="checked" id="test18" name="notifywhenUpdateProfile" data-name="notifywhenUpdateProfile">
                                            <label for="test18" class="u-cursor-default">Updates their profile</label -->
                                        </p>
                                        <p>

                                            <label>
                                                <input type="checkbox" class="settings-check" name="nm_creates_a_group" id="nm_creates_a_group" />
                                                <span>Creates a group</span>
                                            </label>

                                            <!-- input type="checkbox" class=settings-check" checked="checked" id="test19" name="notifywhenCreateGroup" data-name="notifywhenCreateGroup">
                                            <label for="test19" class="u-cursor-default">Creates a group</label -->
                                        </p>
                                        <p>
                                            <label>
                                                <input type="checkbox" class="settings-check" name="nm_creates_an_event" id="nm_creates_an_event" />
                                                <span>Creates an event</span>
                                            </label>

                                            <!-- input type="checkbox" class=settings-check" checked="checked" id="test20" name="notifywhenCreateEvent" data-name="notifywhenCreateEvent">
                                            <label for="test20" class="u-cursor-default">Creates an event</label -->
                                        </p>
                                        <p>
                                            <label>
                                                <input type="checkbox" class="settings-check" name="nm_has_joined_a_group" id="nm_has_joined_a_group" />
                                                <span>Has joined a group</span>
                                            </label>

                                            <!-- input type="checkbox" class=settings-check" checked="checked" id="test21" name="notifywhenJoinGroup" data-name="notifywhenJoinGroup">
                                            <label for="test21" class="u-cursor-default">Has joined a group</label -->
                                        </p>
                                        <p>

                                            <label>
                                                <input type="checkbox" class="settings-check" name="nm_has_joined_a_event" id="nm_has_joined_a_event" />
                                                <span>Has joined a event</span>
                                            </label>

                                            <!-- input type="checkbox" class=settings-check" checked="checked" id="test22" name="notifywhenJoinEvent" data-name="notifywhenJoinEvent">
                                            <label for="test22" class="u-cursor-default">Has joined an event</label -->
                                        </p>
                                        <p>
                                            <label>
                                                <input type="checkbox" class="settings-check" name="nm_posted_on_a_group_or_event" id="nm_posted_on_a_group_or_event" />
                                                <span>Posted on a Group or Event</span>
                                            </label>
                                        </p>
                                        <!-- p>
                                            <input type="checkbox" class=settings-check" checked="checked" id="test23" name="notifywhenPostedGE" data-name="notifywhenPostedGE">
                                            <label for="test23" class="u-cursor-default">Posted on a Group or Event</label>
                                        </p -->

                                        <br>
                                        <h1 class="title-settings left-align">Notify me when someone</h1>
                                        <p>

                                            <label>
                                                <input type="checkbox" class="settings-check" name="nms_joins_a_group_im_in" id="nms_joins_a_group_im_in" />
                                                <span>Joins a group I’m in</span>
                                            </label>

                                            <!-- input type="checkbox" class=settings-check" checked="checked" id="test230" name="notifywhenJoinsGroup" data-name="notifywhenJoinsGroup">
                                            <label for="test230" class="u-cursor-default">Joins a group I’m in</label -->
                                        </p>
                                        <p>

                                            <label>
                                                <input type="checkbox" class="settings-check" name="nms_joins_a_event_im_in" id="nms_joins_a_event_im_in" />
                                                <span>Joins an event I’m in</span>
                                            </label>

                                            <!-- input type="checkbox" class=settings-check" checked="checked" id="test231" name="notifywhenJoinsEvent" data-name="notifywhenJoinsEvent">
                                            <label for="test231" class="u-cursor-default">Joins an event I’m in</label -->
                                        </p>

                                        */
                                        ?>
                                        <br>
                                        <div class="settings-hr"></div>
                                        <br>
                                        <br>
                                        <h1 class="title-settings left-align">Summary Notifications</h1>

                                        <p class="settings-enable-disable settings-enable-disable-default">

                                            <label>
                                                <input type="checkbox" class="settings-check check-ios" name="sn_send_to_my_email_once_a_day" id="sn_send_to_my_email_once_a_day" />
                                                <span>Send to my email once a day (default)</span>
                                            </label>

                                            <!-- input type="checkbox" class=settings-check" data-type="days" checked="checked" id="test25" name="sumarycheck" data-name="sumarynotfiSendMailDefault">
                                            <label for="test25" class="u-cursor-default">Send to my email once a day (default)</label -->
                                        </p>
                                        <p class="settings-enable-disable  settings-enable-disable-default">
                                            <label>
                                                <input type="checkbox" class="settings-check check-ios" name="sn_send_to_my_email_once_a_week" id="sn_send_to_my_email_once_a_week" />
                                                <span>Send to my email once a week (default)</span>
                                            </label>
                                            <!-- input type="checkbox" data-type="week" id="test26" name="sumarycheck" data-name="sumarynotfiSendWeek">
                                            <label for="test26" class="u-cursor-default">Send to my email once a week</label -->
                                        </p>
                                    </div>
                                    <!--<div id="notifications" class="tabcontent col s12 m12 l12 xl12 items-settings "  >
                                      <h1 class="title-settings left-align">Notifications</h1>
                                      <p>
                                          <input type="checkbox" checked="checked" id="test10" name="noty[]">
                                          <label for="test10">Receive a friend request.</label>
                                      </p>
                                      <p>
                                          <input type="checkbox" checked="checked" id="test11" name="noty[]">
                                          <label for="test11">Receive a message from a friend.</label>
                                      </p>
                                      <p>
                                          <input type="checkbox" checked="checked" id="test12" name="noty[]">
                                          <label for="test12">Receive a message from a member.</label>
                                      </p>
                                      <p>
                                          <input type="checkbox" checked="checked" id="test13" name="noty[]">
                                          <label for="test13">Receive a notification from friends when they update their profile.</label>
                                      </p>
                                      <p>
                                          <input type="checkbox" checked="checked" id="test14" name="noty[]">
                                          <label for="test14">Receive a notification from friends when they update a project.</label>
                                      </p>
                                      <p>
                                          <input type="checkbox" checked="checked" id="test15" name="noty[]">
                                          <label for="test15">Receive a notification when someone comments on one of my projects.</label>
                                      </p>
                                      <p>
                                          <input type="checkbox" checked="checked" id="test16" name="noty[]">
                                          <label for="test16">Receive a notification when someone adds one of my projects to their 'Read List'.</label>
                                      </p>
                                      <p>
                                          <input type="checkbox" checked="checked" id="test17" name="noty[]">
                                          <label for="test17">Receive a notification when someone lists me as a co-authr of a project.</label>
                                      </p>
                                      <p>
                                          <input type="checkbox" checked="checked" id="test18" name="noty[]">
                                          <label for="test18">Receive notifications of new activity on projects by my friends.</label>
                                      </p>
                                      <p>
                                          <input type="checkbox" checked="checked" id="test19" name="noty[]">
                                          <label for="test19">Receive notifications of new activity on projects by people I am following.</label>
                                      </p>
                                      <p>
                                          <input type="checkbox" checked="checked" id="test20" name="noty[]">
                                          <label for="test20">Receive a summary of notifications of new activity on projects by my friends and people I am following.</label>
                                      </p>
                                      <p>
                                          <input type="checkbox" checked="checked" id="test21" name="noty[]">
                                          <label for="test21">Receive updates about the performance of my projects.</label>
                                      </p>
                                      <p>
                                          <input type="checkbox" checked="checked" id="test22" name="noty[]">
                                          <label for="test22">Receive recommendations based on my activity.</label>
                                      </p>
                                      <p>
                                          <input type="checkbox" checked="checked" id="test23" name="noty[]">
                                          <label for="test23">StoryRocket wants to send you the projects, scripts, ideas, news, notes and videos that are important to you.</label>
                                      </p>
                                    </div>-->


                                </div>
                            </article>
                        </article>
                    </form>
                </article>
            </section>
        </div>
    </div>
    </div>
</div>
@endsection
