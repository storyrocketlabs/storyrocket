@extends('layouts.app-main')

@section('content')
    <div class="wrapper tooltip-nav">
        <!-- slide left start -->
    @auth
        @include('profile.partials.slideleft')
    @endauth
    <!-- slide left end -->
        <div class="page-content-wrapper page-content-wrapper-plans" style="background-color: #e6ecf0;">
            <section class="area-payment-st">
                <div class="row">
                    <div class="col s12 m6 l6 area-payment-camps">
                        <h1 class="title-payment">Payment Details</h1>
                        <div class="method-pay">
                            <div>
                                <img width="30" src="{{asset('images/pay1-l.svg')}}" alt="">
                            </div>
                            <div>
                                <img width="30" src="{{asset('images/pay2-l.svg')}}" alt="">
                            </div>
                            <div>
                                <img width="30" src="{{asset('images/pay3-l.svg')}}" alt="">
                            </div>
                            <div>
                                <img width="30" src="{{asset('images/pay4-l.svg')}}" alt="">
                            </div>
                            <div>
                                <img width="30" src="{{asset('images/pay5-l.svg')}}" alt="">
                            </div>
                            <div>
                                <img width="30" src="{{asset('images/pay6-l.svg')}}" alt="">
                            </div>
                        </div>
                        <form action="{{route('plans.store-subscription', $plan->id)}}" method="POST" id="payment-form"
                              name="payment-form" class="form-payment" autocomplete="off">
                            @csrf
                            <input type="hidden" value="{{$plan->price}}" id="price-plan">
                            <input type="hidden" name="coupon" id="plan-coupon">
                            <div class="listcard">
                                <div class="area-payment-camps-block"></div>
                                <div class="listship">
                                    <div class="col s12 m11 l11 xl11 ">
                                        <div>
                                            <label class="font-label-plans">Credit Card Number:</label>
                                        </div>
                                        <input title="Card Number" class="input-plans1 cls_valid txtcards" type="text"
                                               placeholder="Enter your card number" value="" data-stripe="number"
                                               name="card-number" id="card-number">
                                    </div>
                                    <div class="col s1 m1 l1 xl1 no-padding">
                                        <i class="material-icons ico-billing1 show-tool-security">https</i>
                                        <div class="tooltip-security">You’re on a secured connection. We process
                                            payments through STRIPE. Your payment information is encrypted before
                                            transfer. Storyrocket does not store your credit card information.
                                        </div>
                                    </div>
                                    <div class="col s12 m11 l11 xl11 ">
                                        <div class="col s12 m12 l4 xl4 no-padding-left">
                                            <div>
                                                <label class="font-label-plans">Expiration Date:</label>
                                            </div>
                                            <div class="select-plans">
                                                {!! Form::selectMonth('month', null ,['class'=>'browser-default font-select-plans cbomont cls_valid', 'placeholder'=>'Month']) !!}
                                            </div>
                                        </div>
                                        <div class="col s12 m12 l4 xl4">
                                            <div class="select-plans camp-year">
                                                {!! Form::selectYear('year', date('Y'), date('Y') + 20, null ,['class'=>'browser-default font-select-plans cls_valid cboyear', 'placeholder'=>'Year']) !!}
                                            </div>
                                        </div>
                                        <div class="col s12 m12 l4 xl4 no-padding-right">
                                            <div class="">
                                                <label class="font-label-plans">Security Code:</label>
                                            </div>
                                            <input class="input-plans1 cls_valid" type="password" data-stripe="cvc" maxlength="3"
                                                   name="card-cvc">
                                        </div>
                                    </div>
                                    <div class="col s1 m1 l1 xl1 no-padding">
                                        <i class="material-icons ico-billing1 show-tool-help">help</i>
                                        <div class="tooltip-help">Enter the security code on the back of your credit
                                            card
                                        </div>
                                    </div>
                                    <div class="col s12 m12 l12 xl12">
                                        <div>
                                            <label class="font-label-plans">First Name:</label>
                                        </div>
                                        <input class="input-plans1 cls_valid txtname" type="text" name="card-first-name"
                                               data-stripe="first_name">
                                    </div>
                                    <div class="col s12 m12 l12 xl12">
                                        <div>
                                            <label class="font-label-plans">Last Name:</label>
                                        </div>
                                        <input class="input-plans1 cls_valid txtlast" type="text" name="card-last-name"
                                               data-stripe="last_name">
                                    </div>
                                </div>
                            </div>
                            <button type="submit" class="btn-send-payment waves-effect waves-light waves-input-wrapper">
                                Process Order
                            </button>
                        </form>
                    </div>


                    <div class="col s12 m6 l6 area-order-summary">
                        <div class="col s12 m12 l12">
                            <div class="popupcoupon">
                                <div class="popupcoupon-content">
                                    <a class="modal-trigger modal-add-promo-code" href="#popupCouponForm">
                                        Add Promo Code
                                    </a>
                                </div>
                            </div>
                        </div>

                        <h1 class="order-summary">Order Summary</h1>

                        <div class="col s12 m12 l12 plans-choosed">
                            <h4 class="font-3plans clr-2plans">
                                <dd class="tp-plan">{{$plan->name}}</dd>
                            </h4>
                            <p class="font-1plans clr-2plans">Post up to <span class="_xct"
                                                                               style="position:relative;top: 0;">{{$plan->projects}}</span>
                                projects (billed {{$plan->cycle}})</p>
                            <span class="crl-4plans price-pln">{{$plan->price}}</span>
                        </div>
                        <div class="col s12 m12 l12 clr-1plans save-percent">
                            SAVE 16.5% by choosing the annual billing, return to <a href="{{route('plans')}}"
                                                                                    class="crl-4plans">PLANS.</a><br>
                        </div>
                        <div id="popupCouponForm" class="modal" style="z-index: 1005;">
                            <div class="popupcouponform-content">
                                <div class="code-promotion">
                                    <input type="hidden" value="{{$plan->id}}" id="plan-id-value">
                                    <a href="#!" class="modal-close modal-close-icon"><i
                                                class="material-icons">close</i></a>

                                    <div class="code-promotion-form">
                                        <div class="col s12 m12 l12 xl12 center" style="margin-top: 45px;">
                                            Enter Promo Code

                                        </div>
                                        <div class="col s12 m12 l12 xl12 center">
                                            <input class="input-promo-code" type="text" value="" id="box-code"
                                                   placeholder="Add Promo Code">
                                        </div>

                                        <div class="col s12 m12 l12 xl12 center" style="margin-bottom: 45px;">
                                            <a class="btn-code-promotion waves-effect waves-light" id="apply-code">
                                                Apply Promo Code
                                            </a>
                                        </div>
                                    </div>

                                    <div class="code-promotion-notvalid">
                                        <div class="msj-paymet">
                                            <div id="messageCoupon"></div>
                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>
                        <!-- end modal coupon code content -->
                        <div class="col s12 m12 l12 ">
                            <div class="flex-container total-payment">
                                <p>GRAND TOTAL</p>
                                <span class="price-pln">{{$plan->price}}</span>
                                <span class="price-pln_new"></span>

                            </div>
                        </div>
                        <div class="e_boxe_messge"></div>


                    </div>
                    <div class="clearfix">

                    </div>
                    <div class="auto-renewal">
                        <div class="flex-container">
                            <i class="material-icons clr-2plans">refresh</i>
                            <p class="clr-2plans">Auto Renewal</p>
                        </div>
                        <p class="font-1plans crl-3plans">Plans are set to Auto Renewal. If you chose annual billing,
                            you will be notified 30 days before your account plan expires to cancel or to make changes.
                            Monthly billing can be set to cancel at any time - cancellations apply to renewals. Once a
                            billing period begins, no refunds will be made.</p>
                    </div>
                </div>

            </section>
        </div>
    </div>
@endsection