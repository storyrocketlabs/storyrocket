@extends('layouts.app-main')

@section('content')
<div class="cover-page cover-page-in cover-page-1 cover-page-plans">
    <div class="cover-page-content">
        <!--<div class="cover-accordion-container">-->
        <div class="">
            <!-- slide left start -->
            @auth
                @include('profile.partials.slideleft')
            @endauth
            <!-- slide left end -->

            <div class="page-content-wrapper page-content-wrapper-plans" style="background-color: #e6ecf0;">
            <section class="cont-general">
                <article class="padding-p-3 text-pc">
                    <h2 class="font-price">Pricing Plans</h2>
                    <h3 class="font-body">Choose the plan that best meets your needs.</h3>
                </article>
                <article class="row" id="respon-ap">
                    <div class="col s12 m12 l12">
                        <div class="col-p col-p-inter col-p-adi">
                            <h5 class="font-p-title">BASIC</h5>
                            <div class="bar-1"></div>
                            <h3 class="font-p-sub" style="margin-bottom: 26px; margin-top: 32px;">FREE</h3>
                            <p class="font-p-p padding-p-2 square-plan-blocktext-responsive">Membership is always FREE for
                                like-minded persons that want to create great TV, Film, Web, Theatre, and more.</p>
                            <button type="button" name="button" class="btn-default-small btn-size btn-mar-p">Current Plan
                            </button>
                        </div>
                    </div>
                    <div class="col s12 m12 l12">
                        <div class="col-p col-p-inter col-p-adi">
                            <h5 class="font-p-title">PLUS MONTHLY</h5>
                            <div class="bar-2"></div>
                            <h3 class="font-p-sub" style=" margin-bottom: 0px;"><span class="font-doll">$</span><span class="font-prix-2">25</span>
                                <p class="cont-p-prix1"><span class="font-prix-1">.00</span><br><span class="font-prix-1-2">/MONTH</span>
                                </p></h3>
                            <span class="font-p-price">(paid monthly)</span>
                            <p class="font-p-p padding-p-2 square-plan-blocktext-responsive">Monthly billing allows you to try our platform with no
                                <br>long-term commitments. Create up to 10 Professional Pitch Packages.</p>
                            <a class="btn-primary-small btn-mar-p-1 order-link btn-size order-link" data-type="PLUS" data-time="monthly" data-price="25.00" data-cant="10" href="{{route('subscription-form',2)}}">Buy</a>
                        </div>
                    </div>
                    <div class="col s12 m12 l12">
                        <div class="col-p col-p-inter col-p-adi">
                            <h5 class="font-p-title">PLUS YEARLY</h5>
                            <div class="bar-3"></div>
                            <h3 class="font-p-sub" style=" margin-bottom: 0px;"><span class="font-doll">$</span><span class="font-prix-2">120</span>
                                <p class="cont-p-prix1"><span class="font-prix-1">.00</span><br><span class="font-prix-1-2">/YEAR</span>
                                </p></h3>
                            <span class="font-p-price">(paid once a year)</span>
                            <p class="font-p-p padding-p-2 square-plan-blocktext-responsive">Billing once a year is the <strong>BETTER VALUE</strong>, that's $10 per month and with 10 projects… that’s $1 per project per month.</p>
                            <a class="btn-primary-small btn-mar-p-1 order-linkl btn-size order-link" data-type="PLUS" data-time="annually" data-price="120.00" data-cant="10" href="{{route('subscription-form',3)}}">Buy</a>
                        </div>
                    </div>
                    <div class="col s12 m12 l12">
                        <div class="col-p col-p-inter col-p-adi">
                            <h5 class="font-p-title">PRO</h5>
                            <div class="bar-4"></div>
                            <h3 class="font-p-sub" style=" margin-bottom: 0px;"><span class="font-doll">$</span><span class="font-prix-2">195</span>
                                <p class="cont-p-prix1"><span class="font-prix-1">.00</span><br><span class="font-prix-1-2">/YEAR</span>
                                </p></h3>
                            <span class="font-p-price">(paid once a year)</span>
                            <p class="font-p-p padding-p-2 square-plan-blocktext-responsive">Billing once a year is the <strong>BEST
                                    VALUE</strong>, that's $16.25 per month and with 20 projects… that’s just over 80 cents per project per
                                month.</p>
                            <a class="btn-primary-small btn-mar-p-1 order-link btn-size order-link" data-type="PROFESSIONAL" data-time="annually" data-price="195.00" data-cant="10" href="{{route('subscription-form',4)}}">Buy</a>
                        </div>
                    </div>
                </article>
                <table class="" id="table-price">
                    <thead>
                    <tr>
                        <th class="col-p" style="padding: 0px;">
                            <img src="{{asset('images/bnr.jpg')}}" alt="" class="full-img-p">
                        </th>
                        <th class="col-p col-p-inter col-p-adi">
                            <h5 class="font-p-title">BASIC</h5>
                            <div class="bar-1"></div>
                            <h3 class="font-p-sub" style="margin-bottom: 26px; margin-top: 32px;">FREE</h3>
                            <p class="font-p-p padding-p-2 square-plan-blocktext-responsive">Membership is always FREE for
                                like-minded persons that want to create great TV, Film, Web, Theatre, and more.</p>
                            <button type="button" name="button" class="btn-default-small btn-size btn-mar-p">Current Plan
                            </button>
                        </th>
                        <th class="col-p col-p-inter col-p-adi">
                            <h5 class="font-p-title">PLUS MONTHLY</h5>
                            <div class="bar-2"></div>
                            <h3 class="font-p-sub" style=" margin-bottom: 0px;"><span class="font-doll">$</span><span class="font-prix-2">25</span>
                                <p class="cont-p-prix1"><span class="font-prix-1">.00</span><br><span class="font-prix-1-2">/MONTH</span>
                                </p></h3>
                            <span class="font-p-price">(paid monthly)</span>
                            <p class="font-p-p padding-p-2 square-plan-blocktext-responsive">Monthly billing allows you to try our platform with no
                                <br>long-term commitments. Create up to 10 Professional Pitch Packages.</p>
                            <a class="btn-primary-small btn-mar-p-1 order-link btn-size order-link" data-type="PLUS" data-time="monthly" data-price="25.00" data-cant="10" href="{{route('subscription-form',2)}}">Buy</a>
                        </th>
                        <th class="col-p col-p-inter col-p-adi">
                            <h5 class="font-p-title">PLUS YEARLY</h5>
                            <div class="bar-3"></div>
                            <h3 class="font-p-sub" style=" margin-bottom: 0px;"><span class="font-doll">$</span><span class="font-prix-2">120</span>
                                <p class="cont-p-prix1"><span class="font-prix-1">.00</span><br><span class="font-prix-1-2">/YEAR</span>
                                </p></h3>
                            <span class="font-p-price">(paid once a year)</span>
                            <p class="font-p-p padding-p-2 square-plan-blocktext-responsive">Billing once a year is the <strong>BETTER VALUE</strong>, that's $10 per month and with 10 projects… that’s $1 per project per month.</p>
                            <a class="btn-primary-small btn-mar-p-1 order-linkl btn-size order-link" data-type="PLUS" data-time="annually" data-price="120.00" data-cant="10" href="{{route('subscription-form',3)}}">Buy</a>
                        </th>
                        <th class="col-p col-p-inter col-p-adi">
                            <h5 class="font-p-title">PRO</h5>
                            <div class="bar-4"></div>
                            <h3 class="font-p-sub" style=" margin-bottom: 0px;"><span class="font-doll">$</span><span class="font-prix-2">195</span>
                                <p class="cont-p-prix1"><span class="font-prix-1">.00</span><br><span class="font-prix-1-2">/YEAR</span>
                                </p></h3>
                            <span class="font-p-price">(paid once a year)</span>
                            <p class="font-p-p padding-p-2 square-plan-blocktext-responsive">Billing once a year is the <strong>BEST
                                    VALUE</strong>, that's $16.25 per month and with 20 projects… that’s just over 80 cents per project per
                                month.</p>
                            <a class="btn-primary-small btn-mar-p-1 order-link btn-size order-link" data-type="PROFESSIONAL" data-time="annually" data-price="195.00" data-cant="10" href="{{route('subscription-form',4)}}">Buy</a>

                        </th>
                    </tr>
                    </thead>
                    <tbody class="responsive-bloque">
                    <tr>
                        <th scope="col" colspan="5" class="bar-th border-p-b">
                            <span>Services and Benefits</span>
                        </th>
                    </tr>
                    <tr>
                        <th class="col-p col-p-inter col-p-adi-cont" style="border-left: 0px solid #ebebeb;">
                            <span class="font-p-p">Projects to post</span>
                        </th>
                        <th class="col-p col-p-inter col-p-adi-cont-ten">
                            <span class="font-p-p-dislable">--</span>
                        </th>
                        <th class="col-p col-p-inter col-p-adi-cont-ten">
                            <span class="font-p-p"><strong>10</strong></span>
                        </th>
                        <th class="col-p col-p-inter col-p-adi-cont-ten">
                            <span class="font-p-p"><strong>10</strong></span>
                        </th>
                        <th class="col-p col-p-inter col-p-adi-cont-ten">
                            <span class="font-p-p"><strong>20</strong></span>
                        </th>
                    </tr>
                    <tr class="border-p-t">
                        <th class="col-p col-p-inter col-p-adi-cont" style="border-left: 0px solid #ebebeb;">
                            <span class="font-p-p">Getty images (included)</span>
                        </th>
                        <th class="col-p col-p-inter col-p-adi-cont-ten">
                            <span class="font-p-p-dislable">--</span>
                        </th>
                        <th class="col-p col-p-inter col-p-adi-cont-ten">
                            <span class="font-p-p-dislable">--</span>
                        </th>
                        <th class="col-p col-p-inter col-p-adi-cont-ten">
                            <span class="font-p-p"><strong>10</strong></span>
                        </th>
                        <th class="col-p col-p-inter col-p-adi-cont-ten">
                            <span class="font-p-p"><strong>30</strong></span>
                        </th>
                    </tr>
                    <tr>
                        <th scope="col" colspan="5" class="bar-th border-p-b border-p-t">
                            <span>Why Join Storyrocket?</span>
                        </th>
                    </tr>
                    <tr>
                        <th class="col-p col-p-inter col-p-adi-cont" style="border-left: 0px solid #ebebeb;">
                            <span class="font-p-p">Create a profile</span>
                        </th>
                        <th class="col-p col-p-inter col-p-adi-cont   icon-price-check">
                            <i class="material-icons">done</i>
                        </th>
                        <th class="col-p col-p-inter col-p-adi-cont  icon-price-check">
                            <i class="material-icons">done</i>
                        </th>
                        <th class="col-p col-p-inter col-p-adi-cont icon-price-check">
                            <i class="material-icons">done</i>
                        </th>
                        <th class="col-p col-p-inter col-p-adi-cont icon-price-check">
                            <i class="material-icons">done</i>
                        </th>
                    </tr>
                    <tr class="border-p-t">
                        <th class="col-p col-p-inter col-p-adi-cont" style="border-left: 0px solid #ebebeb;">
                            <span class="font-p-p">Message members</span>
                        </th>
                        <th class="col-p col-p-inter col-p-adi-cont icon-price-check">
                            <i class="material-icons">done</i>
                        </th>
                        <th class="col-p col-p-inter col-p-adi-cont icon-price-check">
                            <i class="material-icons">done</i>
                        </th>
                        <th class="col-p col-p-inter col-p-adi-cont icon-price-check">
                            <i class="material-icons">done</i>
                        </th>
                        <th class="col-p col-p-inter col-p-adi-cont icon-price-check">
                            <i class="material-icons">done</i>
                        </th>
                    </tr>
                    <tr class="border-p-t">
                        <th class="col-p col-p-inter col-p-adi-cont" style="border-left: 0px solid #ebebeb;">
                            <span class="font-p-p">See trending projects</span>
                        </th>
                        <th class="col-p col-p-inter col-p-adi-cont icon-price-check">
                            <i class="material-icons">done</i>
                        </th>
                        <th class="col-p col-p-inter col-p-adi-cont icon-price-check">
                            <i class="material-icons">done</i>
                        </th>
                        <th class="col-p col-p-inter col-p-adi-cont icon-price-check">
                            <i class="material-icons">done</i>
                        </th>
                        <th class="col-p col-p-inter col-p-adi-cont icon-price-check">
                            <i class="material-icons">done</i>
                        </th>
                    </tr>
                    <tr class="border-p-t">
                        <th class="col-p col-p-inter col-p-adi-cont" style="border-left: 0px solid #ebebeb;">
                            <span class="font-p-p">Join groups</span>
                        </th>
                        <th class="col-p col-p-inter col-p-adi-cont icon-price-check">
                            <i class="material-icons">done</i>
                        </th>
                        <th class="col-p col-p-inter col-p-adi-cont icon-price-check">
                            <i class="material-icons">done</i>
                        </th>
                        <th class="col-p col-p-inter col-p-adi-cont icon-price-check">
                            <i class="material-icons">done</i>
                        </th>
                        <th class="col-p col-p-inter col-p-adi-cont icon-price-check">
                            <i class="material-icons">done</i>
                        </th>
                    </tr>
                    <tr class="border-p-t">
                        <th class="col-p col-p-inter col-p-adi-cont" style="border-left: 0px solid #ebebeb;">
                            <span class="font-p-p">Create groups</span>
                        </th>
                        <th class="col-p col-p-inter col-p-adi-cont icon-price-check">
                            <i class="material-icons">done</i>
                        </th>
                        <th class="col-p col-p-inter col-p-adi-cont icon-price-check">
                            <i class="material-icons">done</i>
                        </th>
                        <th class="col-p col-p-inter col-p-adi-cont icon-price-check">
                            <i class="material-icons">done</i>
                        </th>
                        <th class="col-p col-p-inter col-p-adi-cont icon-price-check">
                            <i class="material-icons">done</i>
                        </th>
                    </tr>
                    <tr class="border-p-t">
                        <th class="col-p col-p-inter col-p-adi-cont" style="border-left: 0px solid #ebebeb;">
                            <span class="font-p-p">Read our blog</span>
                        </th>
                        <th class="col-p col-p-inter col-p-adi-cont-ten">
                            <span class="font-p-p-dislable">--</span>
                        </th>
                        <th class="col-p col-p-inter col-p-adi-cont icon-price-check">
                            <i class="material-icons">done</i>
                        </th>
                        <th class="col-p col-p-inter col-p-adi-cont icon-price-check">
                            <i class="material-icons">done</i>
                        </th>
                        <th class="col-p col-p-inter col-p-adi-cont icon-price-check">
                            <i class="material-icons">done</i>
                        </th>
                    </tr>
                    <tr class="border-p-t">
                        <th class="col-p col-p-inter col-p-adi-cont" style="border-left: 0px solid #ebebeb;">
                            <span class="font-p-p">Add a bio, awards, images, videos to your profile</span>
                        </th>
                        <th class="col-p col-p-inter col-p-adi-cont icon-price-check">
                            <i class="material-icons">done</i>
                        </th>
                        <th class="col-p col-p-inter col-p-adi-cont icon-price-check">
                            <i class="material-icons">done</i>
                        </th>
                        <th class="col-p col-p-inter col-p-adi-cont icon-price-check">
                            <i class="material-icons">done</i>
                        </th>
                        <th class="col-p col-p-inter col-p-adi-cont icon-price-check">
                            <i class="material-icons">done</i>
                        </th>
                    </tr>
                    <tr class="border-p-t">
                        <th class="col-p col-p-inter col-p-adi-cont" style="border-left: 0px solid #ebebeb;">
                            <span class="font-p-p">Search and be searched by industry professionals</span>
                        </th>
                        <th class="col-p col-p-inter col-p-adi-cont icon-price-check">
                            <i class="material-icons">done</i>
                        </th>
                        <th class="col-p col-p-inter col-p-adi-cont icon-price-check">
                            <i class="material-icons">done</i>
                        </th>
                        <th class="col-p col-p-inter col-p-adi-cont icon-price-check">
                            <i class="material-icons">done</i>
                        </th>
                        <th class="col-p col-p-inter col-p-adi-cont icon-price-check">
                            <i class="material-icons">done</i>
                        </th>
                    </tr>
                    <tr class="border-p-t">
                        <th class="col-p col-p-inter col-p-adi-cont" style="border-left: 0px solid #ebebeb;">
                            <span class="font-p-p">Develop a network of Storyrocket contacts</span>
                        </th>
                        <th class="col-p col-p-inter col-p-adi-cont icon-price-check">
                            <i class="material-icons">done</i>
                        </th>
                        <th class="col-p col-p-inter col-p-adi-cont icon-price-check">
                            <i class="material-icons">done</i>
                        </th>
                        <th class="col-p col-p-inter col-p-adi-cont icon-price-check">
                            <i class="material-icons">done</i>
                        </th>
                        <th class="col-p col-p-inter col-p-adi-cont icon-price-check">
                            <i class="material-icons">done</i>
                        </th>
                    </tr>
                    <tr class="border-p-t">
                        <th class="col-p col-p-inter col-p-adi-cont" style="border-left: 0px solid #ebebeb;">
                            <span class="font-p-p">Use the “Advance Search” feature to find projects</span>
                        </th>
                        <th class="col-p col-p-inter col-p-adi-cont icon-price-check">
                            <i class="material-icons">done</i>
                        </th>
                        <th class="col-p col-p-inter col-p-adi-cont icon-price-check">
                            <i class="material-icons">done</i>
                        </th>
                        <th class="col-p col-p-inter col-p-adi-cont icon-price-check">
                            <i class="material-icons">done</i>
                        </th>
                        <th class="col-p col-p-inter col-p-adi-cont icon-price-check">
                            <i class="material-icons">done</i>
                        </th>
                    </tr>
                    <tr class="border-p-t">
                        <th class="col-p col-p-inter col-p-adi-cont" style="border-left: 0px solid #ebebeb;">
                            <span class="font-p-p">Events &amp; Contests</span>
                        </th>
                        <th class="col-p col-p-inter col-p-adi-cont text-pc ">
                            <span class="font-p-p-dislable">--</span>
                        </th>
                        <th class="col-p col-p-inter col-p-adi-cont text-pc">
                            <span class="font-p-p-dislable">Coming soon</span>
                        </th>
                        <th class="col-p col-p-inter col-p-adi-cont text-pc">
                            <span class="font-p-p-dislable">Coming soon</span>
                        </th>
                        <th class="col-p col-p-inter col-p-adi-cont text-pc">
                            <span class="font-p-p-dislable">Coming soon</span>
                        </th>
                    </tr>
                    <tr>
                        <th scope="col" colspan="5" class="bar-th border-p-b border-p-t">
                            <span>Why Post Projects?</span>
                        </th>
                    </tr>
                    <tr>
                        <th class="col-p col-p-inter col-p-adi-cont " style="border-left: 0px solid #ebebeb;">
                            <span class="font-p-p">Create professional-looking “Pitch Packages”</span>
                        </th>
                        <th class="col-p col-p-inter col-p-adi-cont text-pc ver-middle">
                            <span class="font-p-p-dislable">--</span>
                        </th>
                        <th class="col-p col-p-inter col-p-adi-cont icon-price-check">
                            <i class="material-icons">done</i>
                        </th>
                        <th class="col-p col-p-inter col-p-adi-cont icon-price-check">
                            <i class="material-icons">done</i>
                        </th>
                        <th class="col-p col-p-inter col-p-adi-cont icon-price-check">
                            <i class="material-icons">done</i>
                        </th>
                    </tr>
                    <tr class="border-p-t">
                        <th class="col-p col-p-inter col-p-adi-cont" style="border-left: 0px solid #ebebeb;">
                            <span class="font-p-p">Each project is its own micro site with unique URL</span>
                        </th>
                        <th class="col-p col-p-inter col-p-adi-cont text-pc ver-middle">
                            <span class="font-p-p-dislable">--</span>
                        </th>
                        <th class="col-p col-p-inter col-p-adi-cont icon-price-check">
                            <i class="material-icons">done</i>
                        </th>
                        <th class="col-p col-p-inter col-p-adi-cont icon-price-check">
                            <i class="material-icons">done</i>
                        </th>
                        <th class="col-p col-p-inter col-p-adi-cont icon-price-check">
                            <i class="material-icons">done</i>
                        </th>
                    </tr>

                    <tr class="border-p-t">
                        <th class="col-p col-p-inter col-p-adi-cont" style="border-left: 0px solid #ebebeb;">
                            <span class="font-p-p">Add a book cover or poster, videos, logline, tagline, cast-wish-list, sample writing and other descriptive details</span>
                        </th>
                        <th class="col-p col-p-inter col-p-adi-cont text-pc ver-middle">
                            <span class="font-p-p-dislable">--</span>
                        </th>
                        <th class="col-p col-p-inter col-p-adi-cont icon-price-check">
                            <i class="material-icons">done</i>
                        </th>
                        <th class="col-p col-p-inter col-p-adi-cont icon-price-check">
                            <i class="material-icons">done</i>
                        </th>
                        <th class="col-p col-p-inter col-p-adi-cont icon-price-check">
                            <i class="material-icons">done</i>
                        </th>
                    </tr>

                    <tr class="border-p-t">
                        <th class="col-p col-p-inter col-p-adi-cont" style="border-left: 0px solid #ebebeb;">
                            <span class="font-p-p">Promote and share (books, scripts, manuscripts, plays) easily through social media</span>
                        </th>
                        <th class="col-p col-p-inter col-p-adi-cont text-pc ver-middle">
                            <span class="font-p-p-dislable">--</span>
                        </th>
                        <th class="col-p col-p-inter col-p-adi-cont  icon-price-check">
                            <i class="material-icons">done</i>
                        </th>
                        <th class="col-p col-p-inter col-p-adi-cont  icon-price-check">
                            <i class="material-icons">done</i>
                        </th>
                        <th class="col-p col-p-inter col-p-adi-cont  icon-price-check">
                            <i class="material-icons">done</i>
                        </th>
                    </tr>

                    <tr class="border-p-t">
                        <th class="col-p col-p-inter col-p-adi-cont" style="border-left: 0px solid #ebebeb;">
                            <span class="font-p-p">Have your works stored in a database</span>
                        </th>
                        <th class="col-p col-p-inter col-p-adi-cont text-pc ver-middle">
                            <span class="font-p-p-dislable">--</span>
                        </th>
                        <th class="col-p col-p-inter col-p-adi-cont icon-price-check">
                            <i class="material-icons">done</i>
                        </th>
                        <th class="col-p col-p-inter col-p-adi-cont icon-price-check">
                            <i class="material-icons">done</i>
                        </th>
                        <th class="col-p col-p-inter col-p-adi-cont icon-price-check">
                            <i class="material-icons">done</i>
                        </th>
                    </tr>

                    <tr class="border-p-t">
                        <th class="col-p col-p-inter col-p-adi-cont" style="border-left: 0px solid #ebebeb;">
                            <span class="font-p-p">Have your projects searched by industry professionals</span>
                        </th>
                        <th class="col-p col-p-inter col-p-adi-cont text-pc ver-middle">
                            <span class="font-p-p-dislable">--</span>
                        </th>
                        <th class="col-p col-p-inter col-p-adi-cont  icon-price-check">
                            <i class="material-icons">done</i>
                        </th>
                        <th class="col-p col-p-inter col-p-adi-cont icon-price-check">
                            <i class="material-icons">done</i>
                        </th>
                        <th class="col-p col-p-inter col-p-adi-cont icon-price-check">
                            <i class="material-icons">done</i>
                        </th>
                    </tr>

                    <tr class="border-p-t">
                        <th class="col-p col-p-inter col-p-adi-cont" style="border-left: 0px solid #ebebeb;">
                            <span class="font-p-p">Have control of who you share your works with</span>
                        </th>
                        <th class="col-p col-p-inter col-p-adi-cont text-pc ver-middle">
                            <span class="font-p-p-dislable">--</span>
                        </th>
                        <th class="col-p col-p-inter col-p-adi-cont icon-price-check">
                            <i class="material-icons">done</i>
                        </th>
                        <th class="col-p col-p-inter col-p-adi-cont icon-price-check">
                            <i class="material-icons">done</i>
                        </th>
                        <th class="col-p col-p-inter col-p-adi-cont icon-price-check">
                            <i class="material-icons">done</i>
                        </th>
                    </tr>

                    <tr class="border-p-t">
                        <th class="col-p col-p-inter col-p-adi-cont" style="border-left: 0px solid #ebebeb;">
                            <span class="font-p-p">Enhanced search features to find members</span>
                        </th>
                        <th class="col-p col-p-inter col-p-adi-cont text-pc ver-middle">
                            <span class="font-p-p-dislable">--</span>
                        </th>
                        <th class="col-p col-p-inter col-p-adi-cont text-pc ver-middle">
                            <span class="font-p-p-dislable">--</span>
                        </th>
                        <th class="col-p col-p-inter col-p-adi-cont text-pc ver-middle">
                            <span class="font-p-p-dislable">--</span>
                        </th>
                        <th class="col-p col-p-inter col-p-adi-cont icon-price-check">
                            <i class="material-icons">done</i>
                        </th>
                    </tr>

                    <tr class="border-p-t">
                        <th class="col-p col-p-inter-last col-p-adi" style="border-left: 0px solid #ebebeb;">

                        </th>
                        <th class="col-p col-p-inter-last col-p-adi" style="border-left: 0px solid #ebebeb;">
                            <button type="button" name="button" class="btn-default-small btn-size ">Current Plan</button>
                        </th>
                        <th class="col-p col-p-inter-last col-p-adi" style="border-left: 0px solid #ebebeb;">
                            <a class="btn-primary-small btn-size order-link" data-type="PLUS" data-time="monthly" data-price="25.00" data-cant="10" href="{{route('subscription-form',6)}}">Buy</a>
                        </th>
                        <th class="col-p col-p-inter-last col-p-adi" style="border-left: 0px solid #ebebeb;">
                            <a class="btn-primary-small btn-size order-link" data-type="PLUS" data-time="annually" data-price="120.00" data-cant="10" href="{{route('subscription-form',7)}}">Buy</a>
                        </th>
                        <th class="col-p col-p-inter-last col-p-adi" style="border-left: 0px solid #ebebeb;">
                            <a class="btn-primary-small btn-size order-link" data-type="PROFESSIONAL" data-time="annually" data-price="195.00" data-cant="20" href="{{route('subscription-form',8)}}">Buy</a>
                        </th>
                    </tr>

                    </tbody>
                </table>

                <section class="card-talk" style="display: none;">
                    <article class="">
                        <p class="font-body-2">Need a larger plan or a custom product?<br> Our experts love to answer questions.
                        </p>
                        <button id="showempresa" type="button" name="button" class="btn-ghost-primary-medium btn-size marg-footer">Let's Talk
                        </button>
                    </article>
                    <article class="empresa-peruana" style="display: none">
                        <form class="form-agents-new" id="letsTalkForm" method="post">
                            <div class="row">
                                <div class="no-mgrn input-field col s12 m6 l6">
                                    <input name="firstname" id="firstname" type="text" class="validate" aria-required="true" data-name="firstname">
                                    <label for="firstname" class="">First Name*</label>
                                </div>
                                <div class="no-mgrn input-field col s12 m6 l6">
                                    <input name="lastname" id="lastname" type="text" class="validate" aria-required="true" data-name="lastname">
                                    <label for="lastname" class="">Last Name*</label>
                                </div>
                                <div class="no-mgrn input-field col s12">
                                    <input name="company" id="company" type="text" class="validate" aria-required="true" data-name="association-name">
                                    <label for="text" data-error="wrong" data-success="right" class="">Name of Association,
                                        Publisher, Agency or Production Company*</label>
                                </div>
                                <div class="no-mgrn input-field col s12 m6 l6" id="area-email">
                                    <input name="email" id="email" type="email" class="validate" aria-required="true" data-name="email">
                                    <label for="email" class="">Email Address*</label>
                                </div>
                                <div class="no-mgrn input-field col s12 m6 l6">
                                    <input name="phonenumber" id="phonenumber" type="text" class="validate" aria-required="true" data-name="phonenumber">
                                    <label for="phonenumber" class="">Phone Number</label>
                                </div>
                                <div class="no-mgrn input-field col s12">
                                    <textarea name="message" id="message" class="materialize-textarea" data-name="message"></textarea>
                                    <label for="message" class="">Message*</label>
                                </div>
                                <div class="no-mgrn input-field col s12 add-alerts" style="margin-bottom:10px; margin-top: 4px;">

                                </div>
                                <div class="down-login">
                                    <div class="clearfix"></div>
                                    <div class="input-field center-align">
                                        <button id="btnLetsTalkPlans" class="btn-secundary-medium" type="submint" name="action">
                                            Submit
                                        </button>
                                        <button class="btn-default-medium close-peruana" type="submint" name="action">Close
                                        </button>
                                    </div>
                                </div>

                            </div>
                        </form>
                    </article>
                </section>

                <article class="row margin-p-1">

                    <div class="col s12 m12 l4 xl4 ">
                        <div class="card-bonus">
                            <p class="font-body-2">Authors</p>
                            <div class="bar-2"></div>
                            <p class="font-p-price marg-footer">Post and be discovered. Most of you know how hard it is to be
                                discovered or even to have an agent look at your work. With Storyrocket, producers and agents
                                use filters to find just what they’re looking for… and it just may be you!</p>
                        </div>
                    </div>
                    <div class="col s12 m12 l4 xl4 ">
                        <div class="card-bonus padding-hepl">
                            <p class="font-body-2">Producers</p>
                            <div class="bar-1"></div>
                            <p class="font-p-price marg-footer">Authors, screenwriters and playwrights post their literary works
                                to the largest, self-submitted, database specifically designed for you to find the next big hit…
                                link directly to the writer and make a deal. Storyrocket takes no commissions. Zero!</p>
                        </div>
                    </div>
                    <div class="col s12 m12 l4 xl4 ">
                        <div class="card-bonus padding-hepl2">
                            <p class="font-body-2">Everyone Else</p>
                            <div class="bar-3"></div>
                            <p class="font-p-price marg-footer">Wouldn’t you like to find the next hottest project before it
                                takes off? (Yes, that’s a rocket reference). Or find like-minded literary and film enthusiasts
                                to work on an exciting project?
                                Join Storyrocket its Easy and FREE.</p>
                        </div>
                    </div>
                </article>
            </section>
        </div>
    </div>
    </div>
</div>
@endsection
