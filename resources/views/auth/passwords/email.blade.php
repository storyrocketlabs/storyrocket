@extends('layouts.app-blank')

@section('content')
    <div class="row main-login">
        <div class="center-align">
            <a href="{{route('home')}}"><img src="{{asset('images/logo-login.svg')}}" class="logo-login"></a>
            <h5 class="txt-login clr2">Forgot Password?</h5>
        </div>
        <div class="">
            <div class="area-white">
                <div class="row">
                    <form class="col s12 no-padding" id="frm-signin" method="POST" action="{{ route('password.email') }}">
                        @csrf
                        <p class="txt-reset-password left-align">Enter the email address you used when you joined and we’ll send you instructions to reset your password.</p>
                        <p class="txt-reset-password left-align">For security reasons, we do NOT store your password. So rest assured that we will never send your password via email.</p>
                        <div class="row">
                            <div class="no-mgrn input-field col s12">
                                <input id="email" type="email" name="email" class="validate {{ $errors->has('email') ? 'invalid' : '' }}" aria-required="true">
                                <label for="email" data-error="wrong" data-success="right">Email</label>
                                @if ($errors->has('email'))
                                    <span class="has-error" data-error="wrong" data-success="right">{{ $errors->first('email') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="down-login">
                            <div class="input-field few-btm-signin">
                                <button class="enviar-form waves-effect waves-light" type="submit" name="action"
                                        id="signin">
                                    Send reset instructions
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
