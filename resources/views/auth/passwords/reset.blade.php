@extends('layouts.app-blank')

@section('content')
    <div class="row main-login">
        <div class="center-align">
            <a href="{{route('home')}}"><img src="{{asset('images/logo-login.svg')}}" class="logo-login"></a>
            <h5 class="txt-login clr2">Reset Your Password</h5>
        </div>
        <div class="">
            <div class="area-white">
                <div class="row">
                    <form class="col s12 no-padding" id="frm-signin" method="POST" action="{{ route('password.update') }}">
                        @csrf
                        <input type="hidden" name="token" value="{{ $token }}">
                        <div class="row">
                            <div class="no-mgrn input-field col s12">
                                <input id="email" type="email" name="email" class="validate {{ $errors->has('email') ? 'invalid' : '' }}" aria-required="true">
                                <label for="email" data-error="wrong" data-success="right">Email</label>
                                @if ($errors->has('email'))
                                    <span class="has-error" data-error="wrong" data-success="right">{{ $errors->first('email') }}</span>
                                @endif
                            </div>
                            <div class="no-mgrn input-field col s12">
                                <input id="password" type="password" class="validate {{ $errors->has('password') ? 'invalid' : '' }}" name="password"
                                       aria-required="true">
                                <label for="password">Password</label>
                                @if ($errors->has('password'))
                                    <span class="has-error" data-error="wrong" data-success="right">{{ $errors->first('password') }}</span>
                                @endif
                            </div>
                            <div class="no-mgrn input-field col s12">
                                <input id="password-confirm" type="password" class="validate" name="password_confirmation"
                                       aria-required="true">
                                <label for="password-confirm">Confirm Password</label>
                            </div>
                        </div>
                        <div class="down-login">
                            <div class="input-field few-btm-signin">
                                <button class="enviar-form waves-effect waves-light" type="submit" name="action">
                                    Send
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
