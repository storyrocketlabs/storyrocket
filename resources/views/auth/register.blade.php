@extends('layouts.app-blank')

@section('content')
    <div class="row main-login">
        <div class="center-align">
            <a href="{{route('home')}}"><img src="{{asset('images/logo-login.svg')}}" class="logo-login"></a>
            <h5 class="txt-login clr2">Create an Account</h5>
        </div>
        <div class="">
            <div class="area-white">
                <div class="row">
                    <form class="col s12 no-padding" id="frm-singup" method="POST" action="{{ route('register') }}">
                        @csrf
                        <div class="row">
                            <div class="no-mgrn input-field col s12 m6 l6">
                                <input id="full_name" type="text" class="validate {{ $errors->has('full_name') ? 'invalid' : '' }}" aria-required="true" data-name="full_name" name="full_name" value="{{ old('full_name') }}">
                                <label for="full_name">Name</label>
                                @if ($errors->has('full_name'))
                                    <span class="has-error" data-error="wrong" data-success="right">{{ $errors->first('full_name') }}</span>
                                @endif
                            </div>
                            <div class="no-mgrn input-field col s12 m6 l6">
                                <input id="last_name" type="text" class="validate {{ $errors->has('last_name') ? 'invalid' : '' }}" aria-required="true" data-name="last_name" name="last_name" value="{{ old('last_name') }}">
                                <label for="last_name">Last Name</label>
                                @if ($errors->has('last_name'))
                                    <span class="has-error" data-error="wrong" data-success="right">{{ $errors->first('last_name') }}</span>
                                @endif
                            </div>
                            <div class="no-mgrn input-field col s12">
                                <input id="email" type="email" class="validate mail-sing {{ $errors->has('email') ? 'invalid' : '' }}" aria-required="true" data-name="email" name="email" value="{{ old('email') }}">
                                <label for="email" data-error="wrong" data-success="right">Enter your Email</label>
                                @if ($errors->has('email'))
                                    <span class="has-error" data-error="wrong" data-success="right">{{ $errors->first('email') }}</span>
                                @endif
                            </div>
                            <div class="no-mgrn input-field col s12">
                                <input id="password" type="password" class="validate {{ $errors->has('password') ? 'invalid' : '' }}" aria-required="true" data-name="password" name="password">
                                <label for="password">Password</label>
                                @if ($errors->has('password'))
                                    <span class="has-error" data-error="wrong" data-success="right">{{ $errors->first('password') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="down-login">
                            <div class="input-field few-btm-signin">
                                <button class="enviar-form waves-effect waves-light" type="Sign up" name="action">CREATE AN ACCOUNT</button>
                            </div>
                            <div class="area-login-fb">
                                <a href="" class="btn-facebook" id="FB-register">
                                    <div class="login-fb2 flex-container waves-effect waves-light">
                                        <img src="{{asset('images/fb.svg')}}">
                                        <p class="clr3">SIGN UP WITH FACEBOOK</p>
                                    </div>
                                </a>
                                <p class="space-text">
                                    <label for="test5">By clicking "Create My Account", you accept our and <span class="clr5"><a href="#" target="_blank" class="clr5">Terms and Service</a></span> and <span class="clr5"><a href="#"  target="_blank"  class="clr5">Privacy Policy.</a></span></label>
                                </p>
                            </div>
                            <p class="no-member-yet center-align">Already have an account? <a href="{{route('login')}}" class=" clr1">Sign In</a></p>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
