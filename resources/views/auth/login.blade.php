@extends('layouts.app-blank')

@section('content')
    <div class="row main-login">
        <div class="center-align">
            <a href="{{route('home')}}"><img src="{{asset('images/logo-login.svg')}}" class="logo-login"></a>
            <h5 class="txt-login clr2">Sign in</h5>
        </div>
        <div class="">
            <div class="area-white">
                <div class="row not-botom">
                    <form class="col s12 no-padding" id="frm-signin" method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="row not-botom">
                            <div class="no-mgrn input-field col s12">
                                <input id="email" type="email" name="email" class="validate {{ $errors->has('email') ? 'invalid' : '' }}" aria-required="true">
                                <label for="email" data-error="wrong" data-success="right">Email</label>
                                @if ($errors->has('email'))
                                    <span class="has-error" data-error="wrong" data-success="right">{{ $errors->first('email') }}</span>
                                @endif

                            </div>
                            <div class="no-mgrn input-field col s12">
                                <input id="password" type="password" class="validate {{ $errors->has('password') ? 'invalid' : '' }}" name="password"
                                       aria-required="true">
                                <label for="password">Password</label>
                                @if ($errors->has('password'))
                                    <span class="has-error" data-error="wrong" data-success="right">{{ $errors->first('email') }}</span>
                                @endif
                            </div>
                            <div class="down-login">
                                <p class="stayed-sign">
                                    <label>
                                        <input type="checkbox" class="filled-in" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }} />
                                        <span class="stay-sigi-in">Stay signed in</span>
                                    </label>
                                    <a href="{{url('/')}}/forgot-password" class="pull-right password-forgot clr1">Forgot password?</a>
                                </p>
                                <div class="input-field few-btm-signin">
                                    <button class="enviar-form waves-effect waves-light" type="submit" name="action"
                                            id="signin">Sign In
                                    </button>
                                </div>
                                <div class="area-login-fb">
                                    <a href="{{ route('social.auth', 'facebook') }}" class="btn-facebook" id="FB-login">
                                        <div class="login-fb2 flex-container">
                                            <img src="{{asset('images/fb.svg')}}">
                                            <p class="clr3">Sign in with Facebook</p>
                                        </div>
                                    </a>
                                    <p class="txt-fb center-align">Sign in with your Facebook it's quick, easy, and
                                        secure - your Storyrocket data will be completely private.</p>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
