@extends('layouts.app-main')

@section('content')
<div class="cover-page cover-page-copyright s-copyright">
    <div class="cover-page-content">
        <section class="banner-top-copy @auth space-top @endauth dis-flex">
            <h1 class="titles-banners center-align dis-flex-p">Copyright Policy</h1>
        </section>
        <section class="area-explains space-top space-bottom-64">
            <div class="row">
                <div class="col s12 l12">
                    <p class="main-txt-pages">
                        <span class="font-term-blod">Last Updated on December 1, 2017.</span><br><br>
                        <span class="font-term-blod">Complaints on content posted on the Storyrocket website</span><br><br>
                        Storyrocket strives to protect the intellectual property rights of others and endeavors to make sure our site contains no content that violates those rights. Our Terms of Service requires that information posted by Members be accurate, lawful and does not violate the rights of any third parties. To address any issue that may arise, Storyrocket provides a process to submit complaints concerning content posted by our Members. Our policy and procedures are described and/or referenced in the sections that follow.
                        <br>
                        <br>
                        Please note that whether or not we disable access to or remove content, Storyrocket may make a good faith attempt to forward the written notification of the “third party’s” contact information, to the Member who posted the content and/or take other reasonable steps to notify the Member that Storyrocket has received notice of an alleged violation of intellectual property rights or other content violation.
                        <br>
                        <br>
                        At our discretion, we may remove the content in question until the dispute is resolved, disable the Member’s account until the dispute is resolved and/or terminate the accounts of the Member, or group as the case may be, who infringe or repeatedly infringe the rights of others or otherwise post unlawful content.
                        <br>
                        <br>
                        Please note that any notice or counter-notice you submit must be truthful and must be submitted under penalty of perjury. A false notice or counter-notice may give rise to personal liability. You may therefore want to seek the advice of legal counsel before submitting a notice or a counter-notice.
                        <br><br>
                        <span class="font-term-blod">Claims regarding copyright infringement</span>
                        <br>
                        <br>
                        <span class="font-term-blod">Notice of Copyright Infringement:</span>
                        <br>
                        <br>
                        Pursuant to the Digital Millennium Copyright Act (17 U.S.C. § 512), Storyrocket has implemented procedures for receiving written notification of claimed infringements. Storyrocket has also designated an agent to receive notices of claimed copyright infringement. If you believe in good faith that your copyright has been infringed, you may submit a written communication which contains:</p>

                    <ul>
                        <li><p class="main-txt-pages">1. An electronic or physical signature of the person authorized to act on behalf of the owner of the copyright interest;</p></li>
                        <li><p class="main-txt-pages">2. A description of the copyrighted work that you claim has been infringed;</p></li>
                        <li><p class="main-txt-pages">3. A description specifying the location on our website of the material that you claim is infringing;</p></li>
                        <li><p class="main-txt-pages">4. Your email address and your mailing address and/or telephone number;</p></li>
                        <li><p class="main-txt-pages">5. A statement by you that you have a good faith belief that the disputed use is not authorized by the copyright owner, its agent, or the law; and</p></li>
                        <li><p class="main-txt-pages">A statement by you, made under penalty of perjury, that the information in your notice is accurate and that you are the copyright owner or authorized to act on the copyright owner’s behalf.</p></li>
                    </ul>
                    <p class="main-txt-pages">
                        <br>
                        Please submit your notice to <span><a href="/" class="clr1">Storyrocket.com</a></span> , LLC’s Copyright Agent as follows:
                        <br><br>
                        <a href="/" class="clr1">Storyrocket.com</a> LLC <br>
                        ATTN: Copyright Agent <br>
                        <a href="mailto:ronk@storyrocket.com ?subject=Copyright Agent" class="clr1">ronk@storyrocket.com </a>
                        <br>
                        <b>Counter-Notice:</b>
                        <br>
                        <br>
                        If you believe that a notice of copyright infringement has been improperly submitted against you, you may submit a Counter-Notice, pursuant to Sections 512(g)(2) and (3) of the Digital Millennium Copyright Act. You may submit a written communication which contains:
                        <br><br>
                        <ul>
                            <li><p class="main-txt-pages">1. Your physical or electronic signature;</p></li>
                    <li><p class="main-txt-pages">2. Identification of the material removed or to which access has been disabled;</p></li>
                    <li><p class="main-txt-pages">3. A statement under penalty of perjury that you have a good faith belief that removal or disablement of the material was a mistake or that the material was misidentified;</p></li>
                    <li><p class="main-txt-pages">4. Your full name, your email address, your mailing address, and a statement that you consent to the jurisdiction of the Federal District court (i) in the judicial district where your address is located if the address is in the United States, or if your address is located outside the United States, and that you will accept service of process from the Complainant submitting the notice or his/her authorized agent.</p></li>
                    </ul>
                    <br>
                    <br>
                    <p class="main-txt-pages">Please submit your Counter-Notice to Storyrocket’s Copyright Agent via email to the address specified above.</p>
                    </p>
                </div>
            </div>
        </section>

    </div>
</div>

@endsection
