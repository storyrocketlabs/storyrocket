@extends('layouts.app-main')

@section('content')
<div class="cover-page cover-page-tutorials s-tutorials">
    <div class="cover-page-content">
        <section class="banner-top-tutos @auth space-top @endauth dis-flex">
            <h1 class="titles-banners center-align dis-flex-p">Tutorials</h1>
        </section>
        <section class="container" id="tutos">
            <h4 class="des-title">These tutorials will answer commonly asked questions.<br> If what you’re looking for is not
                listed below,<br> please message us through the Help Desk.</h4>
            <div class="section">
                <ul class="collapsible">
                    <li class="tutos-mar-p">
                        <div class="collapsible-header des-mar SeeMore52">Edit a profile <i class="material-icons see-moreic">add_circle</i>
                        </div>
                        <div class="collapsible-body bod-steps">
                            <div class="row">
                                <div class="col s12 m12 l6 xl6">
                                    <h1><span>Updated:</span> August 27, 2018</h1><br>
                                    <p>It’s easy to make changes to your profile.  You can add and update as often as you want.
                                        Once you’re logged in, access your profile from one of two places.</p>
                                </div>
                                <div class="col s12 m12 l6 xl6">
                                    <div class="tuto-tips">
                                        <p>Take a look at the <span>GIFs</span> below and you’ll see how simple it is to make
                                            changes.</p>
                                        <div class="img-tip">
                                            <img src="{{asset("images/miguelito.png")}}" alt="uno"
                                                 class="full-img">
                                        </div>
                                    </div>
                                    <div class="steps-tutos">
                                        <span>ACCESS MY PROFILE</span>
                                        <ul>
                                            <li>1 – From the Navigation Bar</li>
                                            <li>2 – From the top Header</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col s12 m12 l12 xl12">
                                    <div class="steps-showing">
                                        <div class="by-steps">
                                            <h2>1 – From the Navigation Bar</h2>
                                            <p>Click your image at the top of the Navigation Bar on the left side of the
                                                monitor. Click the Edit Profile button below your name. This will open the
                                                profile form. Now edit any section or continue adding background information.
                                                When you’re done editing, remember to click Save & Return.</p>
                                            <div class="show-gif">
                                                <iframe width="100%" height="600"
                                                        src="https://www.youtube.com/embed/o6g95hArzE4?autoplay=1&fs=1&iv_load_policy=3&loop=1&rel=0&showinfo=0&showsearch=0"
                                                        frameborder="0" allow="autoplay; encrypted-media"
                                                        allowfullscreen=""></iframe>
                                            </div>
                                        </div>
                                        <div class="by-steps">
                                            <h2>2- From the top header</h2>
                                            <p>Click your image at the top right of the Header. Click the Edit Profile tab. This
                                                will open the profile form. Now edit any section or continue adding background
                                                information. When you’re done editing, remember to click Save & Return.</p>
                                            <div class="show-gif">
                                                <iframe width="100%" height="600"
                                                        src="https://www.youtube.com/embed/HwXAisQSFx4?autoplay=1&fs=1&iv_load_policy=3&loop=1&rel=0&showinfo=0&showsearch=0"
                                                        frameborder="0" allow="autoplay; encrypted-media"
                                                        allowfullscreen=""></iframe>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>

                    <li class="tutos-mar-p">
                        <div class="collapsible-header des-mar SeeMore52">Upload a profile photo<i
                                    class="material-icons see-moreic">add_circle</i></div>
                        <div class="collapsible-body bod-steps">
                            <div class="row">
                                <div class="col s12 m12 l6 xl6">
                                    <h1><span>Updated:</span>August 27, 2018</h1><br>
                                    <p>You can update your profile photo in four easy steps.</p>
                                </div>
                                <div class="col s12 m12 l6 xl6">
                                    <div class="tuto-tips">
                                        <p>Take a look at the <span>GIFs</span> below and you’ll see how simple it is to make
                                            changes.</p>
                                        <div class="img-tip">
                                            <img src="{{asset("images/miguelito.png")}}" alt="uno"
                                                 class="full-img">
                                        </div>
                                    </div>
                                    <div class="steps-tutos">
                                        <span>4 EASY STEPS</span>
                                        <ul>
                                            <li>1 – At the top right of the Header, click your image to display the drop down
                                                menu.
                                            </li>
                                            <li>2 – Click Edit Profile.</li>
                                            <li>3 – Go to your image in your profile and click the Upload photo icon.</li>
                                            <li>4 – Select a file from your computer, crop the image and upload photo.</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col s12 m12 l12 xl12">
                                    <div class="steps-showing">
                                        <div class="by-steps">
                                            <h2 class="step-help">1 – Go to your image at the top right of the header, click to
                                                display the drop down menu.</h2>
                                            <h2 class="step-help">2 – Click Edit Profile when displayed.</h2>
                                            <div class="show-gif">
                                                <iframe width="100%" height="600"
                                                        src="https://www.youtube.com/embed/3LgHAeUBJNE?autoplay=1&fs=1&iv_load_policy=3&loop=1&rel=0&showinfo=0&showsearch=0"
                                                        frameborder="0" allow="autoplay; encrypted-media"
                                                        allowfullscreen></iframe>
                                            </div>
                                        </div>
                                        <div class="by-steps">
                                            <h2 class="step-help">3 – Go to your image in your profile and click the Upload
                                                photo icon.</h2>
                                            <h2 class="step-help">4 – Select a file from your computer, crop the image and
                                                upload photo.</h2>
                                            <div class="show-gif">
                                                <iframe width="100%" height="600"
                                                        src="https://www.youtube.com/embed/vnjgLjUMOQ8?autoplay=1&fs=1&iv_load_policy=3&loop=1&rel=0&showinfo=0&showsearch=0"
                                                        frameborder="0" allow="autoplay; encrypted-media"
                                                        allowfullscreen></iframe>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>

                    <li class="tutos-mar-p">
                        <div class="collapsible-header des-mar SeeMore52">Create a Project<i class="material-icons see-moreic">add_circle</i>
                        </div>
                        <div class="collapsible-body bod-steps">
                            <div class="row">
                                <div class="col s12 m12 l6 xl6">
                                    <h1><span>Updated:</span> August 27, 2018</h1><br>
                                    <p>You can find the Create a Project button in several places throughout the site and each
                                        one will take you to the “Storyrocket Project Assistant.”  This program will walk you
                                        through the process by 1) answering questions, 2) selecting from drop down menus and 3)
                                        attaching links and files.<br><br>
                                        The Storyrocket Project Assistant saves as you go, so if you can’t finish right away,
                                        you can come back and continue the submission when you’re ready.<br><br>
                                        <strong>Note:</strong> Remember to look for unfinished submissions in your “Projects”
                                        tab, which can be found on the left side Navigation Bar.
                                    </p>
                                </div>
                                <div class="col s12 m12 l6 xl6">
                                    <div class="tuto-tips">
                                        <p> If you can’t finish in one go, continue your submission when you’re ready.</p>
                                        <div class="img-tip">
                                            <img src="{{asset("images/miguelito.png")}}" class="full-img">
                                        </div>
                                    </div>
                                    <div class="steps-tutos">
                                        <span>JUST 1 EASY STEP</span>
                                        <ul>
                                            <li>1 – Go to the top Header, click on Create a Project.</li>
                                            <!--<li>Step 2 - completa los campos de cada seccion hasta completar tu projecto</li>-->
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col s12 m12 l12 xl12">
                                    <div class="steps-showing">
                                        <div class="by-steps">
                                            <h2 class="step-help">1 – Go to the top Header, click on Create a Project.  The
                                                Storyrocket Project Assistant will launch and guide you through the submission
                                                process.</h2>
                                            <div class="show-gif">
                                                <iframe width="100%" height="600"
                                                        src="https://www.youtube.com/embed/M2J37sH02cY?autoplay=1&fs=1&iv_load_policy=3&loop=1&rel=0&showinfo=0&showsearch=0"
                                                        frameborder="0" allow="autoplay; encrypted-media"
                                                        allowfullscreen></iframe>
                                            </div>
                                        </div>

                                        <!--<div class="by-steps">
                                          <p>Step 2 - Completa los campos de cada seccion hasta completar tu projecto</p>
                                          <div class="show-gif">
                                            <img src="images/" class="full-img">
                                          </div>
                                        </div>-->

                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>


                    <li class="tutos-mar-p">
                        <div class="collapsible-header des-mar SeeMore52">Upload Videos to a Profile<i
                                    class="material-icons see-moreic">add_circle</i></div>
                        <div class="collapsible-body bod-steps">
                            <div class="row">
                                <div class="col s12 m12 l6 xl6">
                                    <h1><span>Updated:</span> August 27, 2018</h1><br>
                                    <p>Adding videos to your profile (and projects) is easy.  All you do is copy and paste the
                                        YouTube or Vimeo links.</p>
                                </div>
                                <div class="col s12 m12 l6 xl6">
                                    <div class="tuto-tips">
                                        <p>Remember to upload your video files to YouTube or Vimeo before starting this
                                            process.</p>
                                        <div class="img-tip">
                                            <img src="{{asset("images/miguelito.png")}}" class="full-img">
                                        </div>
                                    </div>
                                    <div class="steps-tutos">
                                        <span>4 EASY STEPS</span>
                                        <ul>
                                            <li>1 – Go to your image at the top, right on the header, click to display the drop
                                                down menu.
                                            </li>
                                            <li>2 – Click Edit Profile when displayed and scroll down to the Videos tab.</li>
                                            <li>3 – Add the title of your video, i.e. “Elevator Pitch For My Story” and copy and
                                                paste the YouTube or Vimeo URL in the box below.
                                            </li>
                                            <li>4 – Click Add Video and remember to click Save & Return.</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col s12 m12 l12 xl12">
                                    <div class="steps-showing">
                                        <div class="by-steps">
                                            <h2 class="step-help">1 – Go to your image at the top, right on the header, click to
                                                display the drop down menu.</h2>
                                            <h2 class="step-help">2 – Click Edit Profile when displayed and scroll down to the
                                                Videos tab.</h2>
                                            <div class="show-gif">
                                                <iframe width="100%" height="600"
                                                        src="https://www.youtube.com/embed/HhPQdCo7mjM?autoplay=1&fs=1&iv_load_policy=3&loop=1&rel=0&showinfo=0&showsearch=0"
                                                        frameborder="0" allow="autoplay; encrypted-media"
                                                        allowfullscreen></iframe>
                                            </div>
                                        </div>
                                        <div class="by-steps">
                                            <h2 class="step-help">3 – Add the title of your video, i.e. “Elevator Pitch For My
                                                Story” and copy and paste the YouTube or Vimeo URL in the box below.</h2>
                                            <h2 class="step-help">4 – Click Add Video and remember to click Save & Return.</h2>
                                            <div class="show-gif">
                                                <iframe width="100%" height="600"
                                                        src="https://www.youtube.com/embed/HhPQdCo7mjM?autoplay=1&fs=1&iv_load_policy=3&loop=1&rel=0&showinfo=0&showsearch=0"
                                                        frameborder="0" allow="autoplay; encrypted-media"
                                                        allowfullscreen></iframe>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>

                    <li class="tutos-mar-p">
                        <div class="collapsible-header des-mar SeeMore52">Upload images to a Profile<i
                                    class="material-icons see-moreic">add_circle</i></div>
                        <div class="collapsible-body bod-steps">
                            <div class="row">
                                <div class="col s12 m12 l6 xl6">
                                    <h1><span>Updated:</span> August 27, 2018</h1><br>
                                    <p>Adding images/pictures; photos, headshots and artwork lets other members know more about
                                        you.  A picture says a thousand words, so tell us more about you.<br><br>
                                        <strong>Photos:</strong> Lets us know a little more about you.  What you do and where
                                        you’ve been.  Show us some awards and adventures.<br><br>
                                        <strong>Headshots:</strong> If you’re an actor, put them up.  This is important. 
                                        Producers and Directors are looking.<br><br>
                                        <strong>Artwork:</strong> If you’re a Graphic Artist or anyone that can help filmmakers
                                        promote their works and help novelist design a book cover, show them your craft.
                                    </p>
                                </div>
                                <div class="col s12 m12 l6 xl6">
                                    <div class="tuto-tips">
                                        <p> Show them what you’re all about.  A picture says a 1000 words.</p>
                                        <div class="img-tip">
                                            <img src="{{asset("images/miguelito.png")}}" class="full-img">
                                        </div>
                                    </div>
                                    <div class="steps-tutos">
                                        <span>4 EASY STEPS</span>
                                        <ul>
                                            <li>1 – Go to your Navigation Bar on the left side of the screen, click Pictures to
                                                display the drop down menu.
                                            </li>
                                            <li>2 – Choose between Photos, Headshots and Artwork.</li>
                                            <li>3 – To add an image, click Add Images, this will open your computer files.</li>
                                            <li>4 – Select an image and click Upload Now.
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col s12 m12 l12 xl12">
                                    <div class="steps-showing">
                                        <div class="by-steps">
                                            <h2 class="step-help">1 – Go to your Navigation Bar on the left side of the screen,
                                                click Pictures to display the drop down menu</h2>
                                            <h2 class="step-help">2 – Choose between Photos, Headshots and Artwork.</h2>
                                            <div class="show-gif">
                                                <iframe width="100%" height="600"
                                                        src="https://www.youtube.com/embed/1SK-9zlLUyc?autoplay=1&fs=1&iv_load_policy=3&loop=1&rel=0&showinfo=0&showsearch=0"
                                                        frameborder="0" allow="autoplay; encrypted-media"
                                                        allowfullscreen></iframe>
                                            </div>
                                        </div>
                                        <div class="by-steps">
                                            <h2 class="step-help">3 – To add an image, click Add Images, this will open your
                                                computer files.</h2>
                                            <h2 class="step-help">4 – Select an image and click Upload Now.</h2>
                                            <div class="show-gif">
                                                <iframe width="100%" height="600"
                                                        src="https://www.youtube.com/embed/bCQKljAU4s8?autoplay=1&fs=1&iv_load_policy=3&loop=1&rel=0&showinfo=0&showsearch=0"
                                                        frameborder="0" allow="autoplay; encrypted-media"
                                                        allowfullscreen></iframe>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>

                    <li class="tutos-mar-p">
                        <div class="collapsible-header des-mar SeeMore52">Create a Group<i class="material-icons see-moreic">add_circle</i>
                        </div>
                        <div class="collapsible-body bod-steps">
                            <div class="row">
                                <div class="col s12 m12 l6 xl6">
                                    <h1><span>Updated:</span> August 27, 2018</h1><br>
                                    <p>Groups are a great way to create communities and collaboratives.  They can be as big as a
                                        1000 members or as small as a 3 member team to work on a project.  You name it what you
                                        want and invite Storyrocket members to join. You can also invite friends and associates
                                        that aren’t members of Storyrocket to join as well.  Professional groups, publishers,
                                        writing association, universities and film departments use this feature.</p>
                                </div>
                                <div class="col s12 m12 l6 xl6">
                                    <div class="tuto-tips">
                                        <p> Note to self, start a group today on Storyrocket.</p>
                                        <div class="img-tip">
                                            <img src="{{asset("images/miguelito.png")}}" class="full-img">
                                        </div>
                                    </div>
                                    <div class="steps-tutos">
                                        <span>4 EASY STEPS</span>
                                        <ul>
                                            <li>1 – Go to your Navigation Bar on the left side of the screen, click Groups to
                                                display the groups section.
                                            </li>
                                            <li>2 – Click on Create a Group at the top of this section.</li>
                                            <li>3 – Add the name of the group. Enter the location. Add a description.</li>
                                            <li>4 – Start inviting members from Storyrocket.  Then click Create Group to
                                                complete the action.
                                            </li>
                                            <br>
                                            <li>Note: Once your group is created, you can invite non-Storyrocket members to join
                                                through this feature.
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col s12 m12 l12 xl12">
                                    <div class="steps-showing">
                                        <div class="by-steps">
                                            <h2 class="step-help">1 – Go to your Navigation Bar on the left side of the screen,
                                                click Groups to display the groups section.</h2>
                                            <h2 class="step-help">2 – Click on Create a Group at the top of this section.</h2>
                                            <div class="show-gif">
                                                <iframe width="100%" height="600"
                                                        src="https://www.youtube.com/embed/NlExCW3e82Y?autoplay=1&fs=1&iv_load_policy=3&loop=1&rel=0&showinfo=0&showsearch=0"
                                                        frameborder="0" allow="autoplay; encrypted-media"
                                                        allowfullscreen></iframe>
                                            </div>
                                        </div>
                                        <div class="by-steps">
                                            <h2 class="step-help">3 – Add the name of the group. Enter the location. Add a
                                                description.</h2>
                                            <h2 class="step-help">4 – Start inviting members from Storyrocket.  Then click
                                                Create Group to complete the action.</h2>
                                            <div class="show-gif">
                                                <iframe width="100%" height="600"
                                                        src="https://www.youtube.com/embed/RocxROk8z6g?autoplay=1&fs=1&iv_load_policy=3&loop=1&rel=0&showinfo=0&showsearch=0"
                                                        frameborder="0" allow="autoplay; encrypted-media"
                                                        allowfullscreen></iframe>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>

                    <li class="tutos-mar-p">
                        <div class="collapsible-header des-mar SeeMore52">Send a Message<i class="material-icons see-moreic">add_circle</i>
                        </div>
                        <div class="collapsible-body bod-steps">
                            <div class="row">
                                <div class="col s12 m12 l6 xl6">
                                    <h1><span>Updated:</span> August 27, 2018</h1><br>
                                    <p>Use the internal messaging system to communicate with members.  Currently, it’s only for
                                        sending and receiving within the Community.<br><br>
                                        We’re working on a feature of Notifications and Preferences that will allow you to
                                        “push” your Storyrocket messages to the email associated with your account.  You’ll be
                                        able to choose which messages you want sent and which ones you’ll check when you log
                                        back into the site.<br><br>
                                        Our system is also a great way to keep track of who requested your projects, when you
                                        sent it to them and when they downloaded it.  It’s a way to keep track of who sees you
                                        IP (Intellectual Property).
                                    </p>
                                </div>
                                <div class="col s12 m12 l6 xl6">
                                    <div class="tuto-tips">
                                        <p> When someone asks to see my entire project, I only forward the PDF through
                                            Storyrocket. That way I keep track of who gets it.</p>
                                        <div class="img-tip">
                                            <img src="{{asset("images/miguelito.png")}}" class="full-img">
                                        </div>
                                    </div>
                                    <div class="steps-tutos">
                                        <span>4 EASY STEPS</span>
                                        <ul>
                                            <li>1 – Go to your Navigation Bar on the left side of the screen, click Mailbox to
                                                display the Message Center.
                                            </li>
                                            <li>2 – See if you have any new messages, or click on Compose message.</li>
                                            <li>3 – Enter the name of the member you’re messaging and the subject.</li>
                                            <li>4 – Write your message and click Send.</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col s12 m12 l12 xl12">
                                    <div class="steps-showing">
                                        <div class="by-steps">
                                            <h2 class="step-help">1 – Go to your Navigation Bar on the left side of the screen,
                                                click Mailbox to display the Message Center.</h2>
                                            <h2 class="step-help">2 – See if you have any new messages, or click on Compose
                                                message.</h2>
                                            <div class="show-gif">
                                                <iframe width="100%" height="600"
                                                        src="https://www.youtube.com/embed/f-Cgv19Y7C4?autoplay=1&fs=1&iv_load_policy=3&loop=1&rel=0&showinfo=0&showsearch=0"
                                                        frameborder="0" allow="autoplay; encrypted-media"
                                                        allowfullscreen></iframe>
                                            </div>
                                        </div>
                                        <div class="by-steps">
                                            <h2 class="step-help">3 – Enter the name of the member you’re messaging and the
                                                subject.</h2>
                                            <h2 class="step-help">4 – Write your message and click Send.</h2>
                                            <div class="show-gif">
                                                <iframe width="100%" height="600"
                                                        src="https://www.youtube.com/embed/2H4JbjdwMBU?autoplay=1&fs=1&iv_load_policy=3&loop=1&rel=0&showinfo=0&showsearch=0"
                                                        frameborder="0" allow="autoplay; encrypted-media"
                                                        allowfullscreen></iframe>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>

                    <li class="tutos-mar-p">
                        <div class="collapsible-header des-mar SeeMore52">Select a plan<i class="material-icons see-moreic">add_circle</i>
                        </div>
                        <div class="collapsible-body bod-steps">
                            <div class="row">
                                <div class="col s12 m12 l6 xl6">
                                    <h1><span>Updated:</span> August 27, 2018</h1><br>
                                    <p>Membership is free.  Posting projects is how we pay the bills. But even then, our goal is
                                        keep this service as affordable and value based as possible. When you’re ready to upload
                                        projects, you’ll need to select a plan… here’s how you do it.</p>
                                </div>
                                <div class="col s12 m12 l6 xl6">
                                    <div class="tuto-tips">
                                        <p> I’ll start with the PLUS Plan.</p>
                                        <div class="img-tip">
                                            <img src="{{asset("images/miguelito.png")}}" class="full-img">
                                        </div>
                                    </div>
                                    <div class="steps-tutos">
                                        <span> 4 EASY STEPS</span>
                                        <ul>
                                            <li>1 – Go to the top Header, click the Select Your Plan button.</li>
                                            <li>2 – Choose the plan you want and then select if you want monthly or annual
                                                billing.
                                            </li>
                                            <li>3 – Fill out your credit card information and apply a code if you have one.</li>
                                            <li>4 – Click PROCESS ORDER to complete the transaction.</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col s12 m12 l12 xl12">
                                    <div class="steps-showing">
                                        <div class="by-steps">
                                            <h2 class="step-help">1 – Go to the top header, click the SELECT YOUR PLAN
                                                button.</h2>
                                            <h2 class="step-help">2 – Choose the Plan you want and then select if you want
                                                monthly or annual billing.</h2>
                                            <div class="show-gif">
                                                <iframe width="100%" height="600"
                                                        src="https://www.youtube.com/embed/JeYkYMP_CK4?autoplay=1&fs=1&iv_load_policy=3&loop=1&rel=0&showinfo=0&showsearch=0"
                                                        frameborder="0" allow="autoplay; encrypted-media"
                                                        allowfullscreen></iframe>
                                            </div>
                                        </div>
                                        <div class="by-steps">
                                            <h2 class="step-help">3 – Fill out your credit card information and apply a code if
                                                you have one.</h2>
                                            <h2 class="step-help">4 – Click PROCESS ORDER to complete the transaction.</h2>
                                            <div class="show-gif">
                                                <iframe width="100%" height="600"
                                                        src="https://www.youtube.com/embed/UVBUIQDXNqQ?autoplay=1&fs=1&iv_load_policy=3&loop=1&rel=0&showinfo=0&showsearch=0"
                                                        frameborder="0" allow="autoplay; encrypted-media"
                                                        allowfullscreen></iframe>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>

                </ul>
            </div>
            <br><br>
        </section>



    </div>
</div>

@endsection
