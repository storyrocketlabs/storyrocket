@extends('layouts.app-main')

@section('content')
<div class="cover-page cover-page-term-of-service s-term-of-service">
    <div class="cover-page-content">

        <section class="banner-top-terms-service @auth space-top @endauth dis-flex">
            <h1 class="titles-banners center-align dis-flex-p">Storyrocket – Terms of Use</h1>
        </section>
        <section class="area-explains space-top space-bottom-64">
            <div class="row">
                <div class="col s12 l12">
                    <p class="main-txt-pages">
                        <span class="font-term-blod">Last updated:&nbsp; December 2016.&nbsp;</span> These terms and conditions are effective immediately for those registering accounts on or after the last updated date.
                        <br>
                        <br><span class="font-term-blod">1. &nbsp;&nbsp;Acceptance of Terms of Use
<br><br xmlns="http://www.w3.org/1999/xhtml"></span> Please read these Terms of Use (the “Terms”) carefully because they govern your use or access of our services accessible via our website at http://storyrocket.com (the “Service”).&nbsp; The Service is owned and controlled by Storyrocket.Com, LLC, a Florida limited liability company.&nbsp; By accessing, contributing, using, and/or signing up for the Service, you are agreeing to be legally bound by these Terms.&nbsp; If you do not agree to be legally bound by all the Terms, immediately stop using or accessing the Service.
                        <br>
                        <br><span class="font-term-blod">2. &nbsp;Definitions</span>
                        <br>
                        <br><span class="font-term-blod">&nbsp;&nbsp;a. Parties
</span>
                        <br><span class="font-term-blod">- “You”</span> and <span class="font-term-blod">“Your”</span> refer to you, as a user of the Service.
                        <br><span class="font-term-blod">- “User”</span> and <span class="font-term-blod">“Using”</span> refer to any person or entity who accesses or uses the Service with crawlers, robots, browsers, data mining, or extraction tools, or other functionality, whether such functionality is installed or placed by such person or entity or a third party. <span class="font-term-blod">- “We,”</span> <span class="font-term-blod">“Us,”</span> and <span class="font-term-blod">“Our”</span> refer to Storyrocket, the Service, and Storyrocket.com, LLC.
                        <br>
                        <br>&nbsp;&nbsp;<span class="font-term-blod">b. Content&nbsp;
</span>
                        <br><span class="font-term-blod">- “Content”</span> means text, data, files, information, usernames, images, graphics, photos, profiles, audio and video clips, sounds, musical works, works of authorship, applications, links and all other forms of data or communication.
                        <br><span class="font-term-blod">- “Your Content”</span> means Content that you submit or transmit to, through, or in connection with the Service, including but not limited to, comments, invitations, photographs, links, messages, and other materials or any information that you publicly display or displayed in your account profile.
                        <br><span class="font-term-blod">- "Third Party Content"</span> means Content that originates from parties other than Storyrocket or its users, which is made available in connection with the Service. "Site Content" means all of the Content that is made available in connection with the Service, including Your Content, and Third Party Content.
                        <br>
                        <br><span class="font-term-blod">3. &nbsp;Changes to the Terms of Service&nbsp;</span>
                        <br>
                        <br> Storyrocket may change these Terms from time to time, at Storyrocket’s discretion.&nbsp; The most current version of these Terms will be located here.&nbsp; Your continued use of the Service following the posting of such changes will constitute your assent to all such changes to these Terms.&nbsp; We will indicate at the top of this page the date that revisions were last made.&nbsp; You should revisit these Terms on a regular basis as revised Terms will be binding on you and will supersede all prior version(s) of the Terms, unless stated otherwise.
                        <br>
                        <br><span class="font-term-blod">4. &nbsp;Access to Site</span>
                        <br>
                        <br> Storyrocket retains the right at our sole discretion to deny service or access to the Service or an account to anyone, at any time, for any reason.&nbsp; You acknowledge that there may be interruptions in service or events that are beyond our control.&nbsp; While we use reasonable efforts to keep the Service accessible, the Service may be unavailable from time to time due to modification, suspension, interruption, routine maintenance, and for any other reason without limitation.
                        <br>
                        <br><span class="font-term-blod">5. &nbsp;Registration/Usage of Account </span>&nbsp;
                        <br>
                        <br> To utilize certain portions of the Service, you will be required to complete a registration process and establish an account with Storyrocket (the <span class="font-term-blod">“Account”</span>) and each Account will be linked to a user’s profile page (the <span class="font-term-blod">“Profile Page”</span>).&nbsp; You represent and warrant that all information provided by you to Storyrocket is current, accurate, and complete, and that you will maintain the accuracy and completeness of this information on a prompt, timely basis.&nbsp; Your Account is non-transferrable and may not be sold, combined or otherwise shared with any person.&nbsp; You may only create and hold one Account that you are solely responsible for managing.&nbsp; If you violate any of these limitations we may terminate your Account.&nbsp; If we terminate your Account, you may not register or join under a new account unless we formally invite you.&nbsp; If you commit fraud or falsify information in connection with your use of the Service or in connection with your Account, your account will be terminated immediately and we reserve the right to hold you liable for any and all damages we suffer, to pursue legal action through relevant local and national law enforcement authorities and to notify your Internet Service Provider of any fraudulent activity we associate with you or your use of the Service.&nbsp; Our use of any personally identifiable information, (a term defined in our Privacy Policy), you provide to us as part of the registration process is governed by the terms of our Privacy Policy.
                        <br>
                        <br><span class="font-term-blod">6. &nbsp;Password and Security</span>
                        <br>
                        <br> As a registered user of the Service, you are solely responsible for maintaining the confidentiality and security of your password and Account.&nbsp; You understand and agree that you are individually and fully responsible for all actions and postings made from your Account.&nbsp; You agree to notify Storyrocket immediately if you become aware of any unauthorized use of your Account.
                        <br>
                        <br><span class="font-term-blod">7. &nbsp;Cancellation&nbsp;</span>
                        <br>
                        <br> You are solely responsible for properly cancelling your account.&nbsp; You can cancel your account at any time by accessing “user preferences” and choosing the “delete account” option.&nbsp; All of Your Content will be deleted from the Service upon cancellation.&nbsp; This information cannot be recovered once your account is cancelled.&nbsp; Please note that some user information may remain in archived/backup copies for our records or as otherwise required by law.
                        <br>
                        <br><span class="font-term-blod">8. &nbsp;General Conditions</span>
                        <br>As a condition of your use of the Service, you agree that:
                        <br>
                        <br>- You must be at least 13 years of age as the Service is not intended for children under the age of 13.&nbsp; If you are between the ages of 13 and 17, then you further agree that you are either an emancipated minor, or possess legal parental or guardian consent, and are fully able and competent to enter into the terms, conditions, obligations, affirmations, representations, and warranties set forth in these Terms, and to abide by and comply with these Terms or you have obtained legal parental or guardian consent.
                        <br>
                        <br>- Your use of the Service is at your sole risk.
                        <br>
                        <br>- You are responsible for updating and correcting information you have submitted to us.
                        <br>
                        <br>- You are not barred from receiving products or services under U.S. law.
                        <br>
                        <br>- You may not use the Service for any illegal or unauthorized purpose.&nbsp; You agree to comply with all the laws, rules and regulations (for example, federal, state, local, and provincial) applicable to your use of the Service and Your Content, including but not limited to, copyright laws.
                        <br>
                        <br>- You must not interfere or disrupt the Service or servers or networks connected to the Service, including by transmitting any worms, viruses, spyware, malware or any other code of a destructive or disruptive nature.&nbsp; You may not inject content or code or otherwise alter or interfere with the way any Storyrocket page is rendered or displayed in a user's browser or device.
                        <br>
                        <br>- There may be links from the Service, or from communications you receive from the Service to third-party web sites or features. There may also be links to third-party web sites or features in images or comments within the Service. The Service also includes Third Party Content that we do not control, maintain or endorse. Functionality on the Service may also permit interactions between the Service and a third party web site or feature, including applications that connect the Service or your profile on the Service with a third-party web site or feature.&nbsp; For example, the Service may include a feature that enables you to share Content from the Service or Your Content with a third party, which may be publicly posted on that third party's service or application.&nbsp; Using this functionality typically requires you to login to your account on the third party service and you do so at your own risk. Storyrocket does not control any of these third party web services or any of their content.&nbsp; You expressly acknowledge and agree that Storyrocket is in no way responsible or liable for any such third-party services or features.&nbsp; YOUR CORRESPONDENCE AND BUSINESS DEALINGS WITH THIRD PARTIES FOUND THROUGH THE SERVICE ARE SOLELY BETWEEN YOU AND THE THIRD PARTY.&nbsp; You may choose, at your sole and absolute discretion and risk, to use applications that connect the Service or your profile on the Service with a third-party service (each, an "Application") and such Application may interact with, connect to or gather and/or pull information from and to your Service profile.&nbsp; By using such Applications, you acknowledge and agree to the following: (i) if you use an Application to share information, you are consenting to information about your profile on the Service being shared; (ii) your use of an Application may cause personally identifying information to be publicly disclosed and/or associated with you, even if Storyrocket has not itself provided such information; and (iii) your use of an Application is at your own option and risk, and you hold Storyrocket, its employees, managers, officers or agents harmless for activity related to the Application.
                        <br>
                        <br>- You are solely responsible for your conduct and Your Content that you submit, post or display on or via the Service.
                        <br>
                        <br>- You are solely responsible for your interaction with other users of the Service, whether online or offline. You agree that Storyrocket is not responsible or liable for the conduct of any user.&nbsp; Storyrocket reserves the right, but has no obligation, to monitor or become involved in disputes between you and other users.
                        <br>
                        <br>- We reserve the right to force forfeiture of any username for any reason.
                        <br>
                        <br>- We may, but have no obligation to, remove, edit, block, and/or monitor Content or accounts containing Content that we determine in our sole discretion violates these Terms.
                        <br>
                        <br>- You agree that you are responsible for all data charges you incur through use of the Service.
                        <br>
                        <br>- We can remove any Content or information you post on Storyrocket if we believe it violates these Terms or our policies.
                        <br>
                        <br><span class="font-term-blod">9. Prohibited Conduct&nbsp;</span>
                        <br> All of your interactions with our Service must be lawful and comply with these Terms.&nbsp; You shall not attempt to or engage in potentially harmful acts that are directed against our Service including, without limitation, any one or more of the following:
                        <br>
                        <br>- Submitting any purposely inaccurate information.
                        <br>
                        <br>- Committing fraud or falsifying information in connection with your Storyrocket Account in order to create multiple Storyrocket accounts.
                        <br>
                        <br>- Causing, allowing, or assisting any other person to impersonate you, or sharing your password or login with any other person.
                        <br>
                        <br>- Forging user names, manipulating identifiers, or otherwise impersonating any other person or misrepresenting your identity or affiliation with any person or entity.
                        <br>
                        <br>- Attempting to access, or actually accessing, data not intended for you, such as logging into a server or an account which you are not authorized to access.
                        <br>
                        <br>- Tampering or interfering with the proper functioning of any part of the Service.
                        <br>
                        <br>- Defaming, stalking, bullying, abusing, harassing, threatening, impersonating or intimidating people or entities and you must not post private or confidential information via the Service, including, without limitation, your or any other person's credit card information, social security or alternate national identity numbers, non-public phone numbers or non-public email addresses.
                        <br>
                        <br>- Accessing, monitoring or copying any Content or information from the Service using any “robot,” “spider,” “scraper” or other automated means or any manual process for any purpose without our express written permission.
                        <br>
                        <br>- Collecting the personal information, statements, data or Content of any other users (except as expressly authorized).
                        <br>
                        <br>- Attempting to decipher, decompile, dissemble, or reverse engineer any of the software used to provide the Service or Content.
                        <br>
                        <br>- Interfering with, or attempting to interfere with, the access of any user, host or network, including, without limitation, sending a virus, overloading, flooding, spamming, or mail-bombing the Service.
                        <br>
                        <br>- Avoiding, bypassing, removing, deactivating, impairing, descrambling or otherwise circumventing any technological measure implemented by Storyrocket or any other third party (including another user) to protect the Service or Site Content.
                        <br>
                        <br>- Attempting to probe, scan, or test the vulnerability of the Service, or any associated system or network, or breach any security or authentication measures.
                        <br>
                        <br>- Harvesting or collecting email addresses or other contact information of other users or clients from the Service by electronic or other means.
                        <br>
                        <br>- Engaging in “framing,” “mirroring,” or otherwise simulating the appearance or function of the Service.
                        <br>
                        <br>- Acting illegally or maliciously against the business interests or reputation of Storyrocket.
                        <br>
                        <br>- Removing, covering or obscuring any advertisement included on the Service.
                        <br>
                        <br>- Using the Service or any of its Content to advertise or solicit, for any other commercial, political or religious purpose, or to compete, directly or indirectly with Storyrocket.
                        <br>
                        <br>- Posting Content or take any action on Storyrocket that infringes or violates someone else’s rights or otherwise violates the law, including, without limitation, privacy laws, intellectual property laws, and anti-spam laws.
                        <br>
                        <br>- Overriding any security feature of the Service.
                        <br>
                        <br><span class="font-term-blod">10. &nbsp;Termination</span>
                        <br>
                        <br> Storyrocket, in its sole discretion, has the right to suspend or terminate your Account and refuse any and all current or future use of the Service for any reason at any time.&nbsp; Such termination of the Service will result in the deactivation or deletion of your Account or your access to your Account, and the forfeiture and relinquishment of all Content in your Account.&nbsp; Storyrocket reserves the right to refuse service to anyone for any reason at any time.&nbsp; Furthermore, violation of the Terms may, in Storyrocket’s sole discretion, result in termination of your Storyrocket account.&nbsp; If you violate the letter or spirit of these Terms, or otherwise create risk or possible legal exposure for Storyrocket, we can stop providing all or part of the Service to you.
                        <br>
                        <br><span class="font-term-blod">11. &nbsp;Release/Indemnification</span>
                        <br>
                        <br> You agree to defend, indemnify and hold harmless Storyrocket, its affiliates, and their respective directors, officers, employees, and agents from and against all claims and expenses, including attorney’s fees, arising out of or related to your use of the Service, your violation of these Terms, the infringement by you or made under your Account, of any intellectual property or other right of any person or entity or arising out of or related to any products or services used by you in connection with the Service.
                        <br>
                        <br><span class="font-term-blod">12. &nbsp;Trademark Protection
</span>
                        <br>
                        <br> You agree that all of Storyrocket’s trademarks, trade names, service marks, and other logos and brand features, including “Storyrocket,” (collectively, the “Marks”) displayed in connection with the Service are trademarks and the property of Storyrocket.&nbsp; You agree not to display or use Storyrocket’s Marks in any manner without Storyrocket’s prior permission.
                        <br>
                        <br><span class="font-term-blod">13. &nbsp;Ownership of Service</span>
                        <br>
                        <br> The Content and information we provide you through our Service as well as the infrastructure used to offer the Service, is proprietary to us, our users, and other partners.&nbsp; Storyrocket grants you a limited, personal, nontransferable, nonexclusive, revocable license to access the Service for your own personal non-commercial purposes and use the Service pursuant to these Terms and to any additional terms and policies set forth by Storyrocket.&nbsp; All rights not expressly granted to you in these Terms are reserved and retained by Storyrocket or its licensors, suppliers, publishers or other content providers.&nbsp; You agree not to reproduce, modify, distribute, create derivative works from, publicly display, publicly perform, license, sell or re-sell any data, Content, information, software, or products obtained through our Service without the express permission of Storyrocket.&nbsp; In addition, you may not make any use that exceeds or violates these Terms.&nbsp; Storyrocket’s licenses granted to you terminate if you do not comply with these Terms and Storyrocket may opt to terminate your Account.
                        <br>
                        <br><span class="font-term-blod">14. &nbsp;Your License to Storyrocket
</span>
                        <br>
                        <br> When providing us with Your Content (not including “Your Original Works” as defined in the following paragraph), or causing Content to be posted using our Service, you grant us a nonexclusive, worldwide, transferable, royalty-free, sublicensable license to use, store, display, reproduce and allow us to exercise any and all copyright, publicity, trademarks, database rights and other intellectual property rights you have in the Content.<br>Your participation in this site does not give up any rights to your work or vest any rights to your work to Storyrocket, or any third party, unless separately agreed to in a written agreement between you and Storyrocket and signed by all parties.<br><br> Your Original Works shall include but is not limited to, novels, screenplays, manuscripts, articles and/or any other similar works in which you possess a copyright ownership.  When posting/uploading Your Original Works on our Service, you grant us a nonexclusive, worldwide, transferable, royalty-free, sublicensable license to store, display, and otherwise make available Your Original Works on Storyrocket for purposes of operating, developing, and providing the Service.
                        <br>
                        <br><span class="font-term-blod">15. &nbsp;Warranty Disclaimers</span>
                        <br>
                        <br> THE SERVICE AND CONTENT ARE PROVIDED “AS IS,” WITHOUT WARRANTY OF ANY KIND.&nbsp; WITHOUT LIMITING THE FOREGOING, WE EXPLICITLY DISCLAIM ANY IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, QUIET ENJOYMENT AND NON-INFRINGEMENT AND ANY WARRANTIES ARISING OUT OF COURSE OF DEALING OR USAGE OF TRADE.&nbsp; WE MAKE NO WARRANTY THAT THE SERVICE WILL MEET YOUR REQUIREMENTS, THAT THE CONTENT OR ANY INFORMATION RECEIVED THROUGH THIS WEBSITE IS FREE OF VIRUSES OR OTHER HARMFUL COMPONENTS OR BE AVAILABLE ON AN UNINTERRUPTED, SECURE, OR ERROR-FREE BASIS. WE MAKE NO WARRANTY REGARDING THE QUALITY, ACCURACY, TIMELINESS, TRUTHFULNESS, COMPLETENESS OR RELIABILITY OF ANY CONTENT.&nbsp; BECAUSE SOME JURISDICTIONS DO NOT PERMIT THE EXCLUSION OF CERTAIN WARRANTIES, THESE EXCLUSIONS MAY NOT APPLY TO YOU BUT SHALL APPLY AS MUCH AS PERMITTED BY LAW OF YOUR JURISDICTION.
                        <br>
                        <br><span class="font-term-blod">16. &nbsp;Limitation of Liability&nbsp;</span>
                        <br>
                        <br> NEITHER STORYROCKET NOR ANY OTHER PARTY INVOLVED IN CREATING, PRODUCING, OR DELIVERING THE SERVICE OR CONTENT WILL BE LIABLE FOR ANY INCIDENTAL, SPECIAL, EXEMPLARY OR CONSEQUENTIAL DAMAGES, INCLUDING LOST PROFITS, LOSS OF DATA OR GOODWILL, SERVICE INTERRUPTION, COMPUTER DAMAGE OR SYSTEM FAILURE OR THE COST OF SUBSTITUTE SERVICE ARISING OUT OF OR IN CONNECTION WITH THESE TERMS OR FROM THE USE OF OR INABILITY TO USE THE SERVICE OR CONTENT, WHETHER BASED ON WARRANTY, CONTRACT, TORT (INCLUDING NEGLIGENCE), PRODUCT LIABILITY OR ANY OTHER LEGAL THEORY, AND WHETHER OR NOT STORYROCKET HAS BEEN INFORMED OF THE POSSIBILITY OF SUCH DAMAGE, EVEN IF A LIMITED REMEDY SET FORTH HEREIN IS FOUND TO HAVE FAILED OF ITS ESSENTIAL PURPOSE.&nbsp; SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF LIABILITY FOR CONSEQUENTIAL OR INCIDENTAL DAMAGES, SO THE ABOVE LIMITATION MAY NOT APPLY TO YOU.&nbsp;
                        <br> YOU AGREE THAT IN THE EVENT YOU INCUR ANY DAMAGES, LOSSES OR INJURIES THAT ARISE OUT OF STORYROCKET’S ACTS OR OMISSIONS, THE DAMAGES, IF ANY, CAUSED TO YOU ARE NOT IRREPARABLE OR SUFFICIENT TO ENTITLE YOU TO AN INJUNCTION PREVENTING ANY EXPLOITATION OF ANY WEBSITE, SERVICE, PROPERTY, PRODUCT OR OTHER CONTENT OWNED OR CONTROLLED BY STORYROCKET, AND YOU WILL HAVE NO RIGHTS TO ENJOIN OR RESTRAIN THE DEVELOPMENT, PRODUCTION, DISTRIBUTION, ADVERTISING, EXHIBITION OR EXPLOITATION OF ANY WEBSITE, PROPERTY, PRODUCT, SERVICE, OR OTHER CONTENT OWNED OR CONTROLLED BY STORYROCKET.
                        <br> YOUR SOLE AND EXCLUSIVE RIGHT AND REMEDY IN CASE OF DISSATISFACTION WITH THE SERVICE, OR ANY OTHER GRIEVANCE SHALL BE YOUR TERMINATION AND DISCONTINUATION OF ACCESS TO, OR USE OF THE SERVICE.
                        <br> IN NO EVENT WILL STORYROCKET’S MAXIMUM AGGREGATE LIABILITY ARISING OUT OF OR IN CONNECTION WITH THESE TERMS OR FROM THE USE OF OR INABILITY TO USE THE SERVICE OR CONTENT EXCEED THE GREATER OF (i) THE AMOUNT PAID, IF ANY, BY YOU TO STORYROCKET IN CONNECTION WITH THE SERVICE IN THE 12 MONTHS PRIOR TO THE ACTION GIVING RISE TO LIABILITY, OR (ii) ONE HUNDRED UNITED STATES DOLLARS ($100).&nbsp; THE LIMITATIONS OF DAMAGES SET FORTH ABOVE ARE FUNDAMENTAL ELEMENTS OF THE BASIS OF THE BARGAIN BETWEEN STORYROCKET AND YOU.&nbsp;
                        <br> STORYROCKET IS NOT RESPONSIBLE FOR THE ACTIONS, CONTENT, INFORMATION, OR DATA OF THIRD PARTIES, AND YOU RELEASE US, OUR DIRECTORS, OFFICERS, EMPLOYEES, AND AGENTS FROM ANY CLAIMS AND DAMAGES, KNOWN AND UNKNOWN, ARISING OUT OF OR IN ANY WAY CONNECTED WITH ANY CLAIM YOU HAVE AGAINST ANY SUCH THIRD PARTIES.
                        <br>
                        <br><span class="font-term-blod">18. &nbsp;Waiver</span>
                        <br>
                        <br> The failure of Storyrocket to exercise or enforce any right or provision of the Terms shall not constitute a waiver of such right or provision.&nbsp; The waiver of any such right or provision will be effective only if in writing and signed by a duly authorized representative of Storyrocket.&nbsp; Except as expressly set forth in these Terms, the exercise by either party of any of its remedies under these Terms will be without prejudice to its other remedies under these Terms or otherwise.
                        <br>
                        <br><span class="font-term-blod">19.&nbsp;&nbsp;Arbitration</span>
                        <br>
                        <br> We will make every reasonable effort to resolve any disagreements that you have with Storyrocket.&nbsp; If those efforts fail, by using our Service you agree that any claim, dispute, or controversy you may have against Storyrocket arising out of, relating to, or connected in any way with these Terms or the Service, shall be resolved exclusively by final and binding arbitration administered by the American Arbitration Association (“AAA”) and conducted before a single arbitrator pursuant to the applicable Rules and Procedures established by AAA (“Rules and Procedures”).&nbsp; You agree further that: (a) the arbitration shall be held in Miami-Dade County, Florida; (b) the arbitrator shall apply Florida law consistent with the Federal Arbitration Act and applicable statutes of limitations, and shall honor claims of privilege recognized at law; (c) THERE SHALL BE NO AUTHORITY FOR ANY CLAIMS TO BE ARBITRATED ON A CLASS OR REPRESENTATIVE BASIS; ARBITRATION CAN DECIDE ONLY YOUR AND/OR STORYROCKET’S INDIVIDUAL CLAIMS; AND THE ARBITRATOR MAY NOT CONSOLIDATE OR JOIN THE CLAIMS OF OTHER PERSONS OR PARTIES WHO MAY BE SIMILARLY SITUATED; (d) in the event that you are able to demonstrate that the costs of arbitration will be prohibitive as compared to the costs of litigation, Storyrocket will pay as much of your filing and hearing fees in connection with the arbitration ( subject to reimbursement of costs to the extent the claim is found by the arbitrator to be frivolous), as the arbitrator deems necessary to prevent the arbitration from being cost-prohibitive, but if the costs of litigation; and (e) with the exception of subpart (c) above, if any part of this arbitration provision is deemed to be invalid, unenforceable or illegal, or otherwise conflicts with the Rules and Procedures established by AAA, then the balance of this arbitration provision shall remain in effect and shall be construed in accordance with its terms as if the invalid, unenforceable, illegal or conflicting provision were not contained herein.&nbsp; If, however, subpart (c) is found to be invalid, unenforceable or illegal, then the entirety of this Section 18 shall be null and void, and neither you nor Storyrocket shall be entitled to arbitrate their <span>dispute</span>.
                        <br>
                        <br>IF FOR ANY REASON A CLAIM PROCEEDS IN COURT RATHER THAN IN ARBITRATION WE EACH WAIVE ANY RIGHT TO A JURY TRIAL.
                        <br>
                        <br> THIS ARBITRATION PROVISION WILL SURVIVE THE TERMINATION OF YOUR RELATIONSHIP WITH STORYROCKET.
                        <br>
                        <br><span class="font-term-blod">19. &nbsp;Choice of Law and Jurisdiction
</span>
                        <br>
                        <br> These Terms shall be governed by the laws of the State of Florida, without respect to its conflict of laws principles and will specifically not be governed by the United Nations Conventions on Contracts for the International Sale of Goods, if otherwise applicable.&nbsp; We each agree to submit to the personal jurisdiction of a state court located in Miami-Dade County, Florida or the United States District Court for the Southern District of Florida, for any actions not subject to Section 18 (Arbitration).
                        <br>
                        <br><span class="font-term-blod">20. &nbsp;Entire Agreement</span>
                        <br>
                        <br> These Terms, together with the Privacy Policy and any amendments and any additional agreements you may enter into with Storyrocket in connection with the Service, shall constitute the entire agreement between you and Storyrocket concerning the Service.&nbsp; You will not assign these Terms or assign any rights or delegate any obligations hereunder, in whole or in part, whether voluntarily or by operation of law, without the prior written consent of Storyrocket.&nbsp; Any purported assignment or delegation by you without the appropriate prior written consent of Storyrocket will be null and void. Storyrocket may assign these Terms or any rights hereunder without your consent. If any provision of these Terms, except for Arbitration Section 18, subpart (c), is deemed invalid, then that provision will be limited or eliminated to the minimum extent necessary, and the remaining provisions of these Terms will remain in full force and effect.
                        <br>
                        <br><span class="font-term-blod">21. &nbsp;Making Claims and Counter-Claims of Copyright Infringement&nbsp;
</span>
                        <br>
                        <br> Storyrocket respects the intellectual property rights of others and expects its users to do the same.&nbsp; It is Storyrocket’s policy, in appropriate circumstances and at its discretion, to disable and/or terminate the Accounts of users who repeatedly infringe or are repeatedly charged with infringing the copyrights or other intellectual property rights of others.
                        <br>
                        <br><span class="font-term-l">Notice</span>
                        <br>
                        <br> If you believe that any material has been posted via our Service in a way that constitutes copyright infringement, and you would like to bring this material to Storyrocket’s attention, in accordance with the Digital Millennium Copyright Act (“DMCA”) you must provide Storyrocket’s DMCA Agent with the following information: (a) an electronic or physical signature of the person authorized to act on behalf of the owner of the copyrighted work; (b) a description of the copyrighted work that you claim is being infringed; (c) a description identifying the location on our website of the allegedly infringing work; (d) a written statement that you have a good faith belief that the disputed use is not authorized by the owner, its agent, or the law; (e) your name and contact information, including telephone number and e-mail address; and (f) a statement by you that the above information in your notice is accurate and, under penalty of perjury, that you are the copyright owner or authorized to act on the copyright owner’s behalf.
                        <br>
                        <br><span class="font-term-l">Counter-Notice
</span>
                        <br>
                        <br> If you believe that a notice of copyright infringement has been improperly submitted against you, you may submit a counter-notice, pursuant to Sections 512(g)(2) and (3) of the DMCA. You must provide Storyrocket’s DCMA agent with the following information:&nbsp; (a) your physical signature; (b) identification of the material removed or to which access has been disabled; (c) a statement under penalty of perjury that you have a good faith belief that removal or disablement of the material was a mistake or that the material was misidentified; (d) your name and contact information, including telephone number and e-mail address, and a statement that you consent to the jurisdiction of the Federal District Court (i) in the judicial district where your address is located if the address is in the United States, or (ii) located in the Southern District of Florida (Miami-Dade County), if your address is located outside the United States, and that you will accept service of process from the complainant submitting the notice or his/her authorized agent.
                        <br>
                        <br>The contact information for Storyrocket’s DMCA Agent for notice of claims of copyright infringement is:
                        <br>
                        <br>Storyrocket.Com, LLC&nbsp;
                        <br>Attn: Copyright Agent&nbsp;
                        <br>12955 Biscayne Blvd., Ste. 320&nbsp;
                        <br>Miami, FL 33181
                        <br> Email:&nbsp; <a href="mailto:copyrightagent@Storyrocket.com?subject=feedback" "email me" class="clr1">copyrightagent@Storyrocket.com</a>
                        <br>
                        <br>
                        <br><span class="font-term-blod">22. &nbsp;Electronic Communications</span>
                        <br>
                        <br> When you use our Service, or send e-mails to us, you are communicating with us electronically.&nbsp; You consent to receive communications from us electronically.&nbsp; We will communicate with you by e-mail or by posting notices on our website or through other means, such as our internal messaging system.&nbsp; You agree that all agreements, notices, disclosures and other communications that we provide to you electronically satisfy any legal requirement that such communications be in writing.
                        <br>
                        <br><span class="font-term-blod">23. &nbsp;Contacting Storyrocket</span>
                        <br>
                        <br> If you have any questions about these Terms or the Service or general inquiries about Storyrocket please contact us at <span><a href="mailto:general@Storyrocket.com?subject=feedback" "email me" class="clr1">general@Storyrocket.com</a></span> and for any support requests please contact us at <span><a href="mailto:helpdesk@Storyrocket.com?subject=feedback" "email me" class="clr1">helpdesk@Storyrocket.com</a></span>.
                        <br>
                        <br><span class="font-term-blod">24. &nbsp;Privacy</span>
                        <br>
                        <br> You represent that you have read and understood our Privacy Policy.&nbsp; Please note that we may disclose information about you to third parties if we have a good faith belief that such a disclosure is reasonably necessary to (i) take action regarding suspected illegal activities; (ii) enforce or apply our Terms and Privacy Policy; (iii) comply with legal process or other government inquiry, such as a search warrant, subpoena, statute, judicial proceeding, or other legal process served on us; or (iv) protect our rights, reputation, and property, or that of our users, affiliates, or the public.&nbsp; If you use the Service outside of the United States, you consent to having your personal data transferred to and processed in the United States or having it transferred to and processed by any and all of our servers located outside of the United States.
                    </p>
                </div>
            </div>
        </section>


    </div>
</div>

@endsection
