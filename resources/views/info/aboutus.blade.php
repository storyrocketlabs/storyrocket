@extends('layouts.app-main')

@section('content')
<div class="cover-page cover-page-aboutus s-aboutus">
    <div class="cover-page-content">
        <div class="cont-example"></div>

        <!-- OPEN TICKET -->
        <section class="row" id="op-1">
            <article class="f-conten1-abus1 container-fluid ">
                <div class="container f_center abus_pand1">
                    <h2 class="abus-font-1 f_color1 f_text-shadow">About Us</h2>

                    <!--<p class="f_color2 abus-font-2 f_text-shadow">Storyrocket is a global marketplace that specializes in putting the best written works in the hands of filmmakers who can turn them into films, television shows, made for TV movies, web series and more!</p>-->
                </div>
            </article>
        </section>

        <section class="row f_bg1" id="op-2">
            <article class="container abus_pand2  w-container f_left">
                <div class="col s12  m12 l6 xl6 abus-pad-b">
                    <p class="abus-font5  f_color3"><strong class="abus-resal">Storyrocket.com</strong> is a unique Marketplace that connects great written works with the Global Production Community with the goal of producing film, TV, theater or web.    Storyrocket is a Miami based startup founded by Media Executive/Screenwriter Ron Karasz and 2 time Emmy-winning Producer Ana Benitez with extensive experience in the Entertainment Marketplace.  The pencil-shaped rocket is a registered trademark of Storyrocket.com.</p>
                </div>
                <div class="col s12  m12 l6 xl6 abus-riht2">
                    <div class="logo-abus">
                        <img src="public/images/bg.png" data-src="{{asset("images/logo-SR.png")}}" alt="" class="responsive-img lazy-image">
                    </div>
                </div>
            </article>
        </section>
        <section class="row abus-color" id="op-3">
            <article class="container abus_pand2  w-container">
                <p class="abus-ceo">"The Entertainment World is a closed industry and chances are if you don't know anyone on the inside or have a big star attached to your project, your work won't be seen, no matter how amazing it is.  We estimate that there are hundreds of thousands of writers globally and millions of books and screenplays that are desperate to get into the right hands, but have nowhere to go. We recognize the challenges the Entertainment Business has and Storyrocket intends to change this"</p>
                <div class="abus_right">
                    <div class="abus_box">
                        <figure>
                            <img src="public/images/bg.png" data-src="{{asset("images/ron-1.jpg")}}" class="responsive-img lazy-image" alt="CEO">
                        </figure>
                    </div>
                    <p class="abus-author">
                        Ronald Karasz
                        <span class="sep">|</span>
                        <a href="" class="col-ceo">CO-FOUNDER</a>

                    </p>
                </div>
            </article>
        </section>


        <section class="row abus-color5" id="op-3">
            <article class="container abus_pand2  w-container">
                <p class="abus-ceo">"Today, not only the traditional networks and studios are in competition for great content, but companies like Netflix, Hulu, and Amazon are producing great original movies, shows and series as well.  This trend will continue to grow as new outlets emerge, creating the need for more and more content.  Storyrocket will be here to meet that demand"</p>
                <div class="abus_right">
                    <div class="abus_box">
                        <figure>
                            <img src="public/images/bg.png" data-src="{{asset("images/ana.jpg")}}" class="responsive-img lazy-image" alt="CEO">
                        </figure>
                    </div>
                    <p class="abus-author">
                        Ana Benitez
                        <span class="sep">|</span>
                        <a href="https://stickermule.com" class="col-ceo"> Emmy-winning producer and co-founder</a>

                    </p>
                </div>
            </article>
        </section>




        <section class="row f_bg1" id="op-4">
            <article class="container w-container f_left">
                <div class="abus-pda1">
                    <div class="abus-r"></div>
                    <p class="abus-title">Team Storyrocket</p>
                </div>
            </article>
            <article class="abus-team">
                <div class="team1">
                    <img src="public/images/bg.png" data-src="{{asset("images/team/ron-1.png")}}" alt="ceo" class="responsive-img lazy-image">
                    <div class="abus-over">
                        <p class="abus-rol">Ron karasz</p>
                        <p class="abus-rol-1">Founder, CEO</p>
                    </div>
                </div>
                <div class="team1">
                    <img src="public/images/bg.png" data-src="{{asset("images/team/ana-1.jpg")}}" alt="ceo" class="responsive-img lazy-image">
                    <div class="abus-over">
                        <p class="abus-rol">Ana Benitez</p>
                        <p class="abus-rol-1">Founder, President</p>
                    </div>
                </div>
                <div class="team1">
                    <img src="public/images/bg.png" data-src="{{asset("images/team/gary.jpg")}}" alt="Full Stack Developer" class="responsive-img lazy-image">
                    <div class="abus-over">
                        <p class="abus-rol">Gary Rojas</p>
                        <p class="abus-rol-1">Full Stack Developer</p>
                    </div>
                </div>
               <!-- <div class="team1">
                    <img src="public/images/bg.png" data-src="{{asset("images/team/jen.jpg")}}" alt="ceo" class="responsive-img lazy-image">
                    <div class="abus-over">
                        <p class="abus-rol">Jennifer Jarvis</p>
                        <p class="abus-rol-1">Director – Literary Relations</p>
                    </div>
                </div>
                <div class="team1">
                    <img src="public/images/bg.png" data-src="{{asset("images/team/oscar.jpg")}}" alt="ceo" class="responsive-img lazy-image">
                    <div class="abus-over">
                        <p class="abus-rol">Sarah Stanforth</p>
                        <p class="abus-rol-1">Social Media Coordinator</p>
                    </div>
                </div>-->
                <div class="team1">
                    <img src="public/images/bg.png" data-src="{{asset("images/team/steve.jpg")}}" alt="ceo" class="responsive-img lazy-image">
                    <div class="abus-over">
                        <p class="abus-rol">Steve Eisenberg</p>
                        <p class="abus-rol-1">Legal Counsel – Intellectual Property</p>
                    </div>
                </div>
                <div class="team1">
                    <img src="public/images/bg.png" data-src="{{asset("images/team/vlad.jpg")}}" alt="ceo" class="responsive-img lazy-image">
                    <div class="abus-over">
                        <p class="abus-rol">Vladimir Rodriguez</p>
                        <p class="abus-rol-1">Web Director</p>
                    </div>
                </div>
                <div class="team1">
                    <img src="public/images/bg.png" data-src="{{asset("images/team/albert.jpg")}}" alt="ceo" class="responsive-img lazy-image">
                    <div class="abus-over">
                        <p class="abus-rol">Alberto Magallanes</p>
                        <p class="abus-rol-1">Lead Programmer</p>
                    </div>
                </div>
                <div class="team1">
                    <img src="public/images/bg.png" data-src="{{asset("images/team/jair.png")}}" alt="ceo" class="responsive-img lazy-image">
                    <div class="abus-over">
                        <p class="abus-rol">Jair Silva</p>
                        <p class="abus-rol-1">Web Designer</p>
                    </div>
                </div>
                <div class="team1">
                    <img src="public/images/bg.png" data-src="{{asset("images/team/ivan.png")}}" alt="ceo" class="responsive-img lazy-image">
                    <div class="abus-over">
                        <p class="abus-rol">Ivan Calvay</p>
                        <p class="abus-rol-1">Full Stack Developer</p>
                    </div>
                </div>
            </article>
        </section>

    </div>
</div>

@endsection
