@extends('layouts.app-main')

@section('content')
<div class="cover-page cover-page-faqs s-faqs">
    <div class="cover-page-content">
        <section class="banner-top-faq @auth space-top @endauth dis-flex">
            <h1 class="titles-banners center-align dis-flex-p">FAQs</h1>
        </section>
        <section class="area-explains space-top space-bottom-64">
            <div class="row">
                <ul class="collapsible faq-icons lists-faqs" data-collapsible="accordion">
                    <li class="">
                        <div class="collapsible-header bck-acordion-faqs">Why would a writer use Storyrocket?<i class="material-icons">add_circle</i></div>
                        <div class="collapsible-body body-faqs"><span> If you don’t have a powerful contact to get you through the door of a studio, production company or television network, getting your story seen by producers is almost impossible, until now. Storyrocket is a an online marketplace that opens the doors to writers around the world that want to have their projects seen by producers looking for great content.<br><br>
					Whether you’re a novelist, screenwriter, playwright or someone with great ideas, the site is designed to house your “works” and make them available to you anytime and anywhere. Storyrocket categorizes your work and allows them to be easily searched by producers using filters.<br><br>
					Furthermore, your self-submitted “pitch package” lives in its own mini-site with your story’s name. Updating is very easy, eliminating the need to have your own website. Your pitch package doubles as a great promotional tool.  You can share your story on social media or through email with just one click.
</span>
                            <div class="box-help-faqs">
                                <p class="title-help">Was this article helpful?</p>
                                <div class="helpful">
                                    <div class="circle-help"><i class="material-icons">check</i></div>
                                    <div class="circle-no-help"><i class="material-icons">close</i></div>
                                    <p>8 out of 10 found this helpful</p>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="">
                        <div class="collapsible-header bck-acordion-faqs"><i class="material-icons">add_circle</i>How would I use Storyrocket to promote my works?</div>
                        <div class="collapsible-body body-faqs"><span>
			Writers, agents, publishers and rights holders can actively promote books, script, plays and ideas by using the social media links to Facebook, Twitter, Reddit, Tumblr, Google+ and also by personally emailing your project link to individuals.
			<br><br>
			* If you have a friend, or know someone with a large social media following, ask them to click on one of the social media buttons while looking at project (book, script or play). When they do that, their Facebook, Twitter, Reddit… page will open and your project poster, title and logline will be embedded into their wall. When they share it, they can add a sentence like, “This is my friend Taylor’s project. It’s really a great read. Someone should get this produced.” And now a direct link to your project will be sent to all their friends and followers.
		</span>
                            <div class="box-help-faqs">
                                <p class="title-help">Was this article helpful?</p>
                                <div class="helpful">
                                    <div class="circle-help"><i class="material-icons">check</i></div>
                                    <div class="circle-no-help"><i class="material-icons">close</i></div>
                                    <p>8 out of 10 found this helpful</p>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="">
                        <div class="collapsible-header bck-acordion-faqs"><i class="material-icons">add_circle</i>Does Storyrocket sell my novel or script for me?</div>
                        <div class="collapsible-body body-faqs">span>Storyrocket does not sell your works. We provide the platform for your stories to be showcased and the search engine for your stories to be discovered by producers. Other than promoting projects by featuring them on our home page or member dashboard, there is no implied relationship between Storyrocket and its members. Transaction are between writers and the producer.</span>
                            <div class="box-help-faqs">
                                <p class="title-help">Was this article helpful?</p>
                                <div class="helpful">
                                    <div class="circle-help"><i class="material-icons">check</i></div>
                                    <div class="circle-no-help"><i class="material-icons">close</i></div>
                                    <p>8 out of 10 found this helpful</p>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="">
                        <div class="collapsible-header bck-acordion-faqs"><i class="material-icons">add_circle</i>Does Storyrocket get involved in my deals?</div>
                        <div class="collapsible-body body-faqs"><span>Storyrocket is not involved in any of your deals. If anyone is interested in you or your writings, they will contact you or your designated representative directly. We believe your deal is yours to keep. If your story gets opted, developed or produced, we do not take any commission, ever!
					</span>
                            <div class="box-help-faqs">
                                <p class="title-help">Was this article helpful?</p>
                                <div class="helpful">
                                    <div class="circle-help"><i class="material-icons">check</i></div>
                                    <div class="circle-no-help"><i class="material-icons">close</i></div>
                                    <p>8 out of 10 found this helpful</p>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="">
                        <div class="collapsible-header bck-acordion-faqs"><i class="material-icons">add_circle</i>Does Storyrocket represent me or my written works?</div>
                        <div class="collapsible-body body-faqs"><span>We do not represent any one or any works on the site. You, your agent, publisher or whomever you have designated as your representative or rights holder represents your work.</span>
                            <div class="box-help-faqs">
                                <p class="title-help">Was this article helpful?</p>
                                <div class="helpful">
                                    <div class="circle-help"><i class="material-icons">check</i></div>
                                    <div class="circle-no-help"><i class="material-icons">close</i></div>
                                    <p>8 out of 10 found this helpful</p>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="">
                        <div class="collapsible-header bck-acordion-faqs"><i class="material-icons">add_circle</i>What if I have an agent?</div>
                        <div class="collapsible-body body-faqs"><span>Storyrocket can be used 2 ways if you have an agent;<br><br>
				1) You can submit your projects as a writer and list your agent to be contacted. This way, when someone clicks on the “who to contact button,” they will get your agent’s info.<br><br>
				2) Your agent can submit your work as his/her submission, listing your name as the writer, in this case your agent is the person to contact.
				</span>
                            <div class="box-help-faqs">
                                <p class="title-help">Was this article helpful?</p>
                                <div class="helpful">
                                    <div class="circle-help"><i class="material-icons">check</i></div>
                                    <div class="circle-no-help"><i class="material-icons">close</i></div>
                                    <p>8 out of 10 found this helpful</p>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="">
                        <div class="collapsible-header bck-acordion-faqs"><i class="material-icons">add_circle</i>What if I have a publisher?</div>
                        <div class="collapsible-body body-faqs"><span>Large publishers provide representation to their “A-list writers.” However, most writers at these companies do not get this level of service and smaller publishers are focused on their core business, so they can’t devote resources for this service. Storyrocket provides this “added value” to the writers and offers affordable packages to publishers.
								<br><br>
					As it is with writers who have agents, the publisher can submit projects and list the authors name, or the writer can submit their own works and list the publisher to be contacted.
					</span>
                            <div class="box-help-faqs">
                                <p class="title-help">Was this article helpful?</p>
                                <div class="helpful">
                                    <div class="circle-help"><i class="material-icons">check</i></div>
                                    <div class="circle-no-help"><i class="material-icons">close</i></div>
                                    <p>8 out of 10 found this helpful</p>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="">
                        <div class="collapsible-header bck-acordion-faqs"><i class="material-icons">add_circle</i>What if I don’t have an agent or a publisher?</div>
                        <div class="collapsible-body body-faqs"><span>Then this site was made for you. Getting your work to be seen by an agent, producer, production company or network is an uphill battle. The site is designed to be affordable, so you can post 1… 20… or 100s of projects to be searched and promoted.
								<br><br>
					Since only 5 pages are visible in your online pitch package, that means that your work can still be considered by a publisher. And screenwriters and playwrights can show as little as they want to protect their ideas.
					</span>
                            <div class="box-help-faqs">
                                <p class="title-help">Was this article helpful?</p>
                                <div class="helpful">
                                    <div class="circle-help"><i class="material-icons">check</i></div>
                                    <div class="circle-no-help"><i class="material-icons">close</i></div>
                                    <p>8 out of 10 found this helpful</p>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="">
                        <div class="collapsible-header bck-acordion-faqs"><i class="material-icons">add_circle</i>Will Storyrocket put me in contact with buyers?</div>
                        <div class="collapsible-body body-faqs"><span>Storyrocket will promote you if your pitch package is an exceptional one.<br>
					From time to time, our team will select projects that have the following; an elevator pitch or trailer that is unique, artistic, funny or captivating. The pitch package should be complete with a unique poster, well-written logline, synopsis and a cast-wish-list.
					</span>
                            <div class="box-help-faqs">
                                <p class="title-help">Was this article helpful?</p>
                                <div class="helpful">
                                    <div class="circle-help"><i class="material-icons">check</i></div>
                                    <div class="circle-no-help"><i class="material-icons">close</i></div>
                                    <p>8 out of 10 found this helpful</p>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="">
                        <div class="collapsible-header bck-acordion-faqs"><i class="material-icons">add_circle</i>Does anyone review my written works?</div>
                        <div class="collapsible-body body-faqs"><span>Someone is always looking, that’s the point. But there is no criteria that would keep you from submitting a project to the site… other than good taste. We reserve the right to remove truly offensive materials that go well beyond social norms. Storyrocket is an aggregator or content, accordingly there will be themes and passages that will push some limits.</span>
                            <div class="box-help-faqs">
                                <p class="title-help">Was this article helpful?</p>
                                <div class="helpful">
                                    <div class="circle-help"><i class="material-icons">check</i></div>
                                    <div class="circle-no-help"><i class="material-icons">close</i></div>
                                    <p>8 out of 10 found this helpful</p>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="">
                        <div class="collapsible-header bck-acordion-faqs"><i class="material-icons">add_circle</i>What should I do if someone posts a project I co-wrote?</div>
                        <div class="collapsible-body body-faqs"><span>
								Storyrocket encourages writers to list all their co-writers on projects. Your name will appear on the site even if you’re not a member of the site. If you accept to be listed, then do nothing more. If you have any dispute regarding the submission or want your name removed, contact us and we will take the appropriate action until the issue is resolved.

							</span>
                            <div class="box-help-faqs">
                                <p class="title-help">Was this article helpful?</p>
                                <div class="helpful">
                                    <div class="circle-help"><i class="material-icons">check</i></div>
                                    <div class="circle-no-help"><i class="material-icons">close</i></div>
                                    <p>8 out of 10 found this helpful</p>
                                </div>
                            </div>
                        </div>
                    </li>

                    <li>
                        <p class="last-call-faqs">Have more questions? <a href="#" class="color-blue-faq">Submit a request</a></p>
                    </li>
                </ul>
            </div>
        </section>


    </div>
</div>

@endsection
