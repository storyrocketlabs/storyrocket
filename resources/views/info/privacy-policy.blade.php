@extends('layouts.app-main')

@section('content')
<div class="cover-page cover-page-privacy-policy s-privacy-policy">
    <div class="cover-page-content">

        <section class="banner-top-privacy-policy @auth space-top @endauth dis-flex">
            <h1 class="titles-banners center-align dis-flex-p">Privacy Policy</h1>
        </section>
        <section class="area-explains space-top space-bottom-64">
            <div class="row">
                <div class="col s12 l12">
                    <p class="main-txt-pages">
                        <span class="font-term-blod">Last Updated on January 1, 2017.</span>
                        <br>
                        <br>This Privacy Policy (<span class="font-term-blod">“Privacy Policy”)</span> explains how Storyrocket (<span class="font-term-blod">“Storyrocket”</span>, <span class="font-term-blod">“us”</span>, <span class="font-term-blod">“our”,</span> and <span class="font-term-blod">“we”</span>), collects and uses information when you access our website http://Storyrocket.com (the <span class="font-term-blod">“Service”</span>).&nbsp; This Privacy Statement applies to all users, visitors and others who access the Service and does not apply to any third party websites, services or applications, even if they are accessible through our Service.&nbsp; Also, please note that, unless we define a term in this Privacy Policy, all words used in this Privacy Policy shall have the same meanings as in our Terms of Use, accessible at <a href="terms-of-service" class="clr1">Terms</a>.
                        <br>
                        <br>Please read this Privacy Policy carefully.&nbsp; When you use the Service, you consent to our collection, use, and disclosure of information about you as described in this Privacy Policy.&nbsp; We welcome your questions and comments on this policy.&nbsp; Please contact us at <span class="font-term-l"><span class="clr1"><a href="mailto:general@storyrocket.com?subject=feedback" "email me" class="clr1">general@storyrocket.com</a></span>.
            <br>
            <br><span class="font-term1"><span class="font-term-blod">Purpose
<br></span>
            <br xmlns="http://www.w3.org/1999/xhtml"> Our primary goals in collecting information are to provide and improve our Service, to administer your use of the Service (including your account, if you are an account holder), and to enable you to enjoy and easily navigate our Service.
            <br>
            <br><span class="font-term-blod">Personally Identifiable Information</span>
            <br>
            <br> When you create an account we will collect certain information that can be used to identify you, such as your name, date of birth, email address, and general location, such as your city and state (“Personally Identifiable Information”).&nbsp; Some of this information, for example, your name, is listed publicly on our Service, including any information you provide on your profile page (except your email address and date of birth) and in search results.&nbsp; We may also collect certain information that is not Personally Identifiable Information because it cannot be used by itself to identify you, such as what content you view on the Service.
            <br>
            <br><span class="font-term-blod">Information We Collect From You Directly
</span>
            <br>
            <br> When you use the Service, open one of our emails, sign up for an account create, or share, and message or communicate with others on Storyrocket, we automatically collect the following:
            <br>
            <br>- Your Content (e.g., photos, comments, projects, and other materials) that you post to the Service.
            <br>
            <br>- Communications between you and Storyrocket.
            <br>
            <br>- Profile page information that you add to your user profile (e.g., first and last name, and email).&nbsp; Please note that during the registration process we give you the opportunity to provide additional information to be added to your profile page, such as your work history, educational background, and to add a picture of yourself (“Additional Information”).&nbsp; This Additional Information will be displayed publicly and you may make changes to the Additional Information at any time.&nbsp; This allows us to help you or others be "found" on Storyrocket.
            <br>
            <br>- Your username, which will be your email address, password, and birthdate provided when you registered for a Storyrocket account.
            <br>
            <br><span class="font-term-blod">Information We Receive or Collect From You Indirectly&nbsp;</span>
            <br>
            <br>- When you use Storyrocket from a computer, mobile phone or other device, our servers automatically record information (“Log Data”), including information that your browser sends whenever you visit a website.&nbsp; This log data may include your Internet Protocol address, the address of the web pages you visited that had Storyrocket features, browser type and settings, the date and time of your request, how you used Storyrocket, and cookie data (explained below).
            <br>
            <br>- Depending on how you are accessing our Service, we may use “cookies” (a small text file sent by your computer each time you visit our website, unique to your Storyrocket account or your browser) or similar technologies to record log data.&nbsp; When we use cookies, we may use “session” cookies (that last until you close your browser) or “persistent” cookies (that last until you or your browser delete them).&nbsp; Overall, Cookies help Storyrocket make your interaction with the Service easier.
            <br>
            <br>- We keep track of the actions you take on Storyrocket, such as posting projects, adding friends, or uploading a picture.
            <br>
            <br><span class="font-term-blod">Information You Share on Storyrocket
</span>
            <br>
            <br> We designed our privacy settings to enable you to control how you share your information on Storyrocket. You should review the default privacy settings to make sure they reflect your preferences.
            <br>
            <br><span class="font-term-blod">Why We Collect Your Information
</span>
            <br>Storyrocket may use information that it receives to:
            <br>
            <br>- Show you advertisement you may be interested in.
            <br>
            <br>- Provide user support, including to our business partners.
            <br>
            <br>- Improve our marketing and promotional efforts.
            <br>
            <br>- Improve the way the Service works and looks.
            <br>
            <br>- Communicate and provide additional information that may be of interest to you about Storyrocket and its business partners.
            <br>
            <br>- Send you reminders, technical notices, updates, security alerts, and support or administrative messages and service bulletins.
            <br>
            <br>- Respond to questions and comments.
            <br>
            <br>- Manage our everyday business needs such as Service administration, analytics, fraud prevention, Terms of Use or to comply with the law.
            <br>
            <br>The above list does not provide you with all the reasons why Storyrocket collects your information.&nbsp; If you should have any further questions or concerns, please send any and all inquiries to <span><a href="mailto:general@storyrocket.com?subject=feedback" "email me" class="clr1">general@storyrocket.com</a></span>.&nbsp; We will be happy to answer any additional questions.
            <br>
            <br><span class="font-term-blod">Sharing Your Personal Information</span>
            <br>
            <br> We do not sell, trade, rent or otherwise transfer to outside parties your Personally Identifiable Information without your consent, except as noted in this Privacy Policy.&nbsp; This does not include trusted third parties who assist us in operating our Service, conducting our business, or servicing you, so long as those parties agree to keep this information confidential.
            <br>
            <br>-&nbsp;&nbsp;We may share your information when we believe release is appropriate to comply with law, enforce our Terms of Use and this Privacy Policy or protect ours or other rights, property, or safety.
            <br>
            <br>- &nbsp;We may also share generic aggregated demographic information not linked to any personal identification information regarding visitors and users with our business partners, trusted affiliates and advertisers for the purposes outlined above.
            <br>
            <br>- &nbsp;We also may share your information as well as information from tools like cookies, log files, and device identifiers and location data, with third party organizations that help us provide the Service to you ("Service Providers").&nbsp; Our Service Providers will be given access to your information as is reasonably necessary to provide the Service under reasonable confidentiality terms.
            <br>
            <br>-&nbsp;&nbsp;We may also share certain information such as cookie data with third party advertising partners.&nbsp; This information would allow third party ad networks to, among other things, deliver targeted advertisements that they believe will be of most interest to you.
            <br>
            <br>In the event that we are acquired by or merged with a third-party entity, we reserve the right, in any of these circumstances, to transfer or assign the information we have collected from you as part of such merger, acquisition, sale, or other change of control.&nbsp; You will be notified via email and/or a prominent notice on our website of any change in ownership or uses of your Personally Identifiable Information, as well as any choices you may have regarding your Personally Identifiable Information.
            <br>
            <br><span class="font-term-blod">Third Party Websites
<br xmlns="http://www.w3.org/1999/xhtml"></span>
            <br> This Privacy Policy only addresses the use and disclosure of information by Storyrocket through your interaction with the Service.&nbsp; Other websites that may be accessible through links from our Service may have their own privacy statements and personal information collection, use, and disclosure practices.&nbsp; Our business partners may have their own privacy statements too.&nbsp; We encourage you to familiarize yourself with the privacy statements provided by these other parties prior to providing them with information or taking advantage of an offer or promotion.&nbsp; In addition, you agree that we are not responsible and do not have control over any third-parties that you authorize to access your content. If you are using a third party website or service and you allow them to access your content you do so at your own risk.
            <br>
            <br><span class="font-term-blod">Security</span>
            <br>
            <br> We follow generally accepted industry standards to protect the personal information submitted to us, both during transmission and once we receive it.&nbsp; However, no method of transmission over the Internet or via mobile device, or method of electronic storage, is 100% secure.&nbsp; Therefore, while we strive to use commercially acceptable means to protect your personal information, we cannot guarantee its absolute security.
            <br>
            <br><span class="font-term-blod">Modifying Your Personal Information
</span>
            <br>
            <br> If you want to delete your Personally Identifiable Information and your account, please contact <span class="font-term-l"><span class="font-term1"><a href="#" class="clr1">helpdesk@storyrocket.com</a></span></span> with your request.&nbsp; We will take steps to delete your information as soon as practicable, but some information may remain in archived/backup copies for our records or as otherwise required by law.
            <br>
            <br><span class="font-term-blod">Our Privacy Policy Concerning Children
</span>
            <br>
            <br>Our Service is not directed to children under 13.&nbsp; We do not knowingly collect personal information from children under 13.&nbsp; If you become aware that a child has provided us with personal information without parental consent, please contact us.
            <br>
            <br><span class="font-term-blod">Changes to Privacy Policy
</span>
            <br>
            <br> Any information that we collect is subject to the privacy policy in effect at the time such information is collected.&nbsp; We may, however, modify and revise this Privacy Policy from time to time.&nbsp; If we make any material changes to this Privacy Policy, we will notify you of such changes by posting them on the Service or by sending you an email or other notification, and we will indicate when such changes will become effective.&nbsp; Your continued use of Storyrocket or the Service after any modification to this Privacy Policy will constitute your acceptance of such modification.
            <br>
            <br><span class="font-term-blod">Questions and Concerns</span>
            <br>
            <br> Please contact us at <a href="mailto:general@storyrocket.com?subject=feedback" "email me" class="clr1">general@storyrocket.com</a> if you have any questions about our Privacy Policy.</span>
            <br>
            </span>
                    </p>
                </div>
            </div>
        </section>


    </div>
</div>

@endsection
