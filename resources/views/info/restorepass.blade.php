@extends('layouts.app-blank')

@section('content')
    <div class="row main-login">
        <div class="center-align">
            <a href="https://www.storyrocket.com/"><img src="https://www.storyrocket.com/public/images/logo-login.svg" class="logo-login"></a>
            <h5 class="txt-login clr2">Forgot Password?</h5>
        </div>
        <div class="">
            <div class="area-white">
                <div class="row">
                    <div class="slidingDiv" style="display: none;">
                        <p class="sub-title-bold clr2 center-align ">Check your email</p>
                        <br>
                        <p class="txt-reset-password left-align">We have sent an email to <span class="clr1" id="mail-rst"></span> Click the link within the email to reset your password.</p>

                        <p class="txt-reset-password left-align">If you do not see the email in your inbox , check other places where you might be , your folders as spam , social or other.</p>
                        <br><br>
                        <p class="center-align"><a class="clr1" href="{{route('login')}}" style="display: inline;">Back to Login</a></p>
                    </div>
                    <form class="col s12 no-padding" id="form-reset" data-type="search_mail" method="post">
                        <div class="row">
                            <p class="txt-reset-password left-align">Enter the email address you used when you joined and we’ll send you instructions to reset your password.</p>
                            <p class="txt-reset-password left-align">For security reasons, we do NOT store your password. So rest assured that we will never send your password via email.</p>
                            <div class="input-field col s12">
                            <input id="email" type="email" class="validate" aria-required="true">
                                <label for="email" data-error="wrong" data-success="right">Email Address</label>
                            </div>
                            <br><br><br>
                            <div class="down-login">
                                <div class="show_hide enviar-form waves-effect waves-light" type="Sign in" name="action" style="display: block;">Send reset instructions</div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection