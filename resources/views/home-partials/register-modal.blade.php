<div id="register-modal-1" class="modal modal-register">
    <div class="modal-content register-modal-content">
        <div class="banner-vd">
            <div class="icon-vr1 modal-close">
                <i class="material-icons">clear</i>
            </div>
            <img src="{{asset('images/bg.png')}}" data-src="{{asset('images/home-welcom-1.jpg')}}" alt=""
                 class="responsive-img vr-pos-img lazy-image">
        </div>
        <p class="vd-cont-t">Hello, I'm the Storyrocket Assistant!</p>
        <p class="vd-cont-t2">I’m going to help you create an account and soon you'll be able to manage your profile;
            update preferences, select a plan, post new projects and more.</p>
        <div class="vd-center">
            <a href="javascript:void(0);" class="btn-on-enter" data-type="name" id="open-register-modal-2">Start Now</a>
        </div>
        <p class="vd-cont-t3">Are you a member? <a href="#" class="vd-style">Sign in to your account</a></p>
        <p class="vd-recoment">*All fields are required unless otherwise noted.</p>
    </div>
</div>

<div id="register-modal-2" class="modal modal-register">
    <div class="modal-content register-modal-content">
        <div class="row">
            <div class="col s12 m6 l7 xl7">
                <div class="area-enter">
                    <p class="title-enter">Great!</p>
                    <p class="vd-cont-t2 not-m">Let's start with your first and last name</p>
                    <label class="lbl-m-cp active">First name</label>
                    <input class="input-profile1 inpt-alone-mb margin-7-vd required _01-i" id="name"
                           data-name="fristName" type="text" placeholder="Enter your first name" value="">
                    <p class="vd-alert-tx vd-alert-tx-firstName">Opps! Please fill out this</p>
                    <label class="lbl-m-cp active">Last name </label>
                    <input class="input-profile1 inpt-alone-mb margin-7-vd required _01-i" id="last"
                           data-name="lastName" type="text" placeholder="Enter your last name" value="">
                    <p class="vd-alert-tx vd-alert-tx-lastName">Opps! Please fill out this</p>
                    <p class="requiered-f">* Required fields</p>
                    <div class="vd-alert-tx-email"></div>
                    <br>
                    <a href="#" class="btn-on-enter" data-type="mail" id="open-register-modal-3">Continue</a>
                </div>
            </div>
            <div class="col s12 m6 l5 xl5 no-padding n-login-mobile">
                <div class="area-img-log">
                    <img src="{{asset('images/bg.png')}}"
                         data-src="{{asset('images/register-one.jpg')}}" alt=""
                         class="full-img lazy-image">
                </div>
            </div>
        </div>
    </div>
</div>

<div id="register-modal-3" class="modal modal-register">
    <div class="modal-content register-modal-content">
        <div class="row">
            <div class="col s12 m6 l7 xl7 register-modal-3">
                <div class="area-enter">
                    <p class="title-enter">Enter your email address</p>
                    <label class="lbl-m-cp active">Email</label>
                    <input class="input-profile1 inpt-alone-mb margin-7-vd required _01-i" id="mail" data-name="email"
                           type="text" placeholder="Enter your email" value="">
                    <p class="requiered-f">* Required fields</p>
                    <p class="vd-alert-tx vd-alert-tx-email">Opps! Please fill out this</p>
                    
                    <div class="block-hide hide">
                        <label class="lbl-m-cp active">Confirm Email</label>
                        <input class="input-profile1 inpt-alone-mb margin-7-vd required _01-i msr-mail" id="mail-verif" data-name="email" type="text" placeholder="Enter your email" value="">
                        <p class="requiered-f">* Required fields</p>
                    </div>
                    <p class="email-ok msg-mail-inf">
                        <i class="material-icons" style="vertical-align: bottom;">error</i>
                        <span style="color:red"> This email is already registered. Want to <a
                                href="{{route('login')}}" class="clr5" style="margin:0 .3rem"> login </a> or recover your <a
                                href="#" class="clr5" style="margin:0 .3rem"> password?</a></span>
                    </p>
                    <a href="#" class="btn-on-enter" data-type="mail" id="open-register-modal-4">Continue</a>
                </div>
            </div>
            <div class="col s12 m6 l5 xl5 no-padding n-login-mobile">
                <div class="area-img-log">
                    <img src="{{asset('images/bg.png')}}"
                         data-src="{{asset('images/register-mail.jpg')}}" alt=""
                         class="full-img lazy-image">
                </div>
            </div>
        </div>
    </div>
</div>

<div id="register-modal-4" class="modal modal-register">
    <div class="modal-content register-modal-content">
        <div class="row">
            <div class="col s12 m6 l7 xl7">
                <div class="area-enter">
                    <p class="title-enter">Create a secure password</p>
                    <p class="vd-cont-t2 not-m">Your password must contain 8 or more characters, at least 1 uppercase
                        &amp; lowercase letters and at least one number</p>
                    <br><br>
                    <label class="lbl-m-cp">Password</label>
                    <div class="u-relative">
                        <input class="input-profile1 inpt-alone-mb margin-7-vd view-pass _01-i required" id="pw1"
                               data-name="pw1" type="password" placeholder="Enter your password"
                               aria-autocomplete="list">
                        <span class="icon-visi-vd tooltip-wap-visi cls-view"><i class="material-icons" id="password-i">visibility_off</i>
                    </span>
                    </div>
                    <p class="vd-alert-tx vd-alert-tx-pw1"></p>
                    <p class="requiered-f">* Required Fields</p>
                    <br>
                    <label class="lbl-m-cp">Confirm password</label>
                    <div class="u-relative">
                        <input class="input-profile1 inpt-alone-mb margin-7-vd view-pass _01-i required" id="pw2"
                               data-name="pw2" type="password" placeholder="Enter your password">
                        <span class="icon-visi-vd tooltip-wap-visi cls-view"><i class="material-icons"
                                                                                id="password-i-confirmed">visibility_off</i>
                    </span>
                    </div>
                    <p class="vd-alert-tx vd-alert-tx-pw2"></p>
                    <p class="requiered-f">* Required Fields</p>
                    <p class="pasword-ok vd-alert-tx">
                        <i class="material-icons dp48 error-pass-icon">close</i>
                        <span id="error-text-password"></span>
                    </p>

                    <br>
                    <a href="#" class="btn-on-enter" data-type="mail" id="open-register-modal-5">Continue</a>
                </div>
            </div>
            <div class="col s12 m6 l5 xl5 no-padding n-login-mobile">
                <div class="area-img-log">
                    <img src="{{asset('images/bg.png')}}"
                         data-src="{{asset('images/register-contrasenia.jpg')}}" alt=""
                         class="full-img lazy-image">
                </div>
            </div>
        </div>
    </div>
</div>

<div id="register-modal-5" class="modal modal-register">
    <div class="modal-content register-modal-content">
        <div class="row">
            <div class="col s12 m6 l7 xl7">
                <div class="area-enter">
                    <p class="title-enter">Let’s continue. Are you a writer or part of the Production Community?</p>
                    <div class="cont-dash">
                        <p class="font-n-vd2">You can select up to 3 options, i.e. producer; director; writer</p>
                    </div>
                    <br>
                    <div class="cont-icon-vd">
                        <button class="icon-vd-w btn-dif _01-i" id="btn-writer" data-name="type" value="writer"
                                data-id="45" data-active="false">
                            <img src="https://www.storyrocket.com/public/images/write-1.svg" class="full-img"
                                 id="ima-writer">
                            <p class="font-icon-w font-btn-writ">I am a Writer</p>
                        </button>

                        <button class="icon-vd-w btn-dif _01-i" id="btn-producer" data-name="type" value="producer"
                                data-id="80" data-active="false">
                            <img src="https://www.storyrocket.com/public/images/producer-1.svg" class="full-img"
                                 id="ima-producer">
                            <p class="font-icon-p font-btn-writ">I am a Producer</p>
                        </button>
                    </div>
                    <div class="">
                        
                        <div class="select-m-cp margin-7-vd select-profesion-ima">
                            <input type="hidden" value="80" class="_01-i" data-name="profession" id="cob-profession">
                            {!! Form::select('occupations', $occupations, null, ['placeholder' => 'Search ..', 'class'=>'browser-default cbo-profesion select-ml-profesion', 'id'=>'cm-prof']) !!}
                        </div>
                        
                        <ul class="list-chip-m-cp" id="spoken">

                        </ul>
                        <div class="msj-fr">
                            <p class="_error-"></p>
                            <p class="requiered-f">* Required Fields</p>
                        </div>
                    </div>
                    <br>
                    <a href="#" class="btn-on-enter" data-type="mail" id="open-register-modal-6">Continue</a>
                </div>
            </div>
            <div class="col s12 m6 l5 xl5 no-padding n-login-mobile">
                <div class="area-img-log">
                    <img src="{{asset('images/bg.png')}}"
                         data-src="{{asset('images/register-contrasenia.jpg')}}" alt=""
                         class="full-img lazy-image">
                </div>
            </div>
        </div>
    </div>
</div>

<div id="register-modal-6" class="modal modal-register">
    <div class="modal-content register-modal-content">
        <div class="row">
            <div class="col s12 m6 l7 xl7">
                
                <div class="area-enter">
                    <p class="title-enter">Select your location</p>
                    <br/>
                <br/>
            <label class="lbl-m-cp">Location</label>
                    <br>
                    <div class="">
                        <div class="select-m-cp margin-7-vd">
                            <input type="hidden" value="80" class="_01-i" data-name="profession" id="cob-profession">
                            {!! Form::select('country', $countries, null, ['placeholder' => 'Search ..', 'class'=>'browser-default cbo-profesion', 'id'=>'country']) !!}
                        </div>
                        <br>
                        <div class="col s12 m6 l6 xl6 vd-l">
                            <div class="select-m-cp cp-arr">
                                {!! Form::select('state', [], null, ['placeholder' => 'State/Province', 'class'=>'browser-default cbo-profesion', 'id'=>'state_m']) !!}
                            </div>
                        </div>
                        <div class="col s12 m6 l6 xl6 margin-3-vd vd-r">
                            <input class="input-profile1 inpt-alone-mb _01-i" data-name="city" type="text"
                                   placeholder="City" value="" id="city_m">
                            <p class="requiered-f">* Required Fields</p>
                        </div>
                    </div>
                    <br>
                    <a href="#" class="btn-on-enter" data-type="mail" id="open-register-modal-7">Continue</a>
                </div>
            </div>
            <div class="col s12 m6 l5 xl5 no-padding n-login-mobile">
                <div class="area-img-log">
                    <img src="{{asset('images/bg.png')}}"
                         data-src="{{asset('images/register-location.jpg')}}" alt=""
                         class="full-img lazy-image">
                </div>
            </div>
        </div>
    </div>
</div>

<div id="register-modal-7" class="modal modal-register">
    <div class="modal-content register-modal-content">
        <div class="row">
            <div class="col s12 m6 l7 xl7">
                <div class="area-enter">
                    <p class="title-enter">Now to finish</p>
                    <div class="cont-dash">
                        <p class="font-n-vd2">Upload a profile picture</p>
                    </div>
                    <br>
                    <div class="vd-center">
                        <input type="file" name="avatar" id="fileImage" class="upload" accept="image/*">
                        <div class="unplad-foto" id="uplad-avatar">
                            <img src="{{asset('images/bg.png')}}"
                                 style="width: 53%; margin: 2rem 0 0 0;"
                                 data-src="{{asset('images/avatar-user-asis.png')}}"
                                 id="avatar-ima-g" class="src-avatar-s image-avatar-user lazy-image">
                            <span
                                style="display: block;color: #fff;font-weight: 600;font-size: 17px;">+ Upload Photo</span>
                        </div>
                        <div class="">
                            <p class="u-center-c lineal-or">OR</p>
                            <p class="u-center-c">
                                <a href="javascript:void(0)" class="unplad-foto hrf-upload">Click here</a> to upload
                                your Photo</p>
                            <p class="requiered-f-1 u-fonts">If you don't have your photo now,<br>you can do it later.
                            </p>
                            <p class="vd-alert-tx vd-alert-tx-avatar"></p>
                        </div>
                    </div>
                    <br>
                    <a href="#" class="btn-on-enter" data-type="mail" id="register-send">Continue</a>
                </div>
            </div>
            <div class="col s12 m6 l5 xl5 no-padding n-login-mobile">
                <div class="area-img-log">
                    <img src="{{asset('images/bg.png')}}"
                         data-src="{{asset('images/register-location.jpg')}}" alt=""
                         class="full-img lazy-image">
                </div>
            </div>
        </div>
    </div>
</div>

<div class="overlay-main box">
    <div class="overlay-body">
        <div class="overlay-body-cont">
            <div class="body-main">
                <div class="wapper-mod-img-drag ">
                    <div class="w-cont-img-drag ">
                        <div class="part1-img-drag">
                            <div class="left-aling">
                                <i class="material-icons">insert_photo</i>
                                <span class="font-part1-img">Update Profile Picture</span>
                            </div>
                            <div class="part1-img-2">
                                <i class="material-icons close-r">close</i>
                            </div>
                        </div>
                        <div class="pad-img-drag">
                            <div class="img-drag">
                                <div class="image-editor">
                                    <div class="cropit-preview">
                                    </div>
                                    <input type="range" class="cropit-image-zoom-input">
                                </div>
                            </div>
                        </div>
                        <div class="pad-img-drag pop-alin-drag borde-btn-drag">
                            <button class="btn-drag-2 close-r" id="_cl-avatar-profile">Cancel</button>
                            <button class="btn-drag-1" id="bnt-avatar-profile">Upload Photo</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="over-light box"></div>

<div id="register-modal-8" class="modal modal-register">
    <div class="modal-content register-modal-content">
        <div class="row">
            <div class="col s12 m6 l7 xl7">
                <div class="area-enter-2 margin-4-vd">
                    <p class="vd-cont-t t-center">Well done <span id="spn-name">Ivan Calvay</span>!</p>
                    <p class="_vd-cont-t2">Thanks for signing up. Now check your email; <font color="#246EBD" id="ok-mail">starsun_4@hotmail.com</font> to activate your account. Your account will not be active until you complete this step.</p>
                    <p class="vd-cont-t t-center">You’re ready to launch!</p>
                    <div class="vd-lgo-login">
                        <img src="{{asset('images/bg.png')}}" data-src="{{asset('images/logo-nav-1.png')}}" alt="" class="full-img lazy-image">
                    </div>
                </div>
            </div>
            <div class="col s12 m6 l5 xl5 no-padding n-login-mobile">
                <div class="area-img-log">
                    <img src="{{asset('images/bg.png')}}"
                         data-src="{{asset('images/img-end.png')}}" alt=""
                         class="full-img lazy-image">
                </div>
            </div>
        </div>
    </div>
</div>


<form action="{{route('register')}}" method="POST" class="hide" id="form-register-assist">
    @csrf
    <input id="full_name" type="text" class="validate {{ $errors->has('full_name') ? 'invalid' : '' }}"
           aria-required="true" data-name="full_name" name="full_name" value="{{ old('full_name') }}">
    <input id="last_name" type="text" class="validate {{ $errors->has('last_name') ? 'invalid' : '' }}"
           aria-required="true" data-name="last_name" name="last_name" value="{{ old('last_name') }}">
    <input id="email" type="email" class="validate mail-sing {{ $errors->has('email') ? 'invalid' : '' }}"
           aria-required="true" data-name="email" name="email" value="{{ old('email') }}">
    <input id="password" type="password" class="validate {{ $errors->has('password') ? 'invalid' : '' }}"
           aria-required="true" data-name="password" name="password">
    <input id="image" type="text" name="image">
    <input id="profile-occupations" type="text" name="profile-occupations">
    <input id="country_id" type="text" name="country_id">
    <input id="state" type="text" name="state">
    <input id="city" type="text" name="city">
</form>
