<div class="newer-card-project">
    <div class="show-poster-card">
        <a href="{{(Config::get('app.url')."/".$project->project_url)}}"
           target="_blank">
            <div class="overlay-card-project"></div>
        </a>
        <div class="head-of-project">
      <span class="truncate font-autor-card">
                        Submited by <a href="{{(Config::get('app.url')."/".$project->profile_url)}}" target="_blank" title="{{$project->full_name}} {{$project->last_name}}">{{$project->full_name}} {{$project->last_name}}</a>
                    </span>
            <div class="circle-on-card">
                <a href="{{$project->profile_url}}" target="_blank"><img
                            src="{{asset('images/bg.png')}}"
                            data-src="{{ empty($project->photo) ? (empty($project->facebook_photo) ? asset('images/avatar_default_user.png') :  $project->facebook_photo ) : $project->photo}}"
                            alt="{{$project->full_name}} {{$project->last_name}}"
                            class="responsive-img lazy-image"></a>
            </div>
        </div>
        <a href="{{(Config::get('app.url')."/".$project->project_url)}}"
           target="_blank">
            <div class="new-info-card-project">
                <p class="font-p-title-card truncate">
                    {{$project->project_name}}
                </p>
                <p class="gender-on-card">
                    <span>{{$project->genres}}</span>
                </p>
                <p class="logline-card">
                    {{$project->synopsis}}
                </p>
            </div>
        </a>
        <img src="{{asset('images/bg.png')}}"
             data-src="{{$project->poster}}"
             alt="Carniphyte Conspiracy, The" class="responsive-img lazy-image">
    </div>
    <div class="down-part-cardproject">
        <div class="icon-on-card">
              <span>
                   <i class="material-icons">remove_red_eye</i>{{$project->views}}
              </span>
            <span>
                    <i class="material-icons">favorite</i>0
            </span>
        </div>
    </div>
</div>