<script type="text/html" id="message-empty">
    <div class="message-empty-square">
        <div class="message-empty-square-content">
            <p class="message-empty-top">Your search did not return any content.</p>
            <p  class="message-empty-bottom">Please make sure all words are spelled correctly or try different keywords.</p>
        </div>
    </div>
</script>
