@extends('layouts.app')

@section('content')
    <div class="page-content-wrapper no-left s-search">
        <div class="container-fluid clearfix">
            <div class="container-fluid">
                <div class="gradient1"></div>
                <div class="banner-advertising-search"></div>
                <div class="area-search-advance-horizontal">
                    <div class="area-main-search">
                        <div class="title-y-search">
                            <p class="title-newsearchad">FILTER, SEARCH &amp; DISCOVER PROJECTS</p>
                        </div>
                        <div class="buttons-search-section-content">
                            <div class="buttons-search-item">
                                <a class="buttons-search-item-link buttons-search-item-link-y li-projects-active">Search
                                    Projects</a>
                            </div>
                            <div class="buttons-search-item" style="margin-left: 19px;">
                                <a class="buttons-search-item-link buttons-search-item-link-x li-membres-active buttons-search-item-link-active">Search
                                    Members</a>
                            </div>
                        </div>
                        <!--inicio searchs-->
                        <ul class="main-search-style" id="projects">
                            <li class="search-box-on">
                                <nav class="search-camp-new">
                                    <div class="nav-wrapper">
                                        <form class="search-area-text2">
                                            <div class="input-field">
                                                <input id="search" type="search" class="txt-on-newsearch"
                                                       data-type="projects" placeholder="Keyword Search"
                                                       value="{{isset($data['q']) ? $data['q'] : ''}}">
                                                <label class="label-icon" for="search">
                                                    <i class="material-icons">search</i>
                                                </label>
                                            </div>
                                        </form>
                                    </div>
                                </nav>
                            </li>
                            <li class="button-dropdown">
                                <a href="javascript:void(0)" class="dropdown-toggle">Genres<span>▼</span></a>
                                <ul class="dropdown-menu big-menu-genres">
                                    @foreach($genres as $genre)
                                        <label class="container-items" data-class="genre">
                                            {{$genre->genre}}
                                            <input type="radio" value="{{$genre->id}}" name="genre"
                                                   class="box-click genre" data-type="genre">
                                            <span class="checkmark">
                                            <i class="large material-icons">clear</i>
                                        </span>
                                        </label>
                                    @endforeach
                                </ul>
                            </li>
                            <li class="button-dropdown">
                                <a href="javascript:void(0)" class="dropdown-toggle">Material Type<span>▼</span></a>
                                <ul class="dropdown-menu">
                                    @foreach($materials as $material)
                                        <label class="container-items" data-class="material">
                                            {{$material->material}}
                                            <input type="radio" value="{{$material->id}}" name="material"
                                                   class="box-click material">
                                            <span class="checkmark">
                                            <i class="large material-icons">clear</i>
                                        </span>
                                        </label>
                                    @endforeach
                                </ul>
                            </li>
                            <li class="button-dropdown">
                                <a href="javascript:void(0)" class="dropdown-toggle">Intended Medium<span>▼</span></a>
                                <ul class="dropdown-menu">
                                    @foreach($mediums as $medium)
                                        <label class="container-items" data-class="medium">
                                            {{$medium->medium}}
                                            <input type="radio" value="{{$medium->id}}" name="medium"
                                                   class="box-click medium">
                                            <span class="checkmark">
                                            <i class="large material-icons">clear</i>
                                        </span>
                                        </label>
                                    @endforeach
                                </ul>
                            </li>
                            <li class="button-dropdown">
                                <a href="javascript:void(0)" class="dropdown-toggle">Language<span>▼</span></a>
                                <ul class="dropdown-menu">
                                    @foreach($languages as $language)
                                        <label class="container-items" data-class="language">
                                            {{$language->language}}
                                            <input type="radio" value="{{$language->id}}" name="language"
                                                   class="box-click language">
                                            <span class="checkmark">
                                            <i class="large material-icons">clear</i>
                                        </span>
                                        </label>
                                    @endforeach
                                </ul>
                            </li>
                            <li class="button-dropdown">
                                <a href="javascript:void(0)" class="dropdown-toggle">Era<span>▼</span></a>
                                <ul class="dropdown-menu">
                                    @foreach($eras as $era)
                                        <label class="container-items" data-class="era">
                                            {{$era->era}}
                                            <input type="radio" value="{{$era->id}}" name="era"
                                                   class="box-click era">
                                            <span class="checkmark">
                                            <i class="large material-icons">clear</i>
                                        </span>
                                        </label>
                                    @endforeach
                                </ul>
                            </li>
                            <li class="button-dropdown">
                                <a href="javascript:void(0)" class="dropdown-toggle">Location<span>▼</span></a>
                                <ul class="dropdown-menu">
                                    @foreach($locations as $location)
                                        <label class="container-items" data-class="location">
                                            {{$location->local_name}}
                                            <input type="radio" value="{{$location->id}}" name="location"
                                                   class="box-click location">
                                            <span class="checkmark">
                                            <i class="large material-icons">clear</i>
                                        </span>
                                        </label>
                                    @endforeach
                                </ul>
                            </li>
                            <li class="button-dropdown">
                                <a href="javascript:void(0)" class="dropdown-toggle">Views<span>▼</span></a>
                                <ul class="dropdown-menu">

                                    <label class="container-items" data-class="carousel">Trending
                                        <input type="radio" name="trending" data-type="most-viewed"
                                               class="box-click rb-feat trending">
                                        <span class="checkmark">
                                            <i class="large material-icons">clear</i>
                                        </span>
                                    </label>


                                    <label class="container-items" data-class="views">Most Viewed
                                        <input type="radio" name="trending" data-type="views-recent"
                                               class="box-click rb-feat views">
                                        <span class="checkmark">
                                            <i class="large material-icons">clear</i>
                                        </span>
                                    </label>
                                    <label class="container-items" data-class="latest">Latest Uploads
                                        <input type="radio" name="trending" data-type="most-appreciated"
                                               class="box-click rb-feat latest">
                                        <span class="checkmark">
                                            <i class="large material-icons">clear</i>
                                        </span>
                                    </label>


                                </ul>
                            </li>

                            <li class="button-dropdown">
                                <a href="javascript:void(0)" class="dropdown-toggle btn-clear-search">Clear all</a>
                            </li>

                        </ul>
                        <!--fin searchs-->
                    </div>
                </div>
                <!-- Fin Search Project -->
                <div class="searchs-bscone result-new-search">
                    <div class="results-searchs">
                        <div class="">
                            <div id="top-projects" class="loader-porjects-tags">
                                <div class="limit-area-result hide-view">
                                    @empty($projects)
                                        There's no results
                                    @else

                                        @foreach($projects as $project)
                                            @include('search.partials.project-card')
                                        @endforeach

                                    @endempty
                                </div>
                                <div id="break-next-page" style="width: 100%; height: 1px"></div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    @include('search.partials.project-string-t')
    @include('profile.partials.profile-view-notification-t')
    @include('search.partials.message-empty')

@endsection
