@include('emails.notification-top')
<body width="100%" style="margin: 0; padding: 0 !important; mso-line-height-rule: exactly; background-color: #ffffff;">
<center style="width: 100%; background-color: #ffffff;">
    <!--[if mso | IE]>
    <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="100%" style="background-color: #222222;">
        <tr>
            <td>
    <![endif]-->

    <!-- Visually Hidden Preheader Text : BEGIN -->
    <div style="display: none; font-size: 1px; line-height: 1px; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden; mso-hide: all; font-family: sans-serif;">
        {{$profile->full_name}} {{$profile->last_name}} just visited your profile
    </div>
    <!-- Visually Hidden Preheader Text : END -->

    <!-- Create white space after the desired preview text so email clients don’t pull other distracting text into the inbox preview. Extend as necessary. -->
    <!-- Preview Text Spacing Hack : BEGIN -->
    <div style="display: none; font-size: 1px; line-height: 1px; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden; mso-hide: all; font-family: sans-serif;">
        &zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;
    </div>
    <!-- Preview Text Spacing Hack : END -->

    <!--
        Set the email width. Defined in two places:
        1. max-width for all clients except Desktop Windows Outlook, allowing the email to squish on narrow but never go wider than 680px.
        2. MSO tags for Desktop Windows Outlook enforce a 680px width.
        Note: The Fluid and Responsive templates have a different width (600px). The hybrid grid is more "fragile", and I've found that 680px is a good width. Change with caution.
    -->
    <div style="max-width: 680px; margin: 0 auto;" class="email-container">
        <!--[if mso]>
        <table align="center" role="presentation" cellspacing="0" cellpadding="0" border="0" width="680">
            <tr>
                <td>
        <![endif]-->

        <!-- Email Body : BEGIN -->
        <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="margin: 0 auto;">

            @include('emails.notification-header')
            <!-- Email Header : END -->
            <tr>
                <td style="padding: 20px; font-family: sans-serif; font-size: 12px; line-height: 15px; text-align: center; color: #0D0E10; background-color: #f4f6f8;">
                    <br>
                    <h2 style="font-weight: normal;">Hey <span style="color: #0D0E10; font-weight: bolder;">{{$profileCurrent->full_name}}</span></h2>
                    <h2 style="    margin: 0 0 10px;font-weight: lighter;line-height: 21px;">{{$profile->full_name}} {{$profile->last_name}} just visited your profile</h2>
                </td>
            </tr>

            <tr>
                    <!-- dir=ltr is where the magic happens. This can be changed to dir=rtl to swap the alignment on wide while maintaining stack order on narrow. -->
                    <td dir="ltr" height="100%" valign="top" width="100%" style="padding: 10px; background-color: #f4f6f8;">
                        <!--[if mso]>
                        <table align="center" role="presentation" border="0" cellspacing="0" cellpadding="0" width="660" style="width: 660px;">
                            <tr>
                                <td valign="top" width="660" style="width: 660px;">
                        <![endif]-->
                        <table align="center" role="presentation" border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:660px;">
                            <tr>
                                <td align="center" valign="top" style="font-size:0; padding: 10px 0;border-bottom: 1px solid #cdcdcd;border-top: 1px solid #cdcdcd;">
                                    <!--[if mso]>
                                    <table role="presentation" border="0" cellspacing="0" cellpadding="0" width="660" style="width: 660px;">
                                        <tr>
                                            <td valign="top" width="220" style="width: 220px;">
                                    <![endif]-->
                                    <div style="display:inline-block; margin: 0 -2px; max-width: 200px; min-width:160px; vertical-align:top; width:100%;" class="stack-column">
                                        <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
                                            <tr>
                                                <td dir="ltr" style="padding: 0 53px;">
                                                    <img src="{{$profile->photo}}" width="100" height="" border="0" alt="alt_text" class="center-on-narrow" style="width: 100%; max-width: 200px; height: auto; background: #dddddd; font-family: sans-serif; font-size: 15px; line-height: 15px; color: #555555;">
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <!--[if mso]>
                                    </td>
                                    <td valign="top" width="440" style="width: 440px;">
                                    <![endif]-->
                                    <div style="display:inline-block; margin: 0 -2px; max-width:66.66%; min-width:320px; vertical-align:top;" class="stack-column">
                                        <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
                                            <tr>
                                                <td dir="ltr" style="font-family: sans-serif; font-size: 15px; line-height: 20px; color: #555555; padding: 10px 10px 0; text-align: left;" class="center-on-narrow">
                                                    <h2 style="margin: 0; font-family: sans-serif; font-size: 18px; line-height: 22px; color: #333333; font-weight: bold;">{{$profile->full_name}} {{$profile->last_name}}</h2>
                                                    <p style="margin: 0;font-weight: bolder;padding: 0;color: black;">Writer</p>
                                                    <p style="margin: 0 0 10px 0; font-weight: lighter;">San Francisco, Usa</p>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <!--[if mso]>
                                    </td>
                                    </tr>
                                    </table>
                                    <![endif]-->
                                </td>
                            </tr>
                        </table>
                        <!--[if mso]>
                        </td>
                        </tr>
                        </table>
                        <![endif]-->
                    </td>
                </tr>

            <!-- Thumbnail Left, Text Right : END -->

            <tr>
                <td style="background-color: #f4f6f8;">
                    <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
                        <tbody>
                        <tr>
                            <td style="padding: 12px 20px 20px 0;border-bottom: 1px solid #cdcdcd;">
                                <!-- Button : BEGIN -->
                                <table align="center" role="presentation" cellspacing="0" cellpadding="0" border="0" style="margin: auto;">
                                    <tbody><tr>
                                        <td class="button-td button-td-primary" style="border-radius: 4px;">
                                            <a class="button-a button-a-primary" href="{{$profile->url}}" target="_blank" style="color: #0D0E10; font-family: sans-serif; font-size: 15px; line-height: 15px; text-decoration: none; padding: 13px 17px; display: block; border-radius: 4px; background: linear-gradient(180deg, #FFD96A 0%, #FFBC00 100%);border: 1px solid #D5AB55;box-sizing: border-box;">View profile</a>
                                        </td>
                                    </tr>
                                    </tbody></table>
                                <!-- Button : END -->
                            </td>
                        </tr>

                        </tbody>
                    </table>
                </td>
            </tr>


        </table>
        <!-- Email Body : END -->

        <!-- Email Footer : BEGIN -->
        @include('emails.notification-footer')
        <!-- Email Footer : END -->

        <!--[if mso]>
        </td>
        </tr>
        </table>
        <![endif]-->
    </div>



    <!--[if mso | IE]>
    </td>
    </tr>
    </table>
    <![endif]-->
</center>
</body>

