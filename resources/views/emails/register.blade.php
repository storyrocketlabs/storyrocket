@component('mail::layout')
    {{-- Header --}}
    @slot('header')
        @component('mail::header')
        @endcomponent
    @endslot
    <table>
        <tr>
            <td>
                <h1>Hello {{$profile->full_name}}</h1>
            </td>
        </tr>
        <tr>
            <td>
                <h2>
                    {{$message}}
                </h2>
            </td>
        </tr>
        <tr>
            <td class="user-info">
                @if($validateEmail)
                    <strong> Name:</strong> {{$profile->full_name}} <strong>Last Name:</strong> {{$profile->last_name}}
                    <br>
                    <strong>Email Address:</strong> {{$user->email}}
                @endif
            </td>
        </tr>
        <tr>
            <td>
                @isset($actionText)
                    @component('mail::button', ['url' => $actionUrl, 'color' => 'primary'])
                        {{ $actionText }}
                    @endcomponent
                @endisset
            </td>
        </tr>
    </table>

    {{-- Footer --}}
    @slot('footer')
        @component('mail::footer')
            © {{ date('Y') }} All Rights Reserved.
        @endcomponent
    @endslot

    @isset($actionText)
        @slot('subcopy')
            @component('mail::subcopy')
                @lang(
                "If you’re having trouble clicking the \":actionText\" button, copy and paste the URL below\n".
                'into your web browser: [:actionURL](:actionURL)',
                [
                    'actionText' => $actionText,
                    'actionURL' => $actionUrl
                ]
            )
            @endcomponent
        @endslot
    @endisset
@endcomponent
