<table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="max-width: 680px;">
    <tr>
        <td style="padding: 20px; font-family: sans-serif; font-size: 12px; line-height: 15px; text-align: center; color: #0D0E10;">
            <webversion>
                <p>
                    <img src="http://dev.storyrocket.com/images/mails/logo-mail-1.png" width="300">
                </p>
                <p>
                    <a href="http://dev.storyrocket.com" target="_blank" style="padding: 5px">
                        <img src="http://dev.storyrocket.com/images/mails/facebook.png?v1" width="30">
                    </a>
                    <a href="http://dev.storyrocket.com" target="_blank" style="padding: 5px">
                        <img src="http://dev.storyrocket.com/images/mails/twitter.png" width="30">
                    </a>
                    <a href="http://dev.storyrocket.com" target="_blank" style="padding: 5px">
                        <img src="http://dev.storyrocket.com/images/mails/instagram.png" width="30">
                    </a>
                    <a href="http://dev.storyrocket.com" target="_blank" style="padding: 5px">
                        <img src="http://dev.storyrocket.com/images/mails/link.png" width="30">
                    </a>

                </p>
            </webversion>



            Copyright © 2019 Storyrocket.com, All rights reserved.<br>
            Want to change how you receive these emails?<br>
            You can <a href="http://dev.storyrocket.com" style="color: #FAA31D; text-decoration:none;">update your preferences</a>
            or <a  href="http://dev.storyrocket.com" style="color: #FAA31D; text-decoration: none;">unsubscribe from this list.</a>
            <br><br><br>
            <!-- span class="unstyle-auto-detected-links">123 Fake Street, SpringField, OR, 97477 US<br>(123) 456-7890</span -->
            <!-- unsubscribe style="color: #888888; text-decoration: underline;">unsubscribe</unsubscribe -->
        </td>
    </tr>
</table>