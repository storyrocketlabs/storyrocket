@include('emails.notification-top')
<body width="100%" style="margin: 0; padding: 0 !important; mso-line-height-rule: exactly; background-color: #ffffff;">
<center style="width: 100%; background-color: #ffffff;">
    <!--[if mso | IE]>
    <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="100%" style="background-color: #222222;">
        <tr>
            <td>
    <![endif]-->

    <!-- Visually Hidden Preheader Text : BEGIN -->
    <div style="display: none; font-size: 1px; line-height: 1px; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden; mso-hide: all; font-family: sans-serif;">
        your projects may be now be seen on Project Spotlight
    </div>
    <!-- Visually Hidden Preheader Text : END -->

    <!-- Create white space after the desired preview text so email clients don’t pull other distracting text into the inbox preview. Extend as necessary. -->
    <!-- Preview Text Spacing Hack : BEGIN -->
    <div style="display: none; font-size: 1px; line-height: 1px; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden; mso-hide: all; font-family: sans-serif;">
        &zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;
    </div>
    <!-- Preview Text Spacing Hack : END -->

    <!--
        Set the email width. Defined in two places:
        1. max-width for all clients except Desktop Windows Outlook, allowing the email to squish on narrow but never go wider than 680px.
        2. MSO tags for Desktop Windows Outlook enforce a 680px width.
        Note: The Fluid and Responsive templates have a different width (600px). The hybrid grid is more "fragile", and I've found that 680px is a good width. Change with caution.
    -->
    <div style="max-width: 680px; margin: 0 auto;" class="email-container">
        <!--[if mso]>
        <table align="center" role="presentation" cellspacing="0" cellpadding="0" border="0" width="680">
            <tr>
                <td>
        <![endif]-->

        <!-- Email Body : BEGIN -->
        <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="margin: 0 auto;">

        @include('emails.notification-header')
        <!-- Email Header : END -->


            <tr>
                <td style="padding: 20px; font-family: sans-serif; font-size: 12px; line-height: 15px; text-align: center; color: #0D0E10; background-color: #f4f6f8;">
                    <br>
                    <h2 style="font-weight: normal;">Hey <span style="color: #0D0E10; font-weight: bolder;">{{$name}},</span></h2>
                    <h2 style="    margin: 0 0 10px;font-weight: lighter;line-height: 21px;">your projects may be now be seen on <b>Project Spotlight</b></h2>
                </td>
            </tr>
            <!-- Thumbnail Left, Text Right : END -->

            <tr>
                <td style="background-color: #f4f6f8;">
                    <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
                        <tbody>
                        <tr>
                            <td style="padding: 12px 20px 20px 0;border-bottom: 1px solid #cdcdcd;">
                                <!-- Button : BEGIN -->
                                <table align="center" role="presentation" cellspacing="0" cellpadding="0" border="0" style="margin: auto;">
                                    <tbody><tr>
                                        <td class="button-td button-td-primary" style="border-radius: 4px;">
                                            <a class="button-a button-a-primary" href="http://storyrocket.com" target="_blank" style="color: #0D0E10; font-family: sans-serif; font-size: 15px; line-height: 15px; text-decoration: none; padding: 13px 17px; display: block; border-radius: 4px; background: linear-gradient(180deg, #FFD96A 0%, #FFBC00 100%);border: 1px solid #D5AB55;box-sizing: border-box;">Go to activity</a>
                                        </td>
                                    </tr>
                                    </tbody></table>
                                <!-- Button : END -->
                            </td>
                        </tr>

                        </tbody>
                    </table>
                </td>
            </tr>


        </table>
        <!-- Email Body : END -->

        <!-- Email Footer : BEGIN -->
    @include('emails.notification-footer')
    <!-- Email Footer : END -->

        <!--[if mso]>
        </td>
        </tr>
        </table>
        <![endif]-->
    </div>



    <!--[if mso | IE]>
    </td>
    </tr>
    </table>
    <![endif]-->
</center>
</body>

