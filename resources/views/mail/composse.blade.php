@extends('layouts.app')
@section('content')
<div class="wrapper tooltip-nav">
    <!-- slide left start -->
    @auth
        @include('profile.partials.slideleft')
    @endauth
    <div class="page-content-wrapper">
	    <section id="mailbox">
	 	    <article class="row">
                @include('mail.partials.aside')
                <div class="col s12 m12 l8 xl8 cont-inbox-pris">
                    <div class="cont-inbox-3">
                        <div class="cont-inbox-4">
                            <div id="load-mail-view">
                                <div class="messega-composse">
                                    <form id="frm-mailbox">
                                        <div class="loader-users">
                                            <input type="text" id="_text-users" name="" value="" placeholder="Name user:" class="input-composse writer-input" data-name="name">
                                            <ul class="resultado" style="display: none;"></ul>
                                        </div>
                                        <input type="hidden" value="" class="_mail" data-name="mail" id="_mailFriend">
                                        <input type="hidden" value="" class="_fecha" data-name="fecha" id="_fecha">
                                        <input type="hidden" value="new-messages" class="_action input-congi" data-name="action">
                                        <input type="hidden" value="" class="_id" id="_idFriend" data-name="friend">
                                        
                                        <input type="text" name="" value="" placeholder="Subject:" class="message_asunto input-composse" data-name="sbj">
                                        <textarea name="name" rows="8" cols="80" placeholder="Write a message" class="message_text text-area-msn-new" data-name="text"></textarea>
                                        <div class="col s12 m12 l12 xl12 msn-aline">
                                            <a href="#" class="btn-cancel-msn a-button" onclick="location.href='http://storyrocket.test/mail'">Cancel</a>
                                            <a href="javascript:void(0);" class=" btn-send-message a-button">Send</a>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
	 	    </article>
        </section>
    </div>
</div>
@endsection