@extends('layouts.app')
@section('content')
<div class="wrapper tooltip-nav">
    <!-- slide left start -->
    @auth
        @include('profile.partials.slideleft')
    @endauth
    <div class="page-content-wrapper">
	    <section id="mailbox">
	 	    <article class="row">
                @include('mail.partials.aside')
                <div class="col s12 m12 l8 xl8 cont-inbox-pris">
                    <div class="cont-inbox-3">
                        <div class="cont-inbox-4">
                            <div class="content-composs">
                                <a href="{{url('mail/composse-message')}}" class="btn_composse render-composer-menssaje" >
                                    <span class="icon16-composs"></span>
                                    <span>Composse message</span>
                                </a>
                            </div>
                            <!--view message-->
                            @if($shared)
                                <ul class="conte-shared"> 
                                    @foreach($mail_inbox as $items)
                                        <li>
                                            <div class="row">
                                                <div class="col xl9">
                                                    <div class="__share-content">
                                                        <h2 class="__share-content-title">You shared full project <a href="{{url('/')}}/{{$items['urlproj']}}" target="_blank">"{{$items['project_name']}}"</a></h2>
                                                        <p>With <a href="{{url('/')}}/{{$items['url']}}" target="_blank">{{$items['full_name']}}</a> 
                                                            (
                                                            @foreach($profile_occupations as $occupation)
                                                                {{$occupation->occupation}}
                                                            @endforeach)</p>
														<p>Date: {{$items['date']}}</p>
                                                    </div>
                                                </div>
                                                <div class="col xl3">
                                                    <div class="__share-date">
                                                        <p>{{$items['timestamp']}} <i class="material-icons">access_time</i></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    @endforeach
                                </ul>
                            @else
                                <div id="load-mail-view">
                                    <div class="cent" style="margin: 10rem auto;">
                                        <img src="https://www.storyrocket.com/public/images/no-messages-mail-box.svg" alt="" class="back-img">
                                        <div class="box">
                                        <h1>No messages…yet!</h1>
                                        <p>Reach out and start a conversation. <br>Great things might happen.</p>
                                        <a href="{{url('mail/composse-message')}}" class="btn-mail" style="margin-top: 1rem;">Start a new message</a>
                                        </div>
                                    </div>
                                </div>
                            @endif
                            
                        </div>
                    </div>
                </div>
	 	    </article>
        </section>
    </div>
</div>
@endsection