<div class="col s12 m12 l4 xl4 cont-cards-mail">
    <div class="cont-card-over">
        <div class="cont-card-over1 scrollable">
                @if($shared)
                    <ul id="" class="mail-nav-aside-menus">
                        <li>
                            <a href="{{url('/mail/inbox')}}" class="ic-msn">
                                <i class="icon16-inbox-white"></i>
                                <span class="font-nav2">Inbox</span>
                                <span class="box-num-mail-nav num-inbox">{{count($count_inbox)}}</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{url('/mail/send')}}" class="ic-msn">
                                <i class="material-icons">send</i>
                                <span class="font-nav2">Sent</span>
                                <span class="box-num-mail-nav num-sent">{{count($count_sent)}}</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{url('/mail/archive')}}" class="ic-msn">
                                <i class="icon16-Archive2"></i>
                                <span class="font-nav2">Archive</span>
                                <span class="box-num-mail-nav num-archive">{{count($count_archive)}}</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{url('/mail/shared')}}" class="ic-msn">
                                <i class="icon16-Shared2"></i>
                                <span class="font-nav2">Shared Project</span>
                                <span class="box-num-mail-nav num-shared">{{count($count_shared)}}</span>
                            </a>
                        </li>
                    </ul>
                @else
                    <div class="ic-search">
                        <i class="material-icons">search</i>
                        <input class="input-mail-search " type="text" name="" value="" placeholder="Search message" id="filters">
                    </div>
                    <div class="nav-mail ic-inbox">
                        <i class="icon16-inbox"></i>
                        <span class="font-title-mail">{{$type}}</span>
                        <span class="box-num-mail">{{count($count_mail)}}</span>
                        <div class="ic-more">
                            <a class="btn-more lit-tag dropdown-trigger" data-target='dropdown1' id="tag-mail" href="#" >
                                <i class="material-icons">more_vert</i>
                            </a>
                            <ul id="dropdown1" class="dropdown-content drop-msn-nav">
                                <li>
                                    <a href="{{url('/mail/inbox')}}" class="ic-msn">
                                        <i class="icon16-inbox-white"></i>
                                        <span class="font-nav2">Inbox</span>
                                        <span class="box-num-mail-nav num-inbox">{{count($count_inbox)}}</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{url('/mail/send')}}" class="ic-msn">
                                        <i class="material-icons">send</i>
                                        <span class="font-nav2">Sent</span>
                                        <span class="box-num-mail-nav num-sent">{{count($count_sent)}}</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{url('/mail/archive')}}" class="ic-msn">
                                        <i class="icon16-Archive2"></i>
                                        <span class="font-nav2">Archive</span>
                                        <span class="box-num-mail-nav num-archive">{{count($count_archive)}}</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{url('/mail/shared')}}" class="ic-msn">
                                        <i class="icon16-Shared2"></i>
                                        <span class="font-nav2">Shared Project</span>
                                        <span class="box-num-mail-nav num-shared">{{count($count_shared)}}</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    @if(count($mail_inbox) > 0)
                        @foreach($mail_inbox as $mail)
                            @if($mail['_isDelete'] == false)
                                <div class="read-mail card-mail tabpanel  {{$mail['isread']}} items-searchs" id="{{$mail['idmessage']}}" data-message="{{$mail['idmessage']}}" data-inbox="{{$mail['keyproject']}}" data-det="{{$mail['keydetalle']}}" data-type="inbox" data-friend="{{$mail['keyfriends']}}" data-key="{{$mail['keypadreMessage']}}" data-tag="{{$type}}">
                                    <div class="card-mail-img">
                                        <img src="public/images/bg.png" data-src="{{$mail['avatar']}}" alt="perfil" class="responsive-img lazy-image">
                                    </div>
                                    <div class="cont-bullet">
                                        <p class="font-name-mail truncate">{{$mail['Fullname']}}</p>
                                        <span class="new-mail"></span>
                                        <div class="ic-bullet">
                                            <i class="material-icons">{{$mail['isactive']}}</i>
                                        </div>
                                        <span class="font-date-mail">{{$mail['fechamsj']}}</span>
                                    </div>
                                    <div class="cont-subject">
                                        <p class="truncate font-name-mail items-searchs">
                                            <?php //if($value['re']){echo 'RE: ';} ?>
                                            @if($mail['re']) <b>RE:</b> @endif
                                            {{$mail['asunto']}}
                                        <p class="truncate font-subject">{{$mail['Message']}}</p>
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    @else
                        <div class="icon-mail">
                            <span>
                                <svg width="75px" height="70px" viewBox="0 0 75 70" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                    <g id="inbox2" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <g id="spam" transform="translate(-330.000000, -328.000000)" fill-rule="nonzero" fill="#BBBBBB">
                                            <g id="Group-2" transform="translate(134.000000, 48.000000)">
                                                <g id="noun_1209534_cc" transform="translate(196.000000, 280.000000)">
                                                    <g id="Group">
                                                        <path d="M6.7,70 L67.4,70 C71.1,70 74.1,67 74.1,63.3 L74.1,28.6 C74.1,26.3 72.9,24.2 71.1,23 L64.1,18.2 L64.1,15.6 C64.1,12.5 61.6,10 58.4,10 L52.3,10 C52.3,10 52.2,9.9 52.2,9.9 L40.7,1.9 C38.5,0.5 35.8,0.5 33.5,2 L22.4,9.7 C22.2,9.8 22.1,9.9 22,10.1 L15.8,10.1 C12.7,10.1 10.1,12.6 10.1,15.7 L10.1,18.3 L3.2,23.1 C1.2,24.3 0.1,26.4 0.1,28.7 L0.1,63.5 C-3.74700271e-16,67 3,70 6.7,70 Z M8.5,66 L36,42.6 C36.5,42.1 37.3,42.1 37.9,42.6 L65.5,66 L8.5,66 Z M64,23 L68.8,26.3 C69.5,26.7 69.9,27.4 70,28.2 L64,33.5 L64,23 Z M70,33.5 L70,63.4 C70,63.8 69.9,64.1 69.8,64.5 L52.1,49.5 L70,33.5 Z M35.6,5.3 C36.5,4.8 37.6,4.8 38.4,5.2 L45.3,10 L28.7,10 L35.6,5.3 Z M14,15.6 C14,14.7 14.7,14 15.7,14 L58.4,14 C59.3,14 60.1,14.7 60.1,15.6 L60.1,37 L49.1,46.8 L40.6,39.6 C38.6,37.9 35.6,37.9 33.5,39.6 L25,46.8 L14,37 L14,15.6 Z M5.3,26.3 L10,23 L10,33.4 L4.1,28.1 C4.2,27.4 4.6,26.7 5.3,26.3 Z M4,33.5 L21.9,49.4 L4.2,64.4 C4.1,64.1 4,63.7 4,63.3 L4,33.5 Z" id="Shape"></path>
                                                        <path d="M24,25 L50,25 C51.1,25 52,24.1 52,23 C52,21.9 51.1,21 50,21 L24,21 C22.9,21 22,21.9 22,23 C22,24.1 22.9,25 24,25 Z" id="Shape"></path>
                                                        <path d="M52,32 C52,30.9 51.1,30 50,30 L24,30 C22.9,30 22,30.9 22,32 C22,33.1 22.9,34 24,34 L50,34 C51.1,34 52,33.1 52,32 Z" id="Shape"></path>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                </svg>
                            </span>
                            <p class="font-none-msn">Your Inbox is empty.</p>
                        </div>
                    @endif
                @endif
        </div>
    </div>
</div>