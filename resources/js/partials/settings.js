$(document).ready(function () {

    var addMeToPS = function () {

        $(".ps_addme_to_profile_spotlight_container .ios-ui-select").click(function() {
            setTimeout(function(){
                if($("#ps_addme_to_profile_spotlight").is(":checked")) {
                        settings.values[$(this).attr('id')] = $(this).prop('checked') ? 1 : 0;
                        axios.post('/settings/ps_addme_to_profile_spotlight_mail', settings).then(response=>{
                            console.log("ps_addme_to_profile_spotlight_mail");
                        });
                }

            }, 2000);

        });

        $(".ps_add_my_projects_to_project_spotlight_container .ios-ui-select").click(function() {
            setTimeout(function(){
                if($("#ps_add_my_projects_to_project_spotlight").is(":checked")) {
                    settings.values[$(this).attr('id')] = $(this).prop('checked') ? 1 : 0;
                    axios.post('/settings/ps_add_my_projects_to_project_spotlight_mail', settings).then(response=>{
                        console.log("ps_add_my_projects_to_project_spotlight_mail");
                    });
                }

            }, 2000);

        });
    };


    /**
     * Click enable/disabled if check option special
     */

    var settingsCheckEnableDisable = function(){
        $(".settings-enable-disable .ios-ui-select").click(function () {


            if($(this).parent().parent().removeClass("settings-enable-disable-default").addClass("settings-enable-disable-selected")){
                var inputLabelDeafult =  $(".settings-enable-disable-default").children("label");
                var idInput = inputLabelDeafult.children(".settings-check").attr("id");
                var inputCheckboxDiv = inputLabelDeafult.children(".ios-ui-select");

                inputCheckboxDiv.toggleClass("checked");
                $("#" + idInput).click();
                $(".settings-enable-disable").removeClass("settings-enable-disable-selected").addClass("settings-enable-disable-default")


            }


        });
    };


    /**
     * If disable button parent then disable child
     */

    var settingsCheckParent = function(){
        $(".check-parent-depend .ios-ui-select").click(function () {

            if(!$(this).hasClass("checked")) {
                var inputCheckbox = $(this).parent().parent().parent().children(".second-check-setting").children("label").children("input[type=checkbox]");
                if (inputCheckbox.is(':checked')) {
                    inputCheckbox.trigger("click");
                }
            }
        });
    };

    /**
     * If enable child button then enabled parent button
     */

    var settingsCheckChild = function(){
        $(".check-parent-depend .second-check-setting input[type=checkbox]").click(function () {
            var inputLabel = $(this).parent().parent().parent().children("p").children("label");
            var idInput = inputLabel.children(".settings-check").attr("id");
            var inputCheckboxDiv = inputLabel.children(".ios-ui-select");

            if (!$("#" + idInput).is(':checked')) {
                // Toggel the check state
                inputCheckboxDiv.toggleClass("checked");
                // Update state
                //$("#" + idInput).prop('checked', true);
                // Run click even in case it was registered to the original checkbox element.
                $("#" + idInput).click();
            }
        });
    }


    if(typeof settings !== 'undefined')
    {
        //console.log(settings.values);
        _.forEach(settings.values, function (value, index) {
            if(value == 1)
            {
                $('#'+index).prop('checked', true);
            }else{
                $('#'+index).prop('checked', false);
            }
        });
        if($(".check-ios").iosCheckbox()){
            settingsCheckParent();
            settingsCheckChild();
            settingsCheckEnableDisable();
            addMeToPS();
        }

    }


    $('.settings-check').on('click', function () {
        settings.values[$(this).attr('id')] = $(this).prop('checked') ? 1 : 0;

        axios.post('/settings/save', settings).then(response=>{
            //console.log(response.data);
        });


    });

});