var red = '#da0707';
var green = '#259405';
$('.button-collapse').sidenav();

const uri = window.location.pathname;
$('.tabpanel.noting-mail-color').each( (index, el)=> {
    if(uri != '/mail/composse-message'){
        if (index == 0) {
            reloadersActive($(el));
        }
    }
	
});
function reloadersActive(_this){
    _this.addClass('mail-active');  
    let obj = JSON.stringify({
            'message'   : _this.data('message')
        ,   'inbox'     : _this.data('inbox')
        ,   'det'       : _this.data('det')
        ,   'friends'   : _this.data('friend')
        ,   'type'      :  _this.data('type')
        ,   'msjpadre'  : _this.data('key') 
        ,   'tags'      : _this.data('tag')
    });
    getData('/loader-mail',obj,(e)=>{
        $("#load-mail-view").html('');
		$("#load-mail-view").append(renderHead(e,_this.data('tag')));
    });
}
$('.dropdown-trigger').dropdown();
$('#tag-mail').on('click', function (event) {
    $("#dropdown-mail").toggleClass('active-dow');
});


$("#_text-users").on("keyup",(event)=>{
    let _body = JSON.stringify({name:$(event.target).val()});
    getData('/mail/ajaxReturnsUsers',_body,function(e){
       let _user = renderUsers(e);
       $('.resultado').empty();
        $('.resultado').show(200);
        $('.resultado').css({ 'height': 'auto' });
        $('.resultado').append(_user);
       
    });
})
$('body').on('click','.items-users',(e)=>{
    $("#_mailFriend").val(e.currentTarget.dataset.mail);
    $("#_idFriend").val(e.currentTarget.dataset.key);
    $("#_text-users").val(e.currentTarget.dataset.name);
    $('._fecha').val(returnDate());
    $(".resultado").hide(100);
})

$('#filters').keyup(function () {
	var rex = new RegExp($(this).val(), 'i');
	$('.tabpanel').hide();
	$('.tabpanel').filter(function () {
		$(this).addClass('mail-active');
		return rex.test($(this).text());
	}).show();
});
$('#close-modal-userMessage').on('click',(e)=>{
    $('#modal-userMessage').modal('close');
})
$("#btn-composer-porject").on('click',(e)=>{
    e.preventDefault();
    let obj = JSON.stringify({
            '_id_friend'    : $(".msj-users").data('friends')
        ,   '_asunto'       : 'New Request Full Project: '+$(".font-resq").text()
        ,   '_text'         : $("#modal-message-project").val()
        ,   'project'       : $(".project-idpages ").attr('id')
        ,   '_fecha'        : returnDate()
        ,   '_action'       : 'project'
    });
    getData('/send-mail',obj,(e)=>{
    })
})

$('#submit-message').on('click',(e)=>{
    let obj =JSON.stringify(
        {
                '_mail'         : 'null'
            ,   '_id_friend'    : $("#frind-user-id").data('friends')
            ,   '_fecha'        : returnDate()
            ,   '_asunto'       : $('#txt_name_obs').val()
            ,   '_text'         : $("#txt_name_descrip").val()
        }  
    );
    getData('/send-mail',obj,(e)=>{
        
        $("#frm-mjs")[0].reset();
        $('#modal-userMessage').modal('close');
    })
})
//enviar mensaje desde el formulario de mail
$('.btn-send-message').on('click',(e)=>{
    let sendNum = parseInt($(".num-sent").text());
    let total = sendNum + parseInt(1);

    let obj =JSON.stringify(
        {
                '_mail'         : $("._mail").val()
            ,   '_id_friend'    : $("._id").val()
            ,   '_fecha'        : returnDate()
            ,   '_asunto'       : $('.message_asunto').val()
            ,   '_text'         : $(".message_text").val()
            
        }  
    );
        //,   '_action': $("._action").val()
    getData('/send-mail',obj,(e)=>{
        $("#frm-mailbox")[0].reset();
        message_text(e,green);
        $(".num-sent").html(total);
    })
})
//ver un mensaje enviado del tag
$('body').on('click','.read-mail',e=>{
    if($(e.currentTarget).hasClass('new-mail-color')){
        let numail = $('.mail-notifications').text();
        let tot = parseInt(numail) - parseInt(1);
        $('.mail-notifications').html(tot);
        $('.box-num-mail').html(tot);
        $('.num-inbox').html(tot);
        if(tot == 0){
            $('.mail-notifications').css('visibility', 'hidden');
        }
    }
    $('.tabpanel').removeClass('mail-active');
    let tag = e.currentTarget.dataset.tag;
    let obj = JSON.stringify({
            'message': e.currentTarget.dataset.message
        ,   'inbox' : e.currentTarget.dataset.inbox
        ,   'det' : e.currentTarget.dataset.det
        ,   'friends': e.currentTarget.dataset.friend
        ,   'type' : e.currentTarget.dataset.type
        ,   'msjpadre' : e.currentTarget.dataset.key
        ,   'tags' : tag
    });
    $(e.currentTarget).addClass('mail-active');
    $(e.currentTarget).removeClass('new-mail-color');
    $(e.currentTarget).find('.ic-bullet').html('<i class="material-icons">radio_button_unchecked</i>');
    getData('/loader-mail',obj,(e)=>{
        $('.not-alert').css('visibility', 'visible');
        $("#load-mail-view").html('');
		$("#load-mail-view").append(renderHead(e,tag));
    });
})
//boton reply para mostrar caja de respuesta
$('body').on('click','.btn-replay',e=>{
    $('.btn-replay').hide();
    let btnDownload = e.currentTarget.dataset.dowloand;
    let obj = {'idproject':e.currentTarget.dataset.idproject,'nameProjec':e.currentTarget.dataset.nameprojec,'message_id':e.currentTarget.dataset.tokem};
    let resp = renderBtnReply(btnDownload,obj);
    let tokem = e.currentTarget.dataset.tokem;
    let friend = e.currentTarget.dataset.friend;
    let dif = e.currentTarget.dataset.dif;
    $(".mail-view").append(resp);
    $('body').find(".btn-message-send").attr('data-tokem',tokem);
    $('body').find(".btn-message-send").attr('data-friend',friend);
    $('body').find(".btn-message-send").attr('data-dif',dif);
});
//oculta caja de respuesta de mensaje
$('body').on('click','.btn-cancel-message',e=>{
    $('.btn-replay').show();
    $(".content-reply").remove();
})
//responder un mensaje
$('body').on('click','.btn-reply-msn',e=>{
    let message = $(".message-text").val();
    let _pdf = false,_prof = 0,_ids = 0;
    if (document.getElementById('check-pl')) {
		if ($('#check-pl').prop('checked')) {
			_pdf = true;
			_prof = $('.check-acept-project').data('proj');
			_ids = $('.check-acept-project').data('message');
		}
    }
    
    if(message.length > 0){
        let obj = JSON.stringify({
                'message_id'    : e.currentTarget.dataset.tokem
            ,   'friend'        : e.currentTarget.dataset.friend
            ,   'dif'           : e.currentTarget.dataset.dif
            ,   'type'          : e.currentTarget.dataset.type
            ,   'message'       : $('.message-text').val()
            ,   'fecha'         : returnDate()
            ,   're'            : '1'
            ,   'pdf'           : _pdf
            ,   'project_id'    : _prof
        });
        getData('/reply-mail',obj,(e)=>{
            $(".messega-content").append(renderBody(e));
            message_text('Your message was sent.',green);
            $(".message-text").val('');
        })
    }else{
        message_text('enter message',red);
    }
})
$('body').on("click",".mailbox-remove",e=>{
    e.preventDefault();
    let idMessage   = $(".mail-view").attr('id');
    let isTag       = e.currentTarget.dataset.tag;
    let isType      = e.currentTarget.dataset.type;

    getData('/message-remove',JSON.stringify({'idMessage':idMessage,'isType':isType,'isTag':isTag}),(e)=>{
        console.log(e);
       message_text(e,green);
       reloaderPages();
    })
})
function returnDate(){
    let d = new Date();
   // return d;
    let _time = d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
    return _time;
}

function message_text(text,color) {
	var inner = `<div class="scope-togle" style="background:${color}">${text}</div>`;
	$(".nav-wrapper").prepend(inner);
	setTimeout(function () {
		$(".scope-togle").fadeOut(300, function () {
			$(this).remove();
		});
	}, 3000);
}
function reloaderPages(){
    setTimeout(function () {
		location.reload();
	}, 2000);
}

function renderUsers(params){
    return params.map(e=>{
        return `
            <li class="items-users" data-mail="${e.Email}" data-key="${e.id}" data-name="${e.full_name} ${e.last_name}">
                <div class="users-images">    
                    <figure><img src="${e.photo}"></figure>
                </div>
                <div class="users-data">
                    <p>
                        ${e.full_name} ${e.last_name}
                    </p>
                </div>
            </li>
        `;
    });
}

function renderHead(params,tag){
    return `
        <div class="mail-view" id="${params[0].id}">
            <div class="messega-content">
                <div class="pad-mail1">
                    <span class="font-subject2">${params[0].asunto}</span>
                    <div class="right">
                        ${(tag != 'archive') ? 
                            `<button type="button" name="button" class="btn-mailbox mailbox-remove" data-tag="${tag}" data-type="archive">
                                <span class="icon16-Archive"></span>
                                <span>Archive</span>
                                </button>`
                        :''}
                        
                        <button type="button" name="button" class="btn-mailbox mailbox-remove" data-tag="${tag}" data-type="delete">
			                <span class="icon16-Delete"></span>
			                <span class="">Delete</span>
			            </button>
                    </div>
                </div>
                ${renderBody(params)}
            </div>
            ${(params[0].from_message != 0) ? `
                <a href="javascript:void(0);" class="btn-replay" 
                    data-dowloand="${params[0].btn_dowloand_projec}" 
                    data-tokem="${params[0].message_id}" 
                    data-friend="${params[0].friend}" 
                    data-idproject="${params[0].project_id}" 
                    data-nameProjec="${params[0].project_name[0].project_name}">Reply</a>` :''}
        </div>
    `;
}

function renderBody(items){
    return items.map(e=>{
        return `
            <div class="content-message-prins">
                <div>
                    <div>
                        <div class="content-message-img bscnt">
                            <a href="${e.url}" class="font-profile-msn" title="${e.full_name}" target="_blank">
                                <img src="${e.avatar}" alt="${e.full_name}" class="responsive-img">
                            </a>
                        </div>
                        <div class="dis-messen ver-1">
                            <p class="font-profile-msn"> <a href="${e.url}" class="font-profile-msn" title="${e.full_name}" target="_blank">${e.full_name}</a></p>
                            <p class="font-city-msn">Peru</p>
                        </div>
                        <div class="dis-messen right">
                            <p class="font-hore-msn">${e.date_send}</p>
                        </div>
                    </div>
                </div>
                <div class="marg-msn-send">
                    <p class="font-msn-princi">${e.message_text}</p>
                    ${(e.view_dowloand) ? `
                    <a href="${e.href}" class="btn-acept-msn btn-download" download>Download full PDF <br>“${e.project_name[0].project_name}”</a>
                    `:''}
                </div>
            </div>
        `;
    }).join('');
}

function renderBtnReply(params,obj){
    return `
        <div class="content-reply conte-send " >
            <textarea name="name" rows="8" cols="80" class="text-area-msn message-text" placeholder="Write a reply..."></textarea>
            <div class="col s12 m12 l6 xl12 msn-aline">
            ${(params === 'true') ? `
                <div class="text-dow-cont flex-btn">
                    <p>
                        <label for="check-pl" class="xgtz label-text">
                            <input type="checkbox" id="check-pl" class="check-acept-project" data-message="${obj.message_id}" data-proj="${obj.idproject}">
                            <span class="font-text-dw">Attach the full PDF “${obj.nameProjec}” to this message </span>
                        </label>
                    </p>
                </div>` : ''}
                <button type="button" name="button" class="btn-cancel-message btn-cnl">Cancel</button>
                <button type="button" name="button" class="btn-reply-msn btn-message-send btn-rplt" data-type="rstcos">Send</button>
            </div>
        </div>
        `;
}

function procesarFormulario(selector, template) {
	var data = template ? template : {};
	var f,r,v,m,$e,$elements = $(selector).find("input, select, textarea, email, button, img");
	for (var i = 0; i < $elements.length; i++) {
		$e = $($elements[i]);
		f = $e.data("name");
		r = $e.attr("required") ? true : false;
		// si no se especificó un campo, ignoramos el elemento
		if (!f) continue;
		// dependiendo de que tipo de control se trate
		v = undefined;
		switch ($e[0].nodeName.toUpperCase()) {
			case "LABEL":
				v = $e.text();
				break;
			case "INPUT":
				var type = $e.attr("type").toUpperCase();
				if (type == "CHECKBOX") {
					v = $e.prop("checked");
				} else if (type == "RADIO") {
					if ($e.prop("checked")) {
						v = $e.val();
					}
				} else {
					v = $.trim($e.val());
				}
				/*else if($e.datepicker){
    	            v = $e.datepicker("getDate");
                }*/
				break;
			case 'IMG':
				v = $.trim($e.attr('src'));
				break;
			case "TEXTAREA":
			default:
				v = $.trim($e.val());
		}
		// grabamos el valor en el objeto
		if (r && (v == undefined || v == "")) {
			m = $e.data("mensaje");
			if (m) console.log(`message procesa formulario ==> ${m}`);else console.log(`Necesitas especificar un valor para el campo ==> ${f}`);
			$e.focus();
			return null;
		} else if (v != undefined) data[f] = v;
	} // next
	return data;
}

function getData(uri,_body,callback){
    var db ={};
    fetch(uri,{
        method: "POST",
        headers: {
            "Content-Type": "application/json",
            "Accept": "application/json",
            "X-Requested-With": "XMLHttpRequest",
            "X-CSRF-Token": $('input[name="_token"]').val()
        },
        credentials: "same-origin",
        body: _body
    })
    .then(res=>res.json())
    .then(data=>callback(data))
    .catch(error=>error);
}