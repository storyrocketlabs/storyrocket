/********
 * Hide
 ********/
$(".assistent-flow").hide();
$(".assistent-flow-left").hide();
$(".modal-create-project-assistent-header-error").hide();

let writer = {};
let writers = [];

let publisher = {};
let publishers = [];


$(document).ready(function () {
    /**
     * Open modal by default when you go to project page
     */
    /*if($(".load-asistent-project").length) {
        $('#modalCreateProjectAssistent').modal("open");
    }*/

    /**
     * button create header
     */
    /*$('.btn-create-projects').click(function (e) {
        e.preventDefault();
        $('#modalCreateProjectAssistent').modal("open");
        writer = {};
        writers = [];
    }); */

    if (window.modalCreateProject == 1)
    {
        writer = {};
        writers = [];
        $('#modalCreateProjectAssistent').modal("open");
    }


    $('.btn-go-create').click(function (e) {
        writer = {};
        writers = [];
        $('#modalCreateProjectAssistent').modal("open");
    });

});

/*****************
 * Next view
 ****************/
$("body").on('click', '.next-views', e => {
    let _idFrom = $(e.target).parents("form").attr("id");
    let _validate = e.currentTarget.getAttribute('currentvalidate');
    let _goto = e.currentTarget.getAttribute('goto');
    let _currentpage = e.currentTarget.getAttribute('currentpage');
    if (_validate == "true") {
        if ($(`#${_idFrom}`).valid()) {
            /***
             * Show new form
             */
            $(`.assistent-flow-left-${_currentpage}`).hide();
            $(`.assistent-flow-left-${_goto}`).show();
        } else {
            showMessageError();
            $("#modalCreateProjectAssistent").scrollTop(0);
        }
    } else {
        /***
         * Show new form
         */
        $(`.assistent-flow-left-${_currentpage}`).hide();
        $(`.assistent-flow-left-${_goto}`).show();
    }
})
/**
 * BOTON READ LIST VISTA PROJECTO
 */

$("body").on("click",".project-readlist",e=>{
    e.preventDefault();

    let i = $(e.currentTarget).find('i');
    if (!i.hasClass('icon-icono-project5') )
    {
        $(e.currentTarget).removeClass('add-collections').addClass('btnico');
        $(e.currentTarget).find('i').removeClass('icon-icono-project4').addClass('icon-icono-project5');

        axios.get('/notification/notification-readlaterlist-your-project').then(function (response) {
        });
    }else{
        $(e.currentTarget).removeClass('btnico').addClass('add-collections');
        $(e.currentTarget).find('i').removeClass('icon-icono-project5').addClass('icon-icono-project4');
    }


})

$("body").on("click",".mar-heart",e=>{

    e.preventDefault();
    let i = $(e.currentTarget).find('i');
    if (!i.hasClass('icon3-heartp') )
    {
        $(e.currentTarget).removeClass('like-projct');
        $(e.currentTarget).find('i').removeClass('icon3-heart2').addClass('icon3-heartp');

        axios.get('/notification/notification-like-your-project').then(response=>{});

    }else{
        $(e.currentTarget).addClass('like-projct');
        $(e.currentTarget).find('i').removeClass('icon3-heartp').addClass('icon3-heart2');
    }


});


/*****************
 * SELECT BOTON GENRE
 ****************/
$("body").on('click', '.btn-genres', e => {
    e.preventDefault();
    let _x = document.getElementsByClassName('btn-no-genres').length;
    if (_x >= 3) {
        showErrorValidator(' You can choose up to 3.');
        return false;
    }
    let _text = $(e.currentTarget).text();
    $(".items-m-cp-intended-genres").removeClass('hide');
    $(".items-m-cp-intended-genres").find('.items-m-cp-selected-row-data-content').append(`<span id="${_text}">${_text}</span>`);
    $('._tag').attr('value', '1');//agrega el valor para la validacion.
    $(e.target).addClass('btn-no-genres').removeClass('btn-genres');
    $(e.target).addClass('color-active-days');
})

$("body").on('click', '.btn-no-genres', e => {
    e.preventDefault();
    let _num = $('.btn-no-genres').length;
    if (_num == 1) {
        $('._tag').attr('value', '0');
    }
    $(e.target).removeClass('color-active-days')
    $(e.target).removeClass('btn-no-genres').addClass('btn-genres');
    let _text = $(e.currentTarget).text();
    $(".items-m-cp-intended-genres").find('.items-m-cp-selected-row-data-content').find(`#${_text}`).remove();
    if ($(".items-m-cp-intended-genres").find('.items-m-cp-selected-row-data-content').length == 0) {
        $(".items-m-cp-intended-genres").addClass('hide');
    }
})

/*****************
 * GENDER VALIDATION MESSAGE
 ****************/
const showErrorValidator = (text) => {
    let html = `<div class="modal-create-project-assistent-header-togle-error"><i class="material-icons">error</i>${text}</div>`;
    $(".modal-create-project-assistent-header").append(html);
    setTimeout(function () {
        $('.modal-create-project-assistent-header-togle-error').remove();
    }, 5000);
    $("#modalCreateProjectAssistent").scrollTop(0);
}


$("body").on('change', '#EraID', e => {
    let _text = $(e.currentTarget).text();
    $(".items-m-cp-intended-era").removeClass('hide');
    $(".items-m-cp-intended-era").find('.items-m-cp-selected-row-data-1').html(_text);
})
$("body").on('change', '.items-contry', e => {
    let _text = $(e.currentTarget).text();
    $(".items-m-cp-intended-location").removeClass('hide');
    $(".items-m-cp-intended-location").find('.items-m-cp-selected-row-data-1').html(_text);
})
$("body").on('click', '.btm-rea', e => {
    let _text = $(e.currentTarget).find('label').text();
    $('.items-m-cp-intended-rating').removeClass('hide');
    $('.items-m-cp-intended-rating').find('.items-m-cp-selected-row-data-content').html(_text);
})
/**
 * @param goToPage: segment url left
 * With this method you change page form
 */
var gotoPage = function (goToPage) {

    $('#modalCreateProjectAssistent .assistent-flow-left').hide();
    $('#modalCreateProjectAssistent .assistent-flow-left-' + goToPage).show();
};


/***
 * Call back button
 */

$('#modalCreateProjectAssistent .btn-back-step').click(function () {
    var goToPage = $(this).attr("goto");
    gotoPage(goToPage);
});


$('.btn-back-step').click(function () {
    var goToPage = $(this).attr("goto");
    gotoPage(goToPage);
});


/*****************
 * Next button form One
 ****************/

$('#modalCreateProjectAssistentOneSubmit').click(function () {

    if ($("#modalCreateProjectAssistentFormOne").valid()) {

        /***
         * Save to BD form ...
         **/

        $(".assistent-flow-left-one").hide();
        $(".assistent-flow-left-two").show();
        console.log("yes");

    } else {
        showMessageError();
        $("#modalCreateProjectAssistent").scrollTop(0);
    }
});

/********************
 * Next button form two
 ********************/

$('#modalCreateProjectAssistentTwoSubmit').click(function () {

    if ($("#modalCreateProjectAssistentFormTwo").valid()) {

        /***
         * Show new form
         */

        $(".assistent-flow-left-two").hide();
        $(".assistent-flow-left-three").show();

        /***
         * Save to BD form ...
         **/


        console.log("yes");

    } else {
        showMessageError();
        $("#modalCreateProjectAssistent").scrollTop(0);
    }
});

/********************
 * Next button form three
 ********************/

$("#modalCreateProjectAssistentThreeSubmit").click(function () {

    if ($("#modalCreateProjectAssistentFormThree").valid()) {

        /***
         * Show new form
         */

        $(".assistent-flow-left-three").hide();
        $(".assistent-flow-left-four").show();

        /***
         * Save to BD form ...
         **/


        console.log("yes");

    } else {
        showMessageError();
        $("#modalCreateProjectAssistent").scrollTop(0);
    }

});

/********************
 * Next button form four
 ********************/

$("#modalCreateProjectAssistentFourSubmit").click(function () {

    if ($("#modalCreateProjectAssistentFormThree").valid()) {

        /***
         * Show new form
         */

        $(".assistent-flow-left-fo").hide();
        $(".assistent-flow-left-four").show();

        /***
         * Save to BD form ...
         **/


        console.log("yes");

    } else {
        showMessageError();
        $("#modalCreateProjectAssistent").scrollTop(0);
    }

});

$(".assistent-flow-left-four .assistent-four-square").click(function () {
    var dataId = $(this).attr("data-id");
    $(".list-reating.font-reating-r").addClass("hide");
    $("#rati-" + dataId).removeClass("hide");
    $(".assistent-flow-left-four .assistent-four-square").removeClass("btn-active-reating");
    $(this).addClass("btn-active-reating");
});

/**************************
 * Back button only form two
 **************************/


$('#modalCreateProjectAssistent #modalCreateProjectAssistentOneBack').click(function () {
    $('#modalCreateProjectAssistent .assistent-flow').hide();
    $('#modalCreateProjectAssistent .assistent-only-title').show();
});


$('#modalCreateProjectAssistent .btn-back-step').click(function () {
    var goToPage = $(this).attr("goto");
    gotoPageLeft(goToPage);
});
/****
 * Next button title
 */
$('#modalCreateProjectAssistent #modalCreateProjectAssistentTitleSubmit').click(function () {
    $("body").css('overflow', 'initial');
    if ($("#modalCreateProjectAssistentFormName").valid()) {
        $('#modalCreateProjectAssistent .assistent-flow').show();
        $('#modalCreateProjectAssistent .assistent-flow-left-one').show();
        $('#modalCreateProjectAssistent .assistent-only-title').hide();
        $("#modalCreateProjectAssistent").scrollTop(0);
        $('.items-m-cp-title .items-m-cp-selected-row-data-content').html($('#modalCreateProjectAssistent #assistantTitleProject').val());
    }

});
/**
 * Edit title
 */
$(".btn-back-step-title").click(function () {
    $('#modalCreateProjectAssistent .assistent-flow').hide();
    $('#modalCreateProjectAssistent .assistent-only-title').show();
});

/*
var elementsValidate = function (currentPage) {
    switch (currentPage) {
        case "one":
            if($('#assistantRelationToTheProject').valid()==false || $('#asistantIntendedMediumCounter').valid()==false ||
                $('#asistantIntendedMaterialType').valid()==false || $('#asistantLenguage').valid()==false) {
                $('#assistantTitleProject').submit();
                showMessageError();
                return false;
            }
            break;
        case "two":
            break;
    }
    return true;
};
*/
var showMessageError = function () {
    $("#modalCreateProjectAssistent").scrollTop(0);
    $(".modal-create-project-assistent-header").hide();
    $(".modal-create-project-assistent-header-error").show();
    setTimeout(function () {
        $(".modal-create-project-assistent-header").show();
        $(".modal-create-project-assistent-header-error").hide();
    }, 3000)
};

var gotoPageLeft = function (goToPage) {
    $('#modalCreateProjectAssistent .assistent-flow-left').hide();
    $('#modalCreateProjectAssistent .assistent-flow-left-' + goToPage).show();
    //clearFormValidate();
};

var clearFormValidate = function () {
    var validator = $("#modalCreateProjectAssistentForm").validate();
    validator.resetForm();
    $("#asistantIntendedMedium").removeClass("error");
}

$("#assistantLogline").keyup(function () {
    $('#modalCreateProjectAssistent .items-m-cp-logline .items-m-cp-selected-row-data-content').html($(this).val());
    $('#modalCreateProjectAssistent .items-m-cp-logline').removeClass("hide");

});

$("#assistantTagline").keyup(function () {
    $('#modalCreateProjectAssistent .items-m-cp-tagline .items-m-cp-selected-row-data-content').html($(this).val());
    $('#modalCreateProjectAssistent .items-m-cp-tagline').removeClass("hide");

});

$("#assistantSynopsis").keyup(function () {
    $('#modalCreateProjectAssistent .items-m-cp-synopsis .items-m-cp-selected-row-data-content').html($(this).val());
    $('#modalCreateProjectAssistent .items-m-cp-synopsis').removeClass("hide");

});


$('#modalCreateProjectAssistent #assistantRelationToTheProject').on('change', function () {
    var assistantRelationToTheProjectValue = this.value;
    var assistantRelationToTheProjectText = $(this).find(":selected").text();
    if (assistantRelationToTheProjectValue != "") {
        $('#modalCreateProjectAssistent .items-m-cp-relation-to-the-project .items-m-cp-selected-row-data-content').html(assistantRelationToTheProjectText);
        $('#modalCreateProjectAssistent .items-m-cp-relation-to-the-project').removeClass("hide");

    }
});

$('#modalCreateProjectAssistent #asistantIntendedMedium').on('change', function () {
    var asistantIntendedMediumValue = this.value;
    var asistantIntendedMediumText = $(this).find(":selected").text();

    var asistantIntendedMediumTextLabel = $('.items-m-cp-intended-medium .items-m-cp-selected-row-data-content').html();
    var asistantIntendedMediumTextCounter = $('#modalCreateProjectAssistent #asistantIntendedMediumCounter').val();
    var asistantIntendedMediumTextCounterSplit = asistantIntendedMediumTextCounter.split(",");
    var asistantIntendedMediumTextCounterExist = jQuery.inArray(asistantIntendedMediumValue, asistantIntendedMediumTextCounterSplit);
    var asistantIntendedMediumTextLength = asistantIntendedMediumTextCounterSplit.length;


    var comaCounter = '';
    var comaSpace = '';

    console.log(asistantIntendedMediumTextCounterExist);
    if (asistantIntendedMediumValue != "") {
        /**
         * If the value is diferrent to empty option therefore when the user choose up select option
         */
        if (asistantIntendedMediumTextLength < 2) {
            /**
             * If a new value
             */
            $('.intended-medium-project-bubble-error-content').addClass("hide");
            if (asistantIntendedMediumTextCounterExist < 0) {
                /**
                 * If the value repeat
                 */

                if (asistantIntendedMediumTextCounter != "") {
                    /**
                     * If is the first value then dont give comma
                     */
                    comaCounter = ','
                    comaSpace = ' ';
                }
                /**
                 * Value hidden
                 * */
                asistantIntendedMediumTextCounter += comaCounter + asistantIntendedMediumValue;
                $('#modalCreateProjectAssistent #asistantIntendedMediumCounter').val(asistantIntendedMediumTextCounter);


                /**
                 * Text
                 * */

                asistantIntendedMediumTextLabel += comaCounter + comaSpace + asistantIntendedMediumText;
                $('#modalCreateProjectAssistent .items-m-cp-intended-medium .items-m-cp-selected-row-data-content').html(asistantIntendedMediumTextLabel);
                $('#modalCreateProjectAssistent .items-m-cp-intended-medium').removeClass("hide");

                /**
                 * bubbles
                 */

                /**
                 * build's bubbles and content
                 *
                 */
                var bubbleLIClass = "intended-medium-project-bubble-" + asistantIntendedMediumValue;
                var bubbleDivClass = "intended-medium-project-bubble-div" + asistantIntendedMediumValue;

                var $intendedMediumProjectBubbleBuble = $("<li>", {"class": bubbleLIClass});
                var $intendedMediumProjectBubbleBubleDiv = $("<div>", {
                    "class": 'chip tag-active ' + bubbleDivClass,
                    'html': asistantIntendedMediumText
                });
                var $intendedMediumProjectBubbleBubleI = $("<i>", {
                    "class": "close material-icons",
                    "data-id": asistantIntendedMediumValue,
                    'html': 'close'
                });

                $('#intendedMediumProjectBubble').append($intendedMediumProjectBubbleBuble);
                $('.' + bubbleLIClass).append($intendedMediumProjectBubbleBubleDiv);
                $('.' + bubbleDivClass).append($intendedMediumProjectBubbleBubleI);

                $intendedMediumProjectBubbleBubleI.click(function () {
                    var asistantIntendedMediumTextCounter = $('#modalCreateProjectAssistent #asistantIntendedMediumCounter').val();
                    var asistantIntendedMediumTextCounterSplit = asistantIntendedMediumTextCounter.split(",");
                    var y = asistantIntendedMediumTextCounterSplit;
                    var removeItem = $(this).attr("data-id");

                    /**
                     * Remove value from index selected object
                     * */
                    y.splice($.inArray(removeItem, y), 1);
                    var yStringSeparteComas = y.join(",");
                    $('#modalCreateProjectAssistent #asistantIntendedMediumCounter').val(yStringSeparteComas);
                    $(this).parent("div").parent("li").remove();

                    /**
                     * Texts
                     * **/

                    var asistantIntendedMediumTextLabelAfterDrop = "";
                    $('#modalCreateProjectAssistent .items-m-cp-intended-medium .items-m-cp-selected-row-data-content').html("");

                    if (y.length > 0) {
                        $.each(y, function (index, value) {
                            console.log(value + " - " + index);
                            asistantIntendedMediumTextLabelAfterDrop += $("#modalCreateProjectAssistent #asistantIntendedMedium option[value='" + value + "']").text();
                            console.log(asistantIntendedMediumTextLabelAfterDrop);

                        });
                        $('#modalCreateProjectAssistent .items-m-cp-intended-medium .items-m-cp-selected-row-data-content').html(asistantIntendedMediumTextLabelAfterDrop);

                    }

                    if (y.length <= 2) {
                        $('.intended-medium-project-bubble-error-content').addClass("hide");
                    }
                });
            }


        } else {
            $('.intended-medium-project-bubble-error-content').removeClass("hide");
        }

    }
});

$('#modalCreateProjectAssistent #asistantIntendedMaterialType').on('change', function () {
    var assistantRelationToTheProjectValue = this.value;
    var assistantRelationToTheProjectText = $(this).find(":selected").text();

    if (assistantRelationToTheProjectValue != "") {
        $('#modalCreateProjectAssistent .items-m-cp-intended-material-type .items-m-cp-selected-row-data-content').html(assistantRelationToTheProjectText);
        $('#modalCreateProjectAssistent .items-m-cp-intended-material-type').removeClass("hide");

    }
});

$('#modalCreateProjectAssistent #asistantLenguage').on('change', function () {
    var assistantRelationToTheProjectValue = this.value;
    var assistantRelationToTheProjectText = $(this).find(":selected").text();
    if (assistantRelationToTheProjectValue != "") {
        $('#modalCreateProjectAssistent .items-m-cp-intended-lenguage-of-project .items-m-cp-selected-row-data-content').html(assistantRelationToTheProjectText);
        $('#modalCreateProjectAssistent .items-m-cp-intended-lenguage-of-project').removeClass("hide");

    }
});

/*******************
 * Validate form title
 ******************/

$("#modalCreateProjectAssistentFormName").validate({
    ignore: "not:hidden",
    rules: {
        assistantTitleProject: {
            required: true
        },
    }
});

/*******************
 * Validate form one
 ******************/

$("#modalCreateProjectAssistentFormOne").validate({
    ignore: "not:hidden",
    rules: {
        assistantRelationToTheProject: {
            required: true
        },
        asistantIntendedMediumCounter: {
            required: {
                depends: function (element) {
                    if ($("#asistantIntendedMediumCounter").val() == "") {
                        $("#asistantIntendedMedium").addClass("error");
                        return false;
                    }
                }
            },

        },
        asistantIntendedMaterialType: {
            required: true
        },
        asistantLenguage: {
            required: true
        }
    }
});

/*******************
 * Validate form two
 ******************/

$("#modalCreateProjectAssistentFormTwo").validate({
    ignore: "not:hidden",
    rules: {
        assistantLogline: {
            required: true
        },
        assistantTagline: {
            required: true
        },
        assistantSynopsis: {
            required: true
        }
    }
});

/*******************
 * Validate form three
 ******************/

$("#modalCreateProjectAssistentFormThree").validate({
    ignore: "not:hidden",
    rules: {}
});

$("#assistentNameWrite").change(function () {

});

$('#btn-create-project-tittle').on('click', function (event) {
    event.preventDefault();
    axios.post('/projects', {project_name: $('#title').val()}).then(
        response => {
            console.log(response);
        })
});

$("#btnAddEntityFour").click(function () {
    $("#addEntityFour").animate({width: '100%'});
});

$("#addWriteThreeButton").click(function () {
    $('.assistent-character-three-content').animate({opacity:0});
    $("#addWriteThree").animate({width: '100%'});
    $('#assistentNameWrite3').val('');
    $('#assistentEmailWrite3').val('');
});

$("#addCharacterButton").click(function () {
    $("#addCharacter").animate({width: '100%'});
});

$("#addPublisherThreeButton").click(function () {
    $('.assistent-character-three-content').animate({opacity:0});
    $("#addPublisherThree").animate({width: '100%'});
    $('#name_writer').val('');
    $('#mail_writer').val('');
});

$("#addAgentThreeButton").click(function () {
    $("#addAgentThree").animate({width: '100%'});
});

$("#addPlatformThreeButton").click(function () {
    $("#addPlatformThree").animate({width: '100%'});
});

$("#addVideoButton").click(function () {
    $("#addVideo").animate({width: '100%'});
});

$(".items-returns").click(function () {
    $(".assistent-content-hide").animate({width: '0%'});
});

$(".assistent-entity-four .wap-back.items-returns").click(function () {
    $(".assistent-entity-four-content-form").animate({width: '0%'});
});

$(".assistent-character-three .wap-back.items-returns").click(function () {
    $('.assistent-character-three-content').animate({opacity:1});
    $(".assistent-charanter-three-content-form").animate({width: '0%'});
});

$(".assistent-character-three .wap-back2.items-returns").click(function () {
    $(".assistent-charanter-three-content-form").animate({width: '0%'});
});

$("#btnAddVideoThree").click(function () {
    $(".assistent-vide-three-content-form").animate({width: '100%'});
});

$(".items-returns-three").click(function () {
    $(".assistent-vide-three-content-form").animate({width: '0%'});
});

/**
 * Expand and hide button enterprise info project view.
 */

$('#show-enterprise').on('click', function (event) {
    event.preventDefault();
    $(this).hide();
    $('#hide-enterprise').show();
    $('#enterprise-info').hide();

});

$('#hide-enterprise').on('click', function (event) {
    event.preventDefault();
    $(this).hide();
    $('#show-enterprise').show();
    $('#enterprise-info').show();
});


/**
 * Autocomplete Create Projects
 */

$('#assistentNameWrite3').on('keyup', function () {
    let q = $(this).val();

    if (q.length > 2) {
        axios.get('/projects/users?q=' + q +'&occupation=45').then(response => {
            let results = response.data.users;
            if (results.length > 0) {
                let template_results = _.template($('#item-autocomplete').html());
                let template_rendered = $(template_results({results: results, className:'writers-result'}));
                $('#writers-results').html(template_rendered);
                $('#writers-results').css('display', 'block');

                $('.writers-result').on('click', function () {
                    writer = {name: $(this).attr('data-name'), email: $(this).attr('data-mail')};
                    $('#assistentNameWrite3').val(writer.name);
                    $('#assistentEmailWrite3').val(writer.email);
                    $('#writers-results').css('display', 'none');
                });
            }
        });
    } else {
        $('#writers-results').css('display', 'none');
        writer = {};
    }
});


function renderWritersList(container, items, className)
{
    let template_results = _.template($('#template-tag-list').html());
    let template_rendered = $(template_results({writers: items, className: className}));
    $(container).html(template_rendered);

    $(className).on('click', function () {
        items.splice($(this).attr('data-index'), 1);
    });

}


$('#btn-add-writer').on('click', function (event) {
    event.preventDefault();

    if(writer.name)
    {
        writers.push(writer);
        $(".assistent-charanter-three-content-form").animate({width: '0%'});
        $('.assistent-character-three-content').animate({opacity:1});
        renderWritersList('#add-writers-list', writers, 'writer-items-list');
    }else{
        return false;
    }
});


$('#name_writer').on('keyup', function () {
    let q = $(this).val();

    if (q.length > 2) {
        axios.get('/projects/users?q=' + q).then(response => {
            let results = response.data.users;
            if (results.length > 0) {
                let template_results = _.template($('#item-autocomplete').html());
                let template_rendered = $(template_results({results: results, className:'publishers-result'}));
                $('#publishers-results').html(template_rendered);
                $('#publishers-results').css('display', 'block');

                $('.publishers-result').on('click', function () {
                    publisher = {name: $(this).attr('data-name'), email: $(this).attr('data-mail')};
                    $('#name_writer').val(publisher.name);
                    $('#mail_writer').val(publisher.email);
                    $('#publishers-results').css('display', 'none');
                });
            }
        });
    } else {
        $('#writers-results').css('display', 'none');
        writer = {};
    }
});

$('#btn-add-publisher').on('click', function (event) {
   event.preventDefault();
    if(publisher.name)
    {
        publishers.push(publisher);
        $(".assistent-charanter-three-content-form").animate({width: '0%'});
        $('.assistent-character-three-content').animate({opacity:1});
        renderWritersList('#add-publishers-list', publishers, 'publisher-items-list');
    }else{
        return false;
    }
});



