
$(document).ready(function () {
    var sendNotificationAddProfile = function(id) {
        var baseUrl = window.location.protocol + "//" + window.location.host + "/"
        var url = baseUrl + 'notification/notification-add-profile/'+id;
        $.get( url)
            .done(function( data ) {
                console.log( "Data Loaded: " + data );
            });
    };

    $(".btn-friend.botom-net-f-p").click(function () {
        var id = $(this).attr("data-id");
        sendNotificationAddProfile(id);

    });

    $('#country').trigger('change');
});



const _protocolo = window.location;
const _prot = _protocolo.protocol;
const host = location.hostname;

let elementVideo = document.getElementById('conte-videos-profiles');
if(elementVideo){
    let linkVideo = elementVideo.getElementsByClassName('view-thmbil-video');
    $(linkVideo).each(function (i, elem) {
        let codeVideo = $(this).data('link');
        let youtube_video_id = (codeVideo.match(/youtube\.com.*(\?v=|\/embed\/)(.{11})/))? codeVideo.match(/youtube\.com.*(\?v=|\/embed\/)(.{11})/).pop(): /vimeo.*\/(\d+)/i.exec(codeVideo);
        console.log("....");
        console.log(youtube_video_id);

        if(youtube_video_id != null ){
            if (youtube_video_id.length == 11) {
                let video_thumbnail = $('<img src="//img.youtube.com/vi/'+youtube_video_id+'/0.jpg">');
                // $('.view-thumbli-images').append(video_thumbnail);
                $(this).find('.view-thumbli-images').append(video_thumbnail);
            }
        }
    });
}
$("body").on("keyup","#mail",function(){
    $(".block-hide").removeClass('hide');
  })
$("body").on("click",'.items-vidio',e=>{
    let code = $(e.target).parents('.view-thmbil-video').data('link');
    let youtube_video_id = code.match(/youtube\.com.*(\?v=|\/embed\/)(.{11})/).pop();
    let viewModalVideo = window.mainModal({type:'video',body:youtube_video_id });
    $('body').append(viewModalVideo);
})
$("body").on("click",".open-overlay",e=>{
    $('.link-photo-drop').removeClass('currentImages');
    $(e.target).siblings('a').addClass('currentImages');
   let ima = $(e.target).siblings('a').attr('href');
   let containerImage = document.getElementById('cards-photo');
   let thumImage = containerImage.getElementsByClassName('image-thumbil-view');

   $('body').append(window.mainModal({type:'images',body:thumImage}));
})
$("body").on("click", '.arrow-left', e=>{
	plusSlides(-1);
});
$("body").on("click", '.arrow-right', e=>{
	plusSlides(1);
});
$("body").on("click",".btn-friend",e=>{
    e.preventDefault();
    let type = e.currentTarget.dataset.type;
    let idFriend = e.currentTarget.dataset.friends;
    $(e.currentTarget).attr("data-type",'Following');
    if(type == 'Follow'){
        $(e.currentTarget).find("span").html("Added");
        $(e.currentTarget).find("i").removeClass('icon-icono-profile3').addClass('icon-icono-profile4');
        $(e.currentTarget).removeClass('botom-net-f-p').addClass('botom-net-fw-p');
    }else{
        $(e.currentTarget).find("span").html("Add");
        $(e.currentTarget).find("i").removeClass('icon-icono-profile4').addClass('icon-icono-profile3');
        $(e.currentTarget).removeClass('botom-net-fw-p').addClass(' botom-net-f-p');
    }
    /**
     * ajax idFriend = id user
     */
})
function addTagRegister(_text, _id) {

    let valid = true;

    $('.tag-active ').each(function () {
        if ($(this).attr('data-id') == _id) {
            valid = false;
        }
    });

    if (valid) {
        let _li = '<li class="chip tag-active tag-icon" data-id="' + _id + '">' + _text + '<i class="close material-icons close-icons" data-type="' + _text + '">close</i></li>';
        $('.list-chip-m-cp').append(_li);
    }
}

function addTagLanguage(_text, _id){
    let valid = true;

    $('.tag-active ').each(function () {
        if ($(this).attr('data-id') == _id) {
            valid = false;
        }
    });

    if (valid) {
        let _li = '<li class="chip tag-active tag-icon" data-id="' + _id + '">' + _text + '<i class="close material-icons close-icons" data-type="' + _text + '">close</i></li>';
        $('.list-chip-m-languages').append(_li);
    }
}

function createObjectURL(file) {
    if (window.webkitURL) {
        return window.webkitURL.createObjectURL(file);
    } else if (window.URL && window.URL.createObjectURL) {
        return window.URL.createObjectURL(file);
    } else {
        return null;
    }
}

function _defineProperty(obj, key, value) {
    if (key in obj) {
        Object.defineProperty(obj, key, {value: value, enumerable: true, configurable: true, writable: true});
    } else {
        obj[key] = value;
    }
    return obj;
}

$('#occupations').on('change', function (event) {
    event.preventDefault();
    let optionSelected = $("option:selected", this);

    if ($(this).val() > 0) {
        let itemOcp = $("#prof-ocup").find("li").length;
        console.log(itemOcp);
        if(itemOcp <= 2){
            addTagRegister(optionSelected.html(), $(this).val());
        }else{
            $("#li-error-msj").remove();
            let _err = addError('You can choose up to 3');
            $('.list-chip-m-cp').append(_err);
            setTimeout(function () {
                $("#li-error-msj").remove();
            }, 3000);
            return false;  
        }
        
    }
});

$('#languages').on('change', function (event) {
    event.preventDefault();
    let optionSelected = $("option:selected", this);

    if ($(this).val() > 0) {
        let items = $(".list-chip-m-languages").find('li').length;
        console.log(items);
        if( items <= 2){
            addTagLanguage(optionSelected.html(), $(this).val());
        }else{
            $("#li-error-msj").remove();
            let _err = addError('You can choose up to 3');
            $('.list-chip-m-languages').append(_err);
            setTimeout(function () {
                $("#li-error-msj").remove();
            }, 3000);
            return false;
        }
       
    }
});
function addError(text) {
    return `
        <li id="li-error-msj">
            <div class="chip">${text}</div>
        </li>
    `;
}
$('#save-profile').on('click', function (event) {
    event.preventDefault();

    $('#list-languages').val(null);
    $('#list-occupations').val(null);

    let occupations = [];
    let languages = [];


    $('.list-chip-m-cp > .tag-active ').each(function () {
        occupations.push($(this).attr('data-id'));
    });

    $('.list-chip-m-languages > .tag-active ').each(function () {
        languages.push($(this).attr('data-id'));
    });


    $('#list-languages').val(languages.join(','));
    $('#list-occupations').val(occupations.join(','));

    $('#form-profile').submit();

});

$('#save-bio').on('click', function (event) {
    event.preventDefault();

    $('#form-bio').submit();

});

$('#save-location').on('click', function (event) {
    event.preventDefault();

    $('#form-location').submit();

});





$('.profile-link-upload').on('click', function (event) {
    event.preventDefault();
    $('#profile-avatar').click();
});

$('#profile-avatar').on('change', function () {
    let file = this.files[0];
    let image_src = createObjectURL(file);

    $('.overlay-main').show();
    $('.over-light').show();

    $('.image-editor').cropit('previewSize', {width: 800, height: 800});

    $('.image-editor').cropit({
        exportZoom: .25,
        allowDragNDrop: false,
        imageBackground: true,
        imageBackgroundBorderWidth: 20,
        imageState: {
            src: image_src
        }
    });

});

$('#_cl-upload-avatar-profile').on('click', function (event) {
    event.preventDefault();
    $('.overlay-main').hide();
    $('.over-light').hide();
    $('#profile-avatar').val(null);
    $('.image-editor').cropit('destroy');
    $('.cropit-preview').html('');
});

$('#upload-avatar-profile').on('click', function (event) {
    event.preventDefault();
    let _$$cropit;
    let imageData = $('.image-editor').cropit('export', (_$$cropit = {
        type: 'image/jpeg'
    }, _defineProperty(_$$cropit, 'type', 'image/jpg'), _defineProperty(_$$cropit, 'type', 'image/gif'), _defineProperty(_$$cropit, 'type', 'image/png'), _defineProperty(_$$cropit, 'quality', .9), _defineProperty(_$$cropit, 'originalSize', true), _$$cropit));


    axios.post('/profile-avatar', {
        image: imageData
    }).then(response =>{
        if(response.data.success)
        {
            $('.overlay-main').hide();
            $('.over-light').hide();
            $('#profile-avatar').val(null);
            $('.image-editor').cropit('destroy');
            $('.cropit-preview').html('');
            $('#profile-avatar-src').attr('src', imageData);
        }
    });
});

$(document).ready(function () {
    if(window.open_collapsible > 0)
    {
        $('#profile-collapsible').collapsible('open', window.open_collapsible );
    }

    if(window.errors_credit)
    {
        $('#form-save-credit').show();
        $('#add-credit-container').hide();
        $('#list-credits-profile').hide();
    }

    if(window.errors_award)
    {
        $('#form-save-award').show();
        $('#add-award-container').hide();
        $('#list-awards-profile').hide();
    }

    if(window.errors_video)
    {
        $('#form-save-video').show();
        $('#add-video-container').hide();
        $('#list-videos-profile').hide();
    }
});

$('#add-credit').on('click', function (event) {
    event.preventDefault();
    $('#form-save-credit').show();
    $('#add-credit-container').hide();
    $('#list-credits-profile').hide();

});

$('#cancel-add-credit').on('click', function (event) {
    event.preventDefault();
    $('#form-save-credit').hide();
    $('#add-credit-container').show();
    $('#list-credits-profile').show();
    $("#form-save-credit").trigger('reset');
});


$('.update-credit').on('click', function (event) {

    event.preventDefault();

    let credit_id = $(this).attr('data-id');

    window.list_credits.map(credit =>{
        if(credit.id == credit_id){
            $('#form-save-credit').show();
            $('#add-credit-container').hide();
            $('#list-credits-profile').hide();

            $('#form-save-credit').attr('action', '/profile-update-credit/'+credit.id);

            $('#credit_id').val(credit.id);
            $('#title').val(credit.title);
            $('#company-credit').val(credit.company);
            $('#position').val(credit.position);
            $('#year').val(credit.year);
        }
    });

});

$('.delete-credit').on('click', function (event) {
    event.preventDefault();

    let credit_id = $(this).attr('data-id');
    let li = $(this).parent('li');

    window.list_credits.map(credit =>{
        if(credit.id == credit_id){
            axios.delete('/profile-delete-credit/'+credit.id).then(response=>{
               if(response.data.success){
                   li.remove();
               }
            });
        }
    });

});

$('#add-award').on('click', function (event) {
    event.preventDefault();
    $('#form-save-award').show();
    $('#add-award-container').hide();
    $('#list-awards-profile').hide();

});

$('#cancel-add-award').on('click', function (event) {
    event.preventDefault();
    $('#form-save-award').hide();
    $('#add-award-container').show();
    $('#list-awards-profile').show();
    $("#form-save-award").trigger('reset');
});

$('.update-award').on('click', function (event) {

    event.preventDefault();

    let award_id = $(this).attr('data-id');

    window.list_awards.map(award =>{
        if(award.id == award_id){
            $('#form-save-award').show();
            $('#add-award-container').hide();
            $('#list-awards-profile').hide();

            $('#form-save-award').attr('action', '/profile-update-award/'+award.id);

            $('#award_id').val(award.id);
            $('#title_award').val(award.title_award);
            $('#year_award').val(award.year_award);
            $('#nominate_award').val(award.nominate_award);
            $('#category_award').val(award.category_award);
            $('#result_award').val(award.result_award);
            $('#description_award').val(award.description_award);
        }
    });

});

$('.delete-award').on('click', function (event) {
    event.preventDefault();

    let award_id = $(this).attr('data-id');
    let li = $(this).parent('li');

    window.list_awards.map(award =>{
        if(award.id == award_id){
            axios.delete('/profile-delete-award/'+award.id).then(response=>{
               if(response.data.success){
                   li.remove();
               }
            });
        }
    });

});

$('#add-video').on('click', function (event) {
    event.preventDefault();
    $('#form-save-video').show();
    $('#add-video-container').hide();
    $('#list-videos-profile').hide();

});

$('#cancel-add-video').on('click', function (event) {
    event.preventDefault();
    $('#form-save-video').hide();
    $('#add-video-container').show();
    $('#list-videos-profile').show();
    $("#form-save-video").trigger('reset');
});

$('.update-video').on('click', function (event) {

    event.preventDefault();

    let video_id = $(this).attr('data-id');

    window.list_videos.map(video =>{
        if(video.id == video_id){
            $('#form-save-video').show();
            $('#add-video-container').hide();
            $('#list-videos-profile').hide();

            $('#form-save-video').attr('action', '/profile-update-video/'+video.id);

            $('#video_id').val(video.id);
            $('#title-video').val(video.title);
            $('#link-video').val(video.link);

        }
    });

});

$('.delete-video').on('click', function (event) {
    event.preventDefault();

    let video_id = $(this).attr('data-id');
    let li = $(this).parent('li');

    window.list_videos.map(video =>{
        if(video.id == video_id){
            axios.delete('/profile-delete-video/'+award.id).then(response=>{
               if(response.data.success){
                   li.remove();
               }
            });
        }
    });

});

/**
 * visualizacion de los tabs
*/
$('body').on('click','.tabs-btn',e=>{
    let item = e.currentTarget.dataset.id;
    $('.tabs-btn').removeClass('tab-active');
    $(e.target).addClass('tab-active');
    $('.tabcontent-info-profile').hide();
    $(`#${item}`).removeClass('hide');
    $(`#${item}`).show();
})

function validatorTags(id) {
    var bool = true;
    $('.msj-err').remove();
    var err = "\n\t\t<div class=\"msj-err\">\n\t\t\t<div class=\"error\">\n\t\t\t  <i class=\"material-icons\">error</i>\n\t\t\t  <span class=\"font-error\">You can't leave this empty.</span>\n\t\t\t</div>\n\t\t</div>\n\t";
    //$('a').bind('click', false);
    $("#" + id).find('.required').each(function (i, el) {
        if ($(el).val() == '' || $(el).val() == 0 || $(el)[0].value == 0 || $(el)[0].value == '') {
            //	$(el).css({'border':'1px solid #ff0000 '});
            //	$(el).parents('.marg-input').append(err);
            //	bool = false;
        }
    });
}
function openHidden() {
    var tabcontent = void 0;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (var i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
}
$('.tabs-profile').on('click', function () {
    var _id = $(this).data('id');
    var _siblins = $(this).siblings('button.active2');
    var _ids = _siblins[0].dataset.id;
    var vld = validatorTags(_ids);
    openHidden();
    $("#" + _id).show();
    $('.tabs-profile').removeClass('active2');
    $(this).addClass('active2');
});

/**
 * eliminar foto
 */
$('body').on('click','.link-remove',e=>{
    let id = e.currentTarget.dataset.id;
    /**
     * funcion ajax para eliminar la imagen
     */
    window.modalRemove();
})

$('body').on('click','.close-apl,.icon-close-evnt,.close-pl',e=>{
    window.modalRemove();
})

$('body').on('click','.delete-images',e=>{
   // let _modal = mainModal(['Are you sure want to delete this imagen?','that image will be permanently deleted.'],'Delete photo');
    let _modal = window.mainModal(
        {
            title:'Delete photo',
            body:['Are you sure want to delete this imagen?','that image will be permanently deleted.']
        }
    );
   
   //optener el id de la imagen
   let id = e.currentTarget.dataset.cod;

    $('body').append(_modal);

    //agrega atributo al boton del modal
    $('#btn-modal-ok').attr('data-id',id);
    
});

/**
 * 
 * @param {recive el titulo del modal} text 
 */
const modalTitle= (text = 'title modal')=>{
    return `<div class="wapper-modal-main-content-title">
                <i class="material-icons">delete</i>
                <span class="font-part1">${text}</span>
                <div class="icon-close-evnt">
                    <i class="material-icons close-apl">close</i>
                </div>
            </div>`;
}

/**
 * 
 * @param {params} options 
 */
const modalContent =(options) =>{
    
    var settings = $.extend({
            type:'info'
        ,   title:''
        ,   body:''
        ,   typeVideo:'youtube'
    },options);

    var mainBody = '';
    if(settings.type == 'info'){
        mainBody = `
            <div class="wapper-modal-main-content">
                ${modalTitle(settings.title)}
                <div class="wapper-modal-main-content-body">
                    ${settings.body.map(function(e){
                        return  `<p>${e}</p>`
                    }).join("")}
                </div>
                ${modalFooter()}
            </div>
        `;
    }
    if(settings.type == 'video'){
        mainBody =  overVideo(settings.body,settings.typeVideo);
    }
    if(settings.type == 'images'){
        var uri = '';
        $(settings.body).each(function (i, elem) {
            let currentClass = ($(this).find('a').hasClass('currentImages')) ? 'style="display:block"':'style="display:none"';
            uri += `<li class="list-images-p current" ${currentClass}><img src="${$(this).find('a').attr('href')}"></li>`;
        });
        let _arrow =`<div class="arrow">
                        <div class="arrow-left arrow-images"><i class="material-icons">keyboard_arrow_left</i></div>
                        <div class="arrow-right arrow-images"><i class="material-icons">keyboard_arrow_right</i></div>
                    </div>`;
        let _list = `<ul class="list-photo">${uri}</ul>`;
        mainBody = modalCont(_list,_arrow);
    }
    return mainBody;
}
/**
 * 
 * @param {codigo de video} video 
 * @param {youtube / vimeo} type 
 */
const modalVideo =(video,type)=>{
    return `
        <div class="wapper-modal-main-content">
            ${overVideo(video,type)}
        </div>
    `;
}
/**
 * componente footer del modal
 */
const modalFooter = ()=>{
    return `
        <div class="wapper-modal-main-content-footer">
            <button class="btn-send-1 link-remove" id="btn-modal-ok">Delete</button>
            <button class="btn-cancel-1 close-apl" id="btn-modal-close">Cancel</button>
        </div>
    `;
}

/**
 * 
 * @param {options params} options 
 */
window.mainModal = (options)=>{
    return `<div class="wapper-modal-main">${modalContent(options)}</div>`;
}

/**
 * 
 * @param {codigo / id video} video 
 * @param {youtube / vimeo} type 
 */
const overVideo =(video,type) =>{
    let html = '';
	if(type == 'youtube'){
        html = `<iframe 
                    width="900"
                    height="600"
                    class='iframe-vidio'
                    id='frame-video' 
                    type="text/html"
                    src="https://www.youtube.com/embed/${video}?rel=0&amp;autoplay=1&amp;controls=0&amp;showinfo=0&fs=0&modestbranding=1&color=white" 
                    frameborder="0" 
                    allowfullscreen>
                </iframe>`;
	} else {
        html = `<iframe 
                    width="600" 
                    height="600" 
                    class='iframe-vidio' 
                    id='frame-video' 
                    src="https://player.vimeo.com/video/${video}?autoplay=1&loop=1&autopause=0" 
                    frameborder="0" 
                    webkitallowfullscreen 
                    mozallowfullscreen 
                    allowfullscreen>
                </iframe>`;
    }
    return modalCont(html);
}

/**
 * @param {contenido solo para videos e imagenes}
 * @param {contenido} html 
 * @param {flechas right, left} arrow 
 */

const modalCont =(html,arrow = '')=>{
    return `
        <span class="icon-icono-form25 close-pl"></span>
        ${arrow}
        <div class="overlay-body">
            <div class="overlay-body-cont">
                <div class="body-main">
                    <img class="loader-modal" src="${_prot}//${host}/images/ajax-loader.gif" >
                    ${html}
                </div>
            </div>
        </div>
    `;
}
/**
 * remover modal
 */
window.modalRemove =()=>{
    $(".wapper-modal-main").remove();
}

var slideIndex = 1;
/**
 * 
 * @param {default 1} n 
 */
const plusSlides =(n)=> {
	showSlides(slideIndex += n);
}
/**
 * 
 * @param {value 1,-1} n 
 */
const currentSlide =(n)=> {
	showSlides(slideIndex = n);
}

const showSlides = (n = 1) => {
	var i;
	var slides = document.getElementsByClassName("list-images-p");
	if (n > slides.length) {
		slideIndex = 1;
	}
	if (n < 1) {
		slideIndex = slides.length;
	}
	for (i = 0; i < slides.length; i++) {
		slides[i].style.display = "none";
	}
	if (slides[slideIndex - 1]) {
		slides[slideIndex - 1].style.display = "block";
	}
}