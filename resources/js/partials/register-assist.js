function _defineProperty(obj, key, value) {
    if (key in obj) {
        Object.defineProperty(obj, key, {value: value, enumerable: true, configurable: true, writable: true});
    } else {
        obj[key] = value;
    }
    return obj;
}

function _validationPass() {
    var pw1 = $("#pw1").val();
    var pw2 = $("#pw2").val();
    var ref = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,100}$/;

    if (pw1 == pw2) {
        if (ref.test(pw1)) {
            return true;
        } else {
            return 'Passwords must contain: a minimum of 8 or more characters, 1 uppercase & lowercase letters and 1 number.';
        }
    }

    return 'This password did not match';
}

function addTagRegister(_text, _id) {

    let valid = true;

    $('.tag-active ').each(function () {
        if ($(this).attr('data-id') == _id) {
            valid = false;
        }
    });

    if (valid) {
        let _li = '<li class="chip tag-active tag-icon" data-id="' + _id + '">' + _text + '<i class="close material-icons close-icons" data-type="' + _text + '">close</i></li>';
        $('.list-chip-m-cp').append(_li);
    }
}

function removeTagRegister(_id) {
    $('.tag-active ').each(function () {
        if ($(this).attr('data-id') == _id) {
            $(this).remove();
        }
    });
}

function readerFile(file, callback) {
    let reader = new FileReader();
    reader.readAsDataURL(file);

    reader.onload = callback;

}

function createObjectURL(file) {
    if (window.webkitURL) {
        return window.webkitURL.createObjectURL(file);
    } else if (window.URL && window.URL.createObjectURL) {
        return window.URL.createObjectURL(file);
    } else {
        return null;
    }
}

$('._sin-here').on('click', (event)=>{
    event.preventDefault();
    $('#register-modal-1').modal('open');
});


$('#close-register-modal-1').on('click', (event) => {
    event.preventDefault();
    $('#register-modal-1').modal('close');
});

$('#open-register-modal-2').on('click', (event) => {
    event.preventDefault();
    $('#register-modal-1').modal('close');
    $('#register-modal-2').modal('open');
});

$('#open-register-modal-3').on('click', (event) => {
    event.preventDefault();
    let error = false;
    $('#name').removeClass('input-error-popup');
    $('#last').removeClass('input-error-popup');

    if ($('#name').val() == '' || $('#name').val() == 0) {
        $('#name').addClass('input-error-popup');
        $(".vd-alert-tx-firstName").show();
        error = true;
    }

    if ($('#last').val() == '' || $('#last').val() == 0) {
        $('#last').addClass('input-error-popup');
        $(".vd-alert-tx-lastName").show();
        error = true;
    }

    if (error) {
        return false;
    }

    $('#register-modal-2').modal('close');
    $('#register-modal-3').modal('open');

    $('#full_name').val($('#name').val());
    $('#last_name').val($('#last').val());
});

$('#open-register-modal-4').on('click', (event) => {
    event.preventDefault();
    let error = false;
    let email = $('#mail').val();
    let emailVerif = $("#mail-verif").val();
    $('#mail').removeClass('input-error-popup');
    $('.email-ok').hide();
    $('.vd-alert-tx-email').hide();

    if (email == '' || email == 0) {
        $('#mail').addClass('input-error-popup');
        $(".vd-alert-tx-email").show();
        error = true;
    }
    if (emailVerif == '' || emailVerif == 0) {
        $('#mail').addClass('input-error-popup');
        $(".vd-alert-tx-email").show();
        error = true;
    }
    if(email != emailVerif){
        $(".vd-alert-tx-email").html('the emails do not match');
        $(".required").addClass('input-error-popup');
        setTimeout(function () {
            $(".required").removeClass('input-error-popup');
            $('.vd-alert-tx').html('');
           // $('.requiered-f-1').removeClass('hide');
        }, 5000);
        error = true;
      }
    if (error) {
        return false;
    }

    axios.get('/validate-email?email=' + email).then((response) => {
        console.log(response);
        if (response.data.valid) {
            $('#register-modal-3').modal('close');
            $('#register-modal-4').modal('open');
            $('#email').val(email);
        } else {
            $('.email-ok').show();
        }
    });
});

$('body').on('click', '.cls-hiden', function () {
    $(this).addClass('cls-view').removeClass('cls-hiden');
    $(this).find('i').html('visibility_off');
    $(this).siblings('input').get(0).type = 'password';
});

$('body').on('click', '.cls-view', function () {
    var inpt = $(this).siblings('input').val().length;
    if (inpt > 0) {
        $(this).addClass('cls-hiden').removeClass('cls-view');
        $(this).find('i').html('visibility');
        $(this).siblings('input').get(0).type = 'text';
    }
});

$('#open-register-modal-5').on('click', (event) => {
    event.preventDefault();
    let valid_password = _validationPass();
    $('.pasword-ok ').hide();

    if (valid_password != true) {
        $('#error-text-password').html(valid_password);
        $('.pasword-ok ').show();

        return false;
    }

    $('#register-modal-4').modal('close');
    $('#register-modal-5').modal('open');
    $('#password').val($('#pw1').val());

});

$('#open-register-modal-6').on('click', (event) => {
    event.preventDefault();

    $('#register-modal-5').modal('close');
    $('#register-modal-6').modal('open');

});

$('#open-register-modal-7').on('click', (event) => {
    event.preventDefault();

    let error = false;
    let country = $('#country').val();

    if (country == '' || country == 0) {
        $('#country').addClass('input-error-popup');
        error = true;
    }

    if (error) {
        return false;
    }

    $('#register-modal-6').modal('close');
    $('#register-modal-7').modal('open');
    $('#country_id').val(country);
    $('#state').val($('#state_m').val());
    $('#city').val($('#city_m').val());

});


$('#btn-writer').on('click', (event) => {
    event.preventDefault();

    let img = $('#ima-writer');
    let img_p = $('#ima-producer');

    let active = JSON.parse($('#btn-writer').attr('data-active'));
    let id = $('#btn-writer').attr('data-id');

    if (!active) {
        img.attr('src', '/images/write-2.svg');
        img_p.attr('src', '/images/producer-1.svg');
        addTagRegister('Writer', id);
        removeTagRegister(80);
    } else {
        img.attr('src', '/images/write-1.svg');
        removeTagRegister(id);

    }

    $('#btn-writer').attr('data-active', !active);

});

$('#btn-producer').on('click', (event) => {
    event.preventDefault();

    let img_w = $('#ima-writer');
    let img = $('#ima-producer');

    let active = JSON.parse($('#btn-producer').attr('data-active'));
    let id = $('#btn-producer').attr('data-id');

    if (!active) {
        img.attr('src', '/images/producer-2.svg');
        img_w.attr('src', '/images/write-1.svg');
        addTagRegister('Producer', id);
        removeTagRegister(45);

    } else {
        img.attr('src', '/images/producer-1.svg');
        removeTagRegister(id);

    }

    $('#btn-producer').attr('data-active', !active);

});

$('#cm-prof').on('change', function () {
    let optionSelected = $("option:selected", this);

    if ($(this).val() > 0) {
        addTagRegister(optionSelected.html(), $(this).val());
    }
});

$('#country').on('change', function () {
    $('#state').html('<option selected="selected" value="">State/Province</option>');
    axios.get('/lists/states?country-id=' + $(this).val()).then(response => {
        if (response.data.success) {
            response.data.data.map(state => {
                $('#state_m').append('<option value="' + state.id + '">' + state.local_name + '</option>');

                if(window.state_value)
                {
                    $('#state_m').val(window.state_value);
                }
            })
        }
    });
});

$('.unplad-foto').on('click', function () {
    $('#fileImage').click();
});


$('#fileImage').on('change', function () {
    let file = this.files[0];

    let image_src = createObjectURL(file);


    $('.overlay-main').show();
    $('.over-light').show();

    $('.image-editor').cropit('previewSize', {width: 800, height: 800});

    $('.image-editor').cropit({
        exportZoom: .25,
        allowDragNDrop: false,
        imageBackground: true,
        imageBackgroundBorderWidth: 20,
        imageState: {
            src: image_src
        }
    });

});


$('#bnt-avatar-profile').on('click', function (event) {
    event.preventDefault();
    let _$$cropit;
    let imageData = $('.image-editor').cropit('export', (_$$cropit = {
        type: 'image/jpeg'
    }, _defineProperty(_$$cropit, 'type', 'image/jpg'), _defineProperty(_$$cropit, 'type', 'image/gif'), _defineProperty(_$$cropit, 'type', 'image/png'), _defineProperty(_$$cropit, 'quality', .9), _defineProperty(_$$cropit, 'originalSize', true), _$$cropit));


    $('.overlay-main').hide();
    $('.over-light').hide();

    $('#avatar-ima-g').attr('src', imageData);
    $('#avatar-ima-g').attr('data-src', imageData);
    $('#image').val(imageData);
    $("#avatar-ima-g").removeAttr('style');
    $("#uplad-avatar").addClass('unplad-foto-hover');

    $('#fileImage').val(null);
    $('.image-editor').cropit('destroy');
    $('.cropit-preview').html('');

});

$('#_cl-avatar-profile').on('click', function (event) {
    event.preventDefault();
    $('.overlay-main').hide();
    $('.over-light').hide();
    $('#fileImage').val(null);
    $('.image-editor').cropit('destroy');
    $('.cropit-preview').html('');
});

$('#register-send').on('click', function (event) {
    event.preventDefault();
    let occupations = [];
    $('.tag-active ').each(function () {
        occupations.push($(this).attr('data-id'));
    });

    $('#profile-occupations').val(occupations.join(','));

    $('#form-register-assist').submit();
});