window.parameters = {};
let page = 1;
let next_page = window.nextPage;

function search() {

    let data = {};
    data['page'] = page;

    _.each(window.parameters, function (item, key) {
        data[key] = item;
    });

    axios.post('/search', data).then(

        response => {
            console.log(response.data.data);
        _.each(response.data.data, function (id) {
            console.log(id);
            if(id.photo.length == 0)
            {
                if(id.facebook_photo.length == 0)
                {
                    id.photo = '/images/avatar_default_user.png';
                }else{
                    id.photo = id.facebook_photo;
                }
            }
        });

        let template = _.template($('#project-template').html());
        let rendered = $(template({projects: response.data.data}));

        if(response.data.last_page > page) {
            next_page = page + 1;
        } else {
            next_page = 0;
        }



        if(page == 1)
        {
            $('.limit-area-result').html(rendered);
        }else{
            $('.limit-area-result').append(rendered);
        }

        var messageEmpty = _.template($('#message-empty').html());
        if(response.data.data.length == 0) {

            $('.limit-area-result').html(messageEmpty);
        }

        renderImages();
        setUrl();

    });
}

function clear() {
    let parameters = window.parameters;
    _.each(parameters, function (item, key) {
        $('.'+key).prop('checked', false);
        delete window.parameters[key];
    });

    $('.material-icons').css('display', 'none');

    $('.txt-on-newsearch').val('');
    delete window.parameters['q'];

    search();

    $('.btn-clear-search').removeClass('show-el');
}

function setUrl() {
    var query_string = '';
    var iteration = 0;

    _.each(window.parameters, function (item, key) {
        if (iteration > 0) {
            query_string += '&';
        }
        query_string += key + '=' + item;
        iteration++;
    });

    if (query_string) {
        window.history.replaceState({}, '', location.pathname + '?' + query_string);
    } else {
        window.history.replaceState({}, '', location.pathname);
    }

    if(iteration > 0)
    {
        $('.btn-clear-search').addClass('show-el');
    }

}

if(window.request_param)
{
    _.each(window.request_param, function (item, key) {
        if(key != 'user')
        {
            $( "."+key ).each(function( index ) {
                if($(this).val() == item)
                {
                    $(this).prop('checked', true);
                    window.parameters[key] = item;
                    $(this).siblings('span').children('i').css('display', 'block');
                }
            });
        }
    });

    if(window.request_param['q'])
    {
        window.parameters['q'] = window.request_param['q'];
    }

    setUrl();
}

function changePage() {
    if ($('#break-next-page').offset() && $(window).scrollTop() > $("#break-next-page").offset().top - $(window).height() * 1.5) {
        if (next_page > 0) {
            page = next_page;
            next_page = 0;
            search();
        }
    }
}


$('.dropdown-toggle').on('click', function () {

    if(!$(this).hasClass('active')){
        $('.button-dropdown a').removeClass('active');
        $('.dropdown-menu').hide();
        $(this).addClass('active');
        $(this).siblings('.dropdown-menu').show();
    }else{
       $('.button-dropdown a').removeClass('active');
       $(this).siblings('.dropdown-menu').hide();
    }
});


$('.container-items').on('click', function (event) {
    event.preventDefault();
    $(this).siblings('.container-items').children('span').children('i').css('display', 'none');
    if($(this).children('input').prop('checked'))
    {
        $(this).children('input').prop('checked', false);
        $(this).children('span').children('i').css('display', 'none');
        delete window.parameters[$(this).attr('data-class')];
    }else{
        $(this).children('span').children('i').css('display', 'block');
        $(this).children('input').prop('checked', true);
        window.parameters[$(this).attr('data-class')] = $(this).children('input').val();
    }

    $(this).children('input').trigger('change');

});


$('.txt-on-newsearch').on('change', function () {
  if($(this).val().length > 2)
  {
      window.parameters['q'] = $(this).val();
      page = 1;
      $('.btn-clear-search').show();
      search();
  }else{

      if(window.parameters['q'])
      {
          delete window.parameters['q'];
          page = 1;
          search();
      }
  }
});

$('.txt-on-newsearch').on('click', function () {
    $('.dropdown-menu').hide();
    $('.dropdown-toggle').removeClass('active');
});

$(".box-click").on('change', function () {
    page = 1;
    search();
});

$(document).click(function(event) {
    if(!$(event.target).closest('.main-search-style').length) {
        if($('.main-search-style').is(":visible")) {
            $('.dropdown-menu').hide();
            $('.dropdown-toggle').removeClass('active');
        }
    }
});

$(window).scroll(function () {
    changePage();
});

$('.btn-clear-search').on('click', function () {
   clear();
});

$(".txt-on-newsearch").keypress(function(e) {
    if(e.which == 13) {
        event.preventDefault();
        event.stopPropagation();
        $(".txt-on-newsearch").change();
    }
});