const data =[
    {
        'title':'Beating Windward Press',
        'location' : 'San Francisco, California',
        'description' : 'Books for Books’ Sake. Beating Windward Press is an independent publisher of novels, short story collections.',
        'image':'https://res.cloudinary.com/storyrocket-llc/image/fetch/dpr_1,f_auto/https://www.storyrocket.com/public/images/beating-windward-press/thumb_banner-59134e84a06ab.jpg',
        'linkGroup':'https://www.storyrocket.com/groups/beating-windward-press',
        'author':'Beating Windward Press',
        'linkAuthor':'https://www.storyrocket.com/Matthew.fkrekgdw',
        'members':[
            {
                'name':'Beating Windward Press',
                'link':'https://www.storyrocket.com/Matthew.fkrekgdw',
                'avatar':'https://res.cloudinary.com/storyrocket-llc/image/fetch/dpr_1,f_auto/https://storyrocket-aws3.s3.amazonaws.com/2110ad32.jpg'
            },
            {
                'name':'Beating Windward Press',
                'link':'https://www.storyrocket.com/Matthew.fkrekgdw',
                'avatar':'https://res.cloudinary.com/storyrocket-llc/image/fetch/dpr_1,f_auto/https://storyrocket-aws3.s3.amazonaws.com/2110ad32.jpg'
            },
            {
                'name':'Beating Windward Press',
                'link':'https://www.storyrocket.com/Matthew.fkrekgdw',
                'avatar':'https://res.cloudinary.com/storyrocket-llc/image/fetch/dpr_1,f_auto/https://storyrocket-aws3.s3.amazonaws.com/2110ad32.jpg'
            }
        ],
        'total':'17'
    },
    {
        'title':'Mariachi Paraiso Writers',
        'location' : 'San Francisco, California',
        'description' : 'Im looking for a couple of comedy writers to co-write a long form treatment or script about 5 mariachis that get lost at sea and end up in the Dominican Republic. Interested or if you want to know m',
        'image':'https://res.cloudinary.com/storyrocket-llc/image/fetch/dpr_0.8999999761581421,f_auto/https://www.storyrocket.com/public/images/mariachi-paraiso-writers/thumb_banner-58e4001a08e26.jpg',
        'linkGroup':'https://www.storyrocket.com/groups/mariachi-paraiso-writers',
        'author':'Ronald Karasz',
        'linkAuthor':'https://www.storyrocket.com/Ron.82c040rm',
        'members':[
            {
                'name':'Ronald Karasz',
                'link':'https://www.storyrocket.com/Ron.82c040rm',
                'avatar':'https://res.cloudinary.com/storyrocket-llc/image/fetch/dpr_0.8999999761581421,f_auto/https://storyrocket-aws3.s3.amazonaws.com/35a82d60.jpg'
            }
            
        ],
        'total':'1'
    }
];
const dataUser =[
    {
        'name':'luis guillanes',
        'avatar': 'https://www.storyrocket.com/public/images/avatar_default_user.png',
        'email':'luigui@evolbit.net',
        'keyUsers':'213155645'
    },
    {
        'name':'Luis Pena',
        'avatar': 'https://storyrocket-aws3.s3.amazonaws.com/0c005b1a.jpg',
        'email':'luiseljpc@gmail.com',
        'keyUsers':'448445151515151'
    },
]
const dataPubli=[
    {
        'name':'alberto luis felix magallanes',
        'avatar':'https://storyrocket-aws3.s3.amazonaws.com/full5c06b0bdadfdb1543942333sr_jpg',
        'linkUser' :'https://www.storyrocket.com/demo/magallanes.pcmh6r55',
        'postText':'orem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s',
        'key':'12456',
        'images':[
            {
                'thumbil':'https://storyrocket-aws3.s3.amazonaws.com/thumb-140x1405a85b300bdaaa1518711552sr_.jpg',
                'fullImage':'https://storyrocket-aws3.s3.amazonaws.com/full5a85b300bdaaa1518711552sr_.jpg',
                'key':'102-12012'
            },
            {
                'thumbil':'https://storyrocket-aws3.s3.amazonaws.com/thumb-140x1405a85b303c1a741518711555sr_.jpg',
                'fullImage':'https://storyrocket-aws3.s3.amazonaws.com/full5a85b303c1a741518711555sr_.jpg',
                'key':'102-12011'
            },
            {
                'thumbil':'https://storyrocket-aws3.s3.amazonaws.com/thumb-140x1405a85b30705cef1518711559sr_.jpg',
                'fullImage':'https://storyrocket-aws3.s3.amazonaws.com/full5a85b30705cef1518711559sr_.jpg',
                'key':'102-12031'
            },
            {
                'thumbil':'https://storyrocket-aws3.s3.amazonaws.com/thumb-140x1405a85b309c93be1518711561sr_.jpg',
                'fullImage':'https://storyrocket-aws3.s3.amazonaws.com/full5a85b309c93be1518711561sr_.jpg',
                'key':'102-12031'
            }
        ]
    }
]
const objComment =[
    {
        'fullname':'alberto luis felix magallanes',
        'avatar':'https://storyrocket-aws3.s3.amazonaws.com/full5c06b0bdadfdb1543942333sr_jpg',
        'comment':'texto de comentario para el cards de cada post',
        'keypostComent':'12354564645641321321546',
        'urlProfile':'https://www.storyrocket.com/demo/magallanes.pcmh6r55',
        'fecha':'3 seconds ago'
    }
]
$('body').on('click','#btnPost',e=>{
    e.preventDefault();
    const _post = $('#post-text').val();

    let bool = window.validateFrom("#frm-comment-group");
    if(bool){
        let __file = final_photos;
        let form_data = new FormData();
        for(var i = 0; i < __file.length; i++){
            form_data.append('file[]', __file[i]);
        }
        form_data.append('post',_post);
        /**
         * 
         * ENVIO DE AJAX CON PRAMETRO form_data
         * en el retorno inicializar final_photos =[]
         */
        
    
        let _html = cardsModulePubl(dataPubli);
        $('.loader-module-groups').prepend(_html);
        $("#frm-comment-group")[0].reset();
        $(".conte-previews").find('table').remove();
    }
})
$('#btnAddPhotos').on('change', function () {

	_uploadPhotos({
		image_type: "jpg|jpeg|png|gif",
		min_size: 24,
		max_size: 1024 * 1024 * 3, /* 3 Mb */
		max_files: 10
	}, function (e) {
		console.log(e);
    });
    $("#post-text").removeClass("required");
});

$('body').on('keyup', '#member_name', e=> {
    let val = $(e.target).val();
    /**
     * val = parametro de envio para realizar la busqueda de usuario en la bd
     * AJAX
     * dataUser [OBJETO QUE DEVUELVE EL AJAX]
     */
    let wrtemp = dataUser.length > 0 ? renderWriter(dataUser) : renderNotFound();
    $('.resultado').empty();
    $('.resultado').show(200);
    $('.resultado').append(wrtemp);
});
$('body').on('click', '.SRLikeLink', function () {
    addlike($(this), 's');
});
$('body').on('click', '.SR-LikeLink', function () {
    addlike($(this), 'r');
});

$('body').on("click",".check-groups",e=>{

    let type = e.currentTarget.dataset.type;
    let text = e.currentTarget.dataset.text;
    $("#title-searchs").html(text);
    /**
     * ajax
     */
    let object = data.length > 0 ? cardsGroups(data) : defaultEmpty();
    $(".cnt-grs-all").html(object);
    console.log(type)
})
var Arrayid =[];
$('body').on('click', '.members-gr', e=> {
	var _this = $(this);
	let mail = e.currentTarget.dataset.mail;
    let name = e.currentTarget.dataset.name;
    let id = e.currentTarget.dataset.id;
    let inpt = `<span class="remove userToken">
        <span class="userTokenText">${name}</span>
        <span class="_xrm" id="${id}"><i class="material-icons">close</i></span>
    </span>`;
    $('.losders-menbers').prepend(inpt);
    Arrayid.push(id);
	$("#menbers").val(Arrayid);
    $('.resultado').hide();
});
$("body").on("click","._xrm",function(){
    let id = $(this).attr('id');
    let indice = Arrayid.indexOf(id); // obtenemos el indice
    Arrayid.splice(indice, 1);
    $("#menbers").val(Arrayid);
    $(this).parent('.userToken').remove();
})
$("body").on("click",".boton-join",e=>{
    $(e.currentTarget).addClass("btn-add_ btn-default-request").removeClass("boton-join SeeMore4");
    $(e.target).html("<span>Request Send</span>");
})

$("body").on("click",".btn-post-comment",e=>{
    e.preventDefault();
    let _val =$(e.currentTarget).siblings('input');
    if(_val.val().length > 0){
        let post_id = e.currentTarget.dataset.post;
        let postComment = cardsComents(objComment);
        $('#list-coments-' + post_id).find(' > li:nth-last-child(1)').before(postComment);
        $('#frm-coments-' + post_id)[0].reset();
    }else{
        error("_val");
    }
    
})
$("body").on("click",".colse-comment-post",e=>{
    e.preventDefault();
    let keypostComent = e.currentTarget.dataset.key;
    let _modal = window.mainModal(
        {
            title:'Delete Coment',
            body:['Are you sure you want to delete this Coment?','All images, text, and views associated with this Coment will be permanently deleted.']
        }
    );
    $('body').append(_modal);
    $(".link-remove").attr("id","btn-modal-removeComent")
    $('#btn-modal-removeComent').attr('data-id',keypostComent);

})
$("body").on("click",'#btn-modal-removeComent',e=>{
    let id = e.currentTarget.dataset.id;
    $(`li#${id}`).remove();
    /**
     * ajax = id = ide del comment
     */
})
$("body").on("click",".btn-invit-g",e=>{
    let html =`
        <div>
            <label class="lebel-co active">Email</label>
            <input class="input-event-f required" type="email" placeholder="Email" id="invited_email">
        </div>
    `;
    let _modal = window.mainModal(
        {
            title:'Invite Friend',
            body:['If your friend is not a member, that’s not a problem. Have them join, it’s easy and free.',html]
        }
    );
    $('body').append(_modal);
    $(".wapper-modal-main-content-title").find('i').html("group");
    $(".icon-close-evnt").find("i").html("close");
    $(".wapper-modal-main-content-body").addClass("wapper-modal-main-content-body-alter");
    $("#btn-modal-ok").html("send");
    $("#btn-modal-ok").removeClass("link-remove").addClass("button-send");
    $(".button-send").attr("id","button-send-ok");
})
$("body").on("click","#button-send-ok",e=>{
    let _mail = document.getElementById("invited_email").value;
    if(window.validateMail(_mail)){
        /**
         * ajax = params value (_mail)
         */
        window.modalRemove();
    }else{
        $("#invited_email").css({'border':'1px solid red'});
        setTimeout(function () {
            $("#invited_email").removeAttr('style');
        }, 3000);
    }
    
})
$("body").on("keyup","#add-member",e=>{
    let _value = $(e.target).val();
    /**
     * ajax
     */
    let fulldata = listUserAdd(dataUser);
    $('.list-menbers').html('');
    $('.list-menbers').html(fulldata);
})

$("body").on("click",".open-images",e=>{
    e.preventDefault();
    $('.open-images').find('a').removeClass('currentImages');
    $(e.currentTarget).find('a').addClass('currentImages');
    let thumImage = document.getElementsByClassName('open-images');

   $('body').append(window.mainModal({type:'images',body:thumImage}));
})
$("body").on("click",'.btnCreateGroup',e=>{
    e.preventDefault();
    let bool = window.validateFrom("#frm-groups");
    if(bool){
        let obj = window.procesarFormulario("#frm-groups");
        /**
         * en el obj eseta toda la data para enviar al ajax para el registro 
         */
         //cambiar por url dinamica
        window.location.href = "http://storyrocket.test/groups/beating-windward-press";
    }
})
function addlike(_this, op) {
	if (op == 's') {
		_this.addClass('SR-LikeLink').removeClass('SRLikeLink');
		_this.find('i').removeClass('icon10-heart-outline').addClass('icon10-heart');
	} else {
		_this.addClass('SRLikeLink').removeClass('SR-LikeLink');
		_this.find('i').removeClass('icon10-heart').addClass('icon10-heart-outline');
	}
	/**
     * envio ajax
     */
}
/**
 * 
 * @param {objeto de array de usuarios} params 
 */
const renderWriter = (params)=> {
    return `
        ${params.map(e=>{
            return `
                <li class="result-search-writter members-gr" data-mail="${e.email}" data-name="${e.name}" data-id="${e.keyUsers}">
                    <div class="display-search-writter">
                        <img src="${e.avatar}" alt="${e.name}">
                    </div>
                    <div class="search-txt-writter">
                        <p>${e.name}</p>
                        <span>${e.email}</span>
                    </div>
                </li>
            `
        }).join("")}
    `;
}
const renderNotFound =()=> {
    return `<li><div class="search-txt-writter"><p>Ops... Member not found</p></div></li>`;
}
/**
 * card de array vacio
 */
const defaultEmpty = ()=>{
    return `
        <div class="col s12 l6 m4 space-card ">
            <div class="add-create dis-flex">
                <span class="icon-icono-profile28"></span>
                <div class="margin-crea x-left-space">
                    <span class="font-create">Create a Group and share it with the World.</span>
                    <div>
                        <a href="#modal1" class="btm-creat-g-e btn-create-groups modal-trigger" data-type="one" data-id="0">Create a Group</a>
                    </div>
                </div>
            </div>
        </div>
    `;
}
/**
 * 
 * @param {Array de data} obj 
 */
const cardsGroups=(obj)=>{
    return  obj.map(e=>{
        return `
            <div class="row cont-general-group">
                <div class="col s12 m12 l3 xl3">
                    <div class="img-g-cont">
                        <a href="${e.linkGroup}" alt="${e.title}" title="${e.title}">
                            <img src="${e.image}" alt="${e.title}" class="responsive-img lazy-image">
                        </a>
                    </div>
                </div>
                <div class="col s12 m12 l8 xl8 group-mr4">
                    <div class="gi-1 truncate">
                        <span class="font-g-title1 truncate"><a href="${e.linkGroup}" title="${e.title}" alt="${e.title}">${e.title}</a></span>
                    </div>
                    <div class="gi-2">
                        <i class="material-icons">location_on</i>
                        <span class="font-loca-g">${e.location}</span>
                    </div>
                    <p class="font-g-p">Books for Books’ Sake.
                    ${e.description}
                    </p>
                    <span class="font-c-t">
                        Create by <strong><a href="${e.linkAuthor}" title="${e.author}" alt="${e.author}">${e.author}</a></strong>
                    </span>
                    <div class="group-mr3">
                        ${e.members.map(user=>{
                            return `
                                <div class="user-event bloque-event">
                                    <figure class="not-margin couponcode tooltip-load" data-type="users" data-key="10-17-19">
                                        <img src="${user.avatar}" data-src="${user.avatar}" alt="${user.name}" class="responsive-img lazy-image">
                                        <div class="tol-dinac"></div>
                                    </figure>
                                </div>
                            `
                        }).join("")}
                        <div class="user-group2">
                            <p class="font-group3">+${e.total}</p>
                        </div>
                        <button class="boton-join SeeMore4" data-list="13">
                            <span>Join Group</span>
                        </button>
                    </div>
                </div>
            </div>
        `
    }).join("");
}

/**
 * 
 * @param {array de objeto de la bd} obj 
 */
const cardsModulePubl =(obj)=>{
    return `
        <div class="mar-mod-2 view-post" id="${obj[0].key}">
			<div class="card-mod-1">
                <div class="card-mod-1-1">
                    <div class="card-mod-img">
                        <img src="${obj[0].avatar}" alt="${obj[0].name}" class="responsive-img">
                    </div>
                    <div class="card-mod-p">
                        <p class="font-mod-1">
                            <a href="${obj[0].linkUser}" title="${obj[0].name}" alt="${obj[0].name}">${obj[0].name}</a>
                        </p>
                        <span class="font-mod-2">Just now</span>
                    </div>
                    <div class="icon-mod-1 close-post" data-type="post" data-tag="${obj.key}">
                    <i class="material-icons">close</i>
                </div>
			</div>
            <div class="card-mod-1-2">
                <p class="font-mod-3">${obj[0].postText}</p>
                <ul>
                    ${obj[0].images.map(e=>{
                        return `
                            <li class="list-images open-images"><a href="${e.fullImage}" id="${e.key}" class="u"><img src="${e.thumbil}"></a></li>
                        `
                    }).join("")}
                </ul>
            </div>
		    <div class="card-mod-1-3 right-align ">
			    <div class="display-new-gr gi-12 card-likes">
                    <i class="icon10-heart"></i>
                    <span class="fo-icon1  _1x5- _5x1_ _50-k">
                        <span class="_cont">0</span>
                    </span>
                    <i class="icon15-icon-chat1"></i>
                    <span class="fo-icon2 counter-coments">0</span>
                </div>
                <a class="icon-comet1 icon-m1 SRLikeLink" data-key="6-16-28" post-key="7-23-20">
                    <i class=" icon10-heart-outline"></i>
                    <span class="font-l-new">Like</span>
                </a>
                <a class="icon-comet1-c icon-m1">
                    <i class="icon15-icon-chat2"></i>
                    <span class="font-l-new">Comment</span>
                </a>
		    </div>
        </div>
		<div class="mod-coment">
            <ul id="list-coments-7-23-20">
                <li>
                    <div class="mod-img-coment">
                        <img src="${obj[0].avatar}" alt="coment" class="responsive-img">
                    </div>
                    <div class="mod-comet-1-send">
                        <form id="frm-coments-7-23-20">
                            <input class="input-g-c required" type="text" name="" placeholder="Write comment….">
                            <button class="btn-post-comment ladda-button 7-23-20" data-style="expand-left"  data-post="7-23-20" data-ip="13-20-23" data-type="pbl"><span class="ladda-label">Post Comment</span></button>
                        </form>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    `;
}
/**
 * 
 * @param {objeto de data para el comentario de cada post} obj 
 */
const cardsComents =(obj)=>{
    return `
        <li id="${obj[0].keypostComent}">
            <div class ="mod-img-coment">
                <a href=""><img src="${obj[0].avatar}" alt="${obj[0].fullname}" class="responsive-img"></a>
            </div>
            <div class="mod-comet-1">
                <p class="font-comet-1">
                    <a class="font-profile" href="${obj[0].urlProfile}">${obj[0].fullname}</a>
                    <span>${obj[0].comment}</span>
                </p>
                <p class="font-comet-2">
                    <span class="font-comet-2">${obj[0].fecha}</span> - <span class="font-comet-2"><a href="#" class="a-delete colse-comment-post" data-type="cmt" data-key="${obj[0].keypostComent}">Delete</a></span>
                </p>
            </div>
        </li>
    `;
}
/**
 * 
 * @param {array data} obj 
 */
const listUserAdd =(obj)=>{
    return `
        ${obj.map(e=>{
            return `
            <li>
                <div class="_avs"><img src="${e.avatar}"></div>
                <div class="_text truncate _xgrxg">${e.name}</div>
                <span><i class="material-icons close3 users-members-ad" data-cod="${e.keyUsers}">add_circle_outline</i></span>
            </li>
            `
        }).join("")}
    `;
}
/**
 * 
 * @param {*} e 
 */
const error =(e)=>{
    $(`.${e}`).css({'border':'1px solid red'});
    setTimeout(function () {
        $(`.${e}`).removeAttr('style');
    }, 3000);
}


/**
 * 
 * @param {*} options 
 * @param {*} callback 
 */
var __items__upload__ = new Array();
var final_photos = [];
function _uploadPhotos(options, callback) {

	var __errors__upload__ = '';
	var content_type = '';
	var element = '';
	var defaults = {
		image_type: "jpg|jpeg|png|gif",
		min_size: 24,
		max_size: 1024 * 1024 * 3,
		max_files: 10
	};
	if (options === undefined) {
		options = defaults;
	}
	var _file = $("#btnAddPhotos")[0].files;
	var count = 0;
	for (var x = 0; x < _file.length; x++) {
        console.log('ingreso');
		count++;
		content_type = options.image_type.split("|");
		var c_t = _file[x]["type"];
		c_t = c_t.split("/");
		var size = _file[x]["size"];
		if (content_type.indexOf(c_t[1]) != -1) {
			__errors__upload__ = '';
			if (size >= options.min_size) {
				__errors__upload__ = '';
				if (size <= options.max_size) {

					var setup_reader = function setup_reader(files, i, random_class) {
						var file = files[i];
						final_photos.push(file);
						var reader = new FileReader();
						reader.onload = function (e) {
							$(".border-text-w").append("<div class='progress'><div class='progress-bar progress-bar-striped active' role='progressbar' aria-valuenow='100' aria-valuemin='0' aria-valuemax='100' style='width: 100%'></div></div><table style='display: none;' class='table table-condensed' image-upload-item='" + random_class + "'><tr><td class='u-relative'><img src='" + e.target.result + "' class='img-responsive' /><button type='button' class='btn btn-danger' data-file='" + file.name + "' image-upload-item='" + random_class + "' ><i class='material-icons'>close</i></button></td></tr></table>");
							$(".border-text-w").find("[image-upload-item='" + random_class + "']").fadeIn(1000, function () {
								$(".progress").fadeOut(1000, function () {
									$(this).remove();
								});
							});

							$.event.trigger({ type: "createImage", file: eval("__items__upload__" + element + "[random_class]") });
							$(".border-text-w").find("button").on("click", function () {
								$(".border-text-w").find("[image-upload-item='" + $(this).attr("image-upload-item") + "']").fadeOut(1000, function () {
									var file_name = $(this).data("file");

									indexes = $.map(final_photos, function(obj, index) {
										if(obj.name == file_name) {
											return index;
										}
									})
									if(indexes[0] >= 0) {
										firstIndex = indexes[0];
										final_photos.splice(firstIndex,1);
									}

									$(this).remove();
								});

								try {
									$.event.trigger({ type: "deleteImage", file: image });
								} catch (e) {}
							});
						};
						reader.readAsDataURL(file);

					};

					__errors__upload__ = '';

					var __errors_upload__ = '';
					var random_class = Math.floor(Math.random() * 10000000000000000000 + 1);

					setup_reader(_file, x, random_class);
				} else {
					__errors__upload__ = 'ERROR_MAX_SIZE';
				}
			} else {
				__errors__upload__ = 'ERROR_MIN_SIZE';
			}
		} else {
			__errors__upload__ = 'ERROR_CONTENT_TYPE';
		}
	}
	callback(element);
}