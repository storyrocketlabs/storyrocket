$('#apply-code').on('click', function (event) {
    event.preventDefault();

    let coupon = $('#box-code').val();

    if(coupon)
    {
        console.log('/plans/'+$('#plan-id-value').val()+'/validate-coupon');
        axios.post('/plans/'+$('#plan-id-value').val()+'/validate-coupon', {
           coupon
        }).then(response => {
            if(response.data.success)
            {
                let amount = $('#price-plan').val();
                let stripe_coupon = response.data.coupon;
                let discount = stripe_coupon.amount_off ? (stripe_coupon.amount_off / 100) : (amount * stripe_coupon.percent_off) / 100;

                amount = amount - discount;

                $('.price-pln').html(amount);
                $('#popupCouponForm').modal('close');
                $('#plan-coupon').val(response.data.coupon.id);

            }else{
                alert('Invalid Promo Code');
            }
        });

    }else{
        alert('Promo code is required');
    }


});

$('#modal-add-promo-code').on('click', function (event) {
   event.preventDefault();
    $('#box-code').val('');
});

if ($('#card-number').length) {
    let cleave = new Cleave('#card-number', {
        creditCard: true
    });
}

