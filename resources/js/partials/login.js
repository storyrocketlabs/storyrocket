$("#signin").on("click", function (ev) {
    ev.preventDefault();
    var error = false;
    var id = $(this).attr('id');
    $('.validate').each(function (i, elem) {
        if ($(elem).val() == '' || $(elem).val() == 0) {
            $(elem).addClass('invalid');
            error = true;
        }
    });

    if(!error)
    {
       $('#frm-signin').submit();
    }else{
        return false;
    }
});
$(document).ready(function() {

    /*
    $('.dropdown-trigger-profileicon').dropdown( {
        inDuration: 300,
        outDuration: 225,
        constrainWidth: false, // Does not change width of dropdown to that of the activator
        //hover: true, // Activate on hover
        gutter: 0, // Spacing from edge
        belowOrigin: false, // Displays dropdown below the button
        alignment: 'left', // Displays dropdown with edge aligned to the left of button
        stopPropagation: false // Stops event propagation
    });
    */
    // burger menu animate
    $(".burger").click(function() {
        $(".upper").toggleClass("move");
        $(".downer").toggleClass("move2");
        $(".line-burger1").toggleClass("big-line");
        $(".line-burger3").toggleClass("big-line");
    });

    // togle menu
    $(".menu-toggle").click(function(e) {
        e.preventDefault();

    });

});
