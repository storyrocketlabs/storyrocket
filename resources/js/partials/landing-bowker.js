
$(document).ready(function () {

    $(".register-password-form-field-eye1 .register-password-form-field-eye-content").click(function () {
        if ($('#register-password-form-field-eye-check1').is(':checked')) {
            $('#password').attr('type', 'password')
            $(this).children("i").removeClass("register-password-form-color-active");
            $('#register-password-form-field-eye-check1').prop('checked', false);
        } else {
            $('#password').attr('type', 'text')
            $(this).children("i").addClass("register-password-form-color-active");
            $('#register-password-form-field-eye-check1').prop('checked', true);
        }
    });


    $(".register-password-form-field-eye2 .register-password-form-field-eye-content").click(function () {
        if ($('#register-password-form-field-eye-check2').is(':checked')) {
            $('#password_confirmation').attr('type', 'password')
            $(this).children("i").removeClass("register-password-form-color-active");
            $('#register-password-form-field-eye-check2').prop('checked', false);
        } else {
            $('#password_confirmation').attr('type', 'text')
            $(this).children("i").addClass("register-password-form-color-active");
            $('#register-password-form-field-eye-check2').prop('checked', true);
        }
    });


    $(".s-register-password form").submit(function () {
        //$(".message-v1").html("asdasdas")

        var password = $("#password").val();
        var password_confirmation = $("#password_confirmation").val();
        if(password_confirmation!=password) {
            $(".message-v2").html("This pasword doesn't match")
            return false;
        }

    });


});