$(document).ready(function () {

    /**
     * Click anypart and close minibar notification
     */
    jQuery(document).click(function(e) {
        //console.log(jQuery(e.target).closest('.notifications-alert').length)
        if (jQuery(e.target).closest('.notifications-alert').length === 0 ) {
            if(!($('.notifications-alert-displayed').hasClass('hide'))) {
                $('.notifications-alert').trigger("click");
            }
        }

        if (jQuery(e.target).closest('.profileicon-minimenu').length === 0 ) {
            if(!($('.profileicon-minimenu-displayed').hasClass('hide'))) {
                $('.profileicon-minimenu').trigger("click");
            }
        }

    });

    var callNotificationBell = function () {
        var baseUrl = window.location.protocol + "//" + window.location.host + "/"
        var url = baseUrl + 'notification/get-alert-bell';
        let data = {};
        //console.log("callNotificationBell");
        axios.get(url, data).then(
            response => {
                //console.log(response.data);
                $("#notificationBellAlert").hide();
                if(response.data!=0) {
                    $("#notificationBellAlert").show();
                    $("#notificationBellAlert span").html(response.data);
                }
            }
        );
    }

    if(window.auth)
    {
        callNotificationBell();
    }

    var bell = "#bell";
    var localNotifications = [];
    var isClosed = true;

    if(localNotifications.length > 0) {
        $(bell).addClass("disabled-icon");
    } else {
        $(bell).removeClass("disabled-icon");
    }

    $('body').on('click', '.profileicon-minimenu', function (e) {

        if(!($('.notifications-alert-displayed').hasClass('hide'))) {
            $('.notifications-alert-displayed').toggleClass('hide');
        }
        $('.profileicon-minimenu-displayed').toggleClass('hide');
    });

    $('body').on('click', '.notifications-alert', function (e) {
        e.preventDefault();
        // Load notifications
        if(isClosed) {
            fetchNotifications();
        }

        if(!($('.profileicon-minimenu-displayed').hasClass('hide'))){
            $('.profileicon-minimenu-displayed').toggleClass('hide');
        }
        $('.notifications-alert-displayed').toggleClass('hide');
        isClosed = !isClosed;
        $('.not-alert').css('visibility', 'hidden');
    });




    var fetchNotifications = function () {

        var baseUrl = window.location.protocol + "//" + window.location.host + "/"
        var url = baseUrl + 'notification/get-views-profile';
        let data = {};
        //console.log(url);
        var containerId = '#noti-1';
        var templateData = '#profile-views-notification-template-empty';
        $(containerId).html($("#profile-loader").html());

        axios.get(url, data).then(
            response => {
                //console.log(response.data);
                //console.log(response.data.length);

                if(response.data.length>0) {
                    templateData = '#profile-views-notification-template';
                }
                let template = _.template($(templateData).html());
                let rendered = $(template({profile_visitor: response.data}));
                var notification_html = rendered;
                $(containerId).empty();
                $(containerId).html(notification_html);
                checkNotificationBell();


            }
        );
    }

    var checkNotificationBell =  function () {
        var baseUrl = window.location.protocol + "//" + window.location.host + "/"
        var url = baseUrl + 'notification/check-alert-bell';
        let data = {};
        //console.log("checkNotificationBell");
        axios.get(url, data).then(
            response => {

            }
        );
    }
});