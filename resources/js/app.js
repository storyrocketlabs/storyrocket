
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes React and other helpers. It's a great starting point while
 * building robust, powerful web applications using React + Laravel.
 */

window._ = require('lodash');
window.cropit = require('cropit');
window.$ = window.jQuery = require('jquery');
window.axios = require('axios');
window.axios = require('axios');
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
let Cleave = require('cleave.js');


jQuery.event.addProp('dataTransfer');

let token = document.head.querySelector('meta[name="csrf-token"]');

if (token) {
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
} else {
    console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}

require('../materialize/js/bin/materialize');
require('./partials/ios-checkbox');
require('./partials/home');
require('./partials/login');
require('./partials/register-assist');
require('./partials/profile');
require('./partials/plans');
require('./partials/search');
require('./partials/projects');
require('./partials/mail');
require('./partials/groups');
require('./partials/alert');
require('./partials/settings');
require('./partials/landing-bowker');

/**
 * Next, we will create a fresh React component instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

M.AutoInit();

let resizeTimer;

$(document).ready(function () {
    renderImages();
    window.addEventListener("resize", function () {
        clearTimeout(resizeTimer);
        resizeTimer = setTimeout(function () {
            renderImages();
        }, 250);
    }, false);
    
});
/**
 * 
 * @param {value mail} mail 
 */
window.validateMail =(mail)=> {
    var valueForm = mail;
    // Patron para el correo
    var patron = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    var _xbool = patron.test(valueForm) ? true : false;
    return _xbool;
}


window.validateFrom=(selector)=>{
	var _xboll = true;
	$(selector).find('.required').each(function (i, el) {
		if ($(el).val() == '' || $(el).val() == 0 || $(el)[0].value == 0 || $(el)[0].value == '') {
			$(el).addClass('required-error');
            _xboll = false;
		}
	});
    if(!_xboll){
        setTimeout(function () {
            $(selector).find("input, select, textarea, email").removeClass('required-error');
        }, 3000);
	}
	return _xboll;
}

window.procesarFormulario =(selector, template)=> {
   
	const data = template ? template : {};
	var f,r,v,m,$e,
        $elements = $(selector).find("input, select, textarea, email");

	for (var i = 0; i < $elements.length; i++) {
       
		$e = $($elements[i]);

		f = $e.data("name");
		r = $e.attr("required") ? true : false;

		// si no se especifica un campo, ignoramos el elemento
		if (!f) continue;

		// dependiendo de que tipo de control se trate
		v = undefined;
		switch ($e[0].nodeName.toUpperCase()) {
			case "LABEL":
				v = $e.text();
				break;
			case "INPUT":
				var type = $e.attr("type").toUpperCase();
				if (type == "CHECKBOX") {
					v = $e.prop("checked");
				} else if (type == "RADIO") {
					if ($e.prop("checked")) {
						v = $e.val();
					}
				} else {
					v = $.trim($e.val());
				}
				break;
			case "TEXTAREA":
			default:
				v = $.trim($e.val());
		}

        // grabamos el valor en el objeto
       
		if (r && (v == undefined || v == "" )) {
			console.log(`Necesitas especificar un valor para el campo ${f}`);
			$e.focus();
			return null;
		} else if (v != undefined) data[f] = v;
	} // next
	return data;
}
window.toggleVideo =(state)=> {
    // if state == 'hide', hide. Else: show video
    var div = document.getElementById("modal111");
    var iframe = div.getElementsByTagName("iframe")[0].contentWindow;
    div.style.display = state == 'hide' ? 'none' : '';
  //  func = state == 'hide' ? 'pauseVideo' : 'playVideo';
    func = state == 'hide' ? 'stopVideo' : 'playVideo';
    iframe.postMessage('{"event":"command","func":"' + func + '","args":""}','*');
}