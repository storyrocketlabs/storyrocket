window.imagenes_svg = [];
window.imagenes_run = [];
window.imagenes_run_new = [];
var abs_url = new RegExp('^(?:[a-z]+:)?//', 'i');

function hasClass(element, cls) {
    return (' ' + element.className + ' ').indexOf(' ' + cls + ' ') > -1;
}

function renderSVG(element) {
    var $img = jQuery(element);
    var imgURL = $img.attr('src');
    if(!imagenes_run[imgURL]) {
        imagenes_run[imgURL] = true;
        if (window.imagenes_svg[imgURL]){
            $svgsource = window.imagenes_svg[imgURL];
            jQuery('img.svg-image[src="'+imgURL+'"]').each(function(){
                var $svg = $svgsource;
                $svg.removeAttr('id');
                $svg.removeAttr('class');
                var imgID = jQuery(this).attr('id');
                var imgClass = jQuery(this).attr('class');
                if (typeof imgID !== 'undefined') {
                    $svg = $svg.attr('id', imgID);
                }

                if (typeof imgClass !== 'undefined') {
                    $svg = $svg.attr('class', imgClass + ' replaced-svg');
                }

                $(this).replaceWith(new XMLSerializer().serializeToString($svg[0]));
            });
        } else {
            jQuery.get(imgURL, function (data) {
                var $svgsource = jQuery(data).find('svg');
                $svgsource = $svgsource.removeAttr('xmlns:a');
                window.imagenes_svg[imgURL] = $svgsource;
                jQuery('img.svg-image[src="'+imgURL+'"]').each(function(){
                    var $svg = $svgsource;
                    $svg.removeAttr('id');
                    $svg.removeAttr('class');
                    var imgID = jQuery(this).attr('id');
                    var imgClass = jQuery(this).attr('class');
                    if (typeof imgID !== 'undefined') {
                        $svg = $svg.attr('id', imgID);
                    }

                    if (typeof imgClass !== 'undefined') {
                        $svg = $svg.attr('class', imgClass + ' replaced-svg');
                    }

                    $(this).replaceWith(new XMLSerializer().serializeToString($svg[0]));
                });
            }, 'xml');
        }
    }
}

function renderNewSVG(element) {
    var $img = jQuery(element);
    var imgURL = $img.attr('data-src');

    if(!imagenes_run_new[imgURL]) {
        imagenes_run_new[imgURL] = true;
        if (window.imagenes_svg[imgURL]){
            $svgsource = window.imagenes_svg[imgURL];
            jQuery('img.svg-img[data-src="'+imgURL+'"]').each(function(){
                var $svg = $svgsource;
                $svg.removeAttr('id');
                $svg.removeAttr('class');
                var imgID = jQuery(this).attr('id');
                var imgClass = jQuery(this).attr('class');
                if (typeof imgID !== 'undefined') {
                    $svg = $svg.attr('id', imgID);
                }

                if (typeof imgClass !== 'undefined') {
                    $svg = $svg.attr('class', imgClass + ' replaced-svg');
                }

                $(this).replaceWith(new XMLSerializer().serializeToString($svg[0]));
            });
        } else {

            var ajaxurl;
            if (window.production) {
                if (!abs_url.test(imgURL)) {
                    ajaxurl = window.location.origin + imgURL;
                }
                ajaxurl = 'https://res.cloudinary.com/storyrocket-llc/image/fetch/'+ajaxurl;
            } else {
                ajaxurl = imgURL;
            }

            jQuery.get(ajaxurl, function (data) {
                var $svgsource = jQuery(data).find('svg');
                $svgsource = $svgsource.removeAttr('xmlns:a');
                window.imagenes_svg[imgURL] = $svgsource;
                jQuery('img.svg-img[data-src="'+imgURL+'"]').each(function(){
                    var $svg = $svgsource;
                    $svg.removeAttr('id');
                    $svg.removeAttr('class');
                    var replacingElement = $(this);
                    var imgID = replacingElement.attr('id');
                    var imgClass = replacingElement.attr('class');
                    if (typeof imgID !== 'undefined') {
                        $svg = $svg.attr('id', imgID);
                    }

                    if (typeof imgClass !== 'undefined') {
                        $svg = $svg.attr('class', imgClass + ' replaced-svg');
                    }
                    replacingElement.replaceWith(new XMLSerializer().serializeToString($svg[0]));
                });
            }, 'xml');
        }
    }

}

function renderImages() {
    window.imagenes_run = [];
    window.imagenes_run_new = [];
    var treshold = 500;
    if (window.devicePixelRatio > 0) {
        treshold = $(window).height()*window.devicePixelRatio;
    } else {
        treshold = $(window).height();
    }
    $('.lazy-image-important').unveil(treshold);
    $('.lazy-image').unveil(treshold);
    jQuery('img.svg-image').each(function(){
        renderSVG(this);
    });
}

(function ($) {

    $.fn.unveil = function (threshold, callback) {

        var $w = $(window),
            th = threshold || 0,
            images = this,
            loaded;

        this.one("unveil", function () {
            var source = this.getAttribute("data-src");

            if (source) {

                if (hasClass(this, 'svg-img')) {

                    // ES SVG
                    renderNewSVG(this);
                    if (typeof callback === "function") callback.call(this);

                } else {

                    // NO ES SVG
                    if (window.production) {
                        source = source.replace("dpr_auto", "dpr_" + window.devicePixelRatio);
                        var options = '';
                        if (window.devicePixelRatio > 0) {
                            options += "dpr_" + window.devicePixelRatio;
                        } else {
                            options += "dpr_auto";
                        }
                        if (this.getAttribute("data-fixed-width")) {
                            var width = parseInt($(this).width());
                            if (width > 100) {
                                width = parseInt((width+9)/10)*10;
                            }
                            options += ',w_' + width;
                            options += ',c_limit';
                        }
                        if (this.getAttribute("data-fixed-height")) {
                            var height = parseInt($(this).height());
                            if (height > 100) {
                                height = parseInt((height+9)/10)*10;
                            }
                            options += ',h_' + height;
                            options += ',c_limit';
                        }
                        if (this.getAttribute("data-fixed")) {
                            var width = parseInt($(this).width());
                            if (width > 100) {
                                width = parseInt((width+9)/10)*10;
                            }
                            var height = parseInt($(this).height());
                            if (height > 100) {
                                height = parseInt((height+9)/10)*10;
                            }
                            options += ',w_' + width;
                            options += ',h_' + height;
                            options += ',c_limit';
                        }
                        if (this.getAttribute("data-quality")) {
                            options += ',q_'+this.getAttribute("data-quality");
                        }
                        if (this.getAttribute("data-fl")) {
                            options += ',fl_'+this.getAttribute("data-fl");
                        }
                        if (this.getAttribute("data-f")) {
                            if (this.getAttribute("data-f") == 'no-change') {
                                // no format change
                            } else {
                                options += ',f_'+this.getAttribute("data-f");
                            }
                        } else {
                            options += ',f_auto';
                        }
                        if (!abs_url.test(source)) {
                            source = window.location.origin + source;
                        }

                        source = 'https://res.cloudinary.com/storyrocket-llc/image/fetch/' + options + '/' + source;
                    }

                    this.setAttribute("src", source);
                    if (typeof callback === "function") callback.call(this);

                }
            };

        });

        function unveil() {
            var inview = images.filter(function () {
                var $e = $(this);
                //if ($e.is(":hidden")) return;

                var wt = $w.scrollTop(),
                    wb = wt + $w.height(),
                    et = $e.offset().top,
                    eb = et + $e.height();

                return eb >= wt - th && et <= wb + th;
            });

            loaded = inview.trigger("unveil");
            images = images.not(loaded);
        }

        $w.on("scroll.unveil resize.unveil lookup.unveil", unveil);

        unveil();

        return this;

    };

})(window.jQuery || window.Zepto);
