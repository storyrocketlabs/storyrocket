<?php

namespace App;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Project extends Model
{

    protected $fillable = [
        'user_id',
        'occupation_id',
        'project_name',
        'registration_id',
        'project_office',
        'registration_number',
        'log_line',
        'synopsis',
        'treatment',
        'story',
        'tag',
        'material_id',
        'language_id',
        'principal_location',
        'principal_state',
        'city',
        'city2',
        'era_id',
        'example',
        'budget_id',
        'rating_id',
        'poster',
        'banner',
        'tagline',
        'project_file',
        'project_trailer',
        'pdf_project_file',
        'pdf_view',
        'log_line_number',
        'create_date',
        'status',
        'user_like',
        'state',
        'publish',
        'ini',
        'fin',
        'urlproj',
        'entity',
        'number_entity',
        'above',
        'below',
        'url_funding',
        'funding',
        'id_plan',
        'params',
        'url_poster',
        'url_banner',
        'date_update',
        'date_publish',
        'topimage',
        'orden',
        'date_trending',
        'trending'
    ];

    public function genres()
    {
        return $this->belongsToMany(Genre::class, 'projects_genre');
    }

    public function mediums()
    {
        return $this->belongsToMany(Medium::class, 'projects_medium');
    }

    public function language()
    {
        return $this->belongsTo(Language::class, 'language_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getPhotoAttribute()
    {

        $photo = $this->attributes['photo'];



        if (!empty($photo)) {
            if (filter_var($photo, FILTER_VALIDATE_URL) === FALSE) {

                if($photo == "picture?type=large") {
                    return $this->attributes['facebook_photo'];
                }
                return env('AWS_URL') . '/' . $photo;
            }

        }
        return $photo;

    }

    public function getPosterAttribute()
    {

        $poster = $this->attributes['poster'];

        if (!empty($poster)) {
            if (filter_var($poster, FILTER_VALIDATE_URL) === FALSE) {
                return env('AWS_URL') . '/' . $poster;
            }

        }
        return $poster;

    }

    public static function getBaseStaticAWS($image)
    {

        if (!empty($image)) {
            if (filter_var($image, FILTER_VALIDATE_URL) === FALSE) {
                return env('AWS_URL') . '/' . $image;
            }

        }
        return $image;

    }

    public static function getBasePhotoProfile($photo, $facebook_photo)
    {
        if(!empty($facebook_photo)) {
            return $facebook_photo;
        }

        if(!empty($photo) && $photo != "avatar_default_user.png") {
            return env('AWS_URL') . '/' . $photo;
        }

        return asset('images/avatar_default_user.png');
    }

    public function getProfileWriterUrlAttribute()
    {
        $url = $this->attributes['profiles_writer_url'];

        if (!empty($url)) {
            if (filter_var($url, FILTER_VALIDATE_URL) === FALSE) {
                return env('URL') . '/' . $url;
            }

        }
        return $url;
    }

    public function getLocalNameLineAttribute()
    {

        $comma_separated = "None";
        $lineArray = [];

        if ($this->attributes['country']) {
            $lineArray[] = $this->attributes['country'];
        }
        if ($this->attributes['state']) {
            $lineArray[] = $this->attributes['state'];
        }
        if ($this->attributes['city']) {
            $lineArray[] = $this->attributes['city'];
        }

        if (!empty($lineArray)) {
            $comma_separated = implode(", ", $lineArray);;
        }
        return $comma_separated;

    }

    /*public function getUrlProfileAttribute()
    {

        $url = $this->attributes['profile_url'];

        if (!empty($url)) {
            if (filter_var($url, FILTER_VALIDATE_URL) === FALSE) {
                return env('URL') . '/' . $url;
            }

        }
        return $url;

    }*/

    public function dashboardNewProjects()
    {

        $dataFrom = date('Y-m-d', strtotime('-1 months')) . " 00:00:00";
        $dataTo = date("Y-m-d") . " 00:00:00";

        $sql = '
        SELECT
        *
        FROM
            (
        SELECT 
            p1.UserID as user_id,
            CONCAT(p2.FullName," ",p2.`LastName`) as full_name,	
            CONCAT("https://www.storyrocket.com/", p2.url) as user_url,	
            p1.ProjectID as project_id,	
            p1.ProjectName as project_name,
            p1.Poster as project_poster,	                        
            CONCAT("https://www.storyrocket.com/", p1.urlproj) as project_url,	
            p1.Synopsis as synopsis,	
            (
                CASE WHEN (p2.photo = "" OR p2.photo = NULL) THEN
                    "https://www.storyrocket.com/public/images/avatar_default_user.png" 
                ELSE
                    p2.photo	
                END
                ) as user_photo,
        
            g.genre_project,
            v.project_vote,
            vi.project_view
        FROM projects p1
            LEFT JOIN (
                
                SELECT 
                    projects_genre.ProjectID, 
                    GROUP_CONCAT((genre.Genre) SEPARATOR \' | \') as genre_project
                FROM projects_genre  
                LEFT JOIN genre ON genre.GenreID = projects_genre.`GenreID`
                GROUP BY projects_genre.ProjectID
                
            ) g ON g.ProjectID = p1.ProjectID    
            LEFT JOIN profile p2 ON p1.UserID = p2.UserID
            
            LEFT JOIN (
                SELECT 
                    count(ProjectID) as project_vote, ProjectID
                FROM vote  
                GROUP BY ProjectID
            ) v ON v.ProjectID = p1.ProjectID
            
            LEFT JOIN (
                SELECT 
                    count(DateView) as project_view, ProjectID
                FROM view  
                GROUP BY ProjectID
            ) vi ON vi.ProjectID = p1.ProjectID
        WHERE `Status` = "publish"
        AND p1.CreateDate >=\'' . $dataFrom . '\'
        AND p1.CreateDate <=\'' . $dataTo . '\'
        
        GROUP BY p1.ProjectID 
        ORDER BY p1.ProjectID DESC
        
        ) t1
        ORDER BY RAND()  limit 15';

        $projects = DB::connection('mysql_old')->select($sql);

        return json_encode($projects);

    }


    public function dashboardTrendingProjects()
    {

        $dataFrom = date('Y-m-d', strtotime('-1 months')) . " 00:00:00";
        $dataTo = date("Y-m-d") . " 00:00:00";

        $sql = '
        SELECT
        *
        FROM
            (
            SELECT 
                p1.UserID as user_id,
                CONCAT(p2.FullName," ",p2.`LastName`) as full_name,	
                CONCAT("https://www.storyrocket.com/", p2.url) as user_url,	
                p1.ProjectID as project_id,	
                p1.ProjectName as project_name,
                p1.Poster as project_poster,	
                CONCAT("https://www.storyrocket.com/", p1.urlproj) as project_url,	
                p1.Synopsis as synopsis,	
                (
                CASE WHEN (p2.photo = "" OR p2.photo = NULL) THEN
                    "https://www.storyrocket.com/public/images/avatar_default_user.png" 
                ELSE
                    p2.photo	
                END
                ) as user_photo,
    
            g.genre_project,
            v.project_vote,
            vi.project_view
        FROM projects p1
        LEFT JOIN (
            
            SELECT 
                projects_genre.ProjectID, 
                GROUP_CONCAT((genre.Genre) SEPARATOR \' | \') as genre_project
            FROM projects_genre  
            LEFT JOIN genre ON genre.GenreID = projects_genre.`GenreID`
            GROUP BY projects_genre.ProjectID
            
        ) g ON g.ProjectID = p1.ProjectID    
        LEFT JOIN profile p2 ON p1.UserID = p2.UserID
        
        LEFT JOIN (
            SELECT 
                count(ProjectID) as project_vote, ProjectID
            FROM vote  
            GROUP BY ProjectID
        ) v ON v.ProjectID = p1.ProjectID
        
        LEFT JOIN (
            SELECT 
                count(DateView) as project_view, ProjectID
            FROM view  
            GROUP BY ProjectID
        ) vi ON vi.ProjectID = p1.ProjectID
        WHERE `Status` = "publish"                
        AND p1.CreateDate >=\'' . $dataFrom . '\'
        AND p1.CreateDate <\'' . $dataTo . '\'
        
        GROUP BY p1.ProjectID 
        ORDER BY v.project_vote  DESC
    
        ) t1
        ORDER BY RAND() limit 20
        ';

        $projects = DB::connection('mysql_old')->select($sql);

        return json_encode($projects);

    }

    public function dashboardMostViewedProjects()
    {

        $sql = '
        SELECT
        *
        FROM
            (
            SELECT 
                p1.UserID as user_id,
                CONCAT(p2.FullName," ",p2.`LastName`) as full_name,	
                CONCAT("https://www.storyrocket.com/", p2.url) as user_url,	
                p1.ProjectID as project_id,	
                p1.ProjectName as project_name,
                p1.Poster as project_poster,	
                CONCAT("https://www.storyrocket.com/", p1.urlproj) as project_url,	
                p1.Synopsis as synopsis,	
                (
                    CASE WHEN (p2.photo = "" OR p2.photo = NULL) THEN
                        "https://www.storyrocket.com/public/images/avatar_default_user.png" 
                    ELSE
                        p2.photo	
                    END
                    ) as user_photo,
            
                g.genre_project,
                v.project_vote,
                vi.project_view
            FROM projects p1
                INNER JOIN (
                    
                    SELECT 
                        projects_genre.ProjectID, 
                        GROUP_CONCAT((genre.Genre) SEPARATOR \' | \') as genre_project
                    FROM projects_genre  
                    LEFT JOIN genre ON genre.GenreID = projects_genre.`GenreID`
                    GROUP BY projects_genre.ProjectID
                    
                ) g ON g.ProjectID = p1.ProjectID    
                INNER JOIN profile p2 ON p1.UserID = p2.UserID
                
                INNER JOIN (
                    SELECT 
                        count(ProjectID) as project_vote, ProjectID
                    FROM vote  
                    GROUP BY ProjectID
                ) v ON v.ProjectID = p1.ProjectID
                
                INNER JOIN (
                    SELECT 
                        count(DateView) as project_view, ProjectID
                    FROM view  
                    GROUP BY ProjectID
                ) vi ON vi.ProjectID = p1.ProjectID
            WHERE `Status` = "publish"
            
            
            GROUP BY p1.ProjectID 
            ORDER BY vi.project_view DESC
            limit 20
            ) t1
            ORDER BY RAND()
        ';

        $projects = DB::connection('mysql_old')->select($sql);

        return json_encode($projects);
    }

    public function dashboardMemberSpotLight()
    {

        $dataFrom = date('Y-m-d', strtotime('-1 months')) . " 00:00:00";
        $dataTo = date("Y-m-d") . " 00:00:00";

        $sql = '
        
         SELECT
                *
        FROM
                    (
            SELECT 
                p1.UserID as user_id,
                CONCAT(p2.FullName," ",p2.`LastName`) as full_name,	
                CONCAT("https://www.storyrocket.com/", p2.url) as user_url,
                                       (
                            CASE WHEN (p2.photo = "" OR p2.photo = NULL) THEN
                                "https://www.storyrocket.com/public/images/avatar_default_user.png" 
                            ELSE
                                p2.photo	
                            END
                            ) as user_photo,
                p1.ProjectID as project_id,	
                p1.Poster as project_poster,	
      uf.following as following,
                        uf2.followers as followers,  
                ml.local_name
            
            FROM users u 
                LEFT JOIN projects p1 ON p1.UserID = u.UserID            
                LEFT JOIN profile p2 ON p1.UserID = p2.UserID
                LEFT JOIN meta_location ml ON ml.id = p2.CountryID
						 LEFT JOIN (
                            SELECT 
                                UserID, count(UserID) as following
                            FROM users_follow  
                            WHERE follow <> 0
                            GROUP BY UserID
                        ) uf ON uf.UserID = p1.UserID

                        LEFT JOIN (
                            SELECT 
							UserFriend, count(UserFriend) as followers
                            FROM users_follow  
                            WHERE follow <> 0
                            GROUP BY UserFriend
                        ) uf2 ON uf2.UserFriend = p1.UserID
                LEFT JOIN profile_occupation po ON po.UserID = p1.UserID	
                LEFT JOIN occupation o ON po.OccupationID = o.OccupationID 
                                WHERE u.Create >=\'' . $dataFrom . '\'
				AND u.Create <\'' . $dataTo . '\'
                AND `Status` = "publish"
             
            GROUP BY u.UserID 

            
            ) t1
            ORDER BY RAND() limit 15
        ';

        $projects = DB::connection('mysql_old')->select($sql);

        return json_encode($projects);
    }


    public function dashboardProjectSpotLight()
    {

        $dataFrom = date('Y-m-d', strtotime('-1 months')) . " 00:00:00";
        $dataTo = date("Y-m-d") . " 00:00:00";

        $sql = '
        
         SELECT
                        *
                        FROM
                            (
                    SELECT 
                        p1.UserID as user_id,
                        CONCAT(p2.FullName," ",p2.`LastName`) as full_name,	
                        CONCAT("https://www.storyrocket.com/", p2.url) as user_url,	
                        p1.ProjectID as project_id,	
                        p1.ProjectName as project_name,
                        p1.Poster as project_poster,	
                        CONCAT("https://www.storyrocket.com/", p1.urlproj) as project_url,	
                        p1.Synopsis as synopsis,	
                        (
                            CASE WHEN (p2.photo = "" OR p2.photo = NULL) THEN
                                "https://www.storyrocket.com/public/images/avatar_default_user.png" 
                            ELSE
                                p2.photo	
                            END
                            ) as user_photo,
                    
                        g.genre_project,
                        v.project_vote,
                        vi.project_view
                    FROM projects p1
                        LEFT JOIN (
                            
                            SELECT 
                                projects_genre.ProjectID, 
                                GROUP_CONCAT((genre.Genre) SEPARATOR \' | \') as genre_project
                            FROM projects_genre  
                            LEFT JOIN genre ON genre.GenreID = projects_genre.`GenreID`
                            GROUP BY projects_genre.ProjectID
                            
                        ) g ON g.ProjectID = p1.ProjectID    
                        LEFT JOIN profile p2 ON p1.UserID = p2.UserID
                        
                        LEFT JOIN (
                            SELECT 
                                count(ProjectID) as project_vote, ProjectID
                            FROM vote  
                            GROUP BY ProjectID
                        ) v ON v.ProjectID = p1.ProjectID
                        
                        LEFT JOIN (
                            SELECT 
                                count(DateView) as project_view, ProjectID
                            FROM view  
                            GROUP BY ProjectID
                        ) vi ON vi.ProjectID = p1.ProjectID
                    WHERE `Status` = "publish"

                AND p1.CreateDate >=\'' . $dataFrom . '\'
				AND p1.CreateDate <\'' . $dataTo . '\'
				
                    GROUP BY p1.ProjectID                     
                    					ORDER BY p1.ProjectID
                    ) t1
                    ORDER BY RAND() limit 15
        ';

        $projects = DB::connection('mysql_old')->select($sql);

        return json_encode($projects);
    }


    public function dashboardNewMember()
    {

        $dataFrom = date('Y-m-d', strtotime('-1 months')) . " 00:00:00";
        $dataTo = date("Y-m-d") . " 00:00:00";

        $sql = '
        
         SELECT
                        *
                        FROM
                            (
                    SELECT 
                        p1.UserID as user_id,
                        CONCAT(p2.FullName," ",p2.`LastName`) as full_name,
                        (
                                                CASE WHEN (p2.photo = "" OR p2.photo = NULL) THEN
                                                    "https://www.storyrocket.com/public/images/avatar_default_user.png" 
                                                ELSE
                                                    p2.photo	
                                                END
                                                ) as user_photo,	
                        CONCAT("https://www.storyrocket.com/", p2.url) as user_url,
                        p1.ProjectID as project_id,	
                        p1.Poster as project_poster,	
                        uf.following as following,
                        uf2.followers as followers,
                        ml.local_name
                    
                    FROM users u
                        LEFT JOIN projects p1 ON p1.UserID = u.UserID
                        INNER JOIN profile p2 ON p1.UserID = p2.UserID
                        INNER JOIN meta_location ml ON ml.id = p2.CountryID
                        LEFT JOIN (
                            SELECT 
                                UserID, count(UserID) as following
                            FROM users_follow  
                            WHERE follow <> 0
                            GROUP BY UserID
                        ) uf ON uf.UserID = p1.UserID

                        LEFT JOIN (
                            SELECT 
							UserFriend, count(UserFriend) as followers
                            FROM users_follow  
                            WHERE follow <> 0
                            GROUP BY UserFriend
                        ) uf2 ON uf2.UserFriend = p1.UserID
                                                
                        INNER JOIN profile_occupation po ON po.UserID = p1.UserID	
                        INNER JOIN occupation o ON po.OccupationID = o.OccupationID                     

            		                   WHERE u.Create >=\'' . $dataFrom . '\'
						AND u.Create <\'' . $dataTo . '\'
                    and `Status` = "publish"
                    group by u.UserID
                    ORDER BY user_id desc

                    ) t1
                    ORDER BY RAND() LIMIT 30
        ';

        $projects = DB::connection('mysql_old')->select($sql);

        return json_encode($projects);
    }

}
