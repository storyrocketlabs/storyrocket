<?php
/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 18/09/18
 * Time: 23:08
 */

namespace App\Notifications;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\URL;
use Carbon\Carbon;

class VerifyEmail extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * undocumented class variable
     *
     * @var string
     **/
    public $user;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        $this->user = $user;
    }
    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }
    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $user = $this->user;
        $profile = $user->profile;
        $validateEmail = true;
        $message = 'Your Storyrocket account has been created, but you still need to verify your email address in order to activate it.';

        return (new MailMessage)
            ->from('donotreply@storyrocket.com', 'Storyrocket')
            ->subject('Your Storyrocket account has been created')
            ->action(
               'Activate your account',
                $this->verificationUrl($notifiable)
            )
            ->markdown('emails.register', compact('user', 'validateEmail', 'profile', 'message'));
    }
    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }

    /**
     * Get the verification URL for the given notifiable.
     *
     * @param  mixed  $notifiable
     * @return string
     */
    protected function verificationUrl($notifiable)
    {
        return URL::temporarySignedRoute(
            'verification.verify', Carbon::now()->addMinutes(60), ['id' => $notifiable->getKey()]
        );
    }
}
