<?php

namespace App;


use Illuminate\Database\Eloquent\Model;
use DB;

class Material extends Model
{

    protected $table = 'materials';

    protected $fillable = [
        'material',
        'iso',
        'lang_id'
    ];


}
