<?php

namespace App;


use Illuminate\Database\Eloquent\Model;


class UserOld extends Model
{

    protected $table = 'users';
    protected $connection = 'mysql_old';
    protected $primaryKey = 'UserID';
    public $timestamps = false;


}
