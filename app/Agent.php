<?php

namespace App;


use Illuminate\Database\Eloquent\Model;

class Agent extends Model
{

    protected $fillable = [
        'nam_agent',
        'lit_agent',
        'ema_agent',
        'pho_agent',
        'user_id',
        'publish'
    ];


}
