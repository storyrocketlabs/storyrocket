<?php

namespace App;

use App\Billing\Billable;
use App\Billing\Coupon;
use App\Billing\Plan;
use App\Notifications\ResetPasswordEmail;
use App\Notifications\VerifyEmail;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable, Billable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'token',
        'login',
        'facebook_id',
        'imported_user_active',
        'password',
        'last_login',
        'imported_user',
        'active',
        'send_mail',
        'migrated',
        'timetamp',
        'email',
        'email_verified_at',
        'state_active',
        'create',
        'stripe_id',
        'stripe_active',
        'stripe_subscription',
        'subscription_end_at',
        'stripe_plan',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function profile()
    {
        return $this->hasOne(Profile::class);
    }

    public function sendEmailVerificationNotification()
    {
        $this->notify((new VerifyEmail($this))->delay(now()));
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify((new ResetPasswordEmail($this, $token))->delay(now()));
    }

    public function agent()
    {
        return $this->hasOne(Agent::class)->withDefault(['id' => 0]);
    }

    public function credits()
    {
        return $this->hasMany(Credit::class);
    }

    public function awards()
    {
        return $this->hasMany(Award::class);
    }

    public function videos()
    {
        return $this->hasMany(ProfileVideo::class);
    }

    public function settings()
    {
        return $this->hasOne(UserSetting::class);
    }

    public function coupons()
    {
        return $this->belongsToMany(Coupon::class)->withPivot(['fech_reg']);
    }

    public function plan()
    {
        return $this->belongsTo(Plan::class, 'stripe_plan', 'stripe_id');
    }
}
