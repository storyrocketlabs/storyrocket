<?php

namespace App;


use Illuminate\Database\Eloquent\Model;

class Credit extends Model
{

    protected $fillable = [
        'title',
        'position',
        'company',
        'year',
        'url_credit',
        'medium_credit',
        'poster',
        'user_id',
        'description'
    ];


}
