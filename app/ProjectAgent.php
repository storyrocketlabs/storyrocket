<?php

namespace App;


use Illuminate\Database\Eloquent\Model;

class ProjectAgent extends Model
{

    protected $fillable = [
        'name',
        'agency',
        'mail',
        'phone',
        'user_id',
        'project_id',
        'publish',
    ];


}
