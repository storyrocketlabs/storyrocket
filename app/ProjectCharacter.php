<?php

namespace App;


use Illuminate\Database\Eloquent\Model;

class ProjectCharacter extends Model
{

    protected $fillable = [
        'project_id',
        'character_name',
        'gender',
        'age',
        'description',
        'real_actor',
        'image',
        'position',
        'state_charte',
        'image_type',
        'getty_images_id'
    ];


}
