<?php

namespace App;


use Illuminate\Database\Eloquent\Model;

class ProfileView extends Model
{

    protected $fillable = [
        'profile_user_id',
        'visitor_user_id',
        'view',
        'mark',
        'last_visit'
    ];




}
