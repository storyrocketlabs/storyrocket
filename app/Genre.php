<?php

namespace App;


use Illuminate\Database\Eloquent\Model;
use DB;

class Genre extends Model
{

    protected $fillable = [
        'genre',
        'image',
        'iso',
        'lang_id'
    ];


    protected $appends = ['tag'];

    public function getTagAttribute()
    {
        $text = $this->attributes['image'];

        $ext = explode('.jpg', $text);

        if( $ext[0] == 'film-noir'){
            $_ext ='film';
        }elseif($ext[0] == 'dark-comedy'){
            $_ext ='dark';
        }
        elseif($ext[0] == 'graphics-novel'){
            $_ext ='graphics';
        }else{
            $_ext = $ext[0];
        }

        return $_ext;

    }

    public function hasProjects()
    {
        return !is_null(
            DB::table('projects_genre')
                ->join('genres', 'genres.id', '=', 'projects_genre.genre_id')
                ->where('projects_genre.genre_id', $this->attributes['id'])
                ->first()
        );
    }

    public function projects()
    {
        return $this->belongsToMany(Project::class, 'projects_genre');
    }

    public function genres()
    {
        return $this->belongsToMany(Genre::class,'projects_genre');
    }


}
