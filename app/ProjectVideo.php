<?php

namespace App;


use Illuminate\Database\Eloquent\Model;

class ProjectVideo extends Model
{

    protected $fillable = [
        'title_video',
        'url_video',
        'project_id',
        'm_type',
        'm_view',
        'm_date',
    ];


}
