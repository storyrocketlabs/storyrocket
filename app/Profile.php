<?php

namespace App;


use Illuminate\Database\Eloquent\Model;
use DateTime;

class Profile extends Model
{

    protected $fillable = [
        'full_name',
        'last_name',
        'email',
        'gender',
        'photo',
        'photo_new',
        'facebook_photo',
        'website',
        'company',
        'day',
        'month',
        'year',
        'country_id',
        'state',
        'city',
        'zipcode',
        'facebook',
        'twitter',
        'google',
        'linkedin',
        'youtube',
        'Instagram',
        'who_am_i',
        'casting',
        'trailer',
        'on_web',
        'url',
        'headline',
        'description',
        'hoping',
        'dream',
        'awards',
        'body_work',
        'banner',
        'background_position',
        'idbm',
        'tmblr',
        'reddit',
        'birthday',
        'user_id'
    ];

    protected $appends = ['day', 'month', 'year'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function registerVisit()
    {

    }

    public function occupations()
    {
        return $this->belongsToMany(Occupation::class, 'profile_occupation');
    }

    public function country()
    {
        return $this->belongsTo(Country::class, 'country_id');
    }

    public function metaLocation()
    {
        return $this->belongsTo(MetaLocation::class, 'state');
    }

    public function languages()
    {
        return $this->belongsToMany(Language::class, 'profile_language');
    }


    public function getPhotoAttribute()
    {

        if(!empty($this->attributes['facebook_photo'])) {
            return $this->attributes['facebook_photo'];
        }

        if(!empty($this->attributes['photo']  and $this->attributes['photo'] != "avatar_default_user.png")) {
            return env('AWS_URL').'/'.$this->attributes['photo'];
        }

        return asset('images/avatar_default_user.png');
    }


    public function getDayAttribute()
    {

        if(!empty($this->attributes['birthday']))
        {
            $date = explode('-', $this->attributes['birthday']);

            return (isset($date[2]) && is_numeric($date[2])) ? $date[2] : null;

        }

        return null;
    }

    public static function timeElapsedString($datetime, $full = false)
    {
        $now = new DateTime;
        $ago = new DateTime($datetime);
        $diff = $now->diff($ago);

        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;

        $string = array(
            'y' => 'year',
            'm' => 'month',
            'w' => 'week',
            'd' => 'day',
            'h' => 'hour',
            'i' => 'minute',
            's' => 'second',
        );
        foreach ($string as $k => &$v) {
            if ($diff->$k) {
                $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
            } else {
                unset($string[$k]);
            }
        }

        if (!$full) $string = array_slice($string, 0, 1);
        return $string ? implode(', ', $string) . ' ago' : 'just now';
    }

    public function getMonthAttribute()
    {
        if(!empty($this->attributes['birthday']))
        {
            $date = explode('-', $this->attributes['birthday']);

            return (isset($date[1]) && is_numeric($date[1])) ? $date[1] : null;

        }

        return null;
    }

    public function getYearAttribute()
    {
        if(!empty($this->attributes['birthday']))
        {
            $date = explode('-', $this->attributes['birthday']);

            return (isset($date[0]) && is_numeric($date[0])) ? $date[0] : null;

        }

        return null;
    }

    public function scopeFilterOccupation($query, $value)
    {
        if(!empty($value))
        {
            return $query->where('occupations.id', $value);
        }
    }


}
