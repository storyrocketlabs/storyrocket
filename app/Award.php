<?php

namespace App;


use Illuminate\Database\Eloquent\Model;

class Award extends Model
{

    protected $fillable = [
        'title_award',
        'year_award',
        'nominate_award',
        'category_award',
        'result_award',
        'description_award',
        'user_id'
    ];


}
