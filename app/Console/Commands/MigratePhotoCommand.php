<?php

namespace App\Console\Commands;

use App\Profile;
use Illuminate\Console\Command;

class MigratePhotoCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'photo-migrate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command for migrate, photo profile users';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        ini_set('memory_limit', '-1');

        $profiles = Profile::all();

        foreach ($profiles as $profile)
        {
            $name = substr($profile->photo, strrpos($profile->photo, '/') + 1);

            $profile->photo = $name;
            $profile->save();

        }



    }
}
