<?php

namespace App\Console\Commands;

use App\Project;
use Illuminate\Console\Command;

class MigratePosterCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'poster-migrate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command for migrate poster.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        ini_set('memory_limit', '-1');

        $projects = Project::all();


        foreach ($projects as $project)
        {
            $name = substr($project->poster, strrpos($project->poster, '/') + 1);
            $project->poster = $name;

            if(!empty($project->poster))
            {
                $project->publish = 1;
            }else{
                $project->publish = 0;
            }

            $project->save();

        }

    }
}
