<?php

namespace App\Http\Controllers;

use App\MetaLocation;
use Illuminate\Http\Request;
use DB;


class ListsController extends Controller
{

    public function states(Request $request)
    {
        $country_id = $request->get('country-id');

        $states = MetaLocation::select('local_name', 'id')->where('in_location', $country_id)->where('type', 'RE')->get();


        return response()->json(['success'=>true, 'data'=>$states]);

    }

}
