<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Profile;
use Illuminate\Support\Facades\Auth;

class GroupsController extends Controller
{
    //
    public function index()
    {

        if(auth()->user()->isActive())
        {
            return redirect()->route('subscription');
        }

        $profile = Profile::select('id', 'full_name', 'last_name', 'state', 'city', 'country_id', 'photo',
            'facebook_photo', 'state','url')
            ->where('user_id', Auth::id())
            ->first();

        return view('groups.index', compact('profile'));
    }
    public function viewGroups($slug)
    {
        if(auth()->user()->isActive())
        {
            return redirect()->route('subscription');
        }

        $profile = Profile::select('id', 'full_name', 'last_name', 'state', 'city', 'country_id', 'photo',
            'facebook_photo', 'state','url')
            ->where('user_id', Auth::id())
            ->first();
        return view('groups.show', compact('profile'));
    }
}
