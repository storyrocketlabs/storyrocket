<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Profile;
use Illuminate\Support\Facades\Auth;

class SearchMainController extends Controller
{
    public function index(Request $request)
    {

        $data = $request->all();

        if (!isset($data['user']) || empty($data['user']))
        {
            return redirect()->back();
        }

        if(count($data) == 1 && isset($data['user']))
        {
            $request->offsetSet('trending', 1);
            $request->offsetSet('view', 1);
        }

        $projects = $this->getProjects($request->all());

        $genres = DB::connection('mysql_old')
            ->table('genre')
            ->select('GenreID', 'Genre')
            ->where('iso', 'like', '%en%')
            ->get();

        $materials = DB::connection('mysql_old')
            ->table('material')
            ->select('MaterialID', 'Material')
            ->where('iso', 'like', '%en%')
            ->get();

        $mediums = DB::connection('mysql_old')
            ->table('medium')
            ->select('MediumID', 'Medium')
            ->where('iso', 'like', '%en%')
            ->get();

        $languages = DB::connection('mysql_old')
            ->table('language')
            ->select('language.LanguageID', 'language.Language')
            ->join('projects', 'projects.LanguageID', '=', 'language.LanguageID')
            ->where('iso', 'like', '%en%')
            ->groupBy('language.LanguageID')
            ->get();

        $eras = DB::connection('mysql_old')
            ->table('era')
            ->select('EraID', 'Era')
            ->where('iso', 'like', '%en%')
            ->get();

        $locations = DB::connection('mysql_old')
            ->table('meta_location')
            ->select('meta_location.id', 'meta_location.local_name')
            ->join('projects', 'projects.PrincipalLocation', '=', 'meta_location.id')
            ->where('type', 'CO')
            ->groupBy('meta_location.id')
            ->get();

        $profile = DB::connection('mysql_old')
            ->table('profile')
            ->select('UserID', 'FullName', 'LastName', 'State', 'City', 'CountryID', 'Photo',
                'FacebookPhoto', 'State','url')
            ->where('url', $data['user'])
            ->first();

        $nextPage = 0;

        if(count($data) == 1 && isset($data['user']))
        {
            $projects = $projects->shuffle()->take(12);
        }else{
            if($projects->currentPage() < $projects->lastPage())
            {
                $nextPage = $projects->currentPage() + 1;
            }
        }


        if(count($data) == 1 && isset($data['user']))
        {
            $data = [];
        }



        return view('search.index', compact('projects', 'genres', 'materials', 'mediums', 'languages', 'eras', 'locations', 'data', 'profile', 'nextPage'));
    }


    public function getJsonResults(Request $request){

        $projects = $this->getProjects($request->all());

        return response()->json($projects);
    }


    public function getProjects($data)
    {
        $projects = DB::connection('mysql_old')
            ->table('projects')
            ->selectRaw('projects.ProjectName, projects.ProjectID as project_id, projects.UserID, projects.Poster, 
            projects.Synopsis,profile.FullName, profile.LastName, profile.Photo,profile.FacebookPhoto, profile.url as profile_url, projects.urlproj as project_url,
            (SELECT GROUP_CONCAT(DISTINCT genre.Genre
        ORDER BY genre.Genre ASC
        SEPARATOR " | ") FROM genre JOIN projects_genre ON projects_genre.GenreID = genre.GenreID WHERE  projects_genre.ProjectID = project_id ) as genres,
            (select count(view.ProjectID) as views from view where view.ProjectID = project_id) as views')
            ->join('profile', 'profile.UserID', '=', 'projects.UserID')
            ->join('projects_medium', 'projects_medium.ProjectID', '=', 'projects.ProjectID')
            ->join('projects_genre', 'projects_genre.ProjectID', '=', 'projects.ProjectID')
            ->whereNotNull('projects.Poster');

        if(isset($data['genre']) & !empty($data['genre'])){
            $projects->where('projects_genre.GenreID', $data['genre']);
        }


        if(isset($data['material']) & !empty($data['material'])){
            $projects->where('projects.MaterialID', $data['material']);
        }

        if(isset($data['medium']) & !empty($data['medium'])){
            $projects->where('projects_medium.MediumID', $data['medium']);
        }

        if(isset($data['language']) & !empty($data['language'])){
            $projects->where('projects.LanguageID', $data['language']);
        }

        if(isset($data['carousel']) & !empty($data['carousel'])){
            //$projects->where('projects.EraID', $data['era']);
            $projects->where('projects.spotligth', 1);
        }

        if(isset($data['era']) & !empty($data['era'])){
            $projects->where('projects.EraID', $data['era']);
        }

        if(isset($data['q']) & !empty($data['q'])){
            $projects->where('projects.ProjectName','like' ,'%'.$data['q'].'%');
        }

        if(isset($data['location']) & !empty($data['location'])){
            $projects->where('projects.PrincipalLocation', $data['location']);
        }

        if(isset($data['views']) & !empty($data['views'])){
            $projects->orderBy('views', 'DESC');
        }

        if(isset($data['latest']) & !empty($data['latest'])){
            $projects->orderBy('projects.date_publish', 'DESC');
        }

        $projects->groupBy('projects.ProjectID');

        if(isset($data['trending']) & !empty($data['trending'])){
            $projects = $projects->take(50)->get();
            /*
             * ->shuffle()->take(12)
             */
        }else{
            $projects = $projects->paginate(12);

        }


        return $projects;
    }

}
