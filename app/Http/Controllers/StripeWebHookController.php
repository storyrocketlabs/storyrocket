<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\User;

class StripeWebHookController extends Controller
{

    public function __construct()
    {
        $this->middleware('guest');
    }

    public function handle(Request $request)
    {
        $payload = $request->all();
        $method = $this->eventToMethod($payload['type']);

        if (method_exists($this, $method)) {
            $this->$method($payload);
        }

        return response()->json('Webhook Received', 200);

    }

    public function whenCustomerSubscriptionDeleted($payload)
    {

        $user = $this->retrieveUser($payload);

        $user->deactivate();

    }

    public function whenChargeSucceeded($payload)
    {
        $user = $this->retrieveUser($payload);

        $plan = $user->plan;

        $add_months = 0;

        if($plan->cycle == 'year')
        {
            $add_months = 12;
        }else if($plan->cycle == 'month'){
            $add_months = 1;
        }

        $user->payments()->create([
            'amount'=>$payload['data']['object']['amount'],
            'charge_id'=>$payload['data']['object']['id'],
            'user_id'=>$user->id,
            'method'=>'Stripe',
            'plan_id'=>$plan->id,
            'date_start'=>date('Y-m-d H:i:s'),
            'date_end'=>Carbon::now()->addMonths($add_months)->format('Y-m-d H:i:s')
        ]);

    }

    public function eventToMethod($event)
    {

        return 'when' . studly_case(str_replace('.', '_', $event));
    }


    public function retrieveUser($payload)
    {
        return User::byStripeId($payload['data']['object']['customer']);

    }

}
