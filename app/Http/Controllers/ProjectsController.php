<?php

namespace App\Http\Controllers;

use App\Genre;
use App\Era;
use App\Comment;
use App\MetaLocation;
use App\ProjectCharacter;
use App\Occupation;
use App\Profile;
use App\Project;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class ProjectsController extends Controller
{
    public function index()
    {
        echo "index";
    }

    public function create(Request $request)
    {
        $genresToggle = Genre::where('iso', 'en')->orderBy('genre', 'ASC')->limit(21)->get();
        if (Auth::check()) {
            $genres = Genre::where('iso', 'en')->orderBy('genre', 'ASC')->limit(21)->get();
            $profile = Profile::select('id', 'full_name', 'last_name', 'state', 'city', 'country_id', 'photo',
                'facebook_photo', 'state', 'url')
                ->where('user_id', Auth::id())
                ->first();

            $state = MetaLocation::select('id', 'local_name')->where('id', $profile->state)->first();
            $city = MetaLocation::select('id', 'local_name')->where('id', $profile->city)->first();
            $country = MetaLocation::select('id', 'local_name')->where('id', $profile->country_id)->first();
            $occupation = $profile->occupations()->first();

            $occupations = [];
            $countries = [];
        } else {

            $genres = [];
            $profile = [];
            $state = [];
            $city = [];
            $country = [];
            $occupation = [];

            $occupations = Occupation::where('iso', 'en')->pluck('occupation', 'id');
            $countries = MetaLocation::where('type', 'CO')->pluck('local_name', 'id');
        }

        $projects = Project::selectRaw('projects.project_name, projects.id as project_id, projects.user_id, projects.poster, 
            projects.synopsis,profiles.full_name, profiles.last_name, profiles.photo,profiles.facebook_photo, profiles.url as profile_url, projects.urlproj as project_url,
            (SELECT GROUP_CONCAT(DISTINCT genres.genre
        ORDER BY genres.genre ASC
        SEPARATOR " | ") FROM genres INNER JOIN projects_genre ON projects_genre.genre_id = genres.id WHERE  projects_genre.project_id = projects.id ) as genres,
            (select count(views.project_id) as views from views where views.project_id = projects.id) as views')
            ->join('profiles', 'profiles.user_id', '=', 'projects.user_id')
            ->leftJoin('projects_medium', 'projects_medium.project_id', '=', 'projects.id')
            ->leftJoin('projects_genre', 'projects_genre.project_id', '=', 'projects.id')
            ->where('projects.user_id', auth()->user()->id)
            ->groupBy('projects.id')
            ->get();

        $open_modal = $request->get('modal-create');


        return view('projects.create', compact('genres', 'genresToggle', 'profile', 'occupation', 'state',
            'city', 'country', 'projects', 'occupations', 'countries', 'states', 'open_modal'));
    }

    public function storeProject(Request $request)
    {
        DB::beginTransaction();

        try {

            $data = $request->all();
            $data['user_id'] = \auth()->user()->id;
            $project = Project::create($data);

            DB::commit();

            return response()->json(['success' => true, 'project' => $project]);

        } catch (\Exception $exception) {
            DB::rollBack();

            return response()->json(['success' => false, 'message' => $exception->getMessage()]);

        }

    }


    public function updateProject(Project $project, Request $request)
    {
        DB::beginTransaction();
        try {

            $data = $request->all();

            $project->update($data);

            DB::commit();

            return response()->json(['success' => true, 'project' => $project]);

        } catch (\Exception $exception) {

            DB::rollBack();

            return response()->json(['success' => false, 'message' => $exception->getMessage()]);

        }

    }

    public function updatePoster(Project $project, Request $request)
    {

        $data = $request->all();

        $pos = strpos($data['poster'], ';');
        $type = explode(':', substr($data['poster'], 0, $pos))[1];
        $type = explode('/', $type);
        $file_image = explode(',', $data['image']);
        $image = base64_decode($file_image[1]);
        $fileName = substr(md5(uniqid(rand(), true)), 0, 10) . '.' . strtolower($type[1]);

        if (!env('PLUGINS_PROD')) {
            $fileName = 'dev/' . $fileName;
        }

        \Storage::disk('public')->put('temp/' . $fileName, $image);

        $project->poster = $fileName;
        $project->save();

        $job = (new OptimizeImages(storage_path('app/public/temp/' . $fileName)))->delay(0);

        $this->dispatch($job);


        return response()->json(['success' => true]);

    }

    public function writeUsers(Request $request)
    {
        $users = Profile::selectRaw('profiles.id,profiles.full_name, profiles.last_name, profiles.user_id, profiles.photo,profiles.facebook_photo, profiles.email')
            ->join('profile_occupation', 'profile_occupation.profile_id', '=', 'profiles.id')
            ->join('occupations', 'occupations.id', '=', 'profile_occupation.occupation_id')
            ->filterOccupation($request->get('occupation'))
            ->whereRaw("CONCAT(profiles.full_name, ' ', profiles.last_name) like '%" . $request->get('q') . "%'")
            ->get()->take(10);

        return response()->json(['success' => true, 'users' => $users]);
    }

    public function projectPage($projectSlug)
    {

        $profile = Profile::select('id', 'full_name', 'last_name', 'state', 'city', 'country_id', 'photo',
            'facebook_photo', 'state', 'url')
            ->where('user_id', Auth::id())
            ->first();

        $paramWritter = 1;
        $project = Project::with([
                'genres' => function ($query) {
                    $query->select('genres.id', 'genres.genre');
                },
                'mediums' => function ($query) {
                    $query->select('mediums.id', 'mediums.medium');
                },
                'language' => function ($query) {
                    $query->select('languages.id', 'languages.language');
                }
            ])->selectRaw('
                projects.*,
                COUNT(DISTINCT(comments.id)) as "comments",
                COUNT(DISTINCT(votes.id)) as "votes",            
                COUNT(DISTINCT(views.id)) as "views",     
                project_writers.id as "project_writers_id",
                project_writers.writer_email,
                project_writers.writer_name,
                profiles_writer.url as "profiles_writer_url",
                occupations.occupation,
                profiles.full_name,
                profiles.last_name,
                profiles.url as "url_profile",
                materials.material,
                eras.era,                  
                ratings.term as "rating_term",
                ml1.local_name as "country", 
                ml2.local_name as "state", 
                ml3.local_name as "city"'
            )
            ->leftJoin('comments', 'comments.project_id', '=', 'projects.id')
            ->leftJoin('votes', 'votes.project_id', '=', 'projects.id')
            ->leftJoin('views', 'views.project_id', '=', 'projects.id')
            ->leftJoin('project_writers', function($q) use ($paramWritter)
            {
                $q->on('project_writers.project_id', '=', 'projects.id')
                    ->where('project_writers.params', '=', $paramWritter);
            })
            ->leftJoin('profiles as profiles_writer', 'project_writers.writer_email', '=', 'profiles_writer.email')
            ->leftJoin('occupations', 'occupations.id', '=', 'projects.occupation_id')
            ->leftJoin('profiles', 'profiles.user_id', '=', 'projects.user_id')
            ->leftJoin('materials', 'materials.id', '=', 'projects.material_id')
            ->leftJoin('eras', 'eras.id', '=', 'projects.era_id')
            ->leftJoin('ratings', 'ratings.id', '=', 'projects.rating_id')
            ->leftJoin('meta_location as ml1', 'ml1.id', '=', 'projects.principal_location')
            ->leftJoin('meta_location as ml2', 'ml2.id', '=', 'projects.principal_state')
            ->leftJoin('meta_location as ml3', 'ml3.id', '=', 'projects.city')
            ->where('projects.urlproj', $projectSlug)
            ->where('projects.publish', 1)
            ->where('projects.status', 'publish')
            ->groupBy('projects.id')
            ->first();


        if (!isset($project->id)) {

            $user = new ProfileController();

            return $user->showProfile($projectSlug);

        } else {

            $projectCharacter = ProjectCharacter::select('id', 'character_name', 'gender', 'age', 'description', 'real_actor', 'image',
                'position', 'state_charte', 'image_type', 'getty_images_id')
                ->where('project_id', $project->id)
                ->get();

            $projectAboutThisMember = Project::select("poster","project_name", "urlproj")
                ->leftJoin('profiles', 'profiles.user_id', '=', 'projects.user_id')
                ->where('projects.publish', 'on')
                ->where('projects.urlproj','!=', $projectSlug)
                ->where('projects.status', 'publish')
                ->where("projects.user_id",$project->user_id)
                ->get();

            $comments = Comment::select("*")
                ->where('project_id',$project->id)
                ->get();
            
                    
            return view('projects.index', compact('project', 'profile', 'projectCharacter', 'projectAboutThisMember'));
        }

    }


}
