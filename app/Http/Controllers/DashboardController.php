<?php

namespace App\Http\Controllers;

use App\Billing\Plan;
use App\Billing\Subscription;
use App\Http\Requests\RegisterSubscriptionForm;
use App\Profile;
use App\Project;
use App\User;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Auth;
use phpDocumentor\Reflection\DocBlock\Tags\Reference\Url;
use Stripe\Token;


class DashboardController extends Controller
{

    public function index()
    {

        $profile = Profile::select('id', 'full_name', 'last_name', 'state', 'city', 'country_id', 'photo',
            'facebook_photo', 'state','url')
            ->where('user_id', Auth::id())
            ->first();

        $user = User::where('id',1163)->first();
        $settings = $user->settings;

        $profile_dashboard = null;
        $project = null;

        if(isset($settings->values->ps_add_my_projects_to_project_spotlight) && $settings->values->ps_add_my_projects_to_project_spotlight == 1)
        {
            $project = Project::where('id', 163)->first();
        }


        if(isset($settings->values->ps_addme_to_profile_spotlight) && $settings->values->ps_addme_to_profile_spotlight == 1)
        {
            $profile_dashboard = $user->profile;
        }



        return view('profile.dashboard', compact('profile', 'profile_dashboard', 'project'));
    }

    public function newProjects(Project $project)
    {
        /**
         * Lastest 15 projects created of the month if refresh page then change photo
         *
         */
        $projectData = $project->dashboardNewProjects();
        echo $projectData;
    }

    public function trendingProjects(Project $project)
    {
        /**
         * 5 projects with most likes in the last week if refresh page then change photo
         */

        $projectData = $project->dashboardTrendingProjects();
        echo $projectData;
    }

    public function mostViewedProjects(Project $project)
    {
        /**
         * 15 projects with most viewed if refresh page then change photo
         */
        $projectData = $project->dashboardMostViewedProjects();
        echo $projectData;

    }

    public function memberSpotLight(Project $project)
    {
        /**
         * Lastest 10 members with projects created in the last month if refresh page then change photo
         */
        $projectData = $project->dashboardMemberSpotLight();
        echo $projectData;

    }

    public function projectSpotLight(Project $project)
    {
        /**
         * Lastest 10 projects if refresh page then change photo
         */
        $projectData = $project->dashboardProjectSpotLight();
        echo $projectData;

    }

    public function newMember(Project $project)
    {
        /**
         * Lastest 5 members created in the last month if refresh page then change photo
         */
        $projectData = $project->dashboardNewMember();
        echo $projectData;
    }



}