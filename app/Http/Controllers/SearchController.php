<?php

namespace App\Http\Controllers;

use App\Project;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Profile;
use App\Genre;
use App\Material;
use App\Medium;
use App\Language;
use App\Era;
use App\MetaLocation;
use Illuminate\Support\Facades\Auth;

class SearchController extends Controller
{
    public function index(Request $request)
    {

        $data = $request->all();

        if(count($data) == 1) {
            $request->offsetSet('trending', 1);
            $request->offsetSet('view', 1);
        }

        $projects = $this->getProjects($request->all());

        $genres = Genre::select('id', 'genre')
            ->where('iso', 'like', '%en%')
            ->get();


        $materials = Material::select('id', 'material')
            ->where('iso', 'like', '%en%')
            ->get();

        $mediums = Medium::select('id', 'medium')
            ->where('iso', 'like', '%en%')
            ->get();

        $languages = Language::select('languages.id', 'languages.language')
            ->join('projects', 'projects.language_id', '=', 'languages.id')
            ->where('iso', 'like', '%en%')
            ->groupBy('languages.id')
            ->get();

        $eras = Era::select('id', 'era')
            ->where('iso', 'like', '%en%')
            ->get();

        $locations = MetaLocation::select('meta_location.id', 'meta_location.local_name')
            ->join('projects', 'projects.principal_location', '=', 'meta_location.id')
            ->where('type', 'CO')
            ->groupBy('meta_location.id')
            ->get();


        $profile = Profile::select('id', 'full_name', 'last_name', 'state', 'city', 'country_id', 'photo',
            'facebook_photo', 'state','url')
            ->where('user_id', Auth::id())
            ->first();

        $nextPage = 0;

        if(count($data) == 1) {
            $data = [];
            $projects = $projects->shuffle()->take(12);
        } else {
            if($projects->currentPage() < $projects->lastPage()) {
                $nextPage = $projects->currentPage() + 1;
            }
        }


        return view('search.index', compact('projects', 'genres', 'materials', 'mediums', 'languages', 'eras', 'locations', 'data', 'profile', 'nextPage'));
    }


    public function getJsonResults(Request $request){

        $projects = $this->getProjects($request->all());

        return response()->json($projects);
    }


    public function getProjects($data)
    {
        $projects = Project::selectRaw('projects.project_name, projects.id as project_id, projects.user_id, projects.poster, 
            projects.synopsis, profiles.full_name, profiles.last_name, profiles.photo,profiles.facebook_photo, profiles.url as profile_url, projects.urlproj as project_url,
            (SELECT GROUP_CONCAT(DISTINCT genres.genre
        ORDER BY genres.genre ASC
        SEPARATOR " | ") FROM genres JOIN projects_genre ON projects_genre.id = genres.id WHERE  projects_genre.project_id = project_id ) as genres,
            (select count(views.project_id) as views from views where views.project_id = project_id) as views')
            ->join('profiles', 'profiles.user_id', '=', 'projects.user_id')
            ->join('projects_medium', 'projects_medium.project_id', '=', 'projects.id')
            ->join('projects_genre', 'projects_genre.project_id', '=', 'projects.id')
            ->where('projects.status', "publish")
            ->whereNotNull('projects.poster');


        if(isset($data['genre']) & !empty($data['genre'])) {
            $projects->where('projects_genre.genre_id', $data['genre']);
        }


        if(isset($data['material']) & !empty($data['material'])) {
            $projects->where('projects.material_id', $data['material']);
        }

        if(isset($data['medium']) & !empty($data['medium'])) {
            $projects->where('projects_medium.medium_id', $data['medium']);
        }

        if(isset($data['language']) & !empty($data['language'])) {
            $projects->where('projects.language_id', $data['language']);
        }

        if(isset($data['carousel']) & !empty($data['carousel'])) {
            //$projects->where('projects.EraID', $data['era']);
            $projects->where('projects.spotligth', 1);
        }

        if(isset($data['era']) & !empty($data['era'])) {
            $projects->where('projects.era_id', $data['era']);
        }

        if(isset($data['q']) & !empty($data['q'])) {
            $projects->where('projects.project_name','like' ,'%'.$data['q'].'%');
        }

        if(isset($data['location']) & !empty($data['location'])) {
            $projects->where('projects.principal_location', $data['location']);
        }

        if(isset($data['views']) & !empty($data['views'])) {
            $projects->orderBy('views', 'DESC');
        }

        if(isset($data['latest']) & !empty($data['latest'])) {
            $projects->orderBy('projects.date_publish', 'DESC');
        }

        $projects->groupBy('projects.id');

        if(isset($data['trending']) & !empty($data['trending'])) {
            $projects = $projects->take(50)->get();
            /*
             * ->shuffle()->take(12)
             */
        } else {
            $projects = $projects->paginate(12);
        }

        return $projects;
    }

}
