<?php

namespace App\Http\Controllers;

use App\Billing\Plan;
use App\Billing\Subscription;
use App\Http\Requests\RegisterSubscriptionForm;
use App\Profile;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Auth;
use Stripe\Token;


class PlansController extends Controller
{

    public function index()
    {
        if(auth()->user()->isActive())
        {
            return redirect()->route('subscription');
        }

        $profile = Profile::select('id', 'full_name', 'last_name', 'state', 'city', 'country_id', 'photo',
            'facebook_photo', 'state','url')
            ->where('user_id', Auth::id())
            ->first();

        return view('plans.index', compact('profile'));
    }

    public function subscriptionForm(Plan $plan)
    {

        $profile = Profile::select('id', 'full_name', 'last_name', 'state', 'city', 'country_id', 'photo',
            'facebook_photo', 'state','url')
            ->where('user_id', Auth::id())
            ->first();
       // var_dump($plan);
        return view('plans.subscription-form', compact('profile', 'plan'));
    }

    public function validateCoupon(Plan $plan, Request $request)
    {
        $subscription = new Subscription(auth()->user());

        $coupon = $subscription->validateCoupon($request->get('coupon'));

        if(!empty($coupon) && $plan->cycle == 'annually')
        {
            $coupon = $subscription->retrieveCoupon($coupon);

            return response()->json(['success'=>true, 'coupon'=>$coupon]);
        }

        return response()->json(['success'=>false]);
    }

    public function storeSubscription(Plan $plan, RegisterSubscriptionForm $registerSubscriptionForm)
    {


        try {
            $token = Token::create(["card"=>[
                "number" => $registerSubscriptionForm->get('card-number'),
                "exp_month" => $registerSubscriptionForm->get('month'),
                "exp_year" => $registerSubscriptionForm->get('year'),
                "cvc" => $registerSubscriptionForm->get('card-cvc'),
                "name" => $registerSubscriptionForm->get('card-first-name').' '.$registerSubscriptionForm->get('card-last-name')
            ]]);

            auth()->user()
                ->subscription()
                ->usingCoupon($registerSubscriptionForm->coupon)
                ->create($plan, $token);

            return redirect()->route('home');

        } catch (\Exception $exception) {

            return back();
        }

    }

    public function subcription()
    {


        $profile = Profile::select('id', 'full_name', 'last_name', 'state', 'city', 'country_id', 'photo',
            'facebook_photo', 'state','url','email')
            ->where('user_id', Auth::id())
            ->first();

        $plan = Plan::where('stripe_id', auth()->user()->stripe_plan)->first();


        return view('plans.subscription', compact('profile', 'plan'));

    }

}
