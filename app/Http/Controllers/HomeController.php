<?php

namespace App\Http\Controllers;

use App\Genre;
use App\MetaLocation;
use App\Occupation;
use App\Profile;
use App\Project;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $genresToggle = Genre::where('iso', 'en')->orderBy('genre', 'ASC')->limit(21)->get();
        if(Auth::check())
        {
            $genres = Genre::where('iso', 'en')->orderBy('genre', 'ASC')->limit(21)->get();
            $profile = Profile::select('id', 'full_name', 'last_name', 'state', 'city', 'country_id', 'photo',
                'facebook_photo', 'state','url')
                ->where('user_id', Auth::id())
                ->first();


            $state = MetaLocation::select('id', 'local_name')->where('id', $profile->state)->first();
            $city = MetaLocation::select('id', 'local_name')->where('id', $profile->city)->first();
            $country = MetaLocation::select('id', 'local_name')->where('id', $profile->country_id)->first();
            $occupation = $profile->occupations()->first();

            $occupations = [];
            $countries = [];
        }else{
            //$genres = [];
            $genres = Genre::where('iso', 'en')->orderBy('genre', 'ASC')->limit(21)->get();
            $profile = [];
            $state = [];
            $states = [];
            $city = [];
            $country = [];
            $occupation = [];

            $occupations = Occupation::where('iso', 'en')->pluck('occupation','id');
            $countries = MetaLocation::where('type', 'CO')->pluck('local_name','id');
        }


        $projectsGet = DB::select(DB::raw("
                SELECT count(views.project_id) AS views,
                    views.project_id,
                    profiles.id as profile_id,
                    projects.log_line,
                    projects.rating_id,
                    projects.tagline,
                    projects.id,
                    projects.project_name,
                    projects.poster,
                    profiles.full_name, profiles.last_name, state.local_name as state,
                    profiles.photo as avatar,
                    country.local_name as country, city.local_name as city
                    FROM views
                  INNER JOIN projects ON projects.id = views.project_id
                  INNER JOIN profiles ON profiles.user_id = views.user_id
                  INNER JOIN meta_location country ON  country.id = profiles.country_id
                  INNER JOIN meta_location state ON  state.id = profiles.state
                  INNER JOIN meta_location city ON  city.id = profiles.city
                WHERE projects.status = 'publish'
                GROUP BY views.project_id
                ORDER BY RAND(), views DESC
                LIMIT 5
                "));

        $projects = array();
        foreach($projectsGet as $projectsGetVal) {
            $projectsGetVal->poster_aws_url = Project::getBaseStaticAWS($projectsGetVal->poster);
            $projectsGetVal->avatar_aws_url = Project::getBaseStaticAWS($projectsGetVal->avatar);
            array_push($projects, $projectsGetVal);
        }

        return view('home', compact('genres', 'genresToggle', 'profile', 'occupation', 'state',
            'city', 'country', 'projects', 'occupations', 'countries', 'states'));

    }
    function restorePass()
    {
        $profile = [];
        $genres = [];
        return view('info.restorepass', compact('genres', 'profile'));
    }
    function aboutUs()
    {
        $profile = [];
        $genres = [];
        if(Auth::check()) {
            $genres = Genre::where('iso', 'en')->orderBy('genre', 'ASC')->limit(21)->get();
            $profile = Profile::select('id', 'full_name', 'last_name', 'state', 'city', 'country_id', 'photo',
                'facebook_photo', 'state', 'url')
                ->where('user_id', Auth::id())
                ->first();
        }

        return view('info.aboutus', compact('genres', 'profile'));
    }

    function faqs()
    {
        $profile = [];
        $genres = [];
        if(Auth::check()) {
            $genres = Genre::where('iso', 'en')->orderBy('genre', 'ASC')->limit(21)->get();
            $profile = Profile::select('id', 'full_name', 'last_name', 'state', 'city', 'country_id', 'photo',
                'facebook_photo', 'state', 'url')
                ->where('user_id', Auth::id())
                ->first();
        }

        return view('info.faqs', compact('genres', 'profile'));
    }

    function tutorials()
    {
        $profile = [];
        $genres = [];
        if(Auth::check()) {
            $genres = Genre::where('iso', 'en')->orderBy('genre', 'ASC')->limit(21)->get();
            $profile = Profile::select('id', 'full_name', 'last_name', 'state', 'city', 'country_id', 'photo',
                'facebook_photo', 'state', 'url')
                ->where('user_id', Auth::id())
                ->first();
        }

        return view('info.tutorials', compact('genres', 'profile'));
    }

    function privacyPolicy()
    {
        $profile = [];
        $genres = [];
        if(Auth::check()) {
            $genres = Genre::where('iso', 'en')->orderBy('genre', 'ASC')->limit(21)->get();
            $profile = Profile::select('id', 'full_name', 'last_name', 'state', 'city', 'country_id', 'photo',
                'facebook_photo', 'state', 'url')
                ->where('user_id', Auth::id())
                ->first();
        }

        return view('info.privacy-policy', compact('genres', 'profile'));
    }

    function termOfService()
    {
        $profile = [];
        $genres = [];
        if(Auth::check()) {
            $genres = Genre::where('iso', 'en')->orderBy('genre', 'ASC')->limit(21)->get();
            $profile = Profile::select('id', 'full_name', 'last_name', 'state', 'city', 'country_id', 'photo',
                'facebook_photo', 'state', 'url')
                ->where('user_id', Auth::id())
                ->first();
        }

        return view('info.term-of-service', compact('genres', 'profile'));
    }

    function copyright()
    {
        $profile = [];
        $genres = [];
        if(Auth::check()) {
            $genres = Genre::where('iso', 'en')->orderBy('genre', 'ASC')->limit(21)->get();
            $profile = Profile::select('id', 'full_name', 'last_name', 'state', 'city', 'country_id', 'photo',
                'facebook_photo', 'state', 'url')
                ->where('user_id', Auth::id())
                ->first();
        }

        return view('info.copyright', compact('genres', 'profile'));
    }
}
