<?php 
namespace App\Http\Controllers;

use App\Message;
use Illuminate\Http\Request;
use App\Occupation;
use App\MetaLocation;
use App\Profile;
use DB;
use Illuminate\Support\Facades\Auth;
use phpDocumentor\Reflection\DocBlock\Tags\Reference\Url;
use Stripe\Token;

class MailController extends Controller
{
    public function index()
    {
        
        return redirect('mail/inbox');
    }

    ##BUSQUEDA DE USUARIO
    public function ajaxReturnsUsers(Request $request)
    {
     if ($request->isMethod('post')){
         $_ = $request->input("name");
         $profile = Profile::select('id','full_name','last_name','photo','facebook_photo','url','Email')->where('full_name','LIKE',"%$_%")->get();
         return response()->json($profile); 
     }else{
         return response()->json(['error'=>$validator->errors()], 505);
     }
    }


    public function composseMenssage()
    {
        $profile                =   $this->_dbConsultaProfile();
        $mail_inbox             =   $this->_buildArray($this->_xx(),true);
        $count_mail             =   $this->_dbConsutalCounInbox();
        $count_sent             =   $this->_dbConsultaMessagesEnviados();
        $count_archive          =   $this->_dbConsultaMessagesArchivados();
        $count_shared           =   $this->_dbConsultaSharedProjects();
        $count_inbox    =   $this->_dbConsutalCounInbox();
        $profile_occupations    =   [];
        $type                   =   'inbox';
        $shared                 =   false;
        return view('mail.composse', compact('profile','mail_inbox','count_mail','count_inbox','count_sent','count_archive','type','count_shared','shared','profile_occupations'));
    }
    ##VISTA SLUG MAIL
    public function viewMails($slug)
    {
        $profile        =   $this->_dbConsultaProfile();
        $count_sent     =   $this->_dbConsultaMessagesEnviados();
        $count_archive  =   $this->_dbConsultaMessagesArchivados();
        $count_shared  =   $this->_dbConsultaSharedProjects();
        $count_inbox    =   $this->_dbConsutalCounInbox();
        $occupations = Occupation::where('iso', 'en')->pluck('occupation', 'id');
        $profile_occupations = $profile->occupations;
        switch ($slug) {
            case 'inbox':
                $type           =   'inbox';
                $mail_inbox     =   $this->_buildArray($this->_xx(),true);
                $count_mail    =   $this->_dbConsutalCounInbox();
                $shared         =   false;
            break;
            case 'send':
                $type           =   'send';
                $mail_inbox     =   $this->_buildArray($this->_dbConsultaMessagesEnviados());
                $count_mail     =   $mail_inbox;
                $shared         =   false;
            break;
            case 'archive':
                $type           =   'archive';
                $mail_inbox     =   $this->_buildArray($this->_dbConsultaMessagesArchivados(),true);
                $count_mail     =   $mail_inbox;
                $shared         =   false;
            break;
            case 'shared':
                $type           =   'shared';
                $mail_inbox     =   $this->_dbConsultaSharedProjects();
                $count_mail     =   $mail_inbox;
                $shared         =   true;
            break;
        }
        return view('mail.index', compact('profile','mail_inbox','count_mail','count_inbox','count_sent','count_archive','count_shared','type','shared','profile_occupations'));
        
    }
    ##CONSUTA DATOS DE USUARIO
    public function _dbConsultaProfile()
    {
        return Profile::select('id', 'full_name', 'last_name', 'state', 'city', 'country_id', 'photo')
        ->where('user_id', Auth::id())
        ->first();
    }


    ##CONSULTA MENSAJE LEIDO
    public function __dbIsReadMail($data)
    {
        
        return DB::table('message_details')
            ->where([
                ['message_details.to_message','=',$data['to']],
                ['message_details.message_id','=',$data['message_id']]
            ])->get();
        
    }
    //reenviado
    public function __dbre($data)
    {
        return DB::table('message_details')
                ->where('message_details.message_id','=',$data['message_id'])
                ->get();
    }

    ##CONSULTA HISTORIAL DE PORJECTOS COMPARTIDOS
    public function _dbConsultaSharedProjects(){
        $data = DB::select("
            SELECT * FROM share_projects a
            INNER JOIN projects b
            ON a.project_id = b.id
            INNER JOIN profiles c
            ON a.`to` = c.user_id
            WHERE a.`from` = ?;
        ",[Auth::id()]);
        if(count($data) > 0)
        {
            foreach($data as $value){
                $arr[] = array(
                        'project_name'  => $value->project_name
                    ,   'urlproj'       => $value->urlproj
                    ,   'shared_date'   =>  $value->shared_date
                    ,   'date'          =>  date('d M, Y',strtotime($value->shared_date))
                    ,   'timestamp'     =>  Profile::timeElapsedString($value->shared_date)
                    ,   'full_name'     =>  $value->full_name
                    ,   'url'           =>  $value->url
                );
            }
        }else{
            $arr = [];
        }
        
        return $arr;
    }
    ##CONSULTA MENSAJES ARCHIVADOS
    public function _dbConsultaMessagesArchivados()
    {
        $data = DB::select("
            SELECT 
                    b.message_id
                ,   min(b.is_read) as top
                ,   a.*
                ,   b.*
                ,   c.*
                ,   d.*
                ,   d.user_id as 'user_message'
                ,   DATE_FORMAT(a.message_date, '%b %d') AS 'fecha_inicio'
            FROM messages a
            INNER JOIN message_details b
                ON a.id = b.message_id
            LEFT JOIN profiles c
                ON b.from_message = c.id
            INNER JOIN message_archives d
                ON a.id = d.message_id
            WHERE b.to_message = ?  OR b.from_message = ?
            GROUP BY b.message_id
            ORDER BY min(b.is_read),b.message_id DESC;",[Auth::id(),Auth::id()]);
        return $data;
    }

    ##CONSULTA MENSAJES ENVIADOS
    public function _dbConsultaMessagesEnviados()
    {
        $data =  DB::select("
            SELECT 
                    b.message_id
                ,   min(b.is_read) as top
                ,	a.*
                ,	b.*
                ,	c.*
                ,	DATE_FORMAT(a.message_date, '%b %d') as 'fecha_inicio'
            FROM messages a
            INNER JOIN message_details b
                ON a.id = b.message_id
            LEFT JOIN profiles c
                ON b.to_message = c.user_id
            WHERE b.from_message = ? AND b.send = 1
            GROUP by b.message_id
            ORDER BY b.is_read,min(b.is_read),b.message_id DESC;
        ",[Auth::id()]);
        return $data;
    }
    public function _xx()
    {
        /*
            return DB::table('messages')
            ->join('message_details','messages.id','=','message_details.message_id')
            ->leftJoin('profiles','message_details.from_message','=','profiles.id')
            ->leftJoin('message_archives','messages.id','=','message_archives.message_id')
            ->select('messages.*','profiles.*','message_details.*')
            ->where([
                ['message_details.to_message','=',Auth::id()],
                ['message_details.inbox','=','1']
            ])
            ->orderBy('messages.message_date', 'desc')
            ->get();
        */
        $data = DB::select("
            SELECT 
               min(b.is_read) as top
            ,	a.*
            ,	b.*
            ,	c.*
            FROM messages a
            INNER JOIN message_details b
            ON a.id = b.message_id
            LEFT JOIN profiles c
            ON b.from_message = c.user_id
            LEFT JOIN message_archives d
            ON a.id = d.message_id
            WHERE  b.to_message = ? AND b.inbox = 1 AND isnull(d.id)
            GROUP BY b.message_id
            ORDER BY b.is_read,MIN(b.is_read),b.message_id DESC;
        ",[Auth::id()]);
        return $data;
    }
    public function _buildArray($message_data,$read = false)
    {
        foreach($message_data as $value){
            //echo $value->asunto;
            $isread_data['to']             =    $value->to_message;
            $isread_data['message_id']     =    $value->message_id;

            $is_delete = $this->_dbConsultaMessageDelete($value->message_id);
            $_idDelete = (count($is_delete)>0) ? true : false;
            #leido
            if($read){
                $is_read = $this->__dbIsReadMail($isread_data);
           
                for($i = 0; $i < sizeof($is_read); $i++){
                    $_boll_read     = ($is_read[$i]->is_read == '0') ? true : false;
                }
            }else{
                $_boll_read = false;
            }
            
            #re-enviado
            $_re = $this->__dbre($isread_data);
           
            for ($j=0; $j < sizeof($_re) ; $j++) { 
                $_isRe = ($_re[$j]->message_id == $value->message_id && $_re[$j]->re == '1' ) ? true : false;
            }
            $icoDefault = 'public/images/preloader.svg';
            /****/
            $timesta = Profile::timeElapsedString($value->message_date);
            $arrg[] = array(
                    'asunto' 			=> $value->subject
                ,	'idmessage' 		=> $value->message_id
                ,	'Message' 			=> $value->message_text
                ,	'keypadreMessage' 	=> $value->message_id
                ,	'keymessage' 		=> $value->message_id
                ,	'keyproject' 		=> $value->project_id
                ,	'keydetalle' 		=> $value->id
                ,	'keyfriends' 		=> $value->user_id
                ,	'type' 				=> $value->type
                ,   'time'              => $timesta
                ,	'fechamsj' 			=> date('d M, Y \a\t h:i A',strtotime($value->message_date))
                ,	'fechaini' 			=> $value->message_date
                ,	'iduser' 			=> $value->user_id
                ,	'isread' 			=> ($_boll_read) ? 'new-mail-color':'noting-mail-color'
                ,	'isactive'			=> ($_boll_read ) ? 'lens':'radio_button_unchecked'
                ,	'Fullname'			=> ($value->from_message =='0')? 'Storyrocket' : $value->full_name.' '.$value->last_name
                ,	'email' 			=> $value->email
                ,	'avatar'			=> ($value->from_message =='0') ? $icoDefault : $value->photo
                ,	'trash'				=> $value->trash
                ,	're'				=> $_isRe
                ,	'_isDelete'			=> $_idDelete
            );
        }
        $arr =(count($message_data) > 0 ) ? $arrg : [];
        return $arr;
    }

    public function _dbConsultaMessageDelete($id){
        $data = DB::select("select * from  message_delete where message_id = ? and user_id = ?",[$id,Auth::id()]);
        return $data;
    }
    ##CONTAMOS LA CANTIDAD DEL INBOX POR LEER
    public function _dbConsutalCounInbox()
    {
        return DB::table('message_details')
                        ->join('messages','message_details.message_id','=','messages.id')
                        ->groupBy('message_details.message_id')
                        ->where([
                            ['message_details.is_read','=',0],
                            ['message_details.to_message','=',Auth::id()]
                        ])->get();
    }
    public function _dbConsultaMail()
    {
        return DB::table('messages')
                        ->join('message_details','messages.id','=','message_details.message_id')
                        ->leftJoin('profiles','message_details.from_message','=','profiles.id')
                        ->select()
                        ->where([
                            ['message_details.to_message','=',Auth::id()],
                            ['message_details.inbox','=','1']
                        ])
                        ->orderBy('messages.message_date', 'desc')
                        ->get();
    }
    
    public function mailReturnsUsers()
    {
        $response = array(
            'status' => 'success',
            'msg' => $request->message,
        );
        return response()->json($response); 
    }
    ##REGISTRA MENSAJE REQUEST PROJECT
    public function sentMessagesProject(Request $request)
    {
        if($request->isMethod('post')){
            ###HACER ENVIO AL MAIL POR CORREO
            return response()->json($request->input());
        }
    }
    ##RESPONDER UN MENSAJE
    public function replyMessage(Request $request)
    {
        $bool = false;
        if ($request->isMethod('post'))
        {
            $data['project_id'] = ($request->input('project_id'))
                ? $request->input('project_id') 
                : '0';
                $data['script'] =($request->input('pdf') == 'true')
                ? '2'
                : '0';
            $data['date_send']  = $request->input('fecha');
            $data['to_message'] = $request->input('friend');
            $data['re']         = $request->input('re');
            $data['message_id'] = $request->input('message_id');
            $data['message_text'] = $request->input('message');
            ####
            # VALIDAMOS SI ES RESPUESTA DE UN REQUEST DE PROJECTO
            ####
            if($request->input('pdf') == 'true'){
                $data['shared'] = '1';
               
                ####
                # CONSULTAMOS PROJECTO POR ID PARA CONSTRUIR EL BOTON DE DESCARGA DEL PDF
                ####
                $result_project = $this->DbProjectProfile($request->input('project_id'));

                $message_hrf = (filter_var($result_project[0]->pdf_project_file, FILTER_VALIDATE_URL))
                                ?  $result_project[0]->pdf_project_file
                                : $result_project[0]->urlproj;

                $data['message_btn'] = "'<a href=".$message_hrf." class='btn-download' download>Download Full PDF '".$result_project[0]->project_name."'</a>";
                $data['message_text'] .= $data['message_btn'];
                $this->DbUpdateShared($data);
            }
            ###
            $this->DbRegisterMessageReply($data);

            ###
            $profile = Profile::select('id', 'full_name', 'last_name', 'state', 'city', 'country_id', 'photo',
            'facebook_photo', 'state','url')
            ->where('user_id', Auth::id())
            ->first();

            $state      = MetaLocation::select('id', 'local_name')->where('id', $profile->state)->first();
            $city       = MetaLocation::select('id', 'local_name')->where('id', $profile->city)->first();
            $country    = MetaLocation::select('id', 'local_name')->where('id', $profile->country_id)->first();

            $arrData[] = array(
                    'state'             => $state
                ,   'city'              => $city
                ,   'country'           => $country
                ,   'avatar'            => $profile->photo
                ,   'full_name'         => $profile->full_name
                ,   'message_text'      => $data['message_text']
                ,   'date_send'         => $data['date_send']
                ,   'url'               =>  $profile->url
            );

            return response()->json($arrData);
        }
        else
        {
            return response()->json(['error'=>$validator->errors()], 505);
        }
    }

    ##ENVIO DE MENSAJE
    public function sendMessage(Request $request)
    {
        if ($request->isMethod('post')){
            $_ = $request->input();
            if($request->input('_action') && $request->input('_action') == 'project'){
                $project_id = $request->input('project');
                $type = '2';
                $script = '1';
            }else{
                $project_id = '0';
                $type = '1';
                $script = '0';

            }

            //insert table messages
            $message_id = DB::table('messages')->insertGetId(
                [
                        'subject' => $request->input('_asunto')
                    ,   'type' => $type
                    ,   'message_date'=> $request->input('_fecha')
                    ,   'new_old'=>'1'
                    ,   'is_read_mail' => '1'
                ]
            );
            if($request->input('is_storyroccket') && $request->input('is_storyroccket') == '1'){
                $from_message = '0';
            }else{
                $from_message =  Auth::id();
            }
            DB::table('message_details')->insert([
                    'message_id' =>$message_id
                ,   'project_id' => $project_id
                ,   'date_send' =>$request->input('_fecha')
                ,   'from_message' => $from_message
                ,   'to_message' => $request->input('_id_friend')
                ,   'script' => $script
                ,   'message_text' => $request->input('_text')
                ,   'inbox' => '1'
                ,   'send' => '1'
            ]);
            return response()->json('your message was sent successfully'); 
        }else{
            return response()->json(['error'=>$validator->errors()], 505);
        }
    }

    ## REMOVER MENSAJES ARCHIVADO,INBOX,SENT
    public function removeMail(Request $request)
    {
        //return response()->json($request->input());exit;
        if ($request->isMethod('post'))
        {
            //return response()->json($request->input());exit;
            //se enviar para archivar el mensaje
            if($request->input('isType') == 'archive'){
                $this->DbRemoveMailArchive($request->input('idMessage'),$request->input('isTag'));
                $resp = 'your message was archived';
            }
            //se envia a eliminar
            if($request->input('isType') == 'delete'){
                $this->DbRemoveMailDelete($request->input('idMessage'));
                $resp = 'the message was deleted';
            }
            //enviar mensaje al inbox
            if($request->input('isTag') == 'archive'){
               $this->DbsenMailInbox($request->input('idMessage'));
               $resp = 'the message was sent to your inbox';
            }
        }
        return response()->json($resp);  
    }
    ### VER EL MAIL DE USUARIO ENVIO
    public function loaderMail(Request $request)
    {
        
        if ($request->isMethod('post'))
        {
            //$message_bol = ($request->input('type')== "send") ? true : false;
            $viewData = $this->DbMessageView($request->input('message'));
            $friend_id = $request->input('friends');
            if($request->input('tags') != 'send'){
                ##marcamos el mensaje como leido
                DB::table('message_details')
                ->where('message_details.message_id', $request->input('message'))
                ->update(['is_read' => '1']);
            }
            foreach($viewData as $value)
            {
                if($value->from_message !=0)
                {
                    $message_id = $value->message_id;
                   
                    //$friend_id = ($message_bol) ? $value->to_message : $value->from_message;
                  //  $friend_id = $value->to_message;
                    //$friend_id = $value->to_message;
                    $avatar = (!empty($value->photo)) ? $value->photo : $value->facebook_photo;
                }else{
                    $avata = 'public/images/preloader.svg';
                }

                $link_message = preg_replace('@<(\w+)\b.*?>.*?</\1>@si', '', $value->message_text);

                $dowloandProject = ($value->project_id <> 0 && $value->to_message == Auth::id() && $value->script == 1 )
                    ? true
                    : false;

                
                $aceptProject = ($value->script == 1 && $value->shared == 1 && $value->to_message == Auth::id() )
                    ? true
                    : false;

                $view_dowloand = ($value->to_message == Auth::id() && $value->script == 2)
                    ? true
                    : false;

                $linkHref = $this->extraerURLs($value->message_text);

               

                $resultList = DB::table('projects')
                     ->select('project_name')
                     ->where('id', '=',$value->project_id)
                     ->get();
                
                $listProject = (count($resultList) > 0) ? $resultList : 'null';
                $arr[] = array(
                        'id'                    => $value->id
                    ,   'message_id'            => $value->message_id
                    ,   'asunto'                => $value->subject
                    ,   'project_id'            => $value->project_id
                    ,   'date_send'             => $value->date_send
                    ,   'message_to'            => $value->to_message
                    ,   'script'                => $value->script
                    ,   'friend'                => $friend_id
                    ,   'link_message'          => nl2br($link_message)
                    ,   'profile_url'           => $value->url
                    ,   'full_name'             => ($value->from_message == '0') ? 'Storyrocket' :  $value->full_name . ' ' . $value->last_name
                    ,   'avatar'                => $avatar
                    ,   'from_message'          => $value->from_message
                    ,   'href'                  => (!empty($linkHref[0])) ? $linkHref[0] : 'null' 
                    ,   'date_dowloand'         => $value->date_download
                    ,   'message_text'          => nl2br($link_message)
                    ,   'subject'               => $value->subject
                    ,   'shared'                => $value->shared
                    ,   'btn_dowloand_projec'   => $dowloandProject
                    ,   'message_acept'         => $aceptProject
                    ,   'view_dowloand'         => $view_dowloand
                    ,   'project_name'          => $listProject
                );
            }
            return response()->json($arr);
        }
    }

    ##EXTRAER URL
    public function extraerURLs($cadena)
    {
        $regex = '/https?\:\/\/[^\" ]+/i';
        preg_match_all($regex, $cadena, $partes);
        return ($partes[0]);
    }
    ##ENVIAR MENSAJE DELETE
    public function DbRemoveMailDelete($id){
        $user_id = Auth::id();
        DB::table('message_delete')->insert(
            [
                    'message_id'        => $id
                ,   'user_id'           => $user_id
                ,   'message_delete'    => '1'
                ,   'flat_delete'          => '1'
            ]
        );
    }
    ##REMOVER MENSAJES DEL ARCHIVADO Y ENVIAR AL INBOX
    public function DbsenMailInbox($id){
        DB::table('message_archives')->where('message_id', '=', $id)->delete();
        DB::table('message_details')
        ->where('message_details.message_id', $id)
        ->update(['inbox' => 1]);
    }
    ##ENVIAR MENSAJES AL ARCHIVADO
    public  function DbRemoveMailArchive($id,$isTag)
    {
        $user_id = Auth::id();
        DB::table('message_archives')->insert(
            [
                    'message_id' => $id
                ,   'user_id'   => $user_id
            ]
        );
        switch ($isTag) {
            case 'inbox':
                DB::table('message_details')
                ->where('message_details.message_id', $id)
                ->update(['inbox' => 0]);
            break;
            case 'send':
                DB::table('message_details')
                ->where('message_details.message_id', $id)
                ->update(['send' => 0]);
            break;
            default:
                # code...
            break;
        }
        
    }

    public function DbUpdateShared($data)
    {
        DB::table('message_details')
            ->where('message_details.id', $data['message_id'])
            ->update(['shared' => 1]);
        

        $file_data = DB::table('message_details')
                     ->select()
                     ->where('message_details.id', '=', $data['message_id'])
                     ->get();

        $i = 0;
        foreach($file_data as $value){
            $i++;
            $my_arr[] = array(
                    "id" => $i
                ,   "message_text"  => $value->message_text
                ,   "profile_id"    => $value->from_message
            );
        }
        $serialize = serialize($my_arr);
      
        DB::table('share_projects')->insert([
                'from'          => $data['message_id']
            ,   'to'            => $data['to_message']
            ,   'project_id'    => $data['project_id']
            ,   'shared_date'   => $data['date_send']
            ,   'texto_info'    => $data['message_text']
        ]);
    }

    public function DbProjectProfile($id)
    {
        return DB::table('projects')
                    ->join('profiles','projects.user_id','=','profiles.id')
                    ->where('projects.id', '=', $id)
                    ->get();
    }

    public function DbMessageView($data)
    {
        return DB::table('message_details')
                        ->leftJoin('profiles','message_details.from_message','=','profiles.id')
                        ->leftJoin('messages','messages.id','=','message_details.message_id')
                        ->where('message_details.message_id',$data)
                        ->get();
    }

    public function DbRegisterMessageReply($data)
    {
        $postData = array('new_old'=>'1');
        DB::table('message_details')->insert(
            [
                'message_id'    => $data['message_id'],
                'project_id'    => $data ['project_id'],
                'date_send'     => $data['date_send'],
                'from_message'  => Auth::id(),
                'to_message'    => $data['to_message'],
                'script'        => $data['script'],
                'message_text'  => $data['message_text'],
                'inbox'         => 1,
                'send'          => 1,
                're'            => $data['re']
            ]
        );

        //
        DB::table('messages')
            ->where('id', $data['message_id'])
            ->update(['is_read_mail' => '0']);

        //
        if($data['project_id'] <> '0')
        {
            $idUser = Auth::id();

            $listData = DB::table('message_details')->where([
                ['message_id', '=', $data['message_id']],
                ['script', '<>', '2'],
            ])->get();

            $i = 0;

            foreach($listData as $key => $value)
            {
                $i++;
                $my_array[] = array(
                        'id'            => $i
                    ,   'message_text'  => $value->message_text
                    ,   'from_message'  => $value->from_message
                );
            }

            $array_serealize = serialize($my_array);
            
            DB::table('share_projects')
            ->where( ['project_id' => $data['project_id']],
                    ['from_message'=>Auth::id()])
            ->update(['texto_info'=> $array_serealize]);
        }
    }

}

?>