<?php

namespace App\Http\Controllers;


use App\Http\Requests\RegisterPassword;
use App\ImportedUser;
use App\UserOld;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Monolog\Handler\StreamHandler;
use Redirect;
use Validator;
use Auth;
use App\Http\Requests\PaymentRequest;
use Stripe\Token;
use Stripe\Customer;
use GuzzleHttp\Client;
use App\MetaLocation;
use Monolog\Logger;


class BowkerController extends Controller
{


    public function formMailRegisterPassword($id)
    {

        $user = DB::connection('mysql_old')->select('select email from users where UserID = ? and imported_user_active != 0', [$id]);

        if (empty($user)) {
            return redirect()->to('https://www.storyrocket.com/');
        }


        return view('landings.bowker.register-password', compact('id'));
    }

    public function savePayment(PaymentRequest $paymentRequest)
    {

        DB::beginTransaction();

        try {
            /**
             * your code for save payment
             */

            $expedition_date = explode('/', $paymentRequest->get('expedition_date'));

            $card = [
                "number" => $paymentRequest->get('name_card'),
                "exp_month" => $expedition_date[0],
                "exp_year" => '20' . $expedition_date[1],
                "cvc" => $paymentRequest->get('cvv'),
                "name" => $paymentRequest->get('name') . ' ' . $paymentRequest->get('last_name')
            ];

            $token = Token::create(["card" => $card]);

            $logger = new Logger('token');
            $logger->pushHandler(new StreamHandler(storage_path('logs/token.log')),  Logger::INFO);
            $logger->info('TokenLog', $card);

            $customer = Customer::create([
                'email' => $_COOKIE['landingEmail'],
                'source' => $token,
                'plan' => 'PRO_YEARLY',
                'coupon' => 'BOWKER_SPECIAL_OFFER',
                'metadata' => ['name' => $paymentRequest->get('name') . ' ' . $paymentRequest->get('last_name')]
            ]);


            $user = ImportedUser::createUserAtStoryRocket($paymentRequest->get('name'), $paymentRequest->get('last_name'), $_COOKIE['landingEmail'], $token, $paymentRequest->get('city'), $paymentRequest->get('state'), $paymentRequest->get('country'), null);
            ImportedUser::assignPlanToUserAtStoryRocket($_COOKIE['landingEmail']);
            ImportedUser::assignGettyimagesToUserAtStoryRocket($_COOKIE['landingEmail']);

            $this->sendMailRegisterPassword($user);

            DB::commit();

            return redirect()->route('bowker.success');


        } catch (\Exception $exception) {

            DB::rollBack();

            session()->flash('error_payment', $exception->getMessage());

            return redirect()->back();
        }

    }

    public function savePassword(RegisterPassword $request)
    {
        DB::beginTransaction();
        try {
            /**
             * Here your code
             */

            $user = UserOld::where('UserID', $request->get('user_id'))->where('imported_user_active', '!=', 0)->first();

            if (empty($user)) {
                return redirect()->to('https://www.storyrocket.com/');
            }

            $client = new Client();

            $response = $client->request('POST', 'https://www.storyrocket.com/sign-in/generatePassword', [
                    'form_params' => [
                        'pass' => $request->get('password')
                    ]
                ]
            );

            $result = json_decode($response->getBody());

            $user->Password = $result->password;
            $user->imported_user_active = 1;
            $user->Active = 1;
            $user->imported_user = 1;
            $user->save();

            DB::commit();

            return redirect()->to('https://www.storyrocket.com/sign-in/importedLogin?email=' . $user->Email . '&token=' . $result->password);

        } catch (\Exception $exception) {
            DB::rollBack();
            return redirect()->back();
        }


    }

    private function sendMailRegisterPassword($user)
    {

        if (isset($_COOKIE['landingEmail']) && $_COOKIE['landingEmail'] == true) {
            $landingMail = $_COOKIE['landingEmail'];

            \Mail::send('landings.bowker.mail-register-password', [
                'landingMail' => $landingMail,
                'user' => $user
            ], function ($m) use ($landingMail, $user) {
                $m->from('backend@storyrocket.com', 'Storyrocket');
                $m->to($landingMail, $user[0]->FullName . " " . $user[0]->LastName)->subject('Storyrocket – activate your account now');
            });
            return Redirect('bowkerspecialoffer/success');

        }
    }


    public function registerSave(Request $request)
    {
        try {

            $validator = Validator::make($request->all(), ['email' => 'required|email|unique:mysql_old.users']);

            if ($validator->fails()) {
                return Redirect::back()->withErrors([$validator->messages()->first()]);
            }
            $email = $request->input('email');
            setcookie("landingEmail", $email, time() + (60 * 60), '/');
            return Redirect('bowkerspecialoffer/cart');

        } catch (\Exception $exception) {

            return Redirect::back()->withErrors([$exception->getMessage()]);
        }
    }

    public function index()
    {
        return view('landings.bowker.index');
    }

    public function register()
    {
        return view('landings.bowker.register');
    }

    public function cart()
    {

        if (isset($_COOKIE['landingEmail']) && $_COOKIE['landingEmail'] == true) {
            return view('landings.bowker.cart');
        }
        return view('landings.bowker.index');

    }

    public function checkout()
    {
        if (isset($_COOKIE['landingEmail']) && $_COOKIE['landingEmail'] == true) {
            $countries = MetaLocation::where('type', 'CO')->pluck('local_name', 'id');

            return view('landings.bowker.checkout', compact('countries'));
        }
        return view('landings.bowker.index');
    }

    public function payment()
    {
        return view('landings.bowker.payment');
    }

    public function success()
    {
        return view('landings.bowker.success');
    }

}
