<?php

namespace App\Http\Controllers;


use App\Profile;
use App\ProfileView;
use App\Project;
use App\UserSetting;
use DB;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use phpDocumentor\Reflection\DocBlock\Tags\Reference\Url;
use Stripe\Token;
use Illuminate\Notifications\Messages\MailMessage;


class SettingsController extends Controller
{

    public function index()
    {
        $profile = Profile::select('id', 'full_name', 'last_name', 'state', 'city', 'country_id', 'photo',
            'facebook_photo', 'state','url')
            ->where('user_id', Auth::id())
            ->first();

        $user = \auth()->user();
        $settings = $user->settings;

        if(!isset($settings->id))
        {
            $settings = [
                'user_id'=> $user->id,
                'values'=>json_encode(
                    [
                        'sn_send_to_my_email_once_a_day' => 0,
                        'sn_send_to_my_email_once_a_week' => 1,
                        'ps_show_other_members_that_you_looked_at_their_profile' => 1,
                        'ps_addme_to_profile_spotlight' => 1,
                        'ps_add_my_projects_to_project_spotlight' => 1,
                        'nm_someone_looks_you_at_my_profile' => 1,
                        'nm_someone_adds_you_to_his_network' => 1,
                        'nm_someone_adds_one_of_my_projects_to_their_read_later_list' => 1,
                        'nm_someone_likes_one_of_my_projects' => 1,
                        'nm_someone_comment_one_of_my_projects' => 1
                    ]
                )
            ];
            $settings = UserSetting::create($settings);
        }

        return view('settings.index', compact('profile', 'settings'));
    }



    public function getViewsNotficationBuild()
    {
        $profileView = ProfileView::select
        (
            'profile_views.visitor_user_id as "visitor_user_id"','profile_views.mark', 'profile_views.profile_user_id as "profile_user_id"', 'profile_views.last_visit'
            , 'profile_views.view', 'profiles.full_name' , 'profiles.last_name',
            'profiles.photo', 'profiles.facebook_photo', 'profiles.url'
        )
            ->leftJoin('profiles', 'profiles.user_id', '=', 'profile_views.visitor_user_id')
            ->where('profile_user_id', Auth::id())
            ->orderBy('last_visit', 'desc')
            ->get();

        $profileViewSet = array();
        foreach($profileView as $profileViewVal) {
            $profileViewVal->photo_profile = Project::getBasePhotoProfile($profileViewVal->photo, $profileViewVal->facebook_photo);
            $profileViewVal->full_name = $profileViewVal->full_name . " " . $profileViewVal->last_name;
            $profileViewVal->time_elapsed = Profile::timeElapsedString($profileViewVal->last_visit);
            $profileViewVal->url = env('APP_URL') . '/' .$profileViewVal->url;
            $profileViewVal->mark_css = ($profileViewVal->mark=="0")?"notification_unread":"";
            array_push($profileViewSet, $profileViewVal);
        }
        //return $this->getNotificationDateTimeAgo();
        return $profileViewSet;
    }

    public function sendMailWeekly()
    {

        //$user = User::findOrFail($id);

        $profile = Profile::select('id', 'full_name', 'last_name', 'state', 'city', 'country_id', 'photo',
            'facebook_photo', 'state', 'url')
            ->where('user_id', Auth::id())
            ->first();
        $html = "";

        $notification = $this->getViewsNotficationBuild();
/*
        foreach ($notification as $item) {
            $html .= '     
            <thead>              
                <tr >
                    <td>&nbsp;<img src="' . $item->photo_profile . '" title="Gary" alt="sdasd" class="img-responsive" width="40"></td>
                    <td align="left" valign="top" style="padding:21px 9px">
                        <strong style="font-size:12px;">' . $item->full_name . '</strong>
                        <span>viewed your profile</span>
                    </td>
            
                    <td align="center" valign="top" style="padding:21px 9px">' . $item->full_name . '</td>
            
                    <td align="right" valign="top" style="padding:21px 9px">
                        <img src="https://cdn2.iconfinder.com/data/icons/flat-ui-icons-24-px/24/eye-24-512.png" width="20">
                    </td>
                </tr>
            </thead>
        ';

        }
*/


        $html .= '     
            <thead>              
                <tr >
                    <td>&nbsp;<a href="http://dev.storyrocket.com/jonathan.pcpsuqpy"><img src="https://storyrocket-aws3.s3.amazonaws.com/fc0914f6.jpg" title="Gary" alt="sdasd" class="img-responsive" width="40"></a></td>
                    <td align="left" valign="top" style="padding:21px 9px">
                        <strong style="font-size:12px;">Jonathan</strong>
                        <span>likes your project</span>
                        <strong style="font-size:12px;"><a href="http://dev.storyrocket.com/MESSAGE-OF-THE-LOCUST">MESSAGE OF THE LOCUST</a></strong>
                    </td>
            
                    <td align="center" valign="top" style="padding:21px 9px"></td>
            
                    <td align="right" valign="top" style="padding:21px 9px">
                        <a href="http://dev.storyrocket.com/jonathan.pcpsuqpy">
                            <img src="https://freeiconshop.com/wp-content/uploads/edd/like-solid.png" width="20">
                        </a>
                    </td>
                </tr>
            </thead>
        ';

        $html .= '     
            <thead>              
                <tr >
                    <td>&nbsp;<a href="http://dev.storyrocket.com/jonathan.pcpsuqpy"><img src="https://storyrocket-aws3.s3.amazonaws.com/fc0914f6.jpg" title="Gary" alt="sdasd" class="img-responsive" width="40"></a></td>
                    <td align="left" valign="top" style="padding:21px 9px">
                        <strong style="font-size:12px;">Jonathan</strong>
                        <span>added your project</span>
                        <strong style="font-size:12px;"><a href="http://dev.storyrocket.com/MESSAGE-OF-THE-LOCUST">MESSAGE OF THE LOCUST</a></strong>
                    </td>
            
                    <td align="center" valign="top" style="padding:21px 9px"></td>
            
                    <td align="right" valign="top" style="padding:21px 9px">
                        <a href="http://dev.storyrocket.com/jonathan.pcpsuqpy">
                            <img src="http://cdn.onlinewebfonts.com/svg/img_82026.png" width="20">
                        </a>
                    </td>
                </tr>
            </thead>
        ';

        $html .= '     
            <thead>              
                <tr >
                    <td>&nbsp;<a href="http://dev.storyrocket.com/jonathan.pcpsuqpy"><img src="https://storyrocket-aws3.s3.amazonaws.com/fc0914f6.jpg" title="Gary" alt="sdasd" class="img-responsive" width="40"></a></td>
                    <td align="left" valign="top" style="padding:21px 9px">
                        <strong style="font-size:12px;">Jonathan</strong>
                        <span>added your profile</span>
                    </td>
            
                    <td align="center" valign="top" style="padding:21px 9px"></td>
            
                    <td align="right" valign="top" style="padding:21px 9px">
                        <a href="http://dev.storyrocket.com/jonathan.pcpsuqpy">
                            <img src="https://image.freepik.com/free-icon/profile-users-silhouette_318-40641.jpg" width="20">
                        </a>
                    </td>
                </tr>
            </thead>
        ';

        $html .= '     
            <thead>              
                <tr >
                    <td>&nbsp;<a href="http://dev.storyrocket.com/jonathan.pcpsuqpy"><img src="https://storyrocket-aws3.s3.amazonaws.com/fc0914f6.jpg" title="Gary" alt="sdasd" class="img-responsive" width="40"></a></td>
                    <td align="left" valign="top" style="padding:21px 9px">
                        <strong style="font-size:12px;">Jonathan</strong>
                        <span>viewed your profile</span>
                    </td>
            
                    <td align="center" valign="top" style="padding:21px 9px"></td>
            
                    <td align="right" valign="top" style="padding:21px 9px">
                        <img src="https://cdn2.iconfinder.com/data/icons/flat-ui-icons-24-px/24/eye-24-512.png" width="20">
                    </td>
                </tr>
            </thead>
        ';

        $html .= '     
            <thead>              
                <tr >
                    <td>&nbsp;<img src="https://res.cloudinary.com/storyrocket-llc/image/fetch/dpr_2,f_auto/https://www.storyrocket.com/public/images/avatar_default_user.png" title="Gary" alt="sdasd" class="img-responsive" width="40"></td>
                    <td align="left" valign="top" style="padding:21px 9px">
                        <strong style="font-size:12px;">Stanley Brown</strong>
                        <span>viewed your profile</span>
                    </td>
            
                    <td align="center" valign="top" style="padding:21px 9px"></td>
            
                    <td align="right" valign="top" style="padding:21px 9px">
                        <img src="https://cdn2.iconfinder.com/data/icons/flat-ui-icons-24-px/24/eye-24-512.png" width="20">
                    </td>
                </tr>
            </thead>
        ';


        $html .= '     
            <thead>              
                <tr >
                    <td>&nbsp;<img src="https://res.cloudinary.com/storyrocket-llc/image/fetch/dpr_2,f_auto/https://storyrocket-aws3.s3.amazonaws.com/b9543948.jpg" title="Gary" alt="sdasd" class="img-responsive" width="40"></td>
                    <td align="left" valign="top" style="padding:21px 9px">
                        <strong style="font-size:12px;">David Case</strong>
                        <span>viewed your profile</span>
                    </td>
            
                    <td align="center" valign="top" style="padding:21px 9px"></td>
            
                    <td align="right" valign="top" style="padding:21px 9px">
                        <img src="https://cdn2.iconfinder.com/data/icons/flat-ui-icons-24-px/24/eye-24-512.png" width="20">
                    </td>
                </tr>
            </thead>
        ';

        $html .= '     
            <thead>              
                <tr >
                    <td>&nbsp;<img src="https://res.cloudinary.com/storyrocket-llc/image/fetch/dpr_2,f_auto/https://storyrocket-aws3.s3.amazonaws.com/b277b163.jpg" title="Gary" alt="sdasd" class="img-responsive" width="40"></td>
                    <td align="left" valign="top" style="padding:21px 9px">
                        <strong style="font-size:12px;">Krystol Diggs</strong>
                        <span>viewed your profile</span>
                    </td>
            
                    <td align="center" valign="top" style="padding:21px 9px"></td>
            
                    <td align="right" valign="top" style="padding:21px 9px">
                        <img src="https://cdn2.iconfinder.com/data/icons/flat-ui-icons-24-px/24/eye-24-512.png" width="20">
                    </td>
                </tr>
            </thead>
        ';

        $html .= '     
            <thead>              
                <tr >
                    <td>&nbsp;<img src="https://res.cloudinary.com/storyrocket-llc/image/fetch/dpr_2,f_auto/https://storyrocket-aws3.s3.amazonaws.com/28c0985f.jpg" title="Gary" alt="sdasd" class="img-responsive" width="40"></td>
                    <td align="left" valign="top" style="padding:21px 9px">
                        <strong style="font-size:12px;">Alex Davis</strong>
                        <span>viewed your profile</span>
                    </td>
            
                    <td align="center" valign="top" style="padding:21px 9px"></td>
            
                    <td align="right" valign="top" style="padding:21px 9px">
                        <img src="https://cdn2.iconfinder.com/data/icons/flat-ui-icons-24-px/24/eye-24-512.png" width="20">
                    </td>
                </tr>
            </thead>
        ';

        \Mail::send('emails.notification-weekly', ['profile' => $html], function ($m) use ($profile) {
            $m->from('backend@storyrocket.com', 'StoryRocket');

            $m->to("garyrojasherrera@gmail.com", "Gary Rojas")->subject('Summary Weekly Notifications');

        });
        echo "sendMailWeekly";
    }

    public function notificationAddProfile($id)
    {

        $authId = Auth::id();
        $profile = Profile::select('id', 'full_name', 'last_name', 'state', 'city', 'country_id', 'photo',
            'facebook_photo', 'state','url','email')
            ->where('user_id', $authId)
            ->first();

        $profile->photo_profile = Project::getBasePhotoProfile($profile->photo, $profile->facebook_photo);
        $profile->url = env('APP_URL') . '/' .$profile->url;


        \Mail::send('emails.notification-addyourprofile', ['profile' => $profile], function ($m) use ($profile) {
            $m->from('backend@storyrocket.com', 'StoryRocket');
            $m->to("garyrojasherrera@gmail.com", "Gary Rojas")->subject($profile->full_name . " " . $profile->last_name . ' adds you to his Network as a friend');

        });


    }

    public function notificationReadLaterListYourProject()
    {
        $profile = Profile::select('id', 'full_name', 'last_name', 'state', 'city', 'country_id', 'photo',
            'facebook_photo', 'state','url')
            ->where('user_id', Auth::id())
            ->first();


        \Mail::send('emails.notification-addyourproject', ['profile' => $profile], function ($m) use ($profile) {
            $m->from('backend@storyrocket.com', 'StoryRocket');

            $m->to("garyrojasherrera@gmail.com", "Gary Rojas")->subject($profile->full_name . " " . $profile->last_name . ' adds one of my projects to their "Read Later List"');

        });
        echo "notificationReadLaterListYourProject";
    }

    public function notificationLikeYourProfile()
    {
        $profile = Profile::select('id', 'full_name', 'last_name', 'state', 'city', 'country_id', 'photo',
            'facebook_photo', 'state','url')
            ->where('user_id', Auth::id())
            ->first();


        \Mail::send('emails.notification-addyourproject', ['profile' => $profile], function ($m) use ($profile) {
            $m->from('backend@storyrocket.com', 'StoryRocket');

            $m->to("garyrojasherrera@gmail.com", "Gary Rojas")->subject($profile->full_name . " " . $profile->last_name . ' likes your project');

        });
        echo "notificationLikeYourProfile";

    }

    public function psAddmeToProfileSpotlightMail()
    {
        $profile = Profile::select('id', 'full_name', 'last_name', 'state', 'city', 'country_id', 'photo',
            'facebook_photo', 'state','url','email')
            ->where('user_id', Auth::id())
            ->first();

        \Mail::send('emails.notification-ps-addme-to-profile-spotlight',
            [
            'name' => $profile->full_name
            ], function ($m) use ($profile) {
            $m->from('backend@storyrocket.com', 'StoryRocket');

            $m->to($profile->email, $profile->full_name . " " . $profile->last_name)->subject('My profile - Recent activity');

        });

        echo "psAddmeToProfileSpotlightMail";
    }

    public function psAddMyProjectsToProjectSpotlight()
    {
        $profile = Profile::select('id', 'full_name', 'last_name', 'state', 'city', 'country_id', 'photo',
            'facebook_photo', 'state','url','email')
            ->where('user_id', Auth::id())
            ->first();

        \Mail::send('emails.notification-ps-add-my-projects-to-project-spotlight',
            [
                'name' => $profile->full_name
            ], function ($m) use ($profile) {
                $m->from('backend@storyrocket.com', 'StoryRocket');

                $m->to($profile->email, $profile->full_name . " " . $profile->last_name)->subject('My project - Recent activity');

            });

        echo "psAddmeToProfileSpotlightMail";
    }


    public function visitProfileMail()
    {
        $profile = Profile::select('id', 'full_name', 'last_name', 'state', 'city', 'country_id', 'photo',
            'facebook_photo', 'state','url','email')
            ->where('user_id', Auth::id())
            ->first();

        \Mail::send('emails.notification-visit-your-profile',
            [
                'name' => $profile->full_name
            ], function ($m) use ($profile) {
                $m->from('backend@storyrocket.com', 'StoryRocket');

                $m->to($profile->email, $profile->full_name . " " . $profile->last_name)->subject('My project - Recent activity');

            });

        echo "psAddmeToProfileSpotlightMail";
    }

    public function sendMailDaily()
    {

        $profile = Profile::select('id', 'full_name', 'last_name', 'state', 'city', 'country_id', 'photo',
            'facebook_photo', 'state','url')
            ->where('user_id', Auth::id())
            ->first();

        //$textActivity = "Likes your project <a href=\"#\" style=\"color: #1b6eb0;\">Message of the Locust</a>";
        $textActivity = '<p style="margin: 0 0 10px 0;">Viewed your profile</p>';
        $notification = $this->getViewsNotficationBuild();


        \Mail::send('emails.notification-daily', ['list' => $notification, 'textActivity' => $textActivity], function ($m) use ($profile) {
            $m->from('backend@storyrocket.com', 'StoryRocket');

            $m->to("garyrojasherrera@gmail.com", "Gary Rojas")->subject('Summary Daily Notifications');

        });

        echo "sendMailDaily";

    }

    public function saveSettings(Request $request)
    {
        $data = $request->all();

        $user = \auth()->user();
        $settings = $user->settings;

        $settings->values = json_encode($data['values']);
        $settings->save();

        return response()->json(true);
    }

}