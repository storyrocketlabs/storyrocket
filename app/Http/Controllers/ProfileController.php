<?php

namespace App\Http\Controllers;

use App\Agent;
use App\Award;
use App\Credit;
use App\Http\Requests\AgentRequest;
use App\Http\Requests\CreditRequest;
use App\Http\Requests\EmailUpdateRequest;
use App\Http\Requests\PasswordUpdateRequest;
use App\Http\Requests\AwardRequest;
use App\Http\Requests\ProfileVideoRequest;
use Illuminate\Support\Facades\Auth;
use App\Jobs\OptimizeImages;
use App\Language;
use App\MetaLocation;
use App\Occupation;
use App\Profile;
use App\ProfileView;
use App\ProfileVideo;
use App\Genre;
use App\Project;

use Illuminate\Http\Request;
use DB;


class ProfileController extends Controller
{

    public $profileView = 'profile-view';

    public function index()
    {
        $languages = Language::pluck('language', 'id');
        $occupations = Occupation::where('iso', 'en')->pluck('occupation', 'id');
        $user = auth()->user();
        /*
        $profile = Profile::select('id', 'full_name', 'last_name', 'state', 'city', 'country_id', 'photo',
            'facebook_photo', 'state', 'url', 'user_id')->where('url', $slug)->first();*/

        $profile = $user->profile;
        $profile_languages = $profile->languages;
        $profile_occupations = $profile->occupations;
        $countries = MetaLocation::where('type', 'CO')->pluck('local_name', 'id');
        $agent = $user->agent;
        $credits = $user->credits;
        $awards = $user->awards;
        $videos = $user->videos;
        $profileView = $this->profileView;
        
        return view('profile.index', compact('profileView','languages', 'occupations', 'profile', 'profile_languages', 'profile_occupations', 'countries', 'agent', 'credits', 'awards', 'videos'));
    }

    public function getNotificationDateTimeAgo()
    {


        return Profile::timeElapsedString('2018-12-20 09:22:35');
        //echo time_elapsed_string('2013-05-01 00:22:35');
        //echo time_elapsed_string('@1367367755'); # timestamp input
        //echo time_elapsed_string('2013-05-01 00:22:35', true);
    }

    public function getViewsNotfication()
    {
        $data = $this->getViewsNotficationBuild();
        //return $this->getNotificationDateTimeAgo();
        return json_encode($data);
    }

    public static function getViewsNotficationBuild()
    {
        $profileView = ProfileView::select
        (
            'profile_views.visitor_user_id as "visitor_user_id"','profile_views.mark', 'profile_views.profile_user_id as "profile_user_id"', 'profile_views.last_visit'
            , 'profile_views.view', 'profiles.full_name' , 'profiles.last_name',
            'profiles.photo', 'profiles.facebook_photo', 'profiles.url'
        )
            ->leftJoin('profiles', 'profiles.user_id', '=', 'profile_views.visitor_user_id')
            ->where('profile_user_id', Auth::id())
            ->orderBy('last_visit', 'desc')
            ->limit(5)
            ->get();

        $profileViewSet = array();
        foreach($profileView as $profileViewVal) {
            $profileViewVal->photo_profile = Project::getBasePhotoProfile($profileViewVal->photo, $profileViewVal->facebook_photo);
            $profileViewVal->full_name = $profileViewVal->full_name . " " . $profileViewVal->last_name;
            $profileViewVal->time_elapsed = Profile::timeElapsedString($profileViewVal->last_visit);
            $profileViewVal->url = env('APP_URL') . '/' .$profileViewVal->url;
            $profileViewVal->mark_css = ($profileViewVal->mark=="0")?"notification_unread":"";
            array_push($profileViewSet, $profileViewVal);
        }
        //return $this->getNotificationDateTimeAgo();
        return $profileViewSet;
    }

    public function callAlertBellNotification()
    {
        $profileView = ProfileView::selectRaw("SUM(mark) as mark, COUNT(mark) as qty")
            ->where('profile_user_id', Auth::id())
            ->first();

        if(!empty($profileView)) {
            $mark = (int)$profileView['mark'];
            $qty = (int)$profileView['qty'];

            (int)$notificationQty =  $qty-$mark;
        }

        echo json_encode($notificationQty);
    }

    public function checkAlertBellNotification()
    {
        ProfileView::where('profile_user_id', Auth::id())
            ->update(['mark' => 1]);
    }

    public function activity()
    {
        $profile = Profile::select('id', 'full_name', 'last_name', 'state', 'city', 'country_id', 'photo',
            'facebook_photo', 'state','url')
            ->where('user_id', Auth::id())
            ->first();

        $profileView = ProfileView::select
        (
            'profile_views.visitor_user_id as "visitor_user_id"', 'profile_views.profile_user_id as "profile_user_id"', 'profile_views.last_visit'
            , 'profile_views.view', 'profiles.full_name' , 'profiles.last_name',
            'profiles.photo', 'profiles.facebook_photo', 'profiles.url'
        )
            ->leftJoin('profiles', 'profiles.user_id', '=', 'profile_views.visitor_user_id')
            ->where('profile_user_id', Auth::id())
            ->orderBy('last_visit', 'desc')
            ->get();
        $profileViewSet = array();
        foreach($profileView as $profileViewVal) {
            $profileViewVal->photo_profile = Project::getBasePhotoProfile($profileViewVal->photo, $profileViewVal->facebook_photo);
            $profileViewVal->full_name = $profileViewVal->full_name . " " . $profileViewVal->last_name;
            $profileViewVal->time_elapsed = Profile::timeElapsedString($profileViewVal->last_visit);
            $profileViewVal->url = env('APP_URL') . '/' .$profileViewVal->url;
            array_push($profileViewSet, $profileViewVal);
        }
        $profileActivity = $profileViewSet;
        return view('profile.activity', compact('profile', 'profileActivity'));
    }

    public function profileUpdate(Request $request)
    {

        $profile = auth()->user()->profile;
        $data = $request->except('email');

        if (isset($data['day']) && isset($data['month']) && isset($data['year'])) {
            $data['birthday'] = $data['year'] . '-' . $data['month'] . '-' . $data['day'];
        }

        $profile->update($data);

        if (isset($data['full_name'])) {
            $profile->languages()->detach();
            $profile->occupations()->detach();

            if (isset($data['list-occupations'])) {

                $occupations = explode(',', $data['list-occupations']);

                foreach ($occupations as $occupation) {
                    $profile->occupations()->attach($occupation);
                }
            }


            if (isset($data['list-languages'])) {
                $languages = explode(',', $data['list-languages']);

                foreach ($languages as $language) {
                    $profile->languages()->attach($language);
                }

            }
        }

        return redirect()->back();
    }

    public function updateAvatar(Request $request)
    {
        $profile = auth()->user()->profile;

        $data = $request->all();

        $pos = strpos($data['image'], ';');
        $type = explode(':', substr($data['image'], 0, $pos))[1];
        $type = explode('/', $type);

        $file_image = explode(',', $data['image']);
        $image = base64_decode($file_image[1]);
        $fileName = substr(md5(uniqid(rand(), true)), 0, 10) . '.' . strtolower($type[1]);
        if(!env('PLUGINS_PROD'))
        {
            $fileName = 'dev/'.$fileName;
        }
        \Storage::disk('public')->put('temp/' . $fileName, $image);
        $profile->photo = $fileName;
        $profile->facebook_photo = null;
        $profile->save();

        $job = (new OptimizeImages(storage_path('app/public/temp/' . $fileName)))->delay(0);

        $this->dispatch($job);


        return response()->json(['success' => true]);

    }

    public function updateEmail(EmailUpdateRequest $request)
    {
        $user = auth()->user();
        $profile = $user->profile;

        $user->email = $request->get('email');
        $profile->email = $request->get('email');

        $user->save();
        $profile->save();

        return redirect()->back();

    }

    public function updatePassword(PasswordUpdateRequest $request)
    {
        $user = auth()->user();
        $data = $request->all();

        if (auth()->attempt(['email' => $user->email, 'password' => $data['old_password']])) {
            $user->password = bcrypt($data['password']);
            $user->save();
        }

        return redirect()->back();

    }

    public function storeAgent(Agent $agent, AgentRequest $request)
    {
        $user = auth()->user();
        $data = $request->all();

        if ($agent->id > 0 && $user->id != $agent->user_id) {
            return redirect()->back();
        }

        $data['user_id'] = $user->id;
        $data['publish'] = 1;
        $agent->updateOrCreate(['id'=>$agent->id], $data);

        return redirect()->back();
    }

    public function storeCredit(CreditRequest $request)
    {
        $data = $request->all();

        $data['user_id'] = auth()->user()->id;

        Credit::create($data);

        return redirect()->back();
    }

    public function updateCredit(Credit $credit, CreditRequest $request)
    {

        $user = auth()->user();

        if ( $user->id != $credit->user_id) {
            return redirect()->back();
        }

        $data = $request->all();

        $credit->update($data);

        return redirect()->back();

    }

    public function deleteCredit(Credit $credit){
        $user = auth()->user();

        if ( $user->id != $credit->user_id) {
            return response()->json(['success'=>false]);
        }

        $credit->delete();

        return response()->json(['success'=>true]);

    }

    public function storeAward(AwardRequest $request)
    {
        $data = $request->all();

        $data['user_id'] = auth()->user()->id;

        Award::create($data);

        return redirect()->back();
    }

    public function updateAward(Award $award, AwardRequest $request)
    {

        $user = auth()->user();

        if ( $user->id != $award->user_id) {
            return redirect()->back();
        }

        $data = $request->all();

        $award->update($data);

        return redirect()->back();

    }

    public function deleteAward(Award $award){
        $user = auth()->user();

        if ( $user->id != $award->user_id) {
            return response()->json(['success'=>false]);
        }

        $award->delete();

        return response()->json(['success'=>true]);

    }

    public function storeProfileVideo(ProfileVideoRequest $request)
    {
        $data = $request->all();

        $data['user_id'] = auth()->user()->id;

        ProfileVideo::create($data);

        return redirect()->back();
    }

    public function updateProfileVideo(ProfileVideo $profileVideo, ProfileVideoRequest $request)
    {

        $user = auth()->user();

        if ( $user->id != $profileVideo->user_id) {
            return redirect()->back();
        }

        $data = $request->all();

        $profileVideo->update($data);

        return redirect()->back();

    }

    public function deleteProfileVideo(ProfileVideo $profileVideo){
        $user = auth()->user();

        if ( $user->id != $profileVideo->user_id) {
            return response()->json(['success'=>false]);
        }

        $profileVideo->delete();

        return response()->json(['success'=>true]);

    }

    function getRealIpAddr()
    {
        if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
        {
            $ip=$_SERVER['HTTP_CLIENT_IP'];
        }
        elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
        {
            $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
        }
        else
        {
            $ip=$_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }

    public function registerVisit($slug)
    {
        $authId = Auth::id();
        $profileCurrent = Profile::select('id')->where('url', $slug)->first();

        if(!empty($profileCurrent)) {
            if($profileCurrent->id != $authId) {
                $profileUpdate = ProfileView::where('visitor_user_id', $authId)->where('profile_user_id', $profileCurrent->id)
                    ->update([
                        'last_visit' => date('Y-m-d H:i:s'),
                        'view' => DB::raw('view + 1'),
                        'mark' => 0
                    ]);

                if(!$profileUpdate) {
                    $profileView = ProfileView::firstOrNew(array(
                        'visitor_user_id' => $authId,
                        'profile_user_id' => $profileCurrent->id,
                        'last_visit' => date('Y-m-d H:i:s'),
                    ));
                    $profileView->save();
                }
            }
        }

    }

    public function visitProfileMail($slug)
    {
        $authId = Auth::id();
        $profileCurrent = Profile::select('id', 'full_name', 'last_name', 'state', 'city', 'country_id', 'photo',
            'facebook_photo', 'state','url','email')->where('url', $slug)->first();



        if(!empty($profileCurrent)) {
            if ($profileCurrent->id != $authId) {
                $profileCurrent->photo_profile = Project::getBasePhotoProfile($profileCurrent->photo, $profileCurrent->facebook_photo);
                $profileCurrent->url = env('APP_URL') . '/' .$profileCurrent->url;


                $profile = Profile::select('id', 'full_name', 'last_name', 'state', 'city', 'country_id', 'photo',
                    'facebook_photo', 'state','url','email')
                    ->where('user_id', $authId)
                    ->first();

                $profile->photo_profile = Project::getBasePhotoProfile($profile->photo, $profile->facebook_photo);
                $profile->url = env('APP_URL') . '/' .$profile->url;

                if($profileCurrent->email=="garyrojasherrera@gmail.com") {

                    \Mail::send('emails.notification-visit-your-profile',
                        [
                            'profileCurrent' => $profileCurrent,
                            'profile' => $profile,
                        ], function ($m) use ($profileCurrent, $profile) {
                            $m->from('backend@storyrocket.com', 'StoryRocket');


                            $m->to($profileCurrent->email, $profileCurrent->full_name . " " . $profileCurrent->last_name)->subject($profile->full_name . " " . $profile->last_name . '  just visited your profile');

                        });
                }



            }
        }


    }

    public function showProfile($slug)
    {
        /**
         * Register view user
         */
        $this->registerVisit($slug);
        $this->visitProfileMail($slug);
        $profileCurrent = Profile::select('id', 'full_name', 'last_name', 'state', 'city', 'country_id', 'photo',
            'facebook_photo', 'state', 'url', 'user_id')->where('url', $slug)->first();

        $profile = Profile::select('id', 'full_name', 'last_name', 'state', 'city', 'country_id', 'photo',
            'facebook_photo', 'state', 'url')
            ->where('user_id', Auth::id())
            ->first();

        if(!isset($profile->id))
        {
            return abort(404);
        }

        $profilePhoto = ProfileVideo::select('title', 'link',
            'user_id', 'm_type', 'view_vid')->where('user_id', $profile->user_id)->get();

        return view('profile.show', compact('profile', 'profilePhoto', 'profileCurrent'));

    }

    public function photo()
    {

        $profile = [];
        if(Auth::check()) {
            $profile = Profile::select('id', 'full_name', 'last_name', 'state', 'city', 'country_id', 'photo',
                'facebook_photo', 'state', 'url')
                ->where('user_id', Auth::id())
                ->first();
        }

        return view('profile.photo', compact('profile'));
    }

    public function headshots()
    {
        $profile = [];
        if(Auth::check()) {
            $profile = Profile::select('id', 'full_name', 'last_name', 'state', 'city', 'country_id', 'photo',
                'facebook_photo', 'state', 'url')
                ->where('user_id', Auth::id())
                ->first();
        }

        return view('profile.headshots', compact('profile'));
    }

    public function artworks()
    {

        $profile = [];
        if(Auth::check()) {
            $profile = Profile::select('id', 'full_name', 'last_name', 'state', 'city', 'country_id', 'photo',
                'facebook_photo', 'state', 'url')
                ->where('user_id', Auth::id())
                ->first();
        }

        return view('profile.artworks', compact('profile'));

    }

    public function myProjects()
    {
        $genresToggle = Genre::where('iso', 'en')->orderBy('genre', 'ASC')->limit(21)->get();
        if (Auth::check()) {
            $genres = Genre::where('iso', 'en')->orderBy('genre', 'ASC')->limit(21)->get();
            $profile = Profile::select('id', 'full_name', 'last_name', 'state', 'city', 'country_id', 'photo',
                'facebook_photo', 'state', 'url')
                ->where('user_id', Auth::id())
                ->first();

            $state = MetaLocation::select('id', 'local_name')->where('id', $profile->state)->first();
            $city = MetaLocation::select('id', 'local_name')->where('id', $profile->city)->first();
            $country = MetaLocation::select('id', 'local_name')->where('id', $profile->country_id)->first();
            $occupation = $profile->occupations()->first();

            $occupations = [];
            $countries = [];
        } else {

            $genres = [];
            $profile = [];
            $state = [];
            $city = [];
            $country = [];
            $occupation = [];

            $occupations = Occupation::where('iso', 'en')->pluck('occupation', 'id');
            $countries = MetaLocation::where('type', 'CO')->pluck('local_name', 'id');
        }

        $projects = Project::selectRaw('projects.project_name, projects.id as project_id, projects.user_id, projects.poster, 
            projects.synopsis,profiles.full_name, profiles.last_name, profiles.photo,profiles.facebook_photo, profiles.url as profile_url, projects.urlproj as project_url,
            (SELECT GROUP_CONCAT(DISTINCT genres.genre
        ORDER BY genres.genre ASC
        SEPARATOR " | ") FROM genres INNER JOIN projects_genre ON projects_genre.genre_id = genres.id WHERE  projects_genre.project_id = projects.id ) as genres,
            (select count(views.project_id) as views from views where views.project_id = projects.id) as views')
            ->join('profiles', 'profiles.user_id', '=', 'projects.user_id')
            ->leftJoin('projects_medium', 'projects_medium.project_id', '=', 'projects.id')
            ->leftJoin('projects_genre', 'projects_genre.project_id', '=', 'projects.id')
            ->where('projects.user_id', auth()->user()->id)
            ->groupBy('projects.id')
            ->get();

        return view('profile.myprojects', compact('genres', 'genresToggle', 'profile', 'occupation', 'state',
            'city', 'country', 'projects', 'occupations', 'countries', 'states'));
    }

    public function coAuthored()
    {
        $genresToggle = Genre::where('iso', 'en')->orderBy('genre', 'ASC')->limit(21)->get();
        if (Auth::check()) {
            $genres = Genre::where('iso', 'en')->orderBy('genre', 'ASC')->limit(21)->get();
            $profile = Profile::select('id', 'full_name', 'last_name', 'state', 'city', 'country_id', 'photo',
                'facebook_photo', 'state', 'url')
                ->where('user_id', Auth::id())
                ->first();

            $state = MetaLocation::select('id', 'local_name')->where('id', $profile->state)->first();
            $city = MetaLocation::select('id', 'local_name')->where('id', $profile->city)->first();
            $country = MetaLocation::select('id', 'local_name')->where('id', $profile->country_id)->first();
            $occupation = $profile->occupations()->first();

            $occupations = [];
            $countries = [];
        } else {

            $genres = [];
            $profile = [];
            $state = [];
            $city = [];
            $country = [];
            $occupation = [];

            $occupations = Occupation::where('iso', 'en')->pluck('occupation', 'id');
            $countries = MetaLocation::where('type', 'CO')->pluck('local_name', 'id');
        }

        $projects = Project::selectRaw('projects.project_name, projects.id as project_id, projects.user_id, projects.poster, 
            projects.synopsis,profiles.full_name, profiles.last_name, profiles.photo,profiles.facebook_photo, profiles.url as profile_url, projects.urlproj as project_url,
            (SELECT GROUP_CONCAT(DISTINCT genres.genre
        ORDER BY genres.genre ASC
        SEPARATOR " | ") FROM genres INNER JOIN projects_genre ON projects_genre.genre_id = genres.id WHERE  projects_genre.project_id = projects.id ) as genres,
            (select count(views.project_id) as views from views where views.project_id = projects.id) as views')
            ->join('profiles', 'profiles.user_id', '=', 'projects.user_id')
            ->leftJoin('projects_medium', 'projects_medium.project_id', '=', 'projects.id')
            ->leftJoin('projects_genre', 'projects_genre.project_id', '=', 'projects.id')
            ->where('projects.user_id', auth()->user()->id)
            ->groupBy('projects.id')
            ->get();

        return view('profile.myprojects', compact('genres', 'genresToggle', 'profile', 'occupation', 'state',
            'city', 'country', 'projects', 'occupations', 'countries', 'states'));

    }


    public function readList()
    {
        $genresToggle = Genre::where('iso', 'en')->orderBy('genre', 'ASC')->limit(21)->get();
        if (Auth::check()) {
            $genres = Genre::where('iso', 'en')->orderBy('genre', 'ASC')->limit(21)->get();
            $profile = Profile::select('id', 'full_name', 'last_name', 'state', 'city', 'country_id', 'photo',
                'facebook_photo', 'state', 'url')
                ->where('user_id', Auth::id())
                ->first();

            $state = MetaLocation::select('id', 'local_name')->where('id', $profile->state)->first();
            $city = MetaLocation::select('id', 'local_name')->where('id', $profile->city)->first();
            $country = MetaLocation::select('id', 'local_name')->where('id', $profile->country_id)->first();
            $occupation = $profile->occupations()->first();

            $occupations = [];
            $countries = [];
        } else {

            $genres = [];
            $profile = [];
            $state = [];
            $city = [];
            $country = [];
            $occupation = [];

            $occupations = Occupation::where('iso', 'en')->pluck('occupation', 'id');
            $countries = MetaLocation::where('type', 'CO')->pluck('local_name', 'id');
        }

        $projects = Project::selectRaw('projects.project_name, projects.id as project_id, projects.user_id, projects.poster, 
            projects.synopsis,profiles.full_name, profiles.last_name, profiles.photo,profiles.facebook_photo, profiles.url as profile_url, projects.urlproj as project_url,
            (SELECT GROUP_CONCAT(DISTINCT genres.genre
        ORDER BY genres.genre ASC
        SEPARATOR " | ") FROM genres INNER JOIN projects_genre ON projects_genre.genre_id = genres.id WHERE  projects_genre.project_id = projects.id ) as genres,
            (select count(views.project_id) as views from views where views.project_id = projects.id) as views')
            ->join('profiles', 'profiles.user_id', '=', 'projects.user_id')
            ->leftJoin('projects_medium', 'projects_medium.project_id', '=', 'projects.id')
            ->leftJoin('projects_genre', 'projects_genre.project_id', '=', 'projects.id')
            ->where('projects.user_id', auth()->user()->id)
            ->groupBy('projects.id')
            ->get();

        return view('profile.myprojects', compact('genres', 'genresToggle', 'profile', 'occupation', 'state',
            'city', 'country', 'projects', 'occupations', 'countries', 'states'));

    }

    public function subscription()
    {
        $profile = [];
        if(Auth::check()) {
            $profile = Profile::select('id', 'full_name', 'last_name', 'state', 'city', 'country_id', 'photo',
                'facebook_photo', 'state', 'url')
                ->where('user_id', Auth::id())
                ->first();
        }

        return view('profile.subscription', compact('profile'));
    }

}
