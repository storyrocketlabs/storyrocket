<?php

namespace App\Http\Controllers\Auth;

use App\Jobs\OptimizeImages;
use App\Profile;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Auth\Events\Registered;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'full_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {

        if(isset($data['image']) && !empty($data['image']))
        {
            $pos  = strpos($data['image'], ';');
            $type = explode(':', substr($data['image'], 0, $pos))[1];
            $type = explode('/', $type);

            $file_image = explode(',', $data['image']);
            $image = base64_decode($file_image[1]);
            $fileName = substr(md5(uniqid(rand(), true)), 0, 10).'.'.strtolower($type[1]);

            if(!env('PLUGINS_PROD'))
            {
                $fileName = 'dev/'.$fileName;
            }

            \Storage::disk('public')->put('temp/' . $fileName, $image);
            $data['photo'] = $fileName;

            $job = (new OptimizeImages(storage_path('app/public/temp/' . $fileName)))->delay(0);

            $this->dispatch($job);
        }

        $user =  User::create([
            'create'=>date('Y-m-d H:i:s'),
            'active' => 0,
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'migrated'=> 1
        ]);

        $data['user_id'] = $user->id;

        $data['url'] =  seoUrl($data['full_name'].$data['last_name'].'.'.getPatter(8));

        $profile = Profile::create($data);



        if(isset($data['profile-occupations']) && !empty($data['profile-occupations']))
        {
            $occupations = explode(',', $data['profile-occupations']);

            foreach ($occupations as $occupation)
            {
                $profile->occupations()->attach($occupation);
            }
        }

        return $user;
    }

    public function validateEmail(Request $request)
    {
        $email = $request->get('email');
        $user = User::where('email', $email)->first();

        if(isset($user->id))
        {
            return response()->json(['success'=>true, 'valid'=>false]);
        }

        return response()->json(['success'=>true, 'valid'=>true]);
    }

    public function register(Request $request)
    {

        if($this->validator($request->all())->fails()) {
            return redirect()->route('register')->withErrors($this->validator($request->all())->errors())->withInput();
        }

        $user = $this->create($request->all());

        event(new Registered($user));

        session()->flash('email', $user->email);

        return redirect()->route('home');
    }


}
