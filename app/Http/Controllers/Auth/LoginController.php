<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Notifications\VerifyEmail;
use App\Profile;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\DB;
use Socialite;
use Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }


    public function login(Request $request)
    {

        $this->validateLogin($request);

        $valid_user = $this->validateUser($request);

        if (!$valid_user) {
            return $this->sendFailedLoginResponse($request);
        }

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        if ($this->attemptLogin($request)) {
            return $this->sendLoginResponse($request);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);

    }

    protected function attemptLogin(Request $request)
    {

        return $this->guard()->attempt(
            $this->credentials($request), $request->filled('remember')
        );
    }

    protected function validateUser($request)
    {

        $user = User::where('email', $request->get('email'))->first();

        if ($user && $user->migrated == 0) {
            $client = new Client();

            $response = $client->request('POST', 'https://www.storyrocket.com/sign-in/validUser', [
                    'form_params' => [
                        'email' => $request->get('email'),
                        'pass' => $request->get('password')
                    ]
                ]
            );

            $result = json_decode($response->getBody());

            if ($result->sucess) {
                $user->password = bcrypt($request->get('password'));
                $user->migrated = 1;
                $user->save();

                return true;
            } else {
                return false;
            }

        }

        return true;
    }


    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->setScopes(['email', 'public_profile'])->redirect();
    }

    public function handleProviderCallback($provider, Request $request)
    {

        DB::beginTransaction();
        try{
            // Obtenemos los datos del usuario
            $social_user = Socialite::driver($provider)->fields(['first_name', 'last_name','name', 'email'])->user();

            // Comprobamos si el usuario ya existe
            if ($user = User::where('email', $social_user->email)->first()) {

                if(empty($user->facebook_id))
                {
                    $user->facebook_id;
                    $user->save();

                    $profile = $user->profile;

                    $profile->facebook_photo = $social_user->avatar_original;
                    $profile->photo = $social_user->avatar_original;
                    $profile->save();

                }

            } else {
                // En caso de que no exista creamos un nuevo usuario con sus datos.
                $user = User::create([
                    'email' => $social_user->email,
                    'password'=>bcrypt(getPatter(8)),
                    'create'=> date("Y-m-d H:i:s"),
                    'facebook_id'=>$social_user->id,
                    'active'=>1,
                    'migrated'=>1
                ]);

                Profile::create([
                    'full_name'=>$social_user->user['first_name'],
                    'last_name'=>$social_user->user['last_name'],
                    'email'=>$social_user->email,
                    'facebook_photo' => $social_user->avatar_original,
                    'photo' => $social_user->avatar_original,
                    'url'=>seoUrl($social_user->user['last_name'].'.'.getPatter(8)),
                    'user_id'=>$user->id
                ]);

            }

            DB::commit();

            return $this->authAndRedirect($user); // Login y redirección

        }catch (\Exception $exception)
        {
            DB::rollBack();

            return redirect()->route('login');

        }
    }


    public function authAndRedirect($user)
    {
        Auth::login($user);

        return redirect()->to('/');
    }


}
