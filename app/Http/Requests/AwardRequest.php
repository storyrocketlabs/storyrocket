<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AwardRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title_award'=>'required',
            'year_award'=>'required',
            'nominate_award'=>'required',
            'category_award'=>'required',
            'result_award'=>'required',
            'description_award'=>'required'
        ];
    }
}
