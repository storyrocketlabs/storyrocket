<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PaymentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required',
            'last_name'=>'required',
            'name_card'=>'required',
            'expedition_date'=>'required',
            'cvv'=>'required',
            'expedition_date'=>'required',
            'country'=>'required',
            'state'=>'required',
            'city'=>'required',
            'postal_code'=>'required'
        ];
    }
}
