<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterSubscriptionForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
       return !!$this->user();;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'card-number'=>'required',
            'month'=>'required',
            'year'=>'required',
            'card-cvc'=>'required',
            'card-first-name'=>'required',
            'card-last-name'=>'required'
        ];
    }
}
