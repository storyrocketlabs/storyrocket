<?php

namespace App\Billing;

use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{

    protected $fillable = [
        'codigo',
        'presumed',
        'state',
        'expiration',
        'params',
        'descrip',
        'stripe_id'
    ];

    protected $dates =[
        'expiration'
    ];

}
