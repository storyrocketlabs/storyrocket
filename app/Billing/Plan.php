<?php

namespace App\Billing;

use Illuminate\Database\Eloquent\Model;

class Plan extends Model
{

    protected $fillable = [
        'name',
        'price',
        'projects',
        'stripe_id',
        'cycle'
    ];

}
