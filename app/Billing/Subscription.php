<?php
/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 10/15/18
 * Time: 11:46 PM
 */

namespace App\Billing;

use App\User;
use Carbon\Carbon;
use Stripe\Customer;
use Stripe\Subscription as StripeSubscription;
use Stripe\Coupon as StripeCoupon;

class Subscription
{

    protected $user;

    protected $coupon;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function create(Plan $plan, $token)
    {
        $customer = Customer::create([
            'email' => $this->user->email,
            'source' => $token,
            'plan' => $plan->stripe_id,
            'coupon'=>$this->coupon
        ]);

        if(isset($this->coupon) && !empty($this->coupon))
        {
            $coupon = Coupon::where('stripe_id', $this->coupon)->first();
            $this->user->coupons()->attach($coupon->id, ['fech_reg'=>date('Y-m-d H:i:s')]);
        }

        $subscriptionId = $customer->subscriptions->data[0]->id;

        $this->user->activate($customer->id, $subscriptionId, $plan->stripe_id);
    }


    public function retrieveStripeSubscription()
    {
        return StripeSubscription::retrieve($this->user->stripe_subscription);
    }

    public function cancel($atPeriodEnd = true)
    {
        $customer = Customer::retrieve($this->user->stripe_id);

        $subscription = $customer->cancelSubscription(['at_period_end'=>$atPeriodEnd]);

        $endDate = Carbon::createFromTimestamp($subscription->current_period_end);

        $this->user->deactivate($endDate);

    }

    public function cancelImmediately()
    {
        return $this->cancel(false);
    }

    public function usingCoupon($coupon)
    {
        if($coupon)
        {
            $this->coupon = $coupon;
        }

        return $this;

    }

    public function validateCoupon($coupon)
    {
        $coupon = Coupon::where('codigo', $coupon)->where('state', 1)->first();

        if(!isset($coupon->id))
        {
            return null;
        }

        $active = $coupon->id ?? Carbon::now()->lt(Carbon::instance($coupon->expiration));

        if(!$active)
        {
            $coupon->state = 0;
            $coupon->save();
            return null;
        }

        return $coupon->stripe_id;

    }

    public function retrieveStripeCustomer()
    {
        return Customer::retrieve($this->user->stripe_id);
    }

    public function resume($plan = nulll)
    {
        $subscription = $this->retrieveStripeSubscription();

        $subscription->plan = $plan ?? $this->user->stripe_plan;
        $subscription->save();

        $this->user->activate();

    }

    public function retrieveCoupon($coupon)
    {
        $coupon = StripeCoupon::retrieve($coupon);

        return $coupon;
    }



}


/*
 * frame.style {
    width: 100vw;
    height: 100vh;
    display: block;
    border: none;
}

body.style {
margin: 0;
}
 */
