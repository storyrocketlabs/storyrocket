<?php

namespace App\Billing;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{

    protected $fillable = [
        'date_start',
        'date_end',
        'type_t',
        'amount',
        'method',
        'plan_id',
        'user_id',
        'charge_id',
        'state',
        'status',
        'type',
        'params'
    ];

}
