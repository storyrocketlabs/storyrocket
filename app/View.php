<?php

namespace App;


use Illuminate\Database\Eloquent\Model;


class View extends Model
{

    protected $fillable = [
        'project_id',
        'date_view',
        'ip',
        'user_id'
    ];


}
