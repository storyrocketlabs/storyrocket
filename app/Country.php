<?php

namespace App;


use Illuminate\Database\Eloquent\Model;

class Country extends Model
{

    protected $fillable = [
        'occupation',
        'iso',
        'lang_id'
    ];

}
