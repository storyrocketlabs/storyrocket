<?php

namespace App;


use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{

    protected $table = 'comments';

    protected $fillable = [
        'user_id',
        'project_id',
        'id',
        'comment',
        'date_comment'
    ];


}
