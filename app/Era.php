<?php

namespace App;


use Illuminate\Database\Eloquent\Model;
use DB;

class Era extends Model
{

    protected $table = 'eras';

    protected $fillable = [
        'era',
        'iso',
        'lang_id'
    ];




}
