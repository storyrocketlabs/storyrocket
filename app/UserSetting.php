<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserSetting extends Model
{
    protected $fillable = ['user_id','values'];


    public function user()
    {
        return $this->belongsTo(User::class);
    }


    public function getValuesAttribute()
    {
        return json_decode($this->attributes['values']);
    }
}
