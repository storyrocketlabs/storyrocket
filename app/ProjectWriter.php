<?php

namespace App;


use Illuminate\Database\Eloquent\Model;

class ProjectWriter extends Model
{

    protected $fillable = [
        'writer_name',
        'writer_email',
        'project_id',
        'state_write',
        'params',
        'idgrop',
    ];


}
