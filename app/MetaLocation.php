<?php

namespace App;


use Illuminate\Database\Eloquent\Model;

class MetaLocation extends Model
{
    protected $table = 'meta_location';

    protected $fillable = [
        'iso',
        'local_name',
        'type',
        'in_location',
        'geo_lat',
        'geo_lng',
        'db_id',
        'Clasification'
    ];

}
