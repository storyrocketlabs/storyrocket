<?php

namespace App;


use Illuminate\Database\Eloquent\Model;

class ProfileVideo extends Model
{

    protected $fillable = [
        'title',
        'link',
        'user_id',
        'm_type',
        'view_vid'
    ];


}
