<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ImportedUser extends Model
{

    const IMPORTED_USER_DISABLED = '0';
    const IMPORTED_USER_ENABLED = '1';


    const ID_PARTNER_BOWKER = 1;
    const BILLING_PLAN_ID = 3;
    const BILLING_PLAN_NAME = "PRO_YEARLY";
    const BILLING_PLAN_TYPE = "PRO_YEARLY";
    const BILLING_PLAN_PERIOD = "ANUAL";
    const BILLING_PLAN_MONTH = 25.00;
    const BILLING_PLAN_YEAR = 195.00;
    const BILLING_PLAN_PROJECT = 20;

    const GETTY_IMAGES_CANT = 50;


    const PAYMENT_METHOD = "import";
    const PAYMENT_STATUS = 1;

    const PARTNER_ID = 1;
    const PARTNER_NAME = "Bowker";
    const PARTNER_EMAIL = "contacto@bowker.com";
    const IMPORTED_USER_STATUS = "new";
    const IMPORTED_USER_IS_ACTIVE = 1;
    const IMPORTED_USER_CURRENCY = 'USD';


    protected $fillable= [
        'user_id',
        'user_firstname',
        'user_lastname',
        'user_email',
        'user_city',
        'user_state',
        'user_country',
        'user_invoice',
        'user_couid',
        'ubigeo',
        'getty_images_cant',
        'billing_plan_id',
        'billing_plan_name',
        'billing_plan_price_month',
        'billing_plan_price_year',
        'billing_plan_project',
        'billing_plan_type',
        'partner_id',
        'partner_name',
        'partner_email',
        'token',
        'status',
        'is_active',
    ];

    public function isEnabled()
    {
        return $this->status = ImportedUser::IMPORTED_USER_ENABLED;
    }

    public function isDisabled()
    {
        return $this->status = ImportedUser::IMPORTED_USER_DISABLED;
    }


    static function assignGettyimagesToUserAtStoryRocket($email)
    {

        $user = DB::connection('mysql_old')->select('select UserId from users where email = ?', [$email]);
        $user = $user[0];
        /**
         * payment_getty
         */
        $dateNow = date("Y-m-d h:i:s");
        DB::connection('mysql_old')->insert(
            'insert into payment_getty 
              (
              `type`,
              `quantity`,
              `UserID`,
              `state`,
              `fecha`,
              `flat`
              ) values 
              (?, ?, ?, ?, ?, ?)',
            [
                ImportedUser::BILLING_PLAN_ID,
                ImportedUser::GETTY_IMAGES_CANT,
                $user->UserId,
                1,
                $dateNow,
                1
            ]);

    }
    static function assignPlanToUserAtStoryRocket($email)
    {
        $user = DB::connection('mysql_old')->select('select UserId from users where email = ?', [$email]);
        $user = $user[0];

        /**
         * invoice
         */
        $dateNow = date("Y-m-d h:i:s");
        $dateEnd = date('Y-m-d h:i:s', strtotime('+1 years'));
        DB::connection('mysql_old')->insert(
            'insert into invoice 
              (
              `fechaRegitro`,
              `fechaInicio`,
              `fechaFin`,
              `estado`,
              `pDisponible`,
              `moneda`,
              `UserID`
              ) values 
              (?, ?, ?, ?, ?, ?, ?)',
            [
                $dateNow,
                $dateNow,
                $dateEnd,
                1,
                ImportedUser::BILLING_PLAN_PROJECT,
                ImportedUser::IMPORTED_USER_CURRENCY,
                $user->UserId
            ]);

        /**
         * invoice Detail
         */

        $dateNowFormatX = date("d/m/Y h:i:s");
        $dateEndFormatX = date("d/m/Y h:i:s", strtotime('+1 years'));
        $dateSaveX = $dateNowFormatX . " - " . $dateEndFormatX;

        DB::connection('mysql_old')->insert(
            'insert into invoiceDetail 
              (
                `idinvoice`, 
                `ID_plan`, 
                `metodo`, 
                `stripeParam`, 
                `estado`, 
                `type`, 
                `precio`, 
                `periodo`, 
                `fechar`, 
                `cupon`, 
                `flat`
              ) values 
              (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)',
            [
                DB::connection('mysql_old')->getPdo()->lastInsertId(),
                ImportedUser::BILLING_PLAN_ID,
                'import',
                '',
                1,
                ImportedUser::BILLING_PLAN_TYPE,
                "$".ImportedUser::BILLING_PLAN_YEAR,
                ImportedUser::BILLING_PLAN_PERIOD,
                $dateSaveX,
                '',
                1
            ]);

    }

    static function getUbigeo($country, $state, $city)
    {
        $ubigeo = array();

        $ubigeoQuery = DB::connection('mysql_old')->select('
        SELECT 
        ci.local_name as "city_name", 
        ci.id as "city_id", 
        re.local_name as "state_name", 
        re.id as "state_id", 
        co.local_name as "country_name",
        co.id as "country_id" 
        FROM meta_location co
        INNER JOIN meta_location re ON (re.local_name=? AND re.type = "RE" AND re.in_location=co.id)
        INNER JOIN meta_location ci ON (ci.local_name=? AND ci.type = "CI" AND ci.in_location=re.id)
        WHERE co.local_name=? AND co.type = "CO" LIMIT 1', [$state, $city, $country]);


        if(isset($ubigeoQuery[0])) {

            $user = $ubigeoQuery[0];
            $cityName = $user->city_name;
            $stateName = $user->state_name;
            $countryName = $user->country_name;
            $cityId = $user->city_id;
            $stateId = $user->state_id;
            $countryId = $user->country_id;

            $ubigeo["cityName"] = $cityName;
            $ubigeo["stateName"] = $stateName;
            $ubigeo["stateName"] = $stateName;
            $ubigeo["countryName"] = $countryName;

            $ubigeo["cityId"] = $cityId;
            $ubigeo["stateId"] = $stateId;
            $ubigeo["countryId"] = $countryId;
        }
        return $ubigeo;
    }

    static function validateIfUserExist($email)
    {
        $billing = DB::connection('mysql_old')->select('select email from users where email = ?', [$email]);
        if(empty($billing)) {
            return false;
        }
        return true;
    }

    static function defineUrlProfile($firstname)
    {
        $length = 8;
        $pattern = "1234567890abcdefghijklmnopqrstuvwxyz.";
        $url='';
        for($i = 0; $i < $length; $i++) {
            $url .= $pattern{rand(0, 35)};
        }
        $url_profile =$firstname.'.'.$url;
        return $url_profile;
    }

    static function createUserAtStoryRocket($firstname, $lastname, $email, $token, $cityId, $stateId, $countryId, $couid)
    {

        /**
         * Users
         */

        $dateNow = date("Y-m-d h:i:s");
         DB::connection('mysql_old')->insert(
            'insert into users 
              (Email, 
              Password, 
              `Create`, 
              LastLogin, 
              Login, 
              Active, 
              FacebookID, 
              State_Active, 
              timetamp, 
              send_mail,
              imported_user,
              imported_user_active,
              token,
              partner_id
              ) values 
              (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,  ?)',
            [
                $email,
                '',
                $dateNow,
                $dateNow,
                0,
                1,
                '',
                0,
                time(),
                0,
                1,
                1,
                $token,
                $couid,
                1
            ]);

        /**
         * Profile
         */

        DB::connection('mysql_old')->insert(
            'insert into profile 
              (FullName, 
              LastName, 
              Email,
              Gender,
              Photo,
              PhotoNew,
              FacebookPhoto,
              Website,
              Day,
              Year,
              Month,
              CountryID,
              State,
              City,
              ZipCode
              , `Facebook`, `Twitter`, `Google`, `Linkedin`, `Youtube`, `Instagram`, `Who_am_i`, `Casting`, `Trailer`, `On_web`, `url`, `headline`, `description`, `hoping`, `dream`, `background_position`, `Birthday`
              ) values
              (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)',
            [
                $firstname,
                $lastname,
                $email,
                '',
                '',
                '',
                '',
                '',
                '',
                '',
                '',
                $countryId,
                $stateId,
                $cityId,
                '',
                '',
                '',
                '',
                '',
                '',
                '',
                '',
                '',
                '',
                '',
                (ImportedUser::defineUrlProfile($firstname)),
                '',
                '',
                '',
                '',
                0,
                'default',
            ]);

        $user = DB::connection('mysql_old')->select('select users.email, profile.FullName, profile.LastName, users.UserID from users left join profile on profile.UserID = users.UserID where users.email = ?', [$email]);


        return $user;

    }

}
