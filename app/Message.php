<?php

namespace App;


use Illuminate\Database\Eloquent\Model;

class Message extends Model
{

    protected $fillable = [
        'asunto',
        'type',
        'fecha_message',
        'new_old',
        'isread_mail',
        'created_at',
        'update_at'
    ];

}
