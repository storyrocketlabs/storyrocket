<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIdToAwardTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE award ENGINE = InnoDB');
        Schema::rename('award', 'awards');

        Schema::table('awards', function (Blueprint $table) {
            DB::statement("ALTER TABLE `awards` CHANGE COLUMN `id_award` `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT FIRST");
            DB::statement("ALTER TABLE `awards` CHANGE COLUMN `UserID` `user_id` INT(11) UNSIGNED");
            DB::statement("DELETE FROM awards WHERE user_id NOT IN (SELECT  users.id FROM users)");
            $table->renameColumn('nonimate_awar', 'nominate_award');
            $table->renameColumn('category_awar', 'category_award');
            $table->renameColumn('result_awar', 'result_award');
            $table->renameColumn('descrip_awar', 'description_award');
            $table->renameColumn('title_awar', 'title_award');
            $table->renameColumn('year_awar', 'year_award');

            $table->foreign('user_id', 'award_users')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('awards', function (Blueprint $table) {

            $table->dropForeign('award_users');
            DB::statement("ALTER TABLE `awards` CHANGE COLUMN  `id` `id_award` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT FIRST");
            DB::statement("ALTER TABLE `awards` CHANGE COLUMN  `user_id` `UserID` INT(11) UNSIGNED");
            $table->renameColumn('nominate_award', 'nonimate_awar');
            $table->renameColumn('category_award', 'category_awar');
            $table->renameColumn('result_award', 'result_awar');
            $table->renameColumn('description_award', 'descrip_awar');
            $table->renameColumn('title_award', 'title_awar');
            $table->renameColumn('year_award', 'year_awar');

            $table->dropTimestamps();

        });

        Schema::rename('awards', 'award');
    }
}
