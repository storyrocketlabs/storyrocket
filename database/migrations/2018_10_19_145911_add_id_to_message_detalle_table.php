<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIdToMessageDetalleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE messagesDetalle ENGINE = InnoDB');
        Schema::rename('messagesDetalle', 'message_details');
        
        Schema::table('message_details', function (Blueprint $table) {
            
            DB::statement("ALTER TABLE `message_details` CHANGE COLUMN `idmsgDetalle` `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT FIRST");
            DB::statement("ALTER TABLE `message_details` CHANGE COLUMN `idmessage` `message_id` INT(11) UNSIGNED");
            DB::statement("ALTER TABLE `message_details` CHANGE COLUMN `ProjectID` `project_id` INT(11) UNSIGNED");
            DB::statement("DELETE FROM message_details WHERE project_id NOT IN (SELECT  projects.id FROM projects)");
            DB::statement("DELETE FROM message_details WHERE message_id NOT IN (SELECT  messages.id FROM messages)");
            $table->renameColumn('datesend', 'date_send');
            $table->renameColumn('fromsg', 'from_message');
            $table->renameColumn('tomsg', 'to_message');
            $table->renameColumn('isread', 'is_read');
            $table->renameColumn('datedowloand', 'date_download');
            $table->renameColumn('Message', 'message_text');
            $table->timestamps();
            $table->foreign('message_id', 'detail_message')->references('id')->on('messages');
            $table->foreign('project_id', 'detail_project')->references('id')->on('projects');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('message_details', function (Blueprint $table) {
            $table->dropForeign('detail_message');
            $table->dropForeign('detail_project');

            DB::statement("ALTER TABLE `message_details` CHANGE COLUMN `id` `idmsgDetalle` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT FIRST");
            DB::statement("ALTER TABLE `message_details` CHANGE COLUMN `message_id` `idmessage` INT(11) UNSIGNED");
            DB::statement("ALTER TABLE `message_details` CHANGE COLUMN `project_id` `ProjectID` INT(11) UNSIGNED");

            $table->renameColumn('date_send', 'datesend');
            $table->renameColumn('from_message', 'fromsg');
            $table->renameColumn('to_message', 'tomsg');
            $table->renameColumn('is_read', 'isread');
            $table->renameColumn('date_download', 'datedowloand');
            $table->renameColumn('message_text', 'Message');
            $table->dropTimestamps();
        });

        Schema::rename('message_details', 'messagesDetalle');
    }
}
