<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIdToBilling extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE billing ENGINE = InnoDB');
        Schema::rename('billing', 'payments');

        Schema::table('payments', function (Blueprint $table) {
            DB::statement("ALTER TABLE `payments` CHANGE COLUMN `ID_Billing` `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT FIRST");
            DB::statement("ALTER TABLE `payments` CHANGE COLUMN `UserID` `user_id` INT(11) UNSIGNED NOT NULL");
            DB::statement("DELETE FROM payments WHERE user_id NOT IN (SELECT  users.id FROM users)");

            $table->renameColumn('stripe_param', 'charge_id')->nullable();
            $table->renameColumn('Metodo', 'method');
            $table->renameColumn('Total', 'amount');
            $table->renameColumn('Tipo', 'type_t')->nullable();
            $table->renameColumn('ID_plan', 'plan_id')->nullable();
            $table->renameColumn('Fecha_Inicio', 'date_start');
            $table->renameColumn('Fecha_Fin', 'date_end');
            $table->timestamps();

            $table->foreign('user_id', 'payment_user')->references('id')->on('users');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('payments', function (Blueprint $table) {
            $table->dropForeign('payment_user');

            DB::statement("ALTER TABLE `payments` CHANGE COLUMN  `id` `ID_Billing` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT FIRST");
            DB::statement("ALTER TABLE `payments` CHANGE COLUMN  `user_id` `UserID` INT(11) UNSIGNED NOT NULL");
            $table->dropTimestamps();

            $table->renameColumn('charge_id', 'stripe_param')->nullable();
            $table->renameColumn('method', 'Metodo');
            $table->renameColumn('amount', 'Total');
            $table->renameColumn('type_t', 'Tipo')->nullable();
            $table->renameColumn('plan_id', 'ID_plan')->nullable();
            $table->renameColumn('date_start', 'Fecha_Inicio');
            $table->renameColumn('date_end', 'Fecha_Fin');

        });

        Schema::rename('payments', 'billing');
    }
}
