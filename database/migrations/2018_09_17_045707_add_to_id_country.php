<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddToIdCountry extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('citynames');
        Schema::dropIfExists('regionnames');
        Schema::dropIfExists('cities');
        Schema::dropIfExists('regions');
        Schema::dropIfExists('countrynames');
        Schema::dropIfExists('countries');
        Schema::dropIfExists('locales');

        Schema::rename('country', 'countries');

        Schema::table('countries', function (Blueprint $table) {
            DB::statement("ALTER TABLE `countries` CHANGE COLUMN `country_id` `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT FIRST");

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::rename('countries', 'country');

        Schema::table('country', function (Blueprint $table) {
            DB::statement("ALTER TABLE `country` CHANGE COLUMN  `id` `country_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT FIRST");

        });
    }
}
