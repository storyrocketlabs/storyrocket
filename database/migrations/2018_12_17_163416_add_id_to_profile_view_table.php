<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIdToProfileViewTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profile_views', function (Blueprint $table) {
            $table->integer('profile_user_id')->unsigned();
            $table->integer('visitor_user_id')->unsigned();
            $table->integer('view')->default(1);
            $table->tinyInteger('mark')->default(0);
            $table->dateTime('last_visit');
            $table->timestamps();

            $table->foreign('profile_user_id', 'profile_user_id_users')->references('id')->on('users');
            $table->foreign('visitor_user_id', 'visitor_user_id_users')->references('id')->on('users');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profile_views');

    }
}
