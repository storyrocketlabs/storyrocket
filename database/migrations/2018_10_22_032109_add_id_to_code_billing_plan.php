<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIdToCodeBillingPlan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE code_billing ENGINE = InnoDB');
        Schema::rename('code_billing', 'coupons');

        Schema::table('coupons', function (Blueprint $table) {
            DB::statement("ALTER TABLE `coupons` CHANGE COLUMN `id_code_bill` `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT FIRST");
            $table->renameColumn('cupon', 'stripe_id');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('coupons', function (Blueprint $table) {
            DB::statement("ALTER TABLE `coupons` CHANGE COLUMN `id` `id_code_bill` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT FIRST");
            $table->renameColumn('stripe_id', 'cupon');
            $table->dropTimestamps();

        });

        Schema::rename('coupons','code_billing');
    }
}
