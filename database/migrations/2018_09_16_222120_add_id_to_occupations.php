<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIdToOccupations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE occupation ENGINE = InnoDB');
        Schema::rename('occupation', 'occupations');


        Schema::table('occupations', function (Blueprint $table) {
            DB::statement("ALTER TABLE `occupations` CHANGE COLUMN `OccupationID` `OccupationID` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT FIRST");

            $table->renameColumn('OccupationID','id');
            $table->renameColumn('Occupation','occupation');
            $table->renameColumn('Iso','iso');
            $table->renameColumn('LangID','lang_id');
            $table->timestamps();

        });

        DB::statement("INSERT INTO `occupations` ( `id`, `iso`, `lang_id`, `occupation`) VALUES ( 80,'en',80 ,'Producer' )");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('occupations', function (Blueprint $table) {
            DB::statement("ALTER TABLE `occupations` CHANGE COLUMN `id` `id` INT(11) NOT NULL AUTO_INCREMENT FIRST");
            DB::statement("DELETE FROM `occupations` WHERE id = 80");

            $table->renameColumn('id', 'OccupationID');
            $table->renameColumn('occupation', 'Occupation');
            $table->renameColumn('iso', 'Iso');
            $table->renameColumn('lang_id', 'LangID');
            $table->dropTimestamps();

        });

        Schema::rename('occupations', 'occupation');
    }
}
