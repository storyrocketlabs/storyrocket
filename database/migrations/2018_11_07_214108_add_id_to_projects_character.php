<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIdToProjectsCharacter extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::rename('projects_character', 'project_characters');
        DB::statement('ALTER TABLE project_characters ENGINE = InnoDB');

        Schema::table('project_characters', function (Blueprint $table) {
            DB::statement("ALTER TABLE `project_characters` CHANGE COLUMN `CharacterID` `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT FIRST");
            DB::statement("ALTER TABLE `project_characters` CHANGE COLUMN `ProjectID` `project_id` INT(11) UNSIGNED");
            DB::statement("DELETE FROM project_characters WHERE project_id NOT IN (SELECT  projects.id FROM projects)");

            $table->renameColumn('CharacterName', 'character_name');
            $table->renameColumn('Gender', 'gender');
            $table->renameColumn('Age', 'age');
            $table->renameColumn('Description', 'description');
            $table->renameColumn('RealActhor', 'real_actor');
            $table->renameColumn('Image', 'image');
            $table->renameColumn('Position', 'position');
            $table->renameColumn('imagetype', 'image_type');
            $table->renameColumn('idgetty_images', 'getty_images_id');
            $table->timestamps();
            $table->foreign('project_id', 'project_character')->references('id')->on('projects');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('project_characters', function (Blueprint $table) {
            $table->dropForeign('project_character');
            $table->dropTimestamps();
            DB::statement("ALTER TABLE `project_characters` CHANGE COLUMN  `id` `CharacterID` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT FIRST");
            DB::statement("ALTER TABLE `project_characters` CHANGE COLUMN  `project_id` `ProjectID` INT(11) UNSIGNED");
            $table->renameColumn( 'character_name', 'CharacterName');
            $table->renameColumn( 'gender', 'Gender');
            $table->renameColumn( 'age', 'Age');
            $table->renameColumn( 'description', 'Description');
            $table->renameColumn( 'real_actor', 'RealActhor');
            $table->renameColumn( 'image', 'Image');
            $table->renameColumn( 'position', 'Position');
            $table->renameColumn( 'image_type', 'imagetype');
            $table->renameColumn( 'getty_images_id', 'idgetty_images');

        });

        Schema::rename('project_characters', 'projects_character');

    }
}
