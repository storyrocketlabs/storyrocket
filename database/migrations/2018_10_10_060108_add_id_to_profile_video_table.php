<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIdToProfileVideoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE profile_video ENGINE = InnoDB');
        Schema::rename('profile_video', 'profile_videos');

        Schema::table('profile_videos', function (Blueprint $table) {
            DB::statement("ALTER TABLE `profile_videos` CHANGE COLUMN `id` `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT FIRST");
            DB::statement("ALTER TABLE `profile_videos` CHANGE COLUMN `UserID` `user_id` INT(11) UNSIGNED");
            DB::statement("DELETE FROM profile_videos WHERE user_id NOT IN (SELECT  users.id FROM users)");
            $table->renameColumn('Mtype','m_type');
            $table->timestamps();
            $table->foreign('user_id', 'video_users')->references('id')->on('users');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('profile_videos', function (Blueprint $table) {
            $table->dropForeign('video_users');
            DB::statement("ALTER TABLE `profile_videos` CHANGE COLUMN  `user_id` `UserID` INT(11) UNSIGNED");
            $table->renameColumn('m_type','Mtype');
            $table->dropTimestamps();
        });

        Schema::rename('profile_videos', 'profile_video');

    }
}
