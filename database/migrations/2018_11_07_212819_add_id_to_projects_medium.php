<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIdToProjectsMedium extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('projects_medium', function (Blueprint $table) {
            DB::statement("ALTER TABLE `projects_medium` CHANGE COLUMN `id` `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT FIRST");
            DB::statement("ALTER TABLE `projects_medium` CHANGE COLUMN `ProjectID` `project_id` INT(11) UNSIGNED");
            DB::statement("ALTER TABLE `projects_medium` CHANGE COLUMN `MediumID` `medium_id` INT(11) UNSIGNED");
            DB::statement("DELETE FROM projects_medium WHERE project_id NOT IN (SELECT  projects.id FROM projects)");
            DB::statement("DELETE FROM projects_medium WHERE medium_id NOT IN (SELECT  mediums.id FROM mediums)");

            $table->foreign('project_id', 'project_medium')->references('id')->on('projects');
            $table->foreign('medium_id', 'medium_project')->references('id')->on('mediums');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('projects_medium', function (Blueprint $table) {
            $table->dropForeign('project_medium');
            $table->dropForeign('medium_project');

            DB::statement("ALTER TABLE `projects_medium` CHANGE COLUMN  `project_id` `ProjectID` INT(11) UNSIGNED");
            DB::statement("ALTER TABLE `projects_medium` CHANGE COLUMN  `medium_id` `MediumID` INT(11) UNSIGNED");
        });
    }
}
