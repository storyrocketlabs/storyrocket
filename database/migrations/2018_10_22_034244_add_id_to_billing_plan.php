<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIdToBillingPlan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE billing_plan ENGINE = InnoDB');
        Schema::rename('billing_plan', 'plans');

        Schema::table('plans', function (Blueprint $table) {
            DB::statement("ALTER TABLE `plans` CHANGE COLUMN `ID_plan` `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT FIRST");
            $table->renameColumn('Name_plan', 'name');
            $table->renameColumn('Price_month', 'price');
            $table->renameColumn('Min_Projects', 'projects');
            $table->dropColumn('Price_year');
            $table->dropColumn('Max_Projects');
            $table->string('stripe_id');
            $table->string('cycle');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('plans', function (Blueprint $table) {
            DB::statement("ALTER TABLE `plans` CHANGE COLUMN  `id` `ID_plan` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT FIRST");
            $table->renameColumn('name', 'Name_plan');
            $table->renameColumn('price', 'Price_month');
            $table->renameColumn('projects', 'Min_Projects');
            $table->decimal('Price_year', 5,2);
            $table->integer('Max_Projects');
            $table->dropColumn('stripe_id');
            $table->dropColumn('cycle');
            $table->dropTimestamps();
        });

        Schema::rename('plans','billing_plan');

    }
}
