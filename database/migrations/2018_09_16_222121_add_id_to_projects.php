<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIdToProjects extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE projects ENGINE = InnoDB');
        DB::table('projects')
            ->where('OccupationID', 0)
            ->update(array('OccupationID' => NULL));

        DB::table('projects')
            ->where('UserID', 0)
            ->orWhere('UserID', 720)
            ->update(array('UserID' => NULL));

        Schema::table('projects', function (Blueprint $table) {
            DB::statement("ALTER TABLE `projects` CHANGE COLUMN `ProjectID` `ProjectID` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT FIRST");
            DB::statement("ALTER TABLE `projects` CHANGE COLUMN `OccupationID` `OccupationID` INT(11) UNSIGNED FIRST");
            DB::statement("ALTER TABLE `projects` CHANGE COLUMN `UserID` `user_id` INT(11) UNSIGNED FIRST");
            DB::statement("DELETE FROM projects WHERE user_id NOT IN (SELECT  users.id FROM users)");
            $table->timestamps();
            $table->renameColumn('ProjectID', 'id');
            $table->renameColumn('OccupationID', 'occupation_id');
            $table->renameColumn('ProjectName', 'project_name');
            $table->renameColumn('RegistrationID', 'registration_id');
            $table->renameColumn('Project_office', 'project_office');
            $table->renameColumn('RegistrationNumber', 'registration_number');
            $table->renameColumn('LogLine', 'log_line');
            $table->renameColumn('Synopsis', 'synopsis');
            $table->renameColumn('Treatment', 'treatment');
            $table->renameColumn('Story', 'story');
            $table->renameColumn('Tag', 'tag');
            $table->renameColumn('MaterialID', 'material_id');
            $table->renameColumn('LanguageID', 'language_id');
            $table->renameColumn('PrincipalLocation', 'principal_location');
            $table->renameColumn('PrincipalState', 'principal_state');
            $table->renameColumn('City', 'city');
            $table->renameColumn('City2', 'city2');
            $table->renameColumn('EraID', 'era_id');
            $table->renameColumn('Example', 'example');
            $table->renameColumn('BudgetID', 'budget_id');
            $table->renameColumn('RatingID', 'rating_id');
            $table->renameColumn('Poster', 'poster');
            $table->renameColumn('Banner', 'banner');
            $table->renameColumn('Tagline', 'tagline');
            $table->renameColumn('ProjectFile', 'project_file');
            $table->renameColumn('ProjectTriler', 'project_trailer');
            $table->renameColumn('PdfProjectFile', 'pdf_project_file');
            $table->renameColumn('Pdfview', 'pdf_view');
            $table->renameColumn('LoglineNumber', 'log_line_number');
            $table->renameColumn('CreateDate', 'create_date');
            $table->renameColumn('Status', 'status');
            $table->renameColumn('UserLike', 'user_like');
            $table->renameColumn('ID_plan', 'id_plan');
            $table->renameColumn('Above', 'above');
            $table->renameColumn('Below', 'below');
            $table->foreign('occupation_id', 'project_occupation')->references('id')->on('occupations');
            $table->foreign('user_id', 'project_user')->references('id')->on('users');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('projects', function (Blueprint $table) {
            $table->dropForeign('project_occupation');
            $table->dropForeign('project_user');
            $table->dropTimestamps();
            $table->renameColumn( 'id', 'ProjectID');
            $table->renameColumn( 'user_id', 'UserID');
            $table->renameColumn( 'occupation_id', 'OccupationID');
            $table->renameColumn( 'project_name', 'ProjectName');
            $table->renameColumn( 'registration_id', 'RegistrationID');
            $table->renameColumn( 'project_office', 'Project_office');
            $table->renameColumn( 'registration_number', 'RegistrationNumber');
            $table->renameColumn( 'log_line', 'LogLine');
            $table->renameColumn( 'synopsis', 'Synopsis');
            $table->renameColumn( 'treatment', 'Treatment');
            $table->renameColumn( 'story', 'Story');
            $table->renameColumn( 'tag', 'Tag');
            $table->renameColumn( 'material_id', 'MaterialID');
            $table->renameColumn( 'language_id', 'LanguageID');
            $table->renameColumn( 'principal_location', 'PrincipalLocation');
            $table->renameColumn( 'principal_state', 'PrincipalState');
            $table->renameColumn( 'city', 'City');
            $table->renameColumn( 'city2', 'City2');
            $table->renameColumn( 'era_id', 'EraID');
            $table->renameColumn( 'example', 'Example');
            $table->renameColumn( 'budget_id', 'BudgetID');
            $table->renameColumn( 'rating_id', 'RatingID');
            $table->renameColumn( 'poster', 'Poster');
            $table->renameColumn( 'banner', 'Banner');
            $table->renameColumn( 'tagline', 'Tagline');
            $table->renameColumn( 'project_file', 'ProjectFile');
            $table->renameColumn( 'project_trailer', 'ProjectTriler');
            $table->renameColumn( 'pdf_project_file', 'PdfProjectFile');
            $table->renameColumn( 'pdf_view', 'Pdfview');
            $table->renameColumn( 'log_line_number', 'LoglineNumber');
            $table->renameColumn( 'create_date', 'CreateDate');
            $table->renameColumn( 'status', 'Status');
            $table->renameColumn( 'user_like', 'UserLike');
            $table->renameColumn( 'id_plan', 'ID_plan');
            $table->renameColumn( 'above', 'Above');
            $table->renameColumn( 'below', 'Below');

        });

    }
}
