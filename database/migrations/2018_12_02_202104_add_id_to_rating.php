<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIdToRating extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::rename('rating', 'ratings');
        DB::statement('ALTER TABLE ratings ENGINE = InnoDB');

        Schema::table('ratings', function (Blueprint $table) {
            DB::statement("ALTER TABLE `ratings` CHANGE COLUMN `RatingID` `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT FIRST");
            $table->renameColumn('Rating', 'rating');
            $table->renameColumn('Term', 'term');
            $table->renameColumn('Iso', 'iso');
            $table->renameColumn('LangID', 'lang_id');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ratings', function (Blueprint $table) {
            DB::statement("ALTER TABLE `ratings` CHANGE COLUMN `id` `RatingID` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT FIRST");
            $table->renameColumn('rating', 'Rating');
            $table->renameColumn('term', 'Term');
            $table->renameColumn('iso', 'Iso');
            $table->renameColumn('lang_id', 'LangID');
        });

        Schema::rename('ratings', 'rating');

    }
}
