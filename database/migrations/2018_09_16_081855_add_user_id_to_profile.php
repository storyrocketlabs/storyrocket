<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Profile;

class AddUserIdToProfile extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::rename('profile', 'profiles');

        Schema::table('profiles', function (Blueprint $table) {
            $table->timestamps();
        });

        $profiles = Profile::all();

        foreach ($profiles as $profile)
        {
            if(empty($profile->user_id))
            {
                $profile->user_id = $profile->id;
            }

            $profile->save();
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::rename('profiles', 'profile');

        Schema::table('profile', function (Blueprint $table) {
            $table->dropTimestamps();
        });
    }
}
