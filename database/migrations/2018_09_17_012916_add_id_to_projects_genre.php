<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIdToProjectsGenre extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE projects_genre ENGINE = InnoDB');
        Schema::table('projects_genre', function (Blueprint $table) {
            DB::statement("ALTER TABLE `projects_genre` CHANGE COLUMN `id` `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT FIRST");
            DB::statement("ALTER TABLE `projects_genre` CHANGE COLUMN `ProjectID` `project_id` INT(11) UNSIGNED");
            DB::statement("ALTER TABLE `projects_genre` CHANGE COLUMN `GenreID` `genre_id` INT(11) UNSIGNED");
            DB::statement("DELETE FROM projects_genre WHERE project_id NOT IN (SELECT  projects.id FROM projects)");

            $table->foreign('project_id', 'project_foreign')->references('id')->on('projects');
            $table->foreign('genre_id', 'genre_foreign')->references('id')->on('genres');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('projects_genre', function (Blueprint $table) {

            $table->dropForeign('project_foreign');
            $table->dropForeign('genre_foreign');

            DB::statement("ALTER TABLE `projects_genre` CHANGE COLUMN `project_id` `ProjectID` INT(11)  UNSIGNED");
            DB::statement("ALTER TABLE `projects_genre` CHANGE COLUMN `genre_id` `GenreID` INT(11)  UNSIGNED");
        });
    }
}
