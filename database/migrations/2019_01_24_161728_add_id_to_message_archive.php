<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIdToMessageArchive extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::rename('MsgArchive', 'message_archives');
        DB::statement('ALTER TABLE message_archives ENGINE = InnoDB');
        Schema::table('message_archives', function (Blueprint $table) {
           // DB::statement("ALTER TABLE `message_archives` CHANGE COLUMN `idarchive` `idarchive` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT FIRST");
            $table->renameColumn('idarchive', 'id');
            $table->renameColumn('idmessage', 'message_id');
            $table->renameColumn('UserID', 'user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::rename('message_archives', 'MsgArchive');
        Schema::table('MsgArchive', function (Blueprint $table) {
          //  DB::statement("ALTER TABLE `message_archives` CHANGE COLUMN `id` `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT FIRST");
            $table->renameColumn('id', 'idarchive');
            $table->renameColumn('message_id', 'idmessage');
            $table->renameColumn('user_id', 'UserID');
        });
    }
}
