<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIdToEra extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::rename('era', 'eras');
        DB::statement('ALTER TABLE eras ENGINE = InnoDB');

        Schema::table('eras', function (Blueprint $table) {
            DB::statement("ALTER TABLE `eras` CHANGE COLUMN `EraID` `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT FIRST");
            $table->renameColumn('Era', 'era');
            $table->renameColumn('Iso', 'iso');
            $table->renameColumn('LangID', 'lang_id');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('eras', function (Blueprint $table) {
            DB::statement("ALTER TABLE `eras` CHANGE COLUMN `id` `EraID` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT FIRST");
            $table->renameColumn('era', 'Era');
            $table->renameColumn('iso', 'Iso');
            $table->renameColumn('lang_id', 'LangID');
        });

        Schema::rename('eras', 'era');

    }
}
