<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIdToProjectsVideo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::rename('projects_video', 'project_videos');
        DB::statement('ALTER TABLE project_videos ENGINE = InnoDB');

        Schema::table('project_videos', function (Blueprint $table) {
            DB::statement("ALTER TABLE `project_videos` CHANGE COLUMN `id` `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT FIRST");
            DB::statement("ALTER TABLE `project_videos` CHANGE COLUMN `ProjectID` `project_id` INT(11) UNSIGNED");
            DB::statement("DELETE FROM project_videos WHERE project_id NOT IN (SELECT  projects.id FROM projects)");
            $table->renameColumn('Mtype', 'm_type');
            $table->renameColumn('Mview', 'm_view');
            $table->renameColumn('Mfecha', 'm_date');
            $table->timestamps();
            $table->foreign('project_id', 'project_videos')->references('id')->on('projects');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('project_videos', function (Blueprint $table) {
            $table->dropForeign('project_videos');
            $table->dropTimestamps();
            DB::statement("ALTER TABLE `project_videos` CHANGE COLUMN `project_id` `ProjectID` INT(11) UNSIGNED");
            $table->renameColumn('m_type', 'Mtype');
            $table->renameColumn('m_view', 'Mview');
            $table->renameColumn('m_date', 'Mfecha');
        });

        Schema::rename('project_videos', 'projects_video');

    }
}
