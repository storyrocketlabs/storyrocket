<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIdToProfileLanguage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE profile_language ENGINE = InnoDB');
        Schema::table('profile_language', function (Blueprint $table) {
            DB::statement("ALTER TABLE `profile_language` CHANGE COLUMN `id` `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT FIRST");
            DB::statement("ALTER TABLE `profile_language` CHANGE COLUMN `UserID` `profile_id` INT(11) UNSIGNED NOT NULL");
            DB::statement("ALTER TABLE `profile_language` CHANGE COLUMN `LanguageID` `language_id` INT(11) UNSIGNED NOT NULL");

            DB::statement("DELETE FROM profile_language WHERE profile_id NOT IN (SELECT  profiles.id FROM profiles)");


            $table->foreign('profile_id', 'language_user')->references('id')->on('profiles');
            $table->foreign('language_id', 'user_language')->references('id')->on('languages');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('profile_language', function (Blueprint $table) {
            $table->dropForeign('language_user');
            $table->dropForeign('user_language');

            DB::statement("ALTER TABLE `profile_language` CHANGE COLUMN `profile_id` `UserID` INT(11) UNSIGNED NOT NULL");
            DB::statement("ALTER TABLE `profile_language` CHANGE COLUMN `language_id` `LanguageID` INT(11) UNSIGNED NOT NULL");
        });
    }
}
