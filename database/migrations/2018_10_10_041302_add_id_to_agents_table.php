<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIdToAgentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE agent ENGINE = InnoDB');
        Schema::rename('agent', 'agents');

        Schema::table('agents', function (Blueprint $table) {
            DB::statement("ALTER TABLE `agents` CHANGE COLUMN `id_agent` `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT FIRST");
            DB::statement("ALTER TABLE `agents` CHANGE COLUMN `UserID` `user_id` INT(11) UNSIGNED");
            DB::statement("DELETE FROM agents WHERE user_id NOT IN (SELECT  users.id FROM users)");
            $table->renameColumn('publihs', 'publish');
            $table->foreign('user_id', 'agent_users')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('agents', function (Blueprint $table) {
            $table->dropForeign('agent_users');
            DB::statement("ALTER TABLE `agents` CHANGE COLUMN  `id` `id_agent` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT FIRST");
            DB::statement("ALTER TABLE `agents` CHANGE COLUMN  `user_id` `UserID` INT(11) UNSIGNED");
            $table->renameColumn('publish','publihs');
            $table->dropTimestamps();

        });

        Schema::rename('agents', 'agent');
    }
}
