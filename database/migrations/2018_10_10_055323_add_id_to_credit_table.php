<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIdToCreditTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE credit ENGINE = InnoDB');
        Schema::rename('credit', 'credits');

        Schema::table('credits', function (Blueprint $table) {
            DB::statement("ALTER TABLE `credits` CHANGE COLUMN `id_credit` `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT FIRST");
            DB::statement("ALTER TABLE `credits` CHANGE COLUMN `UserID` `user_id` INT(11) UNSIGNED");
            DB::statement("DELETE FROM credits WHERE user_id NOT IN (SELECT  users.id FROM users)");
            $table->renameColumn('descrit', 'description');
            $table->foreign('user_id', 'credits_users')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('credits', function (Blueprint $table) {
            $table->dropForeign('credits_users');
            DB::statement("ALTER TABLE `credits` CHANGE COLUMN  `id` `id_credit` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT FIRST");
            DB::statement("ALTER TABLE `credits` CHANGE COLUMN  `user_id` `UserID` INT(11) UNSIGNED");
            $table->renameColumn('description','descrit');
            $table->dropTimestamps();
        });

        Schema::rename('credits', 'credit');

    }
}
