<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStripeIdToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('stripe_id')->nullable();
            $table->string('stripe_subscription')->nullable();
            $table->boolean('stripe_active')->default(false);
            $table->timestamp('subscription_end_at')->nullable();
            $table->string('stripe_plan')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('stripe_id');
            $table->dropColumn('stripe_subscription');
            $table->dropColumn('stripe_active');
            $table->dropColumn('subscription_end_at');
            $table->dropColumn('stripe_plan');
        });
    }
}
