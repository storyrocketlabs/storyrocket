<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMessageDeleteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::rename('MsgDelete', 'message_delete');
        DB::statement('ALTER TABLE message_delete ENGINE = InnoDB');
        Schema::table('message_delete', function (Blueprint $table) {
            $table->renameColumn('idmessage', 'message_id');
            $table->renameColumn('dlt', 'message_delete');
            $table->renameColumn('UserID', 'user_id');
            $table->renameColumn('flat', 'flat_delete');
            $table->renameColumn('addfecha', 'fecha_delete');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::rename('message_delete', 'MsgDelete');
        Schema::table('MsgDelete', function (Blueprint $table) {
            $table->renameColumn('message_id', 'idmessage');
            $table->renameColumn('message_delete', 'dlt');
            $table->renameColumn('user_id', 'UserID');
            $table->renameColumn('flat_delete', 'flat');
            $table->renameColumn('fecha_delete', 'addfecha');
        });
    }
}
