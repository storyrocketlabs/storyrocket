<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIdMessageSpansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::rename('MsgSpam', 'message_spans');
        DB::statement('ALTER TABLE message_spans ENGINE = InnoDB');

        Schema::table('message_spans', function (Blueprint $table) {
            $table->increments('id');
            DB::statement("ALTER TABLE `message_spans` CHANGE COLUMN `UserID` `user_id` INT(11) UNSIGNED NOT NULL");
            DB::statement("ALTER TABLE `message_spans` CHANGE COLUMN `idmessage` `message_id` INT(11) UNSIGNED NOT NULL");
            DB::statement("DELETE FROM message_spans WHERE user_id NOT IN (SELECT  users.id FROM users)");

            $table->timestamps();
            $table->foreign('message_id', 'span_message')->references('id')->on('messages');
            $table->foreign('user_id', 'span_user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('message_spans', function (Blueprint $table) {
            $table->dropColumn('id');
            $table->dropForeign('span_message');
            $table->dropForeign('span_user_id');
            DB::statement("ALTER TABLE `message_spans` CHANGE COLUMN `user_id` `UserID` INT(11) UNSIGNED NOT NULL");
            DB::statement("ALTER TABLE `message_spans` CHANGE COLUMN `message_id` `idmessage` INT(11) UNSIGNED NOT NULL");
            $table->dropTimestamps();
        });

        Schema::rename('message_spans', 'MsgSpam');
    }
}
