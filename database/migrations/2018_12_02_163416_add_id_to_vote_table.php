<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIdToVoteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::rename('vote', 'votes');

        Schema::table('votes', function (Blueprint $table) {
            DB::statement("ALTER TABLE `votes` CHANGE COLUMN `ProjectID` `project_id` INT(11) UNSIGNED FIRST");
            DB::statement("ALTER TABLE `votes` CHANGE COLUMN `UserID` `user_id` INT(11) UNSIGNED FIRST");

            DB::statement("DELETE FROM votes WHERE user_id NOT IN (SELECT  users.id FROM users)");
            DB::statement("DELETE FROM votes WHERE project_id NOT IN (SELECT  projects.id FROM projects)");

            $table->increments('id');
            $table->renameColumn('IP', 'ip');
            $table->renameColumn('DateVote', 'date_vote');
            $table->timestamps();
            $table->foreign('project_id', 'vote_project')->references('id')->on('projects');
            $table->foreign('user_id', 'vote_user')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::rename('votes', 'vote');

        Schema::table('vote', function (Blueprint $table) {
            $table->dropForeign('vote_project');
            $table->dropForeign('vote_user');
            $table->dropColumn('id');
            $table->renameColumn('ip', 'IP');
            $table->renameColumn('date_vote', 'DateVote');
            $table->dropTimestamps();

            DB::statement("ALTER TABLE `vote` CHANGE COLUMN `project_id` `ProjectID` INT(11) UNSIGNED FIRST");
            DB::statement("ALTER TABLE `vote` CHANGE COLUMN `user_id` `UserID` INT(11) UNSIGNED FIRST");
        });
    }
}
