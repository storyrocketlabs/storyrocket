<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIdToLanguage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE language ENGINE = InnoDB');
        Schema::rename('language', 'languages');

        Schema::table('languages', function (Blueprint $table) {
            DB::statement("ALTER TABLE `languages` CHANGE COLUMN `LanguageID` `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT FIRST");
            DB::statement("ALTER TABLE `languages` CHANGE COLUMN `LangID` `lang_id` INT(11) UNSIGNED");
            $table->renameColumn('Language', 'language');
            $table->renameColumn('Iso', 'iso');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('languages', function (Blueprint $table) {
            DB::statement("ALTER TABLE `languages` CHANGE COLUMN  `id` `LanguageID` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT FIRST");
            DB::statement("ALTER TABLE `languages` CHANGE COLUMN  `lang_id` `LangID` INT(11) UNSIGNED");
            $table->renameColumn('language','Language');
            $table->renameColumn('iso','Iso');
        });

        Schema::rename('languages', 'language');
    }
}
