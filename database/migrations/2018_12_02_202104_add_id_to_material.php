<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIdToMaterial extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::rename('material', 'materials');
        DB::statement('ALTER TABLE materials ENGINE = InnoDB');

        Schema::table('materials', function (Blueprint $table) {
            DB::statement("ALTER TABLE `materials` CHANGE COLUMN `MaterialID` `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT FIRST");
            $table->renameColumn('Material', 'material');
            $table->renameColumn('Iso', 'iso');
            $table->renameColumn('LangID', 'lang_id');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('materials', function (Blueprint $table) {
            DB::statement("ALTER TABLE `materials` CHANGE COLUMN `id` `MaterialID` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT FIRST");
            $table->renameColumn('material', 'Material');
            $table->renameColumn('iso', 'Iso');
            $table->renameColumn('lang_id', 'LangID');
        });

        Schema::rename('materials', 'material');

    }
}
