<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIdToAgentProject extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::rename('agent_project', 'project_agents');
        DB::statement('ALTER TABLE project_agents ENGINE = InnoDB');

        Schema::table('project_agents', function (Blueprint $table) {
            DB::statement("ALTER TABLE `project_agents` CHANGE COLUMN `id_agent` `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT FIRST");
            DB::statement("ALTER TABLE `project_agents` CHANGE COLUMN `ProjectID` `project_id` INT(11) UNSIGNED");
            DB::statement("ALTER TABLE `project_agents` CHANGE COLUMN `UserID` `user_id` INT(11) UNSIGNED");
            DB::statement("DELETE FROM project_agents WHERE project_id NOT IN (SELECT  projects.id FROM projects)");
            DB::statement("DELETE FROM project_agents WHERE user_id NOT IN (SELECT  users.id FROM users)");

            $table->renameColumn('publihs', 'publish');
            $table->timestamps();
            $table->foreign('project_id', 'project_agents')->references('id')->on('projects');
            $table->foreign('user_id', 'project_agent_users')->references('id')->on('users');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('project_agents', function (Blueprint $table) {
            DB::statement("ALTER TABLE `project_agents` CHANGE COLUMN `id` `id_agent` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT FIRST");
            DB::statement("ALTER TABLE `project_agents` CHANGE COLUMN `project_id` `ProjectID` INT(11) UNSIGNED");
            DB::statement("ALTER TABLE `project_agents` CHANGE COLUMN `user_id` `UserID` INT(11) UNSIGNED");
            $table->dropForeign('project_agents');
            $table->dropForeign('project_agent_users');
            $table->renameColumn('publish', 'publihs');
            $table->dropTimestamps();
        });

        Schema::rename('project_agents', 'agent_project');

    }
}
