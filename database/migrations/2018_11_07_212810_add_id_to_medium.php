<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIdToMedium extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE medium ENGINE = InnoDB');
        Schema::rename('medium', 'mediums');

        Schema::table('mediums', function (Blueprint $table) {
            DB::statement("ALTER TABLE `mediums` CHANGE COLUMN `MediumID` `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT FIRST");
            $table->renameColumn('Medium', 'medium');
            $table->renameColumn('Iso', 'iso');
            $table->renameColumn('LangID', 'lang_id');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mediums', function (Blueprint $table) {
            DB::statement("ALTER TABLE `mediums` CHANGE COLUMN  `id` `MediumID` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT FIRST");
            $table->renameColumn('medium', 'Medium');
            $table->renameColumn('iso', 'Iso');
            $table->renameColumn('lang_id', 'LangID');
        });

        Schema::rename( 'mediums', 'medium');
    }
}
