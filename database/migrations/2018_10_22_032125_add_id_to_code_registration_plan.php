<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIdToCodeRegistrationPlan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE code_registration ENGINE = InnoDB');
        Schema::rename('code_registration', 'user_coupons');

        Schema::table('user_coupons', function (Blueprint $table) {
            DB::statement("ALTER TABLE `user_coupons` CHANGE COLUMN `id_code_regs` `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT FIRST");
            DB::statement("ALTER TABLE `user_coupons` CHANGE COLUMN `UserID` `user_id` INT(11) UNSIGNED");
            DB::statement("ALTER TABLE `user_coupons` CHANGE COLUMN `id_code_bill` `coupon_id` INT(11) UNSIGNED");
            DB::statement("DELETE FROM user_coupons WHERE user_id NOT IN (SELECT  users.id FROM users)");
            $table->timestamps();

            $table->foreign('user_id', 'user_coupons')->references('id')->on('users');
            $table->foreign('coupon_id', 'coupon_user')->references('id')->on('coupons');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_coupons', function (Blueprint $table) {
            $table->dropForeign('user_coupons');
            $table->dropForeign('coupon_user');
            DB::statement("ALTER TABLE `user_coupons` CHANGE COLUMN  `id` `id_code_regs` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT FIRST");
            DB::statement("ALTER TABLE `user_coupons` CHANGE COLUMN  `user_id` `UserID` INT(11) UNSIGNED");
            DB::statement("ALTER TABLE `user_coupons` CHANGE COLUMN  `coupon_id` `id_code_bill` INT(11) UNSIGNED");
            $table->dropTimestamps();
        });

        Schema::rename('user_coupons', 'code_registration');

    }
}
