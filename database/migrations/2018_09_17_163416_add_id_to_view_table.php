<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIdToViewTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::rename('view', 'views');

        Schema::table('views', function (Blueprint $table) {
            DB::statement("ALTER TABLE `views` CHANGE COLUMN `ProjectID` `project_id` INT(11) UNSIGNED FIRST");
            DB::statement("ALTER TABLE `views` CHANGE COLUMN `UserID` `user_id` INT(11) UNSIGNED FIRST");

            DB::statement("DELETE FROM views WHERE user_id NOT IN (SELECT  users.id FROM users)");
            DB::statement("DELETE FROM views WHERE project_id NOT IN (SELECT  projects.id FROM projects)");

            $table->increments('id');
            $table->renameColumn('IP', 'ip');
            $table->renameColumn('DateView', 'date_view');
            $table->timestamps();
            $table->foreign('project_id', 'view_project')->references('id')->on('projects');
            $table->foreign('user_id', 'view_user')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::rename('views', 'view');

        Schema::table('view', function (Blueprint $table) {
            $table->dropForeign('view_project');
            $table->dropForeign('view_user');
            $table->dropColumn('id');
            $table->renameColumn('ip', 'IP');
            $table->renameColumn('date_view', 'DateView');
            $table->dropTimestamps();

            DB::statement("ALTER TABLE `view` CHANGE COLUMN `project_id` `ProjectID` INT(11) UNSIGNED FIRST");
            DB::statement("ALTER TABLE `view` CHANGE COLUMN `user_id` `UserID` INT(11) UNSIGNED FIRST");
        });
    }
}
