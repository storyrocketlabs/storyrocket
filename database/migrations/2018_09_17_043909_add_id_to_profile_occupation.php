<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIdToProfileOccupation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE profile_occupation ENGINE = InnoDB');

        Schema::table('profile_occupation', function (Blueprint $table) {
            DB::statement("ALTER TABLE `profile_occupation` CHANGE COLUMN `id` `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT FIRST");
            DB::statement("ALTER TABLE `profile_occupation` CHANGE COLUMN `OccupationID` `occupation_id` INT(11) UNSIGNED FIRST");
            DB::statement("ALTER TABLE `profile_occupation` CHANGE COLUMN `UserID` `profile_id` INT(11) UNSIGNED FIRST");

            DB::statement("DELETE FROM profile_occupation WHERE occupation_id NOT IN (SELECT  occupations.id FROM occupations)");
            DB::statement("DELETE FROM profile_occupation WHERE profile_id NOT IN (SELECT  profiles.id FROM profiles)");

            $table->foreign('occupation_id', 'profile_occupation_occupation')->references('id')->on('occupations');
            $table->foreign('profile_id', 'profile_occupation_profile')->references('id')->on('profiles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('profile_occupation', function (Blueprint $table) {
            $table->dropForeign('profile_occupation_occupation');
            $table->dropForeign('profile_occupation_profile');

            DB::statement("ALTER TABLE `profile_occupation` CHANGE COLUMN `occupation_id` `OccupationID` INT(11) UNSIGNED FIRST");
            DB::statement("ALTER TABLE `profile_occupation` CHANGE COLUMN `profile_id` `UserID` INT(11) UNSIGNED FIRST");

        });
    }
}
