<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIdToProjectsWriter extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::rename('projects_writer', 'project_writers');
        DB::statement('ALTER TABLE project_writers ENGINE = InnoDB');

        Schema::table('project_writers', function (Blueprint $table) {
            DB::statement("ALTER TABLE `project_writers` CHANGE COLUMN `WriterID` `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT FIRST");
            DB::statement("ALTER TABLE `project_writers` CHANGE COLUMN `ProjectID` `project_id` INT(11) UNSIGNED");
            DB::statement("DELETE FROM project_writers WHERE project_id NOT IN (SELECT  projects.id FROM projects)");

            $table->renameColumn('WriterName', 'writer_name');
            $table->renameColumn('WriterEMail', 'writer_email');
            $table->timestamps();
            $table->foreign('project_id', 'project_writers')->references('id')->on('projects');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('project_writers', function (Blueprint $table) {
            $table->dropForeign('project_writers');
            $table->dropTimestamps();
            DB::statement("ALTER TABLE `project_writers` CHANGE COLUMN  `id` `WriterID` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT FIRST");
            DB::statement("ALTER TABLE `project_writers` CHANGE COLUMN  `project_id` `ProjectID` INT(11) UNSIGNED");
            $table->renameColumn( 'writer_name', 'WriterName');
            $table->renameColumn( 'writer_email', 'WriterEMail');
        });

        Schema::rename('project_writers', 'projects_writer');

    }
}
