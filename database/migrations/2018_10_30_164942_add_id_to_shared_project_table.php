<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIdToSharedProjectTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE share_projects ENGINE = InnoDB');
        Schema::table('share_projects', function (Blueprint $table) {
            DB::statement("ALTER TABLE `share_projects` CHANGE COLUMN `ProjectID` `project_id` INT(11) UNSIGNED FIRST");
            DB::statement("ALTER TABLE `share_projects` CHANGE COLUMN `ShareProjectsID` `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT FIRST");
            DB::statement("DELETE FROM share_projects WHERE project_id NOT IN (SELECT  projects.id FROM projects)");

            $table->renameColumn('From','from');
            $table->renameColumn('To','to');
            $table->renameColumn('ShareDate','shared_date');
            $table->timestamps();
            $table->foreign('project_id', 'shared_projects')->references('id')->on('projects');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('share_projects', function (Blueprint $table) {
            $table->dropForeign('shared_projects');

            DB::statement("ALTER TABLE `share_projects` CHANGE COLUMN `project_id` `ProjectID` INT(11) UNSIGNED FIRST");
            DB::statement("ALTER TABLE `share_projects` CHANGE COLUMN `id` `ShareProjectsID` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT FIRST");

            $table->renameColumn('from','From');
            $table->renameColumn('to','To');
            $table->renameColumn('shared_date','ShareDate');
            $table->dropTimestamps();
        });
    }
}
