<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIdToCommentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('comment')) {

            Schema::rename('comment', 'comments');
            DB::statement('ALTER TABLE comments ENGINE = InnoDB');
            Schema::table('comments', function (Blueprint $table) {
                DB::statement("ALTER TABLE `comments` CHANGE COLUMN `CommentID` `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT FIRST");
                DB::statement("ALTER TABLE `comments` CHANGE COLUMN `ProjectID` `project_id` INT(11) UNSIGNED FIRST");
                DB::statement("ALTER TABLE `comments` CHANGE COLUMN `UserID` `user_id` INT(11) UNSIGNED FIRST");

                DB::statement("DELETE FROM `comments` WHERE project_id NOT IN (SELECT  projects.id FROM projects)");
                DB::statement("DELETE FROM `comments` WHERE user_id NOT IN (SELECT  users.id FROM users)");

                $table->renameColumn('Comment', 'comment');
                $table->renameColumn('DateComment', 'date_comment');
                $table->timestamps();
                $table->foreign('project_id', 'project_comment')->references('id')->on('projects');
                $table->foreign('user_id', 'user_comment')->references('id')->on('users');
            });
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        if (Schema::hasTable('comments')) {

            Schema::rename('comments', 'comment');
            Schema::table('comment', function (Blueprint $table) {

                DB::statement("ALTER TABLE `comment` CHANGE COLUMN `id` `CommentID` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT FIRST");
                DB::statement("ALTER TABLE `comment` CHANGE COLUMN `project_id` `ProjectID` INT(11) UNSIGNED FIRST");
                DB::statement("ALTER TABLE `comment` CHANGE COLUMN `user_id` `UserID` INT(11) UNSIGNED FIRST");

                $table->renameColumn('comment', 'Comment');
                $table->renameColumn('date_comment', 'DateComment');
                $table->dropForeign('project_comment');
                $table->dropForeign('user_comment');

                $table->dropTimestamps();
            });
        }

    }
}
