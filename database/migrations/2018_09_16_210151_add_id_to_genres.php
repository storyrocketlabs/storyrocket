<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIdToGenres extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE genre ENGINE = InnoDB');
        Schema::rename('genre', 'genres');

        Schema::table('genres', function (Blueprint $table) {
            DB::statement("ALTER TABLE `genres` CHANGE COLUMN `GenreID` `GenreID` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT FIRST");
            $table->renameColumn('GenreID', 'id');
            $table->renameColumn('Genre', 'genre');
            $table->renameColumn('Image', 'image');
            $table->renameColumn('Iso', 'iso');
            $table->renameColumn('LangID', 'lang_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('genres', function (Blueprint $table) {
            DB::statement("ALTER TABLE `genres` CHANGE COLUMN `id` `id` INT(11) NOT NULL AUTO_INCREMENT FIRST");
            $table->renameColumn('id', 'GenreID');
            $table->renameColumn('genre', 'Genre');
            $table->renameColumn('image', 'Image');
            $table->renameColumn('iso', 'Iso');
            $table->renameColumn('lang_id', 'LangID');
            $table->dropTimestamps();
        });

        Schema::rename('genres', 'genre');
    }
}
