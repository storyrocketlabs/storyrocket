<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIdToProfileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE profile ENGINE = InnoDB');

        Schema::table('profile', function (Blueprint $table) {
            DB::statement("ALTER TABLE `profile` CHANGE COLUMN `UserID` `UserID` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT FIRST");

            $table->renameColumn('UserID', 'id');
            $table->renameColumn('FullName', 'full_name')->nullable();
            $table->renameColumn('LastName', 'last_name')->nullable();
            $table->renameColumn('Email', 'email')->nullable();
            $table->renameColumn('Gender', 'gender')->nullable();
            $table->renameColumn('Photo', 'photo')->nullable();
            $table->renameColumn('PhotoNew', 'photo_new')->nullable();
            $table->renameColumn('FacebookPhoto', 'facebook_photo')->nullable();
            $table->renameColumn('Website', 'website')->nullable();
            $table->renameColumn('Company', 'company')->nullable();
            $table->renameColumn('Day', 'day')->nullable();
            $table->renameColumn('Month', 'month')->nullable();
            $table->renameColumn('Year', 'year')->nullable();
            $table->renameColumn('CountryID', 'country_id');
            $table->renameColumn('State', 'state')->nullable();
            $table->renameColumn('City', 'city')->nullable();
            $table->renameColumn('ZipCode', 'zipcode')->nullable();
            $table->renameColumn('Facebook', 'facebook')->nullable();
            $table->renameColumn('Twitter', 'twitter')->nullable();
            $table->renameColumn('Google', 'google')->nullable();
            $table->renameColumn('Linkedin', 'linkedin')->nullable();
            $table->renameColumn('Youtube', 'youtube')->nullable();
            $table->renameColumn('Instagram', 'Instagram')->nullable();
            $table->renameColumn('Who_am_i', 'who_am_i')->nullable();
            $table->renameColumn('Casting', 'casting')->nullable();
            $table->renameColumn('Trailer', 'trailer')->nullable();
            $table->renameColumn('On_web', 'on_web')->nullable();
            $table->renameColumn('Tmblr', 'tmblr')->nullable();
            $table->renameColumn('Reddit', 'reddit')->nullable();
            $table->renameColumn('Birthday', 'birthday')->nullable();
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id', 'profile_user')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('profile', function (Blueprint $table) {
            DB::statement("ALTER TABLE `profile` CHANGE COLUMN `id` `id` INT(11) NOT NULL AUTO_INCREMENT FIRST");

            $table->dropForeign('profile_user');
            $table->dropColumn('user_id');

            $table->renameColumn('id', 'UserID');
            $table->renameColumn('full_name','FullName');
            $table->renameColumn('last_name', 'LastName');
            $table->renameColumn('email', 'Email');
            $table->renameColumn('gender', 'Gender');
            $table->renameColumn('photo', 'Photo');
            $table->renameColumn('photo_new', 'PhotoNew');
            $table->renameColumn('facebook_photo', 'FacebookPhoto');
            $table->renameColumn('website', 'Website');
            $table->renameColumn('company', 'Company');
            $table->renameColumn('day', 'Day');
            $table->renameColumn('month', 'Month');
            $table->renameColumn('year', 'Year');
            $table->renameColumn('country_id', 'CountryID');
            $table->renameColumn('state', 'State');
            $table->renameColumn('city', 'City');
            $table->renameColumn('zipcode', 'ZipCode');
            $table->renameColumn('facebook', 'Facebook');
            $table->renameColumn('twitter', 'Twitter');
            $table->renameColumn('google', 'Google');
            $table->renameColumn('linkedin', 'Linkedin');
            $table->renameColumn('youtube', 'Youtube');
            $table->renameColumn('Instagram', 'Instagram');
            $table->renameColumn('who_am_i', 'Who_am_i');
            $table->renameColumn('casting', 'Casting');
            $table->renameColumn('trailer', 'Trailer');
            $table->renameColumn('on_web', 'On_web');
            $table->renameColumn('tmblr', 'Tmblr');
            $table->renameColumn('reddit', 'Reddit');
            $table->renameColumn('birthday', 'Birthday');

        });
    }
}
