<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIdToMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE messages ENGINE = InnoDB');

        Schema::table('messages', function (Blueprint $table) {
            DB::statement("ALTER TABLE `messages` CHANGE COLUMN `idmessage` `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT FIRST");
            $table->renameColumn('asunto', 'subject');
            $table->renameColumn('fechmsg', 'message_date');
            $table->renameColumn('isreadMail', 'is_read_mail');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('messages', function (Blueprint $table) {
            DB::statement("ALTER TABLE `messages` CHANGE COLUMN  `id` `idmessage` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT FIRST");
            $table->renameColumn('subject', 'asunto');
            $table->renameColumn('message_date', 'fechmsg');
            $table->renameColumn('is_read_mail', 'isreadMail');
            $table->dropTimestamps();

        });
    }
}
