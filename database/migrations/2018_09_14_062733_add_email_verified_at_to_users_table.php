<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEmailVerifiedAtToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE users ENGINE = InnoDB');

        Schema::table('users', function (Blueprint $table) {
            DB::statement("ALTER TABLE `users` CHANGE COLUMN `UserID` `UserID` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT FIRST");

            $table->renameColumn('UserID', 'id');
            $table->renameColumn('Email', 'email');
            $table->renameColumn('Password', 'password');
            $table->renameColumn('Create', 'create');
            $table->renameColumn('LastLogin', 'last_login');
            $table->renameColumn('Login', 'login');
            $table->renameColumn('Active', 'active');
            $table->renameColumn('FacebookID', 'facebook_id');
            $table->renameColumn('State_Active', 'state_active');
            $table->timestamp('email_verified_at')->nullable();
            $table->tinyInteger('migrated')->default(0);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            DB::statement("ALTER TABLE `users` CHANGE COLUMN `id` `id` INT(11) NOT NULL AUTO_INCREMENT FIRST");

            $table->renameColumn('id', 'UserID');
            $table->renameColumn('email', 'Email');
            $table->renameColumn('password', 'Password');
            $table->renameColumn('create', 'Create');
            $table->renameColumn('last_login', 'LastLogin');
            $table->renameColumn('login', 'Login');
            $table->renameColumn('active', 'Active');
            $table->renameColumn('facebook_id', 'FacebookID');
            $table->renameColumn('state_active', 'State_Active');
            $table->dropColumn('email_verified_at');
            $table->dropColumn('migrated');
            $table->dropRememberToken();
            $table->dropTimestamps();
        });
    }
}
