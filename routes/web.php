<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
 * Provisional Route Pre Migration
 */
/*
Route::domain('search.' . env('DOMAIN'))->group(function () {
    Route::get('/', 'SearchController@index')->name('search');
    Route::post('/', 'SearchController@getJsonResults')->name('results');
});
 */


Route::post('/stripe/webhook', 'StripeWebHookController@handle')->name('stripe.webhooks');

Route::get('/search', 'SearchController@index')->name('search');
Route::post('/search', 'SearchController@getJsonResults')->name('results');

Route::get('/forgot-password', 'HomeController@restorePass')->name('home.restorepass');

Route::domain(env('PRINCIPAL_DOMAIN'))->group(function (){
    Auth::routes(['verify' => true]);

    Route::get('/home', function () {
        return redirect()->route('home');
    });


    Route::get('auth/{provider}', 'Auth\LoginController@redirectToProvider')->name('social.auth');
    Route::get('auth/{provider}/callback', 'Auth\LoginController@handleProviderCallback');
    Route::get('logout', 'Auth\LoginController@logout')->name('auth.logout');

    Route::get('/', 'HomeController@index')->name('home');

    Route::get('/lists/states', 'ListsController@states')->name('list-states');


    Route::get('/validate-email', 'Auth\RegisterController@validateEmail')->name('validate-email');
    

    Route::get('projects/users', 'ProjectsController@writeUsers')->name('projects-users');

    Route::get('bowkerspecialoffer', 'BowkerController@index')->name('bowker.index');

    Route::get('bowkerspecialoffer/register', 'BowkerController@register')->name('bowker.register');
    Route::post('bowkerspecialoffer/register/save', 'BowkerController@registerSave')->name('bowker.register.save');
    Route::get('bowkerspecialoffer/cart', 'BowkerController@cart')->name('bowker.cart');
    Route::get('bowkerspecialoffer/checkout', 'BowkerController@checkout')->name('bowker.checkout');
    Route::get('bowkerspecialoffer/payment', 'BowkerController@payment')->name('bowker.payment');
    Route::get('bowkerspecialoffer/success', 'BowkerController@success')->name('bowker.success');

    Route::get('bowkerspecialoffer/mailregister', 'BowkerController@sendMailRegisterPassword')->name('bowker.mailregister');
    Route::post('bowkerspecialoffer/save-payment', 'BowkerController@savePayment')->name('bowker.savePayment');
    Route::post('bowkerspecialoffer/save-password', 'BowkerController@savePassword')->name('bowker.savePassword');
    Route::get('bowkerspecialoffer/register-step2/{id}', 'BowkerController@formMailRegisterPassword')->name('bowker.formMailRegisterPassword');

    Route::post('mail/ajaxReturnsUsers','MailController@ajaxReturnsUsers');
    Route::post('loader-mail','MailController@loaderMail');
    Route::post('message-remove','MailController@removeMail');

    
    Route::middleware(['auth'])->group(function () {

        /**
         * Mail
         */
        
        Route::get('mail','MailController@index');
        //Route::get('mail/shared','MailController@viewShared');
        Route::get('mail/composse-message','MailController@composseMenssage');
        Route::get('mail/{slug}','MailController@viewMails');
        
        Route::post('send-mail','MailController@sendMessage');
        Route::post('reply-mail','MailController@replyMessage');
        Route::post('send-mail-project','MailController@sentMessagesProject');
        
       // Route::post('ajax','AjaxController@mailReturnsUsers');

        /**
         * Grupos
         */
        Route::get('groups','GroupsController@index')->name('group-index');
        Route::get('groups/{slug}', 'GroupsController@viewGroups')->name('view-groups');


        /**
         * Settings
         */


        Route::get('weekly-notification', 'SettingsController@sendMailWeekly')->name('settings.weekly.notification');
        Route::get('daily-notification', 'SettingsController@sendMailDaily')->name('settings.daily.notification');
        Route::get('settings', 'SettingsController@index')->name('settings.index');
        Route::post('settings/save', 'SettingsController@saveSettings')->name('settings.save');


        Route::post('settings/ps_addme_to_profile_spotlight_mail', 'SettingsController@psAddmeToProfileSpotlightMail')->name('settings.ps_addme_to_profile_spotlight_mail');
        Route::post('settings/ps_add_my_projects_to_project_spotlight_mail', 'SettingsController@psAddMyProjectsToProjectSpotlight')->name('settings.ps_add_my_projects_to_project_spotlight_mail');


        /**
         * Notification
         */

        Route::get('notification/notification-add-profile/{id}', 'SettingsController@notificationAddProfile')->name('notification-add-profile');

        Route::get('notification/notification-like-your-project', 'SettingsController@notificationLikeYourProfile')->name('notification-like-your-project');
        Route::get('notification/notification-readlaterlist-your-project', 'SettingsController@notificationReadLaterListYourProject')->name('notification-readlaterlist-your-project');

        Route::get('notification/get-views-profile', 'ProfileController@getViewsNotfication')->name('get-views-profile-notification');
        Route::get('notification/get-alert-bell', 'ProfileController@callAlertBellNotification')->name('get-alert-bell-notification');
        Route::get('notification/check-alert-bell', 'ProfileController@checkAlertBellNotification')->name('check-alert-bell-notification');



        /*
         * Project Routes
         */

        Route::get('projects', 'ProjectsController@index')->name('projects-index');
        Route::get('projects/create', 'ProjectsController@create')->name('projects.create');
        Route::post('projects', 'ProjectsController@storeProject')->name('projects.store');
        Route::put('projects/{project}', 'ProjectsController@updateProject')->name('projects.update');
        Route::put('projects/poster/{project}', 'ProjectsController@updatePoster')->name('projects.poster');

        /*
         * Profile User & Dashboard Routes
         */

        Route::get('dashboard', 'DashboardController@index')->name('dashboard.index');

        Route::get('activity', 'ProfileController@activity')->name('activity-show');

        Route::get('profile/edit', 'ProfileController@index')->name('profile-index');
        Route::post('profile-update', 'ProfileController@profileUpdate')->name('profile.update');
        Route::post('profile-avatar', 'ProfileController@updateAvatar')->name('profile.avatar');
        Route::post('profile-email', 'ProfileController@updateEmail')->name('profile.email');
        Route::post('profile-password', 'ProfileController@updatePassword')->name('profile.password');
        Route::post('profile-agent/{agent?}', 'ProfileController@storeAgent')->name('profile.store-agent');
        Route::post('profile-store-credit', 'ProfileController@storeCredit')->name('profile.store-credit');
        Route::post('profile-update-credit/{credit}', 'ProfileController@updateCredit')->name('profile.update-credit');
        Route::delete('profile-delete-credit/{credit}', 'ProfileController@deleteCredit')->name('profile.delete-credit');
        Route::post('profile-store-award', 'ProfileController@storeAward')->name('profile.store-award');
        Route::post('profile-update-award/{award}', 'ProfileController@updateAward')->name('profile.update-award');
        Route::delete('profile-delete-award/{award}', 'ProfileController@deleteAward')->name('profile.delete-award');
        Route::post('profile-store-video', 'ProfileController@storeProfileVideo')->name('profile.store-video');
        Route::post('profile-update-video/{profileVideo}', 'ProfileController@updateProfileVideo')->name('profile.update-video');
        Route::delete('profile-delete-video/{profileVideo}', 'ProfileController@deleteProfileVideo')->name('profile.delete-video');


        Route::get('profile-photo', 'ProfileController@photo')->name('profile.photo');
        Route::get('profile-headshots', 'ProfileController@headshots')->name('profile.headshots');
        Route::get('profile-artworks', 'ProfileController@artworks')->name('profile.artworks');


        Route::get('profile-artwork', 'ProfileController@artwork')->name('profile.artwork');
        Route::get('profile-my-projects', 'ProfileController@myProjects')->name('profile.my-projects');
        Route::get('profile-co-authored', 'ProfileController@coAuthored')->name('profile.co-authored');
        Route::get('profile-read-list', 'ProfileController@readList')->name('profile.readlist');

        Route::get('subscription', 'PlansController@subcription')->name('subscription');

        /*
         * Plans & Subscription Routes
         */

        Route::get('plans', 'PlansController@index')->name('plans');
        Route::get('plans/{plan}', 'PlansController@subscriptionForm')->name('subscription-form');
        Route::post('plans/{plan}/validate-coupon', 'PlansController@validateCoupon')->name('plans.validate-coupon');
        Route::post('plans/{plan}/store', 'PlansController@storeSubscription')->name('plans.store-subscription');
        Route::get('subscription', 'PlansController@subcription')->name('subscription');


        /*
         * Search Routes
         */
        Route::get('/search', 'SearchMainController@index')->name('search-main');

    });

    /**
     * Footer options
     *
     *
    **/
   
    Route::get('/about-us', 'HomeController@aboutUs')->name('home.aboutus');
    Route::get('/faqs', 'HomeController@faqs')->name('home.faqs');
    Route::get('/tutorials', 'HomeController@tutorials')->name('home.tutorials');
    Route::get('/privacy-policy', 'HomeController@privacyPolicy')->name('home.privacyPolicy');
    Route::get('/terms-of-service', 'HomeController@termOfService')->name('home.termOfService');
    Route::get('/copyright', 'HomeController@copyright')->name('home.copyright');


    Route::get('/dashboard/trendingprojects', 'DashboardController@trendingProjects')->name('dashboard.trendingprojects');
    Route::get('/dashboard/mostviewedprojects', 'DashboardController@mostViewedProjects')->name('dashboard.mostviewedprojects');
    Route::get('/dashboard/memberspotlight', 'DashboardController@memberSpotLight')->name('dashboard.memberspotlight');
    Route::get('/dashboard/projectspotlight', 'DashboardController@projectSpotLight')->name('dashboard.projectspotlight');
    Route::get('/dashboard/newmember', 'DashboardController@newMember')->name('dashboard.newmember');



    Route::get('/dashboard/newmember', 'DashboardController@newMember')->name('dashboard.newmember');

    Route::get('/{projectSlug}', 'ProjectsController@projectPage')->name('project-page');
    Route::get('{slug}', 'ProfileController@showProfile')->name('profile-show');


});
