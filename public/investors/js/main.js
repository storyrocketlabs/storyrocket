
/*(function () {
  
  'use strict';
  
  var pop_up_video = $('#pop_up_video'),
    pop_up_video_iframe = $('#pop_up_video_iframe'),
    close_pop_up_video_id = $('#pop_up_video_bg'),
    open_pop_up_video_id = $('#open_pop_up_video'),
    mobile_open_pop_up_video_id = $('#mobile_open_pop_up_video');

    // Pop-up Video
    
    var close_pop_up_video = function(event){
      event.preventDefault();
      pop_up_video_iframe.attr('src', '');
      pop_up_video.css('display', 'none');
    };
    close_pop_up_video_id.on('click', close_pop_up_video);
    
    
    var open_pop_up_video = function(event){
      event.preventDefault();    
      pop_up_video_iframe.attr('src', 'https://player.vimeo.com/video/153339497?byline=0');
      pop_up_video.css('display', 'block');
    };
    
    open_pop_up_video_id.on('click', open_pop_up_video);    
    mobile_open_pop_up_video_id.on('click', open_pop_up_video);
                                   
}()); */




$(document).ready(function(){

      $('.single-item').slick({
         dots: true,
         autoplay: true,
         autoplaySpeed: 3000,
      });

      $('.responsive').slick({
  dots: false,
  infinite: false,
  speed: 300,
  slidesToShow: 5,
  slidesToScroll: 5,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: true,
        dots: false
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        autoplay: true,
         autoplaySpeed: 3000,
        slidesToScroll: 1
      }
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ]
});



      $('.responsive-2').slick({
  dots: false,
  infinite: false,
  speed: 300,
  slidesToShow: 6,
  slidesToScroll: 6,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: true,
        dots: false
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        autoplay: true,
         autoplaySpeed: 3000,
        slidesToScroll: 1
      }
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ]
});


        

$('.variable-width').slick({
  dots: true,
  infinite: true,
  speed: 300,
  slidesToShow: 1,
  centerMode: true,
  variableWidth: true
});

    $('.center').slick({
  centerMode: true,
  dots: true,
  autoplay: true,
  autoplaySpeed: 10000,
  centerPadding: '90px',  
  slidesToShow: 4,
  responsive: [
  {
      breakpoint: 1030,
      settings: {
        arrows: false,
        centerMode: true,
        centerPadding: '30px',
        slidesToShow: 3
      }
    },
    {
      breakpoint: 801,
      settings: {
        arrows: false,
        centerMode: true,
        centerPadding: '30px',
        slidesToShow: 2
      }
    },
    {
      breakpoint: 780,
      settings: {
        arrows: false,
        centerMode: true,
        centerPadding: '90px',
        slidesToShow: 2
      }
    },
    {
      breakpoint: 768,
      settings: {
        arrows: false,
        centerMode: true,
        centerPadding: '75px',
        slidesToShow: 2
      }
    },
    {
     breakpoint: 600,
      settings: {
        arrows: false,
        centerMode: true,
        centerPadding: '40px',
        slidesToShow: 2
      }
    },
    {
      breakpoint: 480,
      settings: {
        arrows: false,
        centerMode: true,
        centerPadding: '40px',
        slidesToShow: 1
      }
    }
  ]
});







    //show and hide two divs

        $("#showempresa2").click(function(){
               $(".empresa-peruana").slideUp("fast");
               //arrows
                $('.arrow-img-two').toggleClass('rotate');
                $('.arrow-img-two').toggleClass('rotate2');
                //scroll to
                /*$('html, body').animate({scrollTop : 1482},800);*/
            $(".empresa-internacional").slideToggle("fast");
            $(this).toggleClass("active");
            return false;
        });
        $("#showempresa").click(function(){
                 $(".empresa-internacional").slideUp("fast");
                 //arrows
                $('.arrow-img').toggleClass('rotate');
                $('.arrow-img').toggleClass('rotate2');
                //scroll to
                /*$('html, body').animate({scrollTop : 1482},800);*/
            $(".empresa-peruana").slideToggle("slow");
            $(this).toggleClass("active");
            return false;
        });

        $("#showempresa3").click(function(){
                 $(".empresa-internacional").slideUp("fast");
                 //arrows
                $('.arrow-img').toggleClass('rotate');
                $('.arrow-img').toggleClass('rotate2');
                //scroll to
                /*$('html, body').animate({scrollTop : 1482},800);*/
            $(".empresa-internacional2").slideToggle("slow");
            $(this).toggleClass("active");
            return false;
        });


     
        // close div
        $(".close-peruana").click(function(){
                 $(".empresa-peruana").slideUp("normal");
                 //scroll to
                /*$('html, body').animate({scrollTop : 720},800);*/
            return false;
        });

         $(".close-inter").click(function(){
                 $(".empresa-internacional").slideUp("normal");
                  //scroll to
                /*$('html, body').animate({scrollTop : 720},800);*/
            return false;
        });

          $(".close-inter2").click(function(){
                 $(".empresa-internacional2").slideUp("normal");
                  //scroll to
                /*$('html, body').animate({scrollTop : 720},800);*/
            return false;
        });



   $(document).scroll(function () {
    var y = $(this).scrollTop();
    if (y > 800) {
        $('.bottomMenu').fadeIn();
    } else {
        $('.bottomMenu').fadeOut();
    }

});



    $('a[href*="#"]:not([href="#"]):not([href="#show"]):not([href="#hide"])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html,body').animate({
          scrollTop: target.offset().top
        }, 1000);
        return false;
      }
    }
  });  







    });




